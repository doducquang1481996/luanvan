package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.NhaSanXuat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NhaSanXuat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhaSanXuatRepository extends JpaRepository<NhaSanXuat, Long> {

}
