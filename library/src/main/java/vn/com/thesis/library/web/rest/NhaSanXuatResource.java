package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.NhaSanXuatService;
import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.NhaCungCapDTO;
import vn.com.thesis.library.service.dto.NhaSanXuatDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NhaSanXuat.
 */
@RestController
@RequestMapping("/api")
public class NhaSanXuatResource {

    private final Logger log = LoggerFactory.getLogger(NhaSanXuatResource.class);

    private static final String ENTITY_NAME = "nhaSanXuat";

    private final NhaSanXuatService nhaSanXuatService;

    public NhaSanXuatResource(NhaSanXuatService nhaSanXuatService) {
        this.nhaSanXuatService = nhaSanXuatService;
    }
    
    @Autowired
    private UltilService ultilService;

    /**
     * POST  /nha-san-xuats : Create a new nhaSanXuat.
     *
     * @param nhaSanXuatDTO the nhaSanXuatDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nhaSanXuatDTO, or with status 400 (Bad Request) if the nhaSanXuat has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/nha-san-xuats")
    @Timed
    public ResponseEntity<NhaSanXuatDTO> createNhaSanXuat(@RequestBody NhaSanXuatDTO nhaSanXuatDTO) throws URISyntaxException {
        log.debug("REST request to save NhaSanXuat : {}", nhaSanXuatDTO);
        if (nhaSanXuatDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhaSanXuat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        nhaSanXuatDTO.setNgayTao(ultilService.timeNow());
        NhaSanXuatDTO result = nhaSanXuatService.save(nhaSanXuatDTO);
        return ResponseEntity.created(new URI("/api/nha-san-xuats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nha-san-xuats : Updates an existing nhaSanXuat.
     *
     * @param nhaSanXuatDTO the nhaSanXuatDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nhaSanXuatDTO,
     * or with status 400 (Bad Request) if the nhaSanXuatDTO is not valid,
     * or with status 500 (Internal Server Error) if the nhaSanXuatDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/nha-san-xuats")
    @Timed
    public ResponseEntity<NhaSanXuatDTO> updateNhaSanXuat(@RequestBody NhaSanXuatDTO nhaSanXuatDTO) throws URISyntaxException {
        log.debug("REST request to update NhaSanXuat : {}", nhaSanXuatDTO);
        if (nhaSanXuatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhaSanXuatDTO data = nhaSanXuatService.findOne(nhaSanXuatDTO.getId()).get();
        nhaSanXuatDTO.setNguoiTao(data.getNguoiTao());
        nhaSanXuatDTO.setNgayTao(data.getNgayTao());
        NhaSanXuatDTO result = nhaSanXuatService.save(nhaSanXuatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nhaSanXuatDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nha-san-xuats : get all the nhaSanXuats.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nhaSanXuats in body
     */
    @GetMapping("/nha-san-xuats")
    @Timed
    public ResponseEntity<List<NhaSanXuatDTO>> getAllNhaSanXuats(Pageable pageable) {
        log.debug("REST request to get a page of NhaSanXuats");
        Page<NhaSanXuatDTO> page = nhaSanXuatService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/nha-san-xuats");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /nha-san-xuats/:id : get the "id" nhaSanXuat.
     *
     * @param id the id of the nhaSanXuatDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nhaSanXuatDTO, or with status 404 (Not Found)
     */
    @GetMapping("/nha-san-xuats/{id}")
    @Timed
    public ResponseEntity<NhaSanXuatDTO> getNhaSanXuat(@PathVariable Long id) {
        log.debug("REST request to get NhaSanXuat : {}", id);
        Optional<NhaSanXuatDTO> nhaSanXuatDTO = nhaSanXuatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhaSanXuatDTO);
    }

    /**
     * DELETE  /nha-san-xuats/:id : delete the "id" nhaSanXuat.
     *
     * @param id the id of the nhaSanXuatDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/nha-san-xuats/{id}")
    @Timed
    public ResponseEntity<Void> deleteNhaSanXuat(@PathVariable Long id) {
        log.debug("REST request to delete NhaSanXuat : {}", id);
        nhaSanXuatService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
