package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.LoaiThuocTinhService;
import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.LoaiThuocTinhDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LoaiThuocTinh.
 */
@RestController
@RequestMapping("/api")
public class LoaiThuocTinhResource {

    private final Logger log = LoggerFactory.getLogger(LoaiThuocTinhResource.class);

    private static final String ENTITY_NAME = "loaiThuocTinh";

    private final LoaiThuocTinhService loaiThuocTinhService;

    public LoaiThuocTinhResource(LoaiThuocTinhService loaiThuocTinhService) {
        this.loaiThuocTinhService = loaiThuocTinhService;
    }
    
    @Autowired
    private UltilService ultilService;

    /**
     * POST  /loai-thuoc-tinhs : Create a new loaiThuocTinh.
     *
     * @param loaiThuocTinhDTO the loaiThuocTinhDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new loaiThuocTinhDTO, or with status 400 (Bad Request) if the loaiThuocTinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/loai-thuoc-tinhs")
    @Timed
    public ResponseEntity<LoaiThuocTinhDTO> createLoaiThuocTinh(@RequestBody LoaiThuocTinhDTO loaiThuocTinhDTO) throws URISyntaxException {
        log.debug("REST request to save LoaiThuocTinh : {}", loaiThuocTinhDTO);
        if (loaiThuocTinhDTO.getId() != null) {
            throw new BadRequestAlertException("A new loaiThuocTinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        loaiThuocTinhDTO.setNgayTao(ultilService.timeNow());
        LoaiThuocTinhDTO result = loaiThuocTinhService.save(loaiThuocTinhDTO);
        return ResponseEntity.created(new URI("/api/loai-thuoc-tinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /loai-thuoc-tinhs : Updates an existing loaiThuocTinh.
     *
     * @param loaiThuocTinhDTO the loaiThuocTinhDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated loaiThuocTinhDTO,
     * or with status 400 (Bad Request) if the loaiThuocTinhDTO is not valid,
     * or with status 500 (Internal Server Error) if the loaiThuocTinhDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/loai-thuoc-tinhs")
    @Timed
    public ResponseEntity<LoaiThuocTinhDTO> updateLoaiThuocTinh(@RequestBody LoaiThuocTinhDTO loaiThuocTinhDTO) throws URISyntaxException {
        log.debug("REST request to update LoaiThuocTinh : {}", loaiThuocTinhDTO);
        if (loaiThuocTinhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoaiThuocTinhDTO data = loaiThuocTinhService.findOne(loaiThuocTinhDTO.getId()).get();
        loaiThuocTinhDTO.setNguoiTao(data.getNguoiTao());
        loaiThuocTinhDTO.setNgayTao(data.getNgayTao());
        LoaiThuocTinhDTO result = loaiThuocTinhService.save(loaiThuocTinhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, loaiThuocTinhDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /loai-thuoc-tinhs : get all the loaiThuocTinhs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of loaiThuocTinhs in body
     */
    @GetMapping("/loai-thuoc-tinhs")
    @Timed
    public ResponseEntity<List<LoaiThuocTinhDTO>> getAllLoaiThuocTinhs(Pageable pageable) {
        log.debug("REST request to get a page of LoaiThuocTinhs");
        Page<LoaiThuocTinhDTO> page = loaiThuocTinhService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/loai-thuoc-tinhs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /loai-thuoc-tinhs/:id : get the "id" loaiThuocTinh.
     *
     * @param id the id of the loaiThuocTinhDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the loaiThuocTinhDTO, or with status 404 (Not Found)
     */
    @GetMapping("/loai-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<LoaiThuocTinhDTO> getLoaiThuocTinh(@PathVariable Long id) {
        log.debug("REST request to get LoaiThuocTinh : {}", id);
        Optional<LoaiThuocTinhDTO> loaiThuocTinhDTO = loaiThuocTinhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loaiThuocTinhDTO);
    }

    /**
     * DELETE  /loai-thuoc-tinhs/:id : delete the "id" loaiThuocTinh.
     *
     * @param id the id of the loaiThuocTinhDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/loai-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<Void> deleteLoaiThuocTinh(@PathVariable Long id) {
        log.debug("REST request to delete LoaiThuocTinh : {}", id);
        loaiThuocTinhService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/loai-thuoc-tinhs/get-by-nhom-thuoc-tinh")
    @Timed
    public ResponseEntity<List<LoaiThuocTinhDTO>> getByNhomThuocTinh(@RequestParam Long nhomThuocTinhId) {
        List<LoaiThuocTinhDTO> loaiThuocTinhDTOs = loaiThuocTinhService.findByNhomThuocTinhId(nhomThuocTinhId);
        return new ResponseEntity<>(loaiThuocTinhDTOs, HttpStatus.OK);
    }
}
