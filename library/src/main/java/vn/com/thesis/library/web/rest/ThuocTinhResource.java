package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.ThuocTinhService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.ThuocTinhDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ThuocTinh.
 */
@RestController
@RequestMapping("/api")
public class ThuocTinhResource {

    private final Logger log = LoggerFactory.getLogger(ThuocTinhResource.class);

    private static final String ENTITY_NAME = "thuocTinh";

    private final ThuocTinhService thuocTinhService;

    public ThuocTinhResource(ThuocTinhService thuocTinhService) {
        this.thuocTinhService = thuocTinhService;
    }

    /**
     * POST  /thuoc-tinhs : Create a new thuocTinh.
     *
     * @param thuocTinhDTO the thuocTinhDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new thuocTinhDTO, or with status 400 (Bad Request) if the thuocTinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/thuoc-tinhs")
    @Timed
    public ResponseEntity<ThuocTinhDTO> createThuocTinh(@RequestBody ThuocTinhDTO thuocTinhDTO) throws URISyntaxException {
        log.debug("REST request to save ThuocTinh : {}", thuocTinhDTO);
        if (thuocTinhDTO.getId() != null) {
            throw new BadRequestAlertException("A new thuocTinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ThuocTinhDTO result = thuocTinhService.save(thuocTinhDTO);
        return ResponseEntity.created(new URI("/api/thuoc-tinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /thuoc-tinhs : Updates an existing thuocTinh.
     *
     * @param thuocTinhDTO the thuocTinhDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated thuocTinhDTO,
     * or with status 400 (Bad Request) if the thuocTinhDTO is not valid,
     * or with status 500 (Internal Server Error) if the thuocTinhDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/thuoc-tinhs")
    @Timed
    public ResponseEntity<ThuocTinhDTO> updateThuocTinh(@RequestBody ThuocTinhDTO thuocTinhDTO) throws URISyntaxException {
        log.debug("REST request to update ThuocTinh : {}", thuocTinhDTO);
        if (thuocTinhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ThuocTinhDTO result = thuocTinhService.save(thuocTinhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, thuocTinhDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /thuoc-tinhs : get all the thuocTinhs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of thuocTinhs in body
     */
    @GetMapping("/thuoc-tinhs")
    @Timed
    public ResponseEntity<List<ThuocTinhDTO>> getAllThuocTinhs(Pageable pageable) {
        log.debug("REST request to get a page of ThuocTinhs");
        Page<ThuocTinhDTO> page = thuocTinhService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/thuoc-tinhs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /thuoc-tinhs/:id : get the "id" thuocTinh.
     *
     * @param id the id of the thuocTinhDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the thuocTinhDTO, or with status 404 (Not Found)
     */
    @GetMapping("/thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<ThuocTinhDTO> getThuocTinh(@PathVariable Long id) {
        log.debug("REST request to get ThuocTinh : {}", id);
        Optional<ThuocTinhDTO> thuocTinhDTO = thuocTinhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(thuocTinhDTO);
    }

    /**
     * DELETE  /thuoc-tinhs/:id : delete the "id" thuocTinh.
     *
     * @param id the id of the thuocTinhDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<Void> deleteThuocTinh(@PathVariable Long id) {
        log.debug("REST request to delete ThuocTinh : {}", id);
        thuocTinhService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
