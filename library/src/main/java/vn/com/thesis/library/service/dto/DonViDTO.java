package vn.com.thesis.library.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

import vn.com.thesis.library.domain.DonVi;

/**
 * A DTO for the DonVi entity.
 */
public class DonViDTO implements Serializable {

    private Long id;

    private String ten;

    private String ma;

    private String moTa;

    private Integer quyDoi;

    private LocalDate ngayTao;

    private String nguoiTao;

    private LocalDate ngayCapNhat;

    private String nguoiCapNhat;

    private Long nhomDonViId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Integer getQuyDoi() {
        return quyDoi;
    }

    public void setQuyDoi(Integer quyDoi) {
        this.quyDoi = quyDoi;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public Long getNhomDonViId() {
        return nhomDonViId;
    }

    public void setNhomDonViId(Long nhomDonViId) {
        this.nhomDonViId = nhomDonViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DonViDTO donViDTO = (DonViDTO) o;
        if (donViDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), donViDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DonViDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", ma='" + getMa() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", quyDoi=" + getQuyDoi() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            ", nhomDonVi=" + getNhomDonViId() +
            "}";
    }

	public DonViDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DonViDTO(DonVi donVi) {
		super();
		this.id = donVi.getId();
		this.ten = donVi.getTen();
		this.ma = donVi.getMa();
		this.moTa = donVi.getMoTa();
		this.quyDoi = donVi.getQuyDoi();
		this.ngayTao = donVi.getNgayTao();
		this.nguoiTao = donVi.getNguoiTao();
		this.ngayCapNhat = donVi.getNgayCapNhat();
		this.nguoiCapNhat = donVi.getNguoiCapNhat();
		this.nhomDonViId = donVi.getNhomDonVi().getId();
	}
    
    
}
