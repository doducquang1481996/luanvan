package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.SanPhamDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SanPham.
 */
public interface SanPhamService {

    /**
     * Save a sanPham.
     *
     * @param sanPhamDTO the entity to save
     * @return the persisted entity
     */
    SanPhamDTO save(SanPhamDTO sanPhamDTO);

    /**
     * Get all the sanPhams.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SanPhamDTO> findAll(Pageable pageable);


    /**
     * Get the "id" sanPham.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SanPhamDTO> findOne(Long id);

    /**
     * Delete the "id" sanPham.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    
    List<SanPhamDTO> findByTenContaining(String ten);
    
    SanPhamDTO findOneByMa(String ma);
}
