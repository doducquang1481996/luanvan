package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.SanPham;
import vn.com.thesis.library.service.dto.SanPhamDTO;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SanPham entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SanPhamRepository extends JpaRepository<SanPham, Long> {
	List<SanPhamDTO> findByTenContaining(String ten);
	
	SanPhamDTO findOneByMa(String ma);
	
}
