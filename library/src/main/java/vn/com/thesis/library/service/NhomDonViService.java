package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.NhomDonViDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing NhomDonVi.
 */
public interface NhomDonViService {

    /**
     * Save a nhomDonVi.
     *
     * @param nhomDonViDTO the entity to save
     * @return the persisted entity
     */
    NhomDonViDTO save(NhomDonViDTO nhomDonViDTO);

    /**
     * Get all the nhomDonVis.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<NhomDonViDTO> findAll(Pageable pageable);


    /**
     * Get the "id" nhomDonVi.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<NhomDonViDTO> findOne(Long id);

    /**
     * Delete the "id" nhomDonVi.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
