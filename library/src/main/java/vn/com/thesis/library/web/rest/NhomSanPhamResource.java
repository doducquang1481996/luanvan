package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.NhomSanPhamService;
import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.NhomSanPhamDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NhomSanPham.
 */
@RestController
@RequestMapping("/api")
public class NhomSanPhamResource {

    private final Logger log = LoggerFactory.getLogger(NhomSanPhamResource.class);

    private static final String ENTITY_NAME = "nhomSanPham";

    private final NhomSanPhamService nhomSanPhamService;

    public NhomSanPhamResource(NhomSanPhamService nhomSanPhamService) {
        this.nhomSanPhamService = nhomSanPhamService;
    }
    
    @Autowired
    private UltilService ultilService;

    /**
     * POST  /nhom-san-phams : Create a new nhomSanPham.
     *
     * @param nhomSanPhamDTO the nhomSanPhamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nhomSanPhamDTO, or with status 400 (Bad Request) if the nhomSanPham has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/nhom-san-phams")
    @Timed
    public ResponseEntity<NhomSanPhamDTO> createNhomSanPham(@RequestBody NhomSanPhamDTO nhomSanPhamDTO) throws URISyntaxException {
        log.debug("REST request to save NhomSanPham : {}", nhomSanPhamDTO);
        if (nhomSanPhamDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhomSanPham cannot already have an ID", ENTITY_NAME, "idexists");
        }
        nhomSanPhamDTO.setNgayTao(ultilService.timeNow());
        NhomSanPhamDTO result = nhomSanPhamService.save(nhomSanPhamDTO);
        return ResponseEntity.created(new URI("/api/nhom-san-phams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nhom-san-phams : Updates an existing nhomSanPham.
     *
     * @param nhomSanPhamDTO the nhomSanPhamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nhomSanPhamDTO,
     * or with status 400 (Bad Request) if the nhomSanPhamDTO is not valid,
     * or with status 500 (Internal Server Error) if the nhomSanPhamDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/nhom-san-phams")
    @Timed
    public ResponseEntity<NhomSanPhamDTO> updateNhomSanPham(@RequestBody NhomSanPhamDTO nhomSanPhamDTO) throws URISyntaxException {
        log.debug("REST request to update NhomSanPham : {}", nhomSanPhamDTO);
        if (nhomSanPhamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhomSanPhamDTO data = nhomSanPhamService.findOne(nhomSanPhamDTO.getId()).get();
        nhomSanPhamDTO.setNguoiTao(data.getNguoiTao());
        nhomSanPhamDTO.setNgayTao(data.getNgayTao());
        NhomSanPhamDTO result = nhomSanPhamService.save(nhomSanPhamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nhomSanPhamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nhom-san-phams : get all the nhomSanPhams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nhomSanPhams in body
     */
    @GetMapping("/nhom-san-phams")
    @Timed
    public ResponseEntity<List<NhomSanPhamDTO>> getAllNhomSanPhams(Pageable pageable) {
        log.debug("REST request to get a page of NhomSanPhams");
        Page<NhomSanPhamDTO> page = nhomSanPhamService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/nhom-san-phams");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /nhom-san-phams/:id : get the "id" nhomSanPham.
     *
     * @param id the id of the nhomSanPhamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nhomSanPhamDTO, or with status 404 (Not Found)
     */
    @GetMapping("/nhom-san-phams/{id}")
    @Timed
    public ResponseEntity<NhomSanPhamDTO> getNhomSanPham(@PathVariable Long id) {
        log.debug("REST request to get NhomSanPham : {}", id);
        Optional<NhomSanPhamDTO> nhomSanPhamDTO = nhomSanPhamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhomSanPhamDTO);
    }

    /**
     * DELETE  /nhom-san-phams/:id : delete the "id" nhomSanPham.
     *
     * @param id the id of the nhomSanPhamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/nhom-san-phams/{id}")
    @Timed
    public ResponseEntity<Void> deleteNhomSanPham(@PathVariable Long id) {
        log.debug("REST request to delete NhomSanPham : {}", id);
        nhomSanPhamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
