package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.LoaiThuocTinhService;
import vn.com.thesis.library.domain.LoaiThuocTinh;
import vn.com.thesis.library.repository.LoaiThuocTinhRepository;
import vn.com.thesis.library.service.dto.LoaiThuocTinhDTO;
import vn.com.thesis.library.service.mapper.LoaiThuocTinhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing LoaiThuocTinh.
 */
@Service
@Transactional
public class LoaiThuocTinhServiceImpl implements LoaiThuocTinhService {

    private final Logger log = LoggerFactory.getLogger(LoaiThuocTinhServiceImpl.class);

    private final LoaiThuocTinhRepository loaiThuocTinhRepository;

    private final LoaiThuocTinhMapper loaiThuocTinhMapper;

    public LoaiThuocTinhServiceImpl(LoaiThuocTinhRepository loaiThuocTinhRepository, LoaiThuocTinhMapper loaiThuocTinhMapper) {
        this.loaiThuocTinhRepository = loaiThuocTinhRepository;
        this.loaiThuocTinhMapper = loaiThuocTinhMapper;
    }

    /**
     * Save a loaiThuocTinh.
     *
     * @param loaiThuocTinhDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LoaiThuocTinhDTO save(LoaiThuocTinhDTO loaiThuocTinhDTO) {
        log.debug("Request to save LoaiThuocTinh : {}", loaiThuocTinhDTO);
        LoaiThuocTinh loaiThuocTinh = loaiThuocTinhMapper.toEntity(loaiThuocTinhDTO);
        loaiThuocTinh = loaiThuocTinhRepository.save(loaiThuocTinh);
        return loaiThuocTinhMapper.toDto(loaiThuocTinh);
    }

    /**
     * Get all the loaiThuocTinhs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoaiThuocTinhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoaiThuocTinhs");
        return loaiThuocTinhRepository.findAll(pageable)
            .map(loaiThuocTinhMapper::toDto);
    }


    /**
     * Get one loaiThuocTinh by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LoaiThuocTinhDTO> findOne(Long id) {
        log.debug("Request to get LoaiThuocTinh : {}", id);
        return loaiThuocTinhRepository.findById(id)
            .map(loaiThuocTinhMapper::toDto);
    }

    /**
     * Delete the loaiThuocTinh by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoaiThuocTinh : {}", id);
        loaiThuocTinhRepository.deleteById(id);
    }

	@Override
	public List<LoaiThuocTinhDTO> findByNhomThuocTinhId(Long nhomThuocTinhId) {
		return loaiThuocTinhRepository.findByNhomThuocTinhId(nhomThuocTinhId);
	}
}
