package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.NhomThuocTinhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity NhomThuocTinh and its DTO NhomThuocTinhDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NhomThuocTinhMapper extends EntityMapper<NhomThuocTinhDTO, NhomThuocTinh> {



    default NhomThuocTinh fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhomThuocTinh nhomThuocTinh = new NhomThuocTinh();
        nhomThuocTinh.setId(id);
        return nhomThuocTinh;
    }
}
