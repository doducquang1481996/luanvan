package vn.com.thesis.library.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ViTriKho entity.
 */
public class ViTriKhoDTO implements Serializable {

    private Long id;

    private String ten;

    private String ma;

    private String moTa;

    private LocalDate ngayTao;

    private String nguoiTao;

    private LocalDate ngayCapNhat;

    private String nguoiCapNhat;

    private Long khoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public Long getKhoId() {
        return khoId;
    }

    public void setKhoId(Long khoId) {
        this.khoId = khoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ViTriKhoDTO viTriKhoDTO = (ViTriKhoDTO) o;
        if (viTriKhoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), viTriKhoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ViTriKhoDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", ma='" + getMa() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            ", kho=" + getKhoId() +
            "}";
    }
}
