package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;

import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.service.ViTriKhoService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.NhomThuocTinhDTO;
import vn.com.thesis.library.service.dto.ViTriKhoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ViTriKho.
 */
@RestController
@RequestMapping("/api")
public class ViTriKhoResource {

    private final Logger log = LoggerFactory.getLogger(ViTriKhoResource.class);

    private static final String ENTITY_NAME = "viTriKho";

    private final ViTriKhoService viTriKhoService;

    public ViTriKhoResource(ViTriKhoService viTriKhoService) {
        this.viTriKhoService = viTriKhoService;
    }
    
    @Autowired
    private UltilService ultilService;

    /**
     * POST  /vi-tri-khos : Create a new viTriKho.
     *
     * @param viTriKhoDTO the viTriKhoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new viTriKhoDTO, or with status 400 (Bad Request) if the viTriKho has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vi-tri-khos")
    @Timed
    public ResponseEntity<ViTriKhoDTO> createViTriKho(@RequestBody ViTriKhoDTO viTriKhoDTO) throws URISyntaxException {
        log.debug("REST request to save ViTriKho : {}", viTriKhoDTO);
        if (viTriKhoDTO.getId() != null) {
            throw new BadRequestAlertException("A new viTriKho cannot already have an ID", ENTITY_NAME, "idexists");
        }
        viTriKhoDTO.setNgayTao(ultilService.timeNow());
        ViTriKhoDTO result = viTriKhoService.save(viTriKhoDTO);
        return ResponseEntity.created(new URI("/api/vi-tri-khos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vi-tri-khos : Updates an existing viTriKho.
     *
     * @param viTriKhoDTO the viTriKhoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated viTriKhoDTO,
     * or with status 400 (Bad Request) if the viTriKhoDTO is not valid,
     * or with status 500 (Internal Server Error) if the viTriKhoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vi-tri-khos")
    @Timed
    public ResponseEntity<ViTriKhoDTO> updateViTriKho(@RequestBody ViTriKhoDTO viTriKhoDTO) throws URISyntaxException {
        log.debug("REST request to update ViTriKho : {}", viTriKhoDTO);
        if (viTriKhoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ViTriKhoDTO data = viTriKhoService.findOne(viTriKhoDTO.getId()).get();
        viTriKhoDTO.setNguoiTao(data.getNguoiTao());
        viTriKhoDTO.setNgayTao(data.getNgayTao());
        ViTriKhoDTO result = viTriKhoService.save(viTriKhoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, viTriKhoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vi-tri-khos : get all the viTriKhos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of viTriKhos in body
     */
    @GetMapping("/vi-tri-khos")
    @Timed
    public ResponseEntity<List<ViTriKhoDTO>> getAllViTriKhos(Pageable pageable) {
        log.debug("REST request to get a page of ViTriKhos");
        Page<ViTriKhoDTO> page = viTriKhoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vi-tri-khos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vi-tri-khos/:id : get the "id" viTriKho.
     *
     * @param id the id of the viTriKhoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the viTriKhoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vi-tri-khos/{id}")
    @Timed
    public ResponseEntity<ViTriKhoDTO> getViTriKho(@PathVariable Long id) {
        log.debug("REST request to get ViTriKho : {}", id);
        Optional<ViTriKhoDTO> viTriKhoDTO = viTriKhoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(viTriKhoDTO);
    }

    /**
     * DELETE  /vi-tri-khos/:id : delete the "id" viTriKho.
     *
     * @param id the id of the viTriKhoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vi-tri-khos/{id}")
    @Timed
    public ResponseEntity<Void> deleteViTriKho(@PathVariable Long id) {
        log.debug("REST request to delete ViTriKho : {}", id);
        viTriKhoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/vi-tri-khos/get-by-kho")
    @Timed
    public ResponseEntity<List<ViTriKhoDTO>> getByKho(@RequestParam Long khoId) {
    	List<ViTriKhoDTO> viTriKhoDTOs = viTriKhoService.findByKhoId(khoId);
    	return new ResponseEntity<>(viTriKhoDTOs, HttpStatus.OK);
    }
}
