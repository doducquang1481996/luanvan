package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.ViTriKhoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ViTriKho and its DTO ViTriKhoDTO.
 */
@Mapper(componentModel = "spring", uses = {KhoMapper.class})
public interface ViTriKhoMapper extends EntityMapper<ViTriKhoDTO, ViTriKho> {

    @Mapping(source = "kho.id", target = "khoId")
    ViTriKhoDTO toDto(ViTriKho viTriKho);

    @Mapping(source = "khoId", target = "kho")
    ViTriKho toEntity(ViTriKhoDTO viTriKhoDTO);

    default ViTriKho fromId(Long id) {
        if (id == null) {
            return null;
        }
        ViTriKho viTriKho = new ViTriKho();
        viTriKho.setId(id);
        return viTriKho;
    }
}
