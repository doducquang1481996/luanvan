package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.NhomSanPhamService;
import vn.com.thesis.library.domain.NhomSanPham;
import vn.com.thesis.library.repository.NhomSanPhamRepository;
import vn.com.thesis.library.service.dto.NhomSanPhamDTO;
import vn.com.thesis.library.service.mapper.NhomSanPhamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing NhomSanPham.
 */
@Service
@Transactional
public class NhomSanPhamServiceImpl implements NhomSanPhamService {

    private final Logger log = LoggerFactory.getLogger(NhomSanPhamServiceImpl.class);

    private final NhomSanPhamRepository nhomSanPhamRepository;

    private final NhomSanPhamMapper nhomSanPhamMapper;

    public NhomSanPhamServiceImpl(NhomSanPhamRepository nhomSanPhamRepository, NhomSanPhamMapper nhomSanPhamMapper) {
        this.nhomSanPhamRepository = nhomSanPhamRepository;
        this.nhomSanPhamMapper = nhomSanPhamMapper;
    }

    /**
     * Save a nhomSanPham.
     *
     * @param nhomSanPhamDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public NhomSanPhamDTO save(NhomSanPhamDTO nhomSanPhamDTO) {
        log.debug("Request to save NhomSanPham : {}", nhomSanPhamDTO);
        NhomSanPham nhomSanPham = nhomSanPhamMapper.toEntity(nhomSanPhamDTO);
        nhomSanPham = nhomSanPhamRepository.save(nhomSanPham);
        return nhomSanPhamMapper.toDto(nhomSanPham);
    }

    /**
     * Get all the nhomSanPhams.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhomSanPhamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhomSanPhams");
        return nhomSanPhamRepository.findAll(pageable)
            .map(nhomSanPhamMapper::toDto);
    }


    /**
     * Get one nhomSanPham by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhomSanPhamDTO> findOne(Long id) {
        log.debug("Request to get NhomSanPham : {}", id);
        return nhomSanPhamRepository.findById(id)
            .map(nhomSanPhamMapper::toDto);
    }

    /**
     * Delete the nhomSanPham by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhomSanPham : {}", id);
        nhomSanPhamRepository.deleteById(id);
    }
}
