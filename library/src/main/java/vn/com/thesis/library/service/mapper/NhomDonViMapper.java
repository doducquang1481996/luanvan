package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.NhomDonViDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity NhomDonVi and its DTO NhomDonViDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NhomDonViMapper extends EntityMapper<NhomDonViDTO, NhomDonVi> {



    default NhomDonVi fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhomDonVi nhomDonVi = new NhomDonVi();
        nhomDonVi.setId(id);
        return nhomDonVi;
    }
}
