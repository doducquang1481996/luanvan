package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.DonVi;
import vn.com.thesis.library.service.dto.DonViDTO;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DonVi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DonViRepository extends JpaRepository<DonVi, Long> {
	
	List<DonViDTO> findByNhomDonViIdAndQuyDoi(Long nhomDonVi, Integer quyDoi);
	
	List<DonViDTO> findByNhomDonViId(Long nhomDonViId);
}
