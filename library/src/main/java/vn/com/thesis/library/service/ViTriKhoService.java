package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.ViTriKhoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing ViTriKho.
 */
public interface ViTriKhoService {

    /**
     * Save a viTriKho.
     *
     * @param viTriKhoDTO the entity to save
     * @return the persisted entity
     */
    ViTriKhoDTO save(ViTriKhoDTO viTriKhoDTO);

    /**
     * Get all the viTriKhos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ViTriKhoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" viTriKho.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ViTriKhoDTO> findOne(Long id);

    /**
     * Delete the "id" viTriKho.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    
    List<ViTriKhoDTO> findByKhoId(Long khoId);
}
