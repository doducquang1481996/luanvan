package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.NhomThuocTinhService;
import vn.com.thesis.library.domain.NhomThuocTinh;
import vn.com.thesis.library.repository.NhomThuocTinhRepository;
import vn.com.thesis.library.service.dto.NhomThuocTinhDTO;
import vn.com.thesis.library.service.mapper.NhomThuocTinhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing NhomThuocTinh.
 */
@Service
@Transactional
public class NhomThuocTinhServiceImpl implements NhomThuocTinhService {

    private final Logger log = LoggerFactory.getLogger(NhomThuocTinhServiceImpl.class);

    private final NhomThuocTinhRepository nhomThuocTinhRepository;

    private final NhomThuocTinhMapper nhomThuocTinhMapper;

    public NhomThuocTinhServiceImpl(NhomThuocTinhRepository nhomThuocTinhRepository, NhomThuocTinhMapper nhomThuocTinhMapper) {
        this.nhomThuocTinhRepository = nhomThuocTinhRepository;
        this.nhomThuocTinhMapper = nhomThuocTinhMapper;
    }

    /**
     * Save a nhomThuocTinh.
     *
     * @param nhomThuocTinhDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public NhomThuocTinhDTO save(NhomThuocTinhDTO nhomThuocTinhDTO) {
        log.debug("Request to save NhomThuocTinh : {}", nhomThuocTinhDTO);
        NhomThuocTinh nhomThuocTinh = nhomThuocTinhMapper.toEntity(nhomThuocTinhDTO);
        nhomThuocTinh = nhomThuocTinhRepository.save(nhomThuocTinh);
        return nhomThuocTinhMapper.toDto(nhomThuocTinh);
    }

    /**
     * Get all the nhomThuocTinhs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhomThuocTinhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhomThuocTinhs");
        return nhomThuocTinhRepository.findAll(pageable)
            .map(nhomThuocTinhMapper::toDto);
    }


    /**
     * Get one nhomThuocTinh by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhomThuocTinhDTO> findOne(Long id) {
        log.debug("Request to get NhomThuocTinh : {}", id);
        return nhomThuocTinhRepository.findById(id)
            .map(nhomThuocTinhMapper::toDto);
    }

    /**
     * Delete the nhomThuocTinh by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhomThuocTinh : {}", id);
        nhomThuocTinhRepository.deleteById(id);
    }
}
