package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.NhomDonVi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NhomDonVi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhomDonViRepository extends JpaRepository<NhomDonVi, Long> {

}
