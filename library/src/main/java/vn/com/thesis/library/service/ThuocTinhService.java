package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.ThuocTinhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing ThuocTinh.
 */
public interface ThuocTinhService {

    /**
     * Save a thuocTinh.
     *
     * @param thuocTinhDTO the entity to save
     * @return the persisted entity
     */
    ThuocTinhDTO save(ThuocTinhDTO thuocTinhDTO);

    /**
     * Get all the thuocTinhs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ThuocTinhDTO> findAll(Pageable pageable);


    /**
     * Get the "id" thuocTinh.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ThuocTinhDTO> findOne(Long id);

    /**
     * Delete the "id" thuocTinh.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
