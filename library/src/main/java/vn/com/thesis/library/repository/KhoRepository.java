package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.Kho;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Kho entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KhoRepository extends JpaRepository<Kho, Long> {

}
