package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.NhaSanXuatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity NhaSanXuat and its DTO NhaSanXuatDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NhaSanXuatMapper extends EntityMapper<NhaSanXuatDTO, NhaSanXuat> {



    default NhaSanXuat fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhaSanXuat nhaSanXuat = new NhaSanXuat();
        nhaSanXuat.setId(id);
        return nhaSanXuat;
    }
}
