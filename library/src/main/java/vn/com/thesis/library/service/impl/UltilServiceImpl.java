package vn.com.thesis.library.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

import vn.com.thesis.library.service.UltilService;

@Service
public class UltilServiceImpl implements UltilService {
	
	@Override
	public LocalDate timeNow() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		LocalDate localDate = LocalDate.parse(formatDateTime, formatter);
		return localDate;
	}
}
