package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.NhomDonViService;
import vn.com.thesis.library.domain.NhomDonVi;
import vn.com.thesis.library.repository.NhomDonViRepository;
import vn.com.thesis.library.service.dto.NhomDonViDTO;
import vn.com.thesis.library.service.mapper.NhomDonViMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing NhomDonVi.
 */
@Service
@Transactional
public class NhomDonViServiceImpl implements NhomDonViService {

    private final Logger log = LoggerFactory.getLogger(NhomDonViServiceImpl.class);

    private final NhomDonViRepository nhomDonViRepository;

    private final NhomDonViMapper nhomDonViMapper;

    public NhomDonViServiceImpl(NhomDonViRepository nhomDonViRepository, NhomDonViMapper nhomDonViMapper) {
        this.nhomDonViRepository = nhomDonViRepository;
        this.nhomDonViMapper = nhomDonViMapper;
    }

    /**
     * Save a nhomDonVi.
     *
     * @param nhomDonViDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public NhomDonViDTO save(NhomDonViDTO nhomDonViDTO) {
        log.debug("Request to save NhomDonVi : {}", nhomDonViDTO);
        NhomDonVi nhomDonVi = nhomDonViMapper.toEntity(nhomDonViDTO);
        nhomDonVi = nhomDonViRepository.save(nhomDonVi);
        return nhomDonViMapper.toDto(nhomDonVi);
    }

    /**
     * Get all the nhomDonVis.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhomDonViDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhomDonVis");
        return nhomDonViRepository.findAll(pageable)
            .map(nhomDonViMapper::toDto);
    }


    /**
     * Get one nhomDonVi by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhomDonViDTO> findOne(Long id) {
        log.debug("Request to get NhomDonVi : {}", id);
        return nhomDonViRepository.findById(id)
            .map(nhomDonViMapper::toDto);
    }

    /**
     * Delete the nhomDonVi by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhomDonVi : {}", id);
        nhomDonViRepository.deleteById(id);
    }
}
