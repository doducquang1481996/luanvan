package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.NhaCungCapService;
import vn.com.thesis.library.domain.NhaCungCap;
import vn.com.thesis.library.repository.NhaCungCapRepository;
import vn.com.thesis.library.service.dto.NhaCungCapDTO;
import vn.com.thesis.library.service.mapper.NhaCungCapMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing NhaCungCap.
 */
@Service
@Transactional
public class NhaCungCapServiceImpl implements NhaCungCapService {

    private final Logger log = LoggerFactory.getLogger(NhaCungCapServiceImpl.class);

    private final NhaCungCapRepository nhaCungCapRepository;

    private final NhaCungCapMapper nhaCungCapMapper;

    public NhaCungCapServiceImpl(NhaCungCapRepository nhaCungCapRepository, NhaCungCapMapper nhaCungCapMapper) {
        this.nhaCungCapRepository = nhaCungCapRepository;
        this.nhaCungCapMapper = nhaCungCapMapper;
    }

    /**
     * Save a nhaCungCap.
     *
     * @param nhaCungCapDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public NhaCungCapDTO save(NhaCungCapDTO nhaCungCapDTO) {
        log.debug("Request to save NhaCungCap : {}", nhaCungCapDTO);
        NhaCungCap nhaCungCap = nhaCungCapMapper.toEntity(nhaCungCapDTO);
        nhaCungCap = nhaCungCapRepository.save(nhaCungCap);
        return nhaCungCapMapper.toDto(nhaCungCap);
    }

    /**
     * Get all the nhaCungCaps.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhaCungCapDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhaCungCaps");
        return nhaCungCapRepository.findAll(pageable)
            .map(nhaCungCapMapper::toDto);
    }


    /**
     * Get one nhaCungCap by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhaCungCapDTO> findOne(Long id) {
        log.debug("Request to get NhaCungCap : {}", id);
        return nhaCungCapRepository.findById(id)
            .map(nhaCungCapMapper::toDto);
    }

    /**
     * Delete the nhaCungCap by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhaCungCap : {}", id);
        nhaCungCapRepository.deleteById(id);
    }
}
