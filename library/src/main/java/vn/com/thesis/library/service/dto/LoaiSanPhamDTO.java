package vn.com.thesis.library.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

import vn.com.thesis.library.domain.LoaiSanPham;

/**
 * A DTO for the LoaiSanPham entity.
 */
public class LoaiSanPhamDTO implements Serializable {

    private Long id;

    private String ten;

    private String ma;

    private String moTa;

    private LocalDate ngayTao;

    private String nguoiTao;

    private LocalDate ngayCapNhat;

    private String nguoiCapNhat;

    private Boolean active;

    private Long nhomSanPhamId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getNhomSanPhamId() {
        return nhomSanPhamId;
    }

    public void setNhomSanPhamId(Long nhomSanPhamId) {
        this.nhomSanPhamId = nhomSanPhamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoaiSanPhamDTO loaiSanPhamDTO = (LoaiSanPhamDTO) o;
        if (loaiSanPhamDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), loaiSanPhamDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LoaiSanPhamDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", ma='" + getMa() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            ", active='" + isActive() + "'" +
            ", nhomSanPham=" + getNhomSanPhamId() +
            "}";
    }

	public LoaiSanPhamDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
    
	public LoaiSanPhamDTO(LoaiSanPham loaiSanPham) {
		super();
		this.id = loaiSanPham.getId();
		this.ten = loaiSanPham.getTen();
		this.ma = loaiSanPham.getMa();
		this.moTa = loaiSanPham.getMoTa();
		this.ngayTao = loaiSanPham.getNgayTao();
		this.nguoiTao = loaiSanPham.getNguoiTao();
		this.ngayCapNhat = loaiSanPham.getNgayCapNhat();
		this.nguoiCapNhat = loaiSanPham.getNguoiCapNhat();
		this.active = loaiSanPham.isActive();
		this.nhomSanPhamId = loaiSanPham.getNhomSanPham().getId();
	}
}
