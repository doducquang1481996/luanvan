package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.NhomDonViService;
import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.NhomDonViDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NhomDonVi.
 */
@RestController
@RequestMapping("/api")
public class NhomDonViResource {

    private final Logger log = LoggerFactory.getLogger(NhomDonViResource.class);

    private static final String ENTITY_NAME = "nhomDonVi";

    private final NhomDonViService nhomDonViService;

    public NhomDonViResource(NhomDonViService nhomDonViService) {
        this.nhomDonViService = nhomDonViService;
    }
    
    @Autowired
    private UltilService ultilService;


    /**
     * POST  /nhom-don-vis : Create a new nhomDonVi.
     *
     * @param nhomDonViDTO the nhomDonViDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nhomDonViDTO, or with status 400 (Bad Request) if the nhomDonVi has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/nhom-don-vis")
    @Timed
    public ResponseEntity<NhomDonViDTO> createNhomDonVi(@RequestBody NhomDonViDTO nhomDonViDTO) throws URISyntaxException {
        log.debug("REST request to save NhomDonVi : {}", nhomDonViDTO);
        if (nhomDonViDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhomDonVi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        nhomDonViDTO.setNgayTao(ultilService.timeNow());
        NhomDonViDTO result = nhomDonViService.save(nhomDonViDTO);
        return ResponseEntity.created(new URI("/api/nhom-don-vis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nhom-don-vis : Updates an existing nhomDonVi.
     *
     * @param nhomDonViDTO the nhomDonViDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nhomDonViDTO,
     * or with status 400 (Bad Request) if the nhomDonViDTO is not valid,
     * or with status 500 (Internal Server Error) if the nhomDonViDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/nhom-don-vis")
    @Timed
    public ResponseEntity<NhomDonViDTO> updateNhomDonVi(@RequestBody NhomDonViDTO nhomDonViDTO) throws URISyntaxException {
        log.debug("REST request to update NhomDonVi : {}", nhomDonViDTO);
        if (nhomDonViDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhomDonViDTO result = nhomDonViService.save(nhomDonViDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nhomDonViDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nhom-don-vis : get all the nhomDonVis.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nhomDonVis in body
     */
    @GetMapping("/nhom-don-vis")
    @Timed
    public ResponseEntity<List<NhomDonViDTO>> getAllNhomDonVis(Pageable pageable) {
        log.debug("REST request to get a page of NhomDonVis");
        Page<NhomDonViDTO> page = nhomDonViService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/nhom-don-vis");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /nhom-don-vis/:id : get the "id" nhomDonVi.
     *
     * @param id the id of the nhomDonViDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nhomDonViDTO, or with status 404 (Not Found)
     */
    @GetMapping("/nhom-don-vis/{id}")
    @Timed
    public ResponseEntity<NhomDonViDTO> getNhomDonVi(@PathVariable Long id) {
        log.debug("REST request to get NhomDonVi : {}", id);
        Optional<NhomDonViDTO> nhomDonViDTO = nhomDonViService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhomDonViDTO);
    }

    /**
     * DELETE  /nhom-don-vis/:id : delete the "id" nhomDonVi.
     *
     * @param id the id of the nhomDonViDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/nhom-don-vis/{id}")
    @Timed
    public ResponseEntity<Void> deleteNhomDonVi(@PathVariable Long id) {
        log.debug("REST request to delete NhomDonVi : {}", id);
        nhomDonViService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
