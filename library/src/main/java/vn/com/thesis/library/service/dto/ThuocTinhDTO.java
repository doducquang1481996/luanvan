package vn.com.thesis.library.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

import vn.com.thesis.library.domain.ThuocTinh;

/**
 * A DTO for the ThuocTinh entity.
 */
public class ThuocTinhDTO implements Serializable {

    private Long id;

    private String ten;

    private String ma;

    private String moTa;

    private LocalDate ngayTao;

    private String nguoiTao;

    private LocalDate ngayCapNhat;

    private String nguoiCapNhat;

    private Long loaiThuocTinhId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public Long getLoaiThuocTinhId() {
        return loaiThuocTinhId;
    }

    public void setLoaiThuocTinhId(Long loaiThuocTinhId) {
        this.loaiThuocTinhId = loaiThuocTinhId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ThuocTinhDTO thuocTinhDTO = (ThuocTinhDTO) o;
        if (thuocTinhDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), thuocTinhDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ThuocTinhDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", ma='" + getMa() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            ", loaiThuocTinh=" + getLoaiThuocTinhId() +
            "}";
    }

	public ThuocTinhDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ThuocTinhDTO(ThuocTinh thuocTinh) {
		super();
		this.id = thuocTinh.getId();
		this.ten = thuocTinh.getTen();
		this.ma = thuocTinh.getMa();
		this.moTa = thuocTinh.getMoTa();
		this.ngayTao = thuocTinh.getNgayTao();
		this.nguoiTao = thuocTinh.getNguoiTao();
		this.ngayCapNhat = thuocTinh.getNgayCapNhat();
		this.nguoiCapNhat = thuocTinh.getNguoiCapNhat();
		this.loaiThuocTinhId = thuocTinh.getLoaiThuocTinh().getId();
	}
    
    
}
