package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.NhomSanPham;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NhomSanPham entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhomSanPhamRepository extends JpaRepository<NhomSanPham, Long> {

}
