package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.ThuocTinhService;
import vn.com.thesis.library.domain.ThuocTinh;
import vn.com.thesis.library.repository.ThuocTinhRepository;
import vn.com.thesis.library.service.dto.ThuocTinhDTO;
import vn.com.thesis.library.service.mapper.ThuocTinhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing ThuocTinh.
 */
@Service
@Transactional
public class ThuocTinhServiceImpl implements ThuocTinhService {

    private final Logger log = LoggerFactory.getLogger(ThuocTinhServiceImpl.class);

    private final ThuocTinhRepository thuocTinhRepository;

    private final ThuocTinhMapper thuocTinhMapper;

    public ThuocTinhServiceImpl(ThuocTinhRepository thuocTinhRepository, ThuocTinhMapper thuocTinhMapper) {
        this.thuocTinhRepository = thuocTinhRepository;
        this.thuocTinhMapper = thuocTinhMapper;
    }

    /**
     * Save a thuocTinh.
     *
     * @param thuocTinhDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ThuocTinhDTO save(ThuocTinhDTO thuocTinhDTO) {
        log.debug("Request to save ThuocTinh : {}", thuocTinhDTO);
        ThuocTinh thuocTinh = thuocTinhMapper.toEntity(thuocTinhDTO);
        thuocTinh = thuocTinhRepository.save(thuocTinh);
        return thuocTinhMapper.toDto(thuocTinh);
    }

    /**
     * Get all the thuocTinhs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ThuocTinhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ThuocTinhs");
        return thuocTinhRepository.findAll(pageable)
            .map(thuocTinhMapper::toDto);
    }


    /**
     * Get one thuocTinh by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ThuocTinhDTO> findOne(Long id) {
        log.debug("Request to get ThuocTinh : {}", id);
        return thuocTinhRepository.findById(id)
            .map(thuocTinhMapper::toDto);
    }

    /**
     * Delete the thuocTinh by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ThuocTinh : {}", id);
        thuocTinhRepository.deleteById(id);
    }
}
