package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.LoaiSanPhamDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing LoaiSanPham.
 */
public interface LoaiSanPhamService {

    /**
     * Save a loaiSanPham.
     *
     * @param loaiSanPhamDTO the entity to save
     * @return the persisted entity
     */
    LoaiSanPhamDTO save(LoaiSanPhamDTO loaiSanPhamDTO);

    /**
     * Get all the loaiSanPhams.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<LoaiSanPhamDTO> findAll(Pageable pageable);


    /**
     * Get the "id" loaiSanPham.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<LoaiSanPhamDTO> findOne(Long id);

    /**
     * Delete the "id" loaiSanPham.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    
    List<LoaiSanPhamDTO> findByNhomSanPhamId(Long nhomSanPhamId);
}
