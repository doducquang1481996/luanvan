package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.NhomSanPhamDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing NhomSanPham.
 */
public interface NhomSanPhamService {

    /**
     * Save a nhomSanPham.
     *
     * @param nhomSanPhamDTO the entity to save
     * @return the persisted entity
     */
    NhomSanPhamDTO save(NhomSanPhamDTO nhomSanPhamDTO);

    /**
     * Get all the nhomSanPhams.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<NhomSanPhamDTO> findAll(Pageable pageable);


    /**
     * Get the "id" nhomSanPham.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<NhomSanPhamDTO> findOne(Long id);

    /**
     * Delete the "id" nhomSanPham.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
