package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.LoaiSanPham;
import vn.com.thesis.library.service.dto.LoaiSanPhamDTO;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the LoaiSanPham entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiSanPhamRepository extends JpaRepository<LoaiSanPham, Long> {
	List<LoaiSanPhamDTO> findByNhomSanPhamId(Long nhomSanPhamId);
}
