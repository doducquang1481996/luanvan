package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.SanPhamService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.SanPhamDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SanPham.
 */
@RestController
@RequestMapping("/api")
public class SanPhamResource {

    private final Logger log = LoggerFactory.getLogger(SanPhamResource.class);

    private static final String ENTITY_NAME = "sanPham";

    private final SanPhamService sanPhamService;

    public SanPhamResource(SanPhamService sanPhamService) {
        this.sanPhamService = sanPhamService;
    }

    /**
     * POST  /san-phams : Create a new sanPham.
     *
     * @param sanPhamDTO the sanPhamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sanPhamDTO, or with status 400 (Bad Request) if the sanPham has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/san-phams")
    @Timed
    public ResponseEntity<SanPhamDTO> createSanPham(@RequestBody SanPhamDTO sanPhamDTO) throws URISyntaxException {
        log.debug("REST request to save SanPham : {}", sanPhamDTO);
        if (sanPhamDTO.getId() != null) {
            throw new BadRequestAlertException("A new sanPham cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SanPhamDTO result = sanPhamService.save(sanPhamDTO);
        return ResponseEntity.created(new URI("/api/san-phams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /san-phams : Updates an existing sanPham.
     *
     * @param sanPhamDTO the sanPhamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sanPhamDTO,
     * or with status 400 (Bad Request) if the sanPhamDTO is not valid,
     * or with status 500 (Internal Server Error) if the sanPhamDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/san-phams")
    @Timed
    public ResponseEntity<SanPhamDTO> updateSanPham(@RequestBody SanPhamDTO sanPhamDTO) throws URISyntaxException {
        log.debug("REST request to update SanPham : {}", sanPhamDTO);
        if (sanPhamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SanPhamDTO result = sanPhamService.save(sanPhamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sanPhamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /san-phams : get all the sanPhams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sanPhams in body
     */
    @GetMapping("/san-phams")
    @Timed
    public ResponseEntity<List<SanPhamDTO>> getAllSanPhams(Pageable pageable) {
        log.debug("REST request to get a page of SanPhams");
        Page<SanPhamDTO> page = sanPhamService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/san-phams");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /san-phams/:id : get the "id" sanPham.
     *
     * @param id the id of the sanPhamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sanPhamDTO, or with status 404 (Not Found)
     */
    @GetMapping("/san-phams/{id}")
    @Timed
    public ResponseEntity<SanPhamDTO> getSanPham(@PathVariable Long id) {
        log.debug("REST request to get SanPham : {}", id);
        Optional<SanPhamDTO> sanPhamDTO = sanPhamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sanPhamDTO);
    }

    /**
     * DELETE  /san-phams/:id : delete the "id" sanPham.
     *
     * @param id the id of the sanPhamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/san-phams/{id}")
    @Timed
    public ResponseEntity<Void> deleteSanPham(@PathVariable Long id) {
        log.debug("REST request to delete SanPham : {}", id);
        sanPhamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/san-phams/tim-kiem")
    @Timed
    public ResponseEntity<List<SanPhamDTO>> timKienSanPham(@RequestParam String ten) {
    	List<SanPhamDTO> sanPhamDTOs = sanPhamService.findByTenContaining(ten);
        return new ResponseEntity<>(sanPhamDTOs, HttpStatus.OK);
    }
}
