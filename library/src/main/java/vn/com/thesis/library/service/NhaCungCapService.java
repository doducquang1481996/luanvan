package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.NhaCungCapDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing NhaCungCap.
 */
public interface NhaCungCapService {

    /**
     * Save a nhaCungCap.
     *
     * @param nhaCungCapDTO the entity to save
     * @return the persisted entity
     */
    NhaCungCapDTO save(NhaCungCapDTO nhaCungCapDTO);

    /**
     * Get all the nhaCungCaps.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<NhaCungCapDTO> findAll(Pageable pageable);


    /**
     * Get the "id" nhaCungCap.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<NhaCungCapDTO> findOne(Long id);

    /**
     * Delete the "id" nhaCungCap.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
