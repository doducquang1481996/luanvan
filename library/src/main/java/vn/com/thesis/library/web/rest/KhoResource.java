package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.KhoService;
import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.KhoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Kho.
 */
@RestController
@RequestMapping("/api")
public class KhoResource {

    private final Logger log = LoggerFactory.getLogger(KhoResource.class);

    private static final String ENTITY_NAME = "kho";

    private final KhoService khoService;

    public KhoResource(KhoService khoService) {
        this.khoService = khoService;
    }
    
    @Autowired
    private UltilService ultilService;

    /**
     * POST  /khos : Create a new kho.
     *
     * @param khoDTO the khoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new khoDTO, or with status 400 (Bad Request) if the kho has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/khos")
    @Timed
    public ResponseEntity<KhoDTO> createKho(@RequestBody KhoDTO khoDTO) throws URISyntaxException {
        log.debug("REST request to save Kho : {}", khoDTO);
        if (khoDTO.getId() != null) {
            throw new BadRequestAlertException("A new kho cannot already have an ID", ENTITY_NAME, "idexists");
        }
        khoDTO.setNgayTao(ultilService.timeNow());
        KhoDTO result = khoService.save(khoDTO);
        return ResponseEntity.created(new URI("/api/khos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /khos : Updates an existing kho.
     *
     * @param khoDTO the khoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated khoDTO,
     * or with status 400 (Bad Request) if the khoDTO is not valid,
     * or with status 500 (Internal Server Error) if the khoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/khos")
    @Timed
    public ResponseEntity<KhoDTO> updateKho(@RequestBody KhoDTO khoDTO) throws URISyntaxException {
        log.debug("REST request to update Kho : {}", khoDTO);
        if (khoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KhoDTO data = khoService.findOne(khoDTO.getId()).get();
        khoDTO.setNguoiTao(data.getNguoiTao());
        khoDTO.setNgayTao(data.getNgayTao());
        KhoDTO result = khoService.save(khoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, khoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /khos : get all the khos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of khos in body
     */
    @GetMapping("/khos")
    @Timed
    public ResponseEntity<List<KhoDTO>> getAllKhos(Pageable pageable) {
        log.debug("REST request to get a page of Khos");
        Page<KhoDTO> page = khoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/khos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /khos/:id : get the "id" kho.
     *
     * @param id the id of the khoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the khoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/khos/{id}")
    @Timed
    public ResponseEntity<KhoDTO> getKho(@PathVariable Long id) {
        log.debug("REST request to get Kho : {}", id);
        Optional<KhoDTO> khoDTO = khoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(khoDTO);
    }

    /**
     * DELETE  /khos/:id : delete the "id" kho.
     *
     * @param id the id of the khoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/khos/{id}")
    @Timed
    public ResponseEntity<Void> deleteKho(@PathVariable Long id) {
        log.debug("REST request to delete Kho : {}", id);
        khoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
