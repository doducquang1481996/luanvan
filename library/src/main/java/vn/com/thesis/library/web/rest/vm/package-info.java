/**
 * View Models used by Spring MVC REST controllers.
 */
package vn.com.thesis.library.web.rest.vm;
