package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.KhoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Kho.
 */
public interface KhoService {

    /**
     * Save a kho.
     *
     * @param khoDTO the entity to save
     * @return the persisted entity
     */
    KhoDTO save(KhoDTO khoDTO);

    /**
     * Get all the khos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<KhoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" kho.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<KhoDTO> findOne(Long id);

    /**
     * Delete the "id" kho.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
