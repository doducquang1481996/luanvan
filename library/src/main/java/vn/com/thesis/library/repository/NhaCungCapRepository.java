package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.NhaCungCap;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NhaCungCap entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhaCungCapRepository extends JpaRepository<NhaCungCap, Long> {

}
