package vn.com.thesis.library.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

import vn.com.thesis.library.domain.LoaiThuocTinh;

/**
 * A DTO for the LoaiThuocTinh entity.
 */
public class LoaiThuocTinhDTO implements Serializable {

    private Long id;

    private String ten;

    private String ma;

    private String moTa;

    private LocalDate ngayTao;

    private String nguoiTao;

    private LocalDate ngayCapNhat;

    private String nguoiCapNhat;

    private Long nhomThuocTinhId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public Long getNhomThuocTinhId() {
        return nhomThuocTinhId;
    }

    public void setNhomThuocTinhId(Long nhomThuocTinhId) {
        this.nhomThuocTinhId = nhomThuocTinhId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoaiThuocTinhDTO loaiThuocTinhDTO = (LoaiThuocTinhDTO) o;
        if (loaiThuocTinhDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), loaiThuocTinhDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LoaiThuocTinhDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", ma='" + getMa() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            ", nhomThuocTinh=" + getNhomThuocTinhId() +
            "}";
    }

	public LoaiThuocTinhDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoaiThuocTinhDTO(LoaiThuocTinh loaiThuocTinh) {
		super();
		this.id = loaiThuocTinh.getId();
		this.ten = loaiThuocTinh.getTen();
		this.ma = loaiThuocTinh.getMa();
		this.moTa = loaiThuocTinh.getMoTa();
		this.ngayTao = loaiThuocTinh.getNgayTao();
		this.nguoiTao = loaiThuocTinh.getNguoiTao();
		this.ngayCapNhat = loaiThuocTinh.getNgayCapNhat();
		this.nguoiCapNhat = loaiThuocTinh.getNguoiCapNhat();
		this.nhomThuocTinhId = loaiThuocTinh.getNhomThuocTinh().getId();
	}
    
    
}
