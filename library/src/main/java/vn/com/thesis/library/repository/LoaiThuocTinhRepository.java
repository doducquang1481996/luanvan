package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.LoaiThuocTinh;
import vn.com.thesis.library.service.dto.LoaiThuocTinhDTO;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the LoaiThuocTinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiThuocTinhRepository extends JpaRepository<LoaiThuocTinh, Long> {
	List<LoaiThuocTinhDTO> findByNhomThuocTinhId(Long nhomThuocTinhId);
}
