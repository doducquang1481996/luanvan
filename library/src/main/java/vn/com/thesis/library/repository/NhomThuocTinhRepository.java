package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.NhomThuocTinh;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NhomThuocTinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhomThuocTinhRepository extends JpaRepository<NhomThuocTinh, Long> {

}
