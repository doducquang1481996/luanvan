package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.NhaCungCapDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity NhaCungCap and its DTO NhaCungCapDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NhaCungCapMapper extends EntityMapper<NhaCungCapDTO, NhaCungCap> {



    default NhaCungCap fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhaCungCap nhaCungCap = new NhaCungCap();
        nhaCungCap.setId(id);
        return nhaCungCap;
    }
}
