package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.DonViDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DonVi and its DTO DonViDTO.
 */
@Mapper(componentModel = "spring", uses = {NhomDonViMapper.class})
public interface DonViMapper extends EntityMapper<DonViDTO, DonVi> {

    @Mapping(source = "nhomDonVi.id", target = "nhomDonViId")
    DonViDTO toDto(DonVi donVi);

    @Mapping(source = "nhomDonViId", target = "nhomDonVi")
    DonVi toEntity(DonViDTO donViDTO);

    default DonVi fromId(Long id) {
        if (id == null) {
            return null;
        }
        DonVi donVi = new DonVi();
        donVi.setId(id);
        return donVi;
    }
}
