package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.NhomThuocTinhService;
import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.NhomSanPhamDTO;
import vn.com.thesis.library.service.dto.NhomThuocTinhDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NhomThuocTinh.
 */
@RestController
@RequestMapping("/api")
public class NhomThuocTinhResource {

    private final Logger log = LoggerFactory.getLogger(NhomThuocTinhResource.class);

    private static final String ENTITY_NAME = "nhomThuocTinh";

    private final NhomThuocTinhService nhomThuocTinhService;

    public NhomThuocTinhResource(NhomThuocTinhService nhomThuocTinhService) {
        this.nhomThuocTinhService = nhomThuocTinhService;
    }
    
    @Autowired
    private UltilService ultilService;

    /**
     * POST  /nhom-thuoc-tinhs : Create a new nhomThuocTinh.
     *
     * @param nhomThuocTinhDTO the nhomThuocTinhDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nhomThuocTinhDTO, or with status 400 (Bad Request) if the nhomThuocTinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/nhom-thuoc-tinhs")
    @Timed
    public ResponseEntity<NhomThuocTinhDTO> createNhomThuocTinh(@RequestBody NhomThuocTinhDTO nhomThuocTinhDTO) throws URISyntaxException {
        log.debug("REST request to save NhomThuocTinh : {}", nhomThuocTinhDTO);
        if (nhomThuocTinhDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhomThuocTinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        nhomThuocTinhDTO.setNgayTao(ultilService.timeNow());
        NhomThuocTinhDTO result = nhomThuocTinhService.save(nhomThuocTinhDTO);
        return ResponseEntity.created(new URI("/api/nhom-thuoc-tinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nhom-thuoc-tinhs : Updates an existing nhomThuocTinh.
     *
     * @param nhomThuocTinhDTO the nhomThuocTinhDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nhomThuocTinhDTO,
     * or with status 400 (Bad Request) if the nhomThuocTinhDTO is not valid,
     * or with status 500 (Internal Server Error) if the nhomThuocTinhDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/nhom-thuoc-tinhs")
    @Timed
    public ResponseEntity<NhomThuocTinhDTO> updateNhomThuocTinh(@RequestBody NhomThuocTinhDTO nhomThuocTinhDTO) throws URISyntaxException {
        log.debug("REST request to update NhomThuocTinh : {}", nhomThuocTinhDTO);
        if (nhomThuocTinhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhomThuocTinhDTO data = nhomThuocTinhService.findOne(nhomThuocTinhDTO.getId()).get();
        nhomThuocTinhDTO.setNguoiTao(data.getNguoiTao());
        nhomThuocTinhDTO.setNgayTao(data.getNgayTao());
        NhomThuocTinhDTO result = nhomThuocTinhService.save(nhomThuocTinhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nhomThuocTinhDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nhom-thuoc-tinhs : get all the nhomThuocTinhs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nhomThuocTinhs in body
     */
    @GetMapping("/nhom-thuoc-tinhs")
    @Timed
    public ResponseEntity<List<NhomThuocTinhDTO>> getAllNhomThuocTinhs(Pageable pageable) {
        log.debug("REST request to get a page of NhomThuocTinhs");
        Page<NhomThuocTinhDTO> page = nhomThuocTinhService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/nhom-thuoc-tinhs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /nhom-thuoc-tinhs/:id : get the "id" nhomThuocTinh.
     *
     * @param id the id of the nhomThuocTinhDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nhomThuocTinhDTO, or with status 404 (Not Found)
     */
    @GetMapping("/nhom-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<NhomThuocTinhDTO> getNhomThuocTinh(@PathVariable Long id) {
        log.debug("REST request to get NhomThuocTinh : {}", id);
        Optional<NhomThuocTinhDTO> nhomThuocTinhDTO = nhomThuocTinhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhomThuocTinhDTO);
    }

    /**
     * DELETE  /nhom-thuoc-tinhs/:id : delete the "id" nhomThuocTinh.
     *
     * @param id the id of the nhomThuocTinhDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/nhom-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<Void> deleteNhomThuocTinh(@PathVariable Long id) {
        log.debug("REST request to delete NhomThuocTinh : {}", id);
        nhomThuocTinhService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
