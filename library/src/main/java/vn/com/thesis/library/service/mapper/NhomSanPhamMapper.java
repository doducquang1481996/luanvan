package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.NhomSanPhamDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity NhomSanPham and its DTO NhomSanPhamDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NhomSanPhamMapper extends EntityMapper<NhomSanPhamDTO, NhomSanPham> {



    default NhomSanPham fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhomSanPham nhomSanPham = new NhomSanPham();
        nhomSanPham.setId(id);
        return nhomSanPham;
    }
}
