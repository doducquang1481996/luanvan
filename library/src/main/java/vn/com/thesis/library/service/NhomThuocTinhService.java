package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.NhomThuocTinhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing NhomThuocTinh.
 */
public interface NhomThuocTinhService {

    /**
     * Save a nhomThuocTinh.
     *
     * @param nhomThuocTinhDTO the entity to save
     * @return the persisted entity
     */
    NhomThuocTinhDTO save(NhomThuocTinhDTO nhomThuocTinhDTO);

    /**
     * Get all the nhomThuocTinhs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<NhomThuocTinhDTO> findAll(Pageable pageable);


    /**
     * Get the "id" nhomThuocTinh.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<NhomThuocTinhDTO> findOne(Long id);

    /**
     * Delete the "id" nhomThuocTinh.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
