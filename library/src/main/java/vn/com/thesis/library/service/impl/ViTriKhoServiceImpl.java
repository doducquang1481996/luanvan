package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.ViTriKhoService;
import vn.com.thesis.library.domain.ViTriKho;
import vn.com.thesis.library.repository.ViTriKhoRepository;
import vn.com.thesis.library.service.dto.ViTriKhoDTO;
import vn.com.thesis.library.service.mapper.ViTriKhoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing ViTriKho.
 */
@Service
@Transactional
public class ViTriKhoServiceImpl implements ViTriKhoService {

    private final Logger log = LoggerFactory.getLogger(ViTriKhoServiceImpl.class);

    private final ViTriKhoRepository viTriKhoRepository;

    private final ViTriKhoMapper viTriKhoMapper;

    public ViTriKhoServiceImpl(ViTriKhoRepository viTriKhoRepository, ViTriKhoMapper viTriKhoMapper) {
        this.viTriKhoRepository = viTriKhoRepository;
        this.viTriKhoMapper = viTriKhoMapper;
    }

    /**
     * Save a viTriKho.
     *
     * @param viTriKhoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ViTriKhoDTO save(ViTriKhoDTO viTriKhoDTO) {
        log.debug("Request to save ViTriKho : {}", viTriKhoDTO);
        ViTriKho viTriKho = viTriKhoMapper.toEntity(viTriKhoDTO);
        viTriKho = viTriKhoRepository.save(viTriKho);
        return viTriKhoMapper.toDto(viTriKho);
    }

    /**
     * Get all the viTriKhos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ViTriKhoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ViTriKhos");
        return viTriKhoRepository.findAll(pageable)
            .map(viTriKhoMapper::toDto);
    }


    /**
     * Get one viTriKho by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ViTriKhoDTO> findOne(Long id) {
        log.debug("Request to get ViTriKho : {}", id);
        return viTriKhoRepository.findById(id)
            .map(viTriKhoMapper::toDto);
    }

    /**
     * Delete the viTriKho by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ViTriKho : {}", id);
        viTriKhoRepository.deleteById(id);
    }

	@Override
	public List<ViTriKhoDTO> findByKhoId(Long khoId) {
		return viTriKhoRepository.findByKhoId(khoId);
	}
}
