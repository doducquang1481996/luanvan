package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.LoaiSanPhamDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LoaiSanPham and its DTO LoaiSanPhamDTO.
 */
@Mapper(componentModel = "spring", uses = {NhomSanPhamMapper.class})
public interface LoaiSanPhamMapper extends EntityMapper<LoaiSanPhamDTO, LoaiSanPham> {

    @Mapping(source = "nhomSanPham.id", target = "nhomSanPhamId")
    LoaiSanPhamDTO toDto(LoaiSanPham loaiSanPham);

    @Mapping(source = "nhomSanPhamId", target = "nhomSanPham")
    LoaiSanPham toEntity(LoaiSanPhamDTO loaiSanPhamDTO);

    default LoaiSanPham fromId(Long id) {
        if (id == null) {
            return null;
        }
        LoaiSanPham loaiSanPham = new LoaiSanPham();
        loaiSanPham.setId(id);
        return loaiSanPham;
    }
}
