package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.NhaSanXuatDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing NhaSanXuat.
 */
public interface NhaSanXuatService {

    /**
     * Save a nhaSanXuat.
     *
     * @param nhaSanXuatDTO the entity to save
     * @return the persisted entity
     */
    NhaSanXuatDTO save(NhaSanXuatDTO nhaSanXuatDTO);

    /**
     * Get all the nhaSanXuats.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<NhaSanXuatDTO> findAll(Pageable pageable);


    /**
     * Get the "id" nhaSanXuat.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<NhaSanXuatDTO> findOne(Long id);

    /**
     * Delete the "id" nhaSanXuat.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
