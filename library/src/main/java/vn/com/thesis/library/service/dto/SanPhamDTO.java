package vn.com.thesis.library.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

import vn.com.thesis.library.domain.SanPham;

/**
 * A DTO for the SanPham entity.
 */
public class SanPhamDTO implements Serializable {

    private Long id;

    private String ten;

    private String ma;

    private String moTa;

    private LocalDate ngayTao;

    private String nguoiTao;

    private LocalDate ngayCapNhat;

    private String nguoiCapNhat;

    private Long loaiSanPhamId;

    private Long nhaSanXuatId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public Long getLoaiSanPhamId() {
        return loaiSanPhamId;
    }

    public void setLoaiSanPhamId(Long loaiSanPhamId) {
        this.loaiSanPhamId = loaiSanPhamId;
    }

    public Long getNhaSanXuatId() {
        return nhaSanXuatId;
    }

    public void setNhaSanXuatId(Long nhaSanXuatId) {
        this.nhaSanXuatId = nhaSanXuatId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SanPhamDTO sanPhamDTO = (SanPhamDTO) o;
        if (sanPhamDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sanPhamDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SanPhamDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", ma='" + getMa() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            ", loaiSanPham=" + getLoaiSanPhamId() +
            ", nhaSanXuat=" + getNhaSanXuatId() +
            "}";
    }

	public SanPhamDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SanPhamDTO(SanPham sanPham) {
		super();
		this.id = sanPham.getId();
		this.ten = sanPham.getTen();
		this.ma = getMa();
		this.moTa = sanPham.getMoTa();
		this.ngayTao = sanPham.getNgayTao();
		this.nguoiTao = sanPham.getNguoiTao();
		this.ngayCapNhat = sanPham.getNgayCapNhat();
		this.nguoiCapNhat = sanPham.getNguoiCapNhat();
		this.loaiSanPhamId = sanPham.getLoaiSanPham().getId();
		this.nhaSanXuatId = sanPham.getNhaSanXuat().getId();
	}
    
    
}
