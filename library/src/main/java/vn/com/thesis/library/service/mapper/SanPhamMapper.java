package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.SanPhamDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SanPham and its DTO SanPhamDTO.
 */
@Mapper(componentModel = "spring", uses = {LoaiSanPhamMapper.class, NhaSanXuatMapper.class})
public interface SanPhamMapper extends EntityMapper<SanPhamDTO, SanPham> {

    @Mapping(source = "loaiSanPham.id", target = "loaiSanPhamId")
    @Mapping(source = "nhaSanXuat.id", target = "nhaSanXuatId")
    SanPhamDTO toDto(SanPham sanPham);

    @Mapping(source = "loaiSanPhamId", target = "loaiSanPham")
    @Mapping(source = "nhaSanXuatId", target = "nhaSanXuat")
    SanPham toEntity(SanPhamDTO sanPhamDTO);

    default SanPham fromId(Long id) {
        if (id == null) {
            return null;
        }
        SanPham sanPham = new SanPham();
        sanPham.setId(id);
        return sanPham;
    }
}
