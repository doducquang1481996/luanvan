package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.KhoService;
import vn.com.thesis.library.domain.Kho;
import vn.com.thesis.library.repository.KhoRepository;
import vn.com.thesis.library.service.dto.KhoDTO;
import vn.com.thesis.library.service.mapper.KhoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Kho.
 */
@Service
@Transactional
public class KhoServiceImpl implements KhoService {

    private final Logger log = LoggerFactory.getLogger(KhoServiceImpl.class);

    private final KhoRepository khoRepository;

    private final KhoMapper khoMapper;

    public KhoServiceImpl(KhoRepository khoRepository, KhoMapper khoMapper) {
        this.khoRepository = khoRepository;
        this.khoMapper = khoMapper;
    }

    /**
     * Save a kho.
     *
     * @param khoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public KhoDTO save(KhoDTO khoDTO) {
        log.debug("Request to save Kho : {}", khoDTO);
        Kho kho = khoMapper.toEntity(khoDTO);
        kho = khoRepository.save(kho);
        return khoMapper.toDto(kho);
    }

    /**
     * Get all the khos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KhoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Khos");
        return khoRepository.findAll(pageable)
            .map(khoMapper::toDto);
    }


    /**
     * Get one kho by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<KhoDTO> findOne(Long id) {
        log.debug("Request to get Kho : {}", id);
        return khoRepository.findById(id)
            .map(khoMapper::toDto);
    }

    /**
     * Delete the kho by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Kho : {}", id);
        khoRepository.deleteById(id);
    }
}
