package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.ThuocTinhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ThuocTinh and its DTO ThuocTinhDTO.
 */
@Mapper(componentModel = "spring", uses = {LoaiThuocTinhMapper.class})
public interface ThuocTinhMapper extends EntityMapper<ThuocTinhDTO, ThuocTinh> {

    @Mapping(source = "loaiThuocTinh.id", target = "loaiThuocTinhId")
    ThuocTinhDTO toDto(ThuocTinh thuocTinh);

    @Mapping(source = "loaiThuocTinhId", target = "loaiThuocTinh")
    ThuocTinh toEntity(ThuocTinhDTO thuocTinhDTO);

    default ThuocTinh fromId(Long id) {
        if (id == null) {
            return null;
        }
        ThuocTinh thuocTinh = new ThuocTinh();
        thuocTinh.setId(id);
        return thuocTinh;
    }
}
