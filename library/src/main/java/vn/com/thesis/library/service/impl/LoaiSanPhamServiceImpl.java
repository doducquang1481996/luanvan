package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.LoaiSanPhamService;
import vn.com.thesis.library.domain.LoaiSanPham;
import vn.com.thesis.library.repository.LoaiSanPhamRepository;
import vn.com.thesis.library.service.dto.LoaiSanPhamDTO;
import vn.com.thesis.library.service.mapper.LoaiSanPhamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing LoaiSanPham.
 */
@Service
@Transactional
public class LoaiSanPhamServiceImpl implements LoaiSanPhamService {

    private final Logger log = LoggerFactory.getLogger(LoaiSanPhamServiceImpl.class);

    private final LoaiSanPhamRepository loaiSanPhamRepository;

    private final LoaiSanPhamMapper loaiSanPhamMapper;

    public LoaiSanPhamServiceImpl(LoaiSanPhamRepository loaiSanPhamRepository, LoaiSanPhamMapper loaiSanPhamMapper) {
        this.loaiSanPhamRepository = loaiSanPhamRepository;
        this.loaiSanPhamMapper = loaiSanPhamMapper;
    }

    /**
     * Save a loaiSanPham.
     *
     * @param loaiSanPhamDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LoaiSanPhamDTO save(LoaiSanPhamDTO loaiSanPhamDTO) {
        log.debug("Request to save LoaiSanPham : {}", loaiSanPhamDTO);
        LoaiSanPham loaiSanPham = loaiSanPhamMapper.toEntity(loaiSanPhamDTO);
        loaiSanPham = loaiSanPhamRepository.save(loaiSanPham);
        return loaiSanPhamMapper.toDto(loaiSanPham);
    }

    /**
     * Get all the loaiSanPhams.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoaiSanPhamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoaiSanPhams");
        return loaiSanPhamRepository.findAll(pageable)
            .map(loaiSanPhamMapper::toDto);
    }


    /**
     * Get one loaiSanPham by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LoaiSanPhamDTO> findOne(Long id) {
        log.debug("Request to get LoaiSanPham : {}", id);
        return loaiSanPhamRepository.findById(id)
            .map(loaiSanPhamMapper::toDto);
    }

    /**
     * Delete the loaiSanPham by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoaiSanPham : {}", id);
        loaiSanPhamRepository.deleteById(id);
    }

	@Override
	public List<LoaiSanPhamDTO> findByNhomSanPhamId(Long nhomSanPhamId) {
		return loaiSanPhamRepository.findByNhomSanPhamId(nhomSanPhamId);
	}
}
