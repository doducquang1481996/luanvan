package vn.com.thesis.library.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A NhaCungCap.
 */
@Entity
@Table(name = "nha_cung_cap")
public class NhaCungCap implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ten")
    private String ten;

    @Column(name = "ma")
    private String ma;

    @Column(name = "dia_chi")
    private String diaChi;

    @Column(name = "so_dien_thoai")
    private String soDienThoai;

    @Column(name = "website")
    private String website;

    @Column(name = "fax")
    private String fax;

    @Column(name = "ma_so_thue")
    private String maSoThue;

    @Column(name = "nguoi_phu_trach")
    private String nguoiPhuTrach;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "warning")
    private Boolean warning;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public NhaCungCap ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMa() {
        return ma;
    }

    public NhaCungCap ma(String ma) {
        this.ma = ma;
        return this;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public NhaCungCap diaChi(String diaChi) {
        this.diaChi = diaChi;
        return this;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public NhaCungCap soDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
        return this;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }

    public String getWebsite() {
        return website;
    }

    public NhaCungCap website(String website) {
        this.website = website;
        return this;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFax() {
        return fax;
    }

    public NhaCungCap fax(String fax) {
        this.fax = fax;
        return this;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMaSoThue() {
        return maSoThue;
    }

    public NhaCungCap maSoThue(String maSoThue) {
        this.maSoThue = maSoThue;
        return this;
    }

    public void setMaSoThue(String maSoThue) {
        this.maSoThue = maSoThue;
    }

    public String getNguoiPhuTrach() {
        return nguoiPhuTrach;
    }

    public NhaCungCap nguoiPhuTrach(String nguoiPhuTrach) {
        this.nguoiPhuTrach = nguoiPhuTrach;
        return this;
    }

    public void setNguoiPhuTrach(String nguoiPhuTrach) {
        this.nguoiPhuTrach = nguoiPhuTrach;
    }

    public Boolean isActive() {
        return active;
    }

    public NhaCungCap active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean isWarning() {
        return warning;
    }

    public NhaCungCap warning(Boolean warning) {
        this.warning = warning;
        return this;
    }

    public void setWarning(Boolean warning) {
        this.warning = warning;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public NhaCungCap ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public NhaCungCap nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public NhaCungCap ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public NhaCungCap nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NhaCungCap nhaCungCap = (NhaCungCap) o;
        if (nhaCungCap.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nhaCungCap.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NhaCungCap{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", ma='" + getMa() + "'" +
            ", diaChi='" + getDiaChi() + "'" +
            ", soDienThoai='" + getSoDienThoai() + "'" +
            ", website='" + getWebsite() + "'" +
            ", fax='" + getFax() + "'" +
            ", maSoThue='" + getMaSoThue() + "'" +
            ", nguoiPhuTrach='" + getNguoiPhuTrach() + "'" +
            ", active='" + isActive() + "'" +
            ", warning='" + isWarning() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
