package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.NhaCungCapService;
import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.LoaiThuocTinhDTO;
import vn.com.thesis.library.service.dto.NhaCungCapDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NhaCungCap.
 */
@RestController
@RequestMapping("/api")
public class NhaCungCapResource {

    private final Logger log = LoggerFactory.getLogger(NhaCungCapResource.class);

    private static final String ENTITY_NAME = "nhaCungCap";

    private final NhaCungCapService nhaCungCapService;

    public NhaCungCapResource(NhaCungCapService nhaCungCapService) {
        this.nhaCungCapService = nhaCungCapService;
    }
    
    @Autowired
    private UltilService ultilService;

    /**
     * POST  /nha-cung-caps : Create a new nhaCungCap.
     *
     * @param nhaCungCapDTO the nhaCungCapDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nhaCungCapDTO, or with status 400 (Bad Request) if the nhaCungCap has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/nha-cung-caps")
    @Timed
    public ResponseEntity<NhaCungCapDTO> createNhaCungCap(@RequestBody NhaCungCapDTO nhaCungCapDTO) throws URISyntaxException {
        log.debug("REST request to save NhaCungCap : {}", nhaCungCapDTO);
        if (nhaCungCapDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhaCungCap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        nhaCungCapDTO.setNgayTao(ultilService.timeNow());
        NhaCungCapDTO result = nhaCungCapService.save(nhaCungCapDTO);
        return ResponseEntity.created(new URI("/api/nha-cung-caps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nha-cung-caps : Updates an existing nhaCungCap.
     *
     * @param nhaCungCapDTO the nhaCungCapDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nhaCungCapDTO,
     * or with status 400 (Bad Request) if the nhaCungCapDTO is not valid,
     * or with status 500 (Internal Server Error) if the nhaCungCapDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/nha-cung-caps")
    @Timed
    public ResponseEntity<NhaCungCapDTO> updateNhaCungCap(@RequestBody NhaCungCapDTO nhaCungCapDTO) throws URISyntaxException {
        log.debug("REST request to update NhaCungCap : {}", nhaCungCapDTO);
        if (nhaCungCapDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhaCungCapDTO data = nhaCungCapService.findOne(nhaCungCapDTO.getId()).get();
        nhaCungCapDTO.setNguoiTao(data.getNguoiTao());
        nhaCungCapDTO.setNgayTao(data.getNgayTao());
        NhaCungCapDTO result = nhaCungCapService.save(nhaCungCapDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nhaCungCapDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nha-cung-caps : get all the nhaCungCaps.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nhaCungCaps in body
     */
    @GetMapping("/nha-cung-caps")
    @Timed
    public ResponseEntity<List<NhaCungCapDTO>> getAllNhaCungCaps(Pageable pageable) {
        log.debug("REST request to get a page of NhaCungCaps");
        Page<NhaCungCapDTO> page = nhaCungCapService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/nha-cung-caps");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /nha-cung-caps/:id : get the "id" nhaCungCap.
     *
     * @param id the id of the nhaCungCapDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nhaCungCapDTO, or with status 404 (Not Found)
     */
    @GetMapping("/nha-cung-caps/{id}")
    @Timed
    public ResponseEntity<NhaCungCapDTO> getNhaCungCap(@PathVariable Long id) {
        log.debug("REST request to get NhaCungCap : {}", id);
        Optional<NhaCungCapDTO> nhaCungCapDTO = nhaCungCapService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhaCungCapDTO);
    }

    /**
     * DELETE  /nha-cung-caps/:id : delete the "id" nhaCungCap.
     *
     * @param id the id of the nhaCungCapDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/nha-cung-caps/{id}")
    @Timed
    public ResponseEntity<Void> deleteNhaCungCap(@PathVariable Long id) {
        log.debug("REST request to delete NhaCungCap : {}", id);
        nhaCungCapService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
