package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.NhaSanXuatService;
import vn.com.thesis.library.domain.NhaSanXuat;
import vn.com.thesis.library.repository.NhaSanXuatRepository;
import vn.com.thesis.library.service.dto.NhaSanXuatDTO;
import vn.com.thesis.library.service.mapper.NhaSanXuatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing NhaSanXuat.
 */
@Service
@Transactional
public class NhaSanXuatServiceImpl implements NhaSanXuatService {

    private final Logger log = LoggerFactory.getLogger(NhaSanXuatServiceImpl.class);

    private final NhaSanXuatRepository nhaSanXuatRepository;

    private final NhaSanXuatMapper nhaSanXuatMapper;

    public NhaSanXuatServiceImpl(NhaSanXuatRepository nhaSanXuatRepository, NhaSanXuatMapper nhaSanXuatMapper) {
        this.nhaSanXuatRepository = nhaSanXuatRepository;
        this.nhaSanXuatMapper = nhaSanXuatMapper;
    }

    /**
     * Save a nhaSanXuat.
     *
     * @param nhaSanXuatDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public NhaSanXuatDTO save(NhaSanXuatDTO nhaSanXuatDTO) {
        log.debug("Request to save NhaSanXuat : {}", nhaSanXuatDTO);
        NhaSanXuat nhaSanXuat = nhaSanXuatMapper.toEntity(nhaSanXuatDTO);
        nhaSanXuat = nhaSanXuatRepository.save(nhaSanXuat);
        return nhaSanXuatMapper.toDto(nhaSanXuat);
    }

    /**
     * Get all the nhaSanXuats.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhaSanXuatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhaSanXuats");
        return nhaSanXuatRepository.findAll(pageable)
            .map(nhaSanXuatMapper::toDto);
    }


    /**
     * Get one nhaSanXuat by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhaSanXuatDTO> findOne(Long id) {
        log.debug("Request to get NhaSanXuat : {}", id);
        return nhaSanXuatRepository.findById(id)
            .map(nhaSanXuatMapper::toDto);
    }

    /**
     * Delete the nhaSanXuat by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhaSanXuat : {}", id);
        nhaSanXuatRepository.deleteById(id);
    }
}
