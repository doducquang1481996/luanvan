package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.LoaiThuocTinhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LoaiThuocTinh and its DTO LoaiThuocTinhDTO.
 */
@Mapper(componentModel = "spring", uses = {NhomThuocTinhMapper.class})
public interface LoaiThuocTinhMapper extends EntityMapper<LoaiThuocTinhDTO, LoaiThuocTinh> {

    @Mapping(source = "nhomThuocTinh.id", target = "nhomThuocTinhId")
    LoaiThuocTinhDTO toDto(LoaiThuocTinh loaiThuocTinh);

    @Mapping(source = "nhomThuocTinhId", target = "nhomThuocTinh")
    LoaiThuocTinh toEntity(LoaiThuocTinhDTO loaiThuocTinhDTO);

    default LoaiThuocTinh fromId(Long id) {
        if (id == null) {
            return null;
        }
        LoaiThuocTinh loaiThuocTinh = new LoaiThuocTinh();
        loaiThuocTinh.setId(id);
        return loaiThuocTinh;
    }
}
