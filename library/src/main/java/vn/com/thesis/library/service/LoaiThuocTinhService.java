package vn.com.thesis.library.service;

import vn.com.thesis.library.service.dto.LoaiThuocTinhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing LoaiThuocTinh.
 */
public interface LoaiThuocTinhService {

    /**
     * Save a loaiThuocTinh.
     *
     * @param loaiThuocTinhDTO the entity to save
     * @return the persisted entity
     */
    LoaiThuocTinhDTO save(LoaiThuocTinhDTO loaiThuocTinhDTO);

    /**
     * Get all the loaiThuocTinhs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<LoaiThuocTinhDTO> findAll(Pageable pageable);


    /**
     * Get the "id" loaiThuocTinh.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<LoaiThuocTinhDTO> findOne(Long id);

    /**
     * Delete the "id" loaiThuocTinh.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    
    List<LoaiThuocTinhDTO> findByNhomThuocTinhId(Long nhomThuocTinhId);
}
