package vn.com.thesis.library.service.impl;

import vn.com.thesis.library.service.SanPhamService;
import vn.com.thesis.library.domain.SanPham;
import vn.com.thesis.library.repository.SanPhamRepository;
import vn.com.thesis.library.service.dto.SanPhamDTO;
import vn.com.thesis.library.service.mapper.SanPhamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing SanPham.
 */
@Service
@Transactional
public class SanPhamServiceImpl implements SanPhamService {

    private final Logger log = LoggerFactory.getLogger(SanPhamServiceImpl.class);

    private final SanPhamRepository sanPhamRepository;

    private final SanPhamMapper sanPhamMapper;

    public SanPhamServiceImpl(SanPhamRepository sanPhamRepository, SanPhamMapper sanPhamMapper) {
        this.sanPhamRepository = sanPhamRepository;
        this.sanPhamMapper = sanPhamMapper;
    }

    /**
     * Save a sanPham.
     *
     * @param sanPhamDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SanPhamDTO save(SanPhamDTO sanPhamDTO) {
        log.debug("Request to save SanPham : {}", sanPhamDTO);
        SanPham sanPham = sanPhamMapper.toEntity(sanPhamDTO);
        sanPham = sanPhamRepository.save(sanPham);
        return sanPhamMapper.toDto(sanPham);
    }

    /**
     * Get all the sanPhams.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SanPhamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SanPhams");
        return sanPhamRepository.findAll(pageable)
            .map(sanPhamMapper::toDto);
    }


    /**
     * Get one sanPham by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SanPhamDTO> findOne(Long id) {
        log.debug("Request to get SanPham : {}", id);
        return sanPhamRepository.findById(id)
            .map(sanPhamMapper::toDto);
    }

    /**
     * Delete the sanPham by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SanPham : {}", id);
        sanPhamRepository.deleteById(id);
    }

	@Override
	public List<SanPhamDTO> findByTenContaining(String ten) {
		// TODO Auto-generated method stub
		return sanPhamRepository.findByTenContaining(ten);
	}

	@Override
	public SanPhamDTO findOneByMa(String ma) {
		// TODO Auto-generated method stub
		return sanPhamRepository.findOneByMa(ma);
	}
}
