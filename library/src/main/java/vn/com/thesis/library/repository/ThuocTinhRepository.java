package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.ThuocTinh;
import vn.com.thesis.library.service.dto.ThuocTinhDTO;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ThuocTinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThuocTinhRepository extends JpaRepository<ThuocTinh, Long> {
	List<ThuocTinhDTO> findByLoaiThuocTinhId(Long loaiThuocTinhId);
}
