package vn.com.thesis.library.service.mapper;

import vn.com.thesis.library.domain.*;
import vn.com.thesis.library.service.dto.KhoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Kho and its DTO KhoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KhoMapper extends EntityMapper<KhoDTO, Kho> {



    default Kho fromId(Long id) {
        if (id == null) {
            return null;
        }
        Kho kho = new Kho();
        kho.setId(id);
        return kho;
    }
}
