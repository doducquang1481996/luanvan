package vn.com.thesis.library.repository;

import vn.com.thesis.library.domain.ViTriKho;
import vn.com.thesis.library.service.dto.ViTriKhoDTO;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ViTriKho entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ViTriKhoRepository extends JpaRepository<ViTriKho, Long> {
	
	List<ViTriKhoDTO> findByKhoId(Long khoId);

}
