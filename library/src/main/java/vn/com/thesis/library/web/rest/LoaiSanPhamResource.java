package vn.com.thesis.library.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.library.service.LoaiSanPhamService;
import vn.com.thesis.library.service.UltilService;
import vn.com.thesis.library.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.library.web.rest.util.HeaderUtil;
import vn.com.thesis.library.web.rest.util.PaginationUtil;
import vn.com.thesis.library.service.dto.KhoDTO;
import vn.com.thesis.library.service.dto.LoaiSanPhamDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LoaiSanPham.
 */
@RestController
@RequestMapping("/api")
public class LoaiSanPhamResource {

    private final Logger log = LoggerFactory.getLogger(LoaiSanPhamResource.class);

    private static final String ENTITY_NAME = "loaiSanPham";

    private final LoaiSanPhamService loaiSanPhamService;

    public LoaiSanPhamResource(LoaiSanPhamService loaiSanPhamService) {
        this.loaiSanPhamService = loaiSanPhamService;
    }
    
    @Autowired
    private UltilService ultilService;

    /**
     * POST  /loai-san-phams : Create a new loaiSanPham.
     *
     * @param loaiSanPhamDTO the loaiSanPhamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new loaiSanPhamDTO, or with status 400 (Bad Request) if the loaiSanPham has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/loai-san-phams")
    @Timed
    public ResponseEntity<LoaiSanPhamDTO> createLoaiSanPham(@RequestBody LoaiSanPhamDTO loaiSanPhamDTO) throws URISyntaxException {
        log.debug("REST request to save LoaiSanPham : {}", loaiSanPhamDTO);
        if (loaiSanPhamDTO.getId() != null) {
            throw new BadRequestAlertException("A new loaiSanPham cannot already have an ID", ENTITY_NAME, "idexists");
        }
        loaiSanPhamDTO.setNgayTao(ultilService.timeNow());
        LoaiSanPhamDTO result = loaiSanPhamService.save(loaiSanPhamDTO);
        return ResponseEntity.created(new URI("/api/loai-san-phams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /loai-san-phams : Updates an existing loaiSanPham.
     *
     * @param loaiSanPhamDTO the loaiSanPhamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated loaiSanPhamDTO,
     * or with status 400 (Bad Request) if the loaiSanPhamDTO is not valid,
     * or with status 500 (Internal Server Error) if the loaiSanPhamDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/loai-san-phams")
    @Timed
    public ResponseEntity<LoaiSanPhamDTO> updateLoaiSanPham(@RequestBody LoaiSanPhamDTO loaiSanPhamDTO) throws URISyntaxException {
        log.debug("REST request to update LoaiSanPham : {}", loaiSanPhamDTO);
        if (loaiSanPhamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoaiSanPhamDTO data = loaiSanPhamService.findOne(loaiSanPhamDTO.getId()).get();
        loaiSanPhamDTO.setNguoiTao(data.getNguoiTao());
        loaiSanPhamDTO.setNgayTao(data.getNgayTao());
        LoaiSanPhamDTO result = loaiSanPhamService.save(loaiSanPhamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, loaiSanPhamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /loai-san-phams : get all the loaiSanPhams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of loaiSanPhams in body
     */
    @GetMapping("/loai-san-phams")
    @Timed
    public ResponseEntity<List<LoaiSanPhamDTO>> getAllLoaiSanPhams(Pageable pageable) {
        log.debug("REST request to get a page of LoaiSanPhams");
        Page<LoaiSanPhamDTO> page = loaiSanPhamService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/loai-san-phams");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /loai-san-phams/:id : get the "id" loaiSanPham.
     *
     * @param id the id of the loaiSanPhamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the loaiSanPhamDTO, or with status 404 (Not Found)
     */
    @GetMapping("/loai-san-phams/{id}")
    @Timed
    public ResponseEntity<LoaiSanPhamDTO> getLoaiSanPham(@PathVariable Long id) {
        log.debug("REST request to get LoaiSanPham : {}", id);
        Optional<LoaiSanPhamDTO> loaiSanPhamDTO = loaiSanPhamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loaiSanPhamDTO);
    }

    /**
     * DELETE  /loai-san-phams/:id : delete the "id" loaiSanPham.
     *
     * @param id the id of the loaiSanPhamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/loai-san-phams/{id}")
    @Timed
    public ResponseEntity<Void> deleteLoaiSanPham(@PathVariable Long id) {
        log.debug("REST request to delete LoaiSanPham : {}", id);
        loaiSanPhamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/loai-san-phams/get-by-nhom-san-pham")
    @Timed
    public ResponseEntity<List<LoaiSanPhamDTO>> getByNhomSanPham(@RequestParam Long nhomSanPhamId) {
        List<LoaiSanPhamDTO> loaiSanPhamDTO = loaiSanPhamService.findByNhomSanPhamId(nhomSanPhamId);
        return new ResponseEntity<>(loaiSanPhamDTO, HttpStatus.OK);
    }
}
