package vn.com.thesis.uaa.web.rest;

import vn.com.thesis.uaa.UaaApp;

import vn.com.thesis.uaa.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.uaa.domain.RoomFuncitonSystem;
import vn.com.thesis.uaa.repository.RoomFuncitonSystemRepository;
import vn.com.thesis.uaa.service.RoomFuncitonSystemService;
import vn.com.thesis.uaa.service.dto.RoomFuncitonSystemDTO;
import vn.com.thesis.uaa.service.mapper.RoomFuncitonSystemMapper;
import vn.com.thesis.uaa.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static vn.com.thesis.uaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RoomFuncitonSystemResource REST controller.
 *
 * @see RoomFuncitonSystemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UaaApp.class)
public class RoomFuncitonSystemResourceIntTest {

    private static final String DEFAULT_ROOM_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ROOM_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_FUNCITION_CODE = "AAAAAAAAAA";
    private static final String UPDATED_FUNCITION_CODE = "BBBBBBBBBB";

    @Autowired
    private RoomFuncitonSystemRepository roomFuncitonSystemRepository;


    @Autowired
    private RoomFuncitonSystemMapper roomFuncitonSystemMapper;
    

    @Autowired
    private RoomFuncitonSystemService roomFuncitonSystemService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRoomFuncitonSystemMockMvc;

    private RoomFuncitonSystem roomFuncitonSystem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RoomFuncitonSystemResource roomFuncitonSystemResource = new RoomFuncitonSystemResource(roomFuncitonSystemService);
        this.restRoomFuncitonSystemMockMvc = MockMvcBuilders.standaloneSetup(roomFuncitonSystemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RoomFuncitonSystem createEntity(EntityManager em) {
        RoomFuncitonSystem roomFuncitonSystem = new RoomFuncitonSystem()
            .roomCode(DEFAULT_ROOM_CODE)
            .funcitionCode(DEFAULT_FUNCITION_CODE);
        return roomFuncitonSystem;
    }

    @Before
    public void initTest() {
        roomFuncitonSystem = createEntity(em);
    }

    @Test
    @Transactional
    public void createRoomFuncitonSystem() throws Exception {
        int databaseSizeBeforeCreate = roomFuncitonSystemRepository.findAll().size();

        // Create the RoomFuncitonSystem
        RoomFuncitonSystemDTO roomFuncitonSystemDTO = roomFuncitonSystemMapper.toDto(roomFuncitonSystem);
        restRoomFuncitonSystemMockMvc.perform(post("/api/room-funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomFuncitonSystemDTO)))
            .andExpect(status().isCreated());

        // Validate the RoomFuncitonSystem in the database
        List<RoomFuncitonSystem> roomFuncitonSystemList = roomFuncitonSystemRepository.findAll();
        assertThat(roomFuncitonSystemList).hasSize(databaseSizeBeforeCreate + 1);
        RoomFuncitonSystem testRoomFuncitonSystem = roomFuncitonSystemList.get(roomFuncitonSystemList.size() - 1);
        assertThat(testRoomFuncitonSystem.getRoomCode()).isEqualTo(DEFAULT_ROOM_CODE);
        assertThat(testRoomFuncitonSystem.getFuncitionCode()).isEqualTo(DEFAULT_FUNCITION_CODE);
    }

    @Test
    @Transactional
    public void createRoomFuncitonSystemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = roomFuncitonSystemRepository.findAll().size();

        // Create the RoomFuncitonSystem with an existing ID
        roomFuncitonSystem.setId(1L);
        RoomFuncitonSystemDTO roomFuncitonSystemDTO = roomFuncitonSystemMapper.toDto(roomFuncitonSystem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoomFuncitonSystemMockMvc.perform(post("/api/room-funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomFuncitonSystemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RoomFuncitonSystem in the database
        List<RoomFuncitonSystem> roomFuncitonSystemList = roomFuncitonSystemRepository.findAll();
        assertThat(roomFuncitonSystemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRoomFuncitonSystems() throws Exception {
        // Initialize the database
        roomFuncitonSystemRepository.saveAndFlush(roomFuncitonSystem);

        // Get all the roomFuncitonSystemList
        restRoomFuncitonSystemMockMvc.perform(get("/api/room-funciton-systems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roomFuncitonSystem.getId().intValue())))
            .andExpect(jsonPath("$.[*].roomCode").value(hasItem(DEFAULT_ROOM_CODE.toString())))
            .andExpect(jsonPath("$.[*].funcitionCode").value(hasItem(DEFAULT_FUNCITION_CODE.toString())));
    }
    

    @Test
    @Transactional
    public void getRoomFuncitonSystem() throws Exception {
        // Initialize the database
        roomFuncitonSystemRepository.saveAndFlush(roomFuncitonSystem);

        // Get the roomFuncitonSystem
        restRoomFuncitonSystemMockMvc.perform(get("/api/room-funciton-systems/{id}", roomFuncitonSystem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(roomFuncitonSystem.getId().intValue()))
            .andExpect(jsonPath("$.roomCode").value(DEFAULT_ROOM_CODE.toString()))
            .andExpect(jsonPath("$.funcitionCode").value(DEFAULT_FUNCITION_CODE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingRoomFuncitonSystem() throws Exception {
        // Get the roomFuncitonSystem
        restRoomFuncitonSystemMockMvc.perform(get("/api/room-funciton-systems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRoomFuncitonSystem() throws Exception {
        // Initialize the database
        roomFuncitonSystemRepository.saveAndFlush(roomFuncitonSystem);

        int databaseSizeBeforeUpdate = roomFuncitonSystemRepository.findAll().size();

        // Update the roomFuncitonSystem
        RoomFuncitonSystem updatedRoomFuncitonSystem = roomFuncitonSystemRepository.findById(roomFuncitonSystem.getId()).get();
        // Disconnect from session so that the updates on updatedRoomFuncitonSystem are not directly saved in db
        em.detach(updatedRoomFuncitonSystem);
        updatedRoomFuncitonSystem
            .roomCode(UPDATED_ROOM_CODE)
            .funcitionCode(UPDATED_FUNCITION_CODE);
        RoomFuncitonSystemDTO roomFuncitonSystemDTO = roomFuncitonSystemMapper.toDto(updatedRoomFuncitonSystem);

        restRoomFuncitonSystemMockMvc.perform(put("/api/room-funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomFuncitonSystemDTO)))
            .andExpect(status().isOk());

        // Validate the RoomFuncitonSystem in the database
        List<RoomFuncitonSystem> roomFuncitonSystemList = roomFuncitonSystemRepository.findAll();
        assertThat(roomFuncitonSystemList).hasSize(databaseSizeBeforeUpdate);
        RoomFuncitonSystem testRoomFuncitonSystem = roomFuncitonSystemList.get(roomFuncitonSystemList.size() - 1);
        assertThat(testRoomFuncitonSystem.getRoomCode()).isEqualTo(UPDATED_ROOM_CODE);
        assertThat(testRoomFuncitonSystem.getFuncitionCode()).isEqualTo(UPDATED_FUNCITION_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingRoomFuncitonSystem() throws Exception {
        int databaseSizeBeforeUpdate = roomFuncitonSystemRepository.findAll().size();

        // Create the RoomFuncitonSystem
        RoomFuncitonSystemDTO roomFuncitonSystemDTO = roomFuncitonSystemMapper.toDto(roomFuncitonSystem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restRoomFuncitonSystemMockMvc.perform(put("/api/room-funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomFuncitonSystemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RoomFuncitonSystem in the database
        List<RoomFuncitonSystem> roomFuncitonSystemList = roomFuncitonSystemRepository.findAll();
        assertThat(roomFuncitonSystemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRoomFuncitonSystem() throws Exception {
        // Initialize the database
        roomFuncitonSystemRepository.saveAndFlush(roomFuncitonSystem);

        int databaseSizeBeforeDelete = roomFuncitonSystemRepository.findAll().size();

        // Get the roomFuncitonSystem
        restRoomFuncitonSystemMockMvc.perform(delete("/api/room-funciton-systems/{id}", roomFuncitonSystem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RoomFuncitonSystem> roomFuncitonSystemList = roomFuncitonSystemRepository.findAll();
        assertThat(roomFuncitonSystemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoomFuncitonSystem.class);
        RoomFuncitonSystem roomFuncitonSystem1 = new RoomFuncitonSystem();
        roomFuncitonSystem1.setId(1L);
        RoomFuncitonSystem roomFuncitonSystem2 = new RoomFuncitonSystem();
        roomFuncitonSystem2.setId(roomFuncitonSystem1.getId());
        assertThat(roomFuncitonSystem1).isEqualTo(roomFuncitonSystem2);
        roomFuncitonSystem2.setId(2L);
        assertThat(roomFuncitonSystem1).isNotEqualTo(roomFuncitonSystem2);
        roomFuncitonSystem1.setId(null);
        assertThat(roomFuncitonSystem1).isNotEqualTo(roomFuncitonSystem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoomFuncitonSystemDTO.class);
        RoomFuncitonSystemDTO roomFuncitonSystemDTO1 = new RoomFuncitonSystemDTO();
        roomFuncitonSystemDTO1.setId(1L);
        RoomFuncitonSystemDTO roomFuncitonSystemDTO2 = new RoomFuncitonSystemDTO();
        assertThat(roomFuncitonSystemDTO1).isNotEqualTo(roomFuncitonSystemDTO2);
        roomFuncitonSystemDTO2.setId(roomFuncitonSystemDTO1.getId());
        assertThat(roomFuncitonSystemDTO1).isEqualTo(roomFuncitonSystemDTO2);
        roomFuncitonSystemDTO2.setId(2L);
        assertThat(roomFuncitonSystemDTO1).isNotEqualTo(roomFuncitonSystemDTO2);
        roomFuncitonSystemDTO1.setId(null);
        assertThat(roomFuncitonSystemDTO1).isNotEqualTo(roomFuncitonSystemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(roomFuncitonSystemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(roomFuncitonSystemMapper.fromId(null)).isNull();
    }
}
