package vn.com.thesis.uaa.web.rest;

import vn.com.thesis.uaa.UaaApp;

import vn.com.thesis.uaa.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.uaa.domain.RoomEmployee;
import vn.com.thesis.uaa.repository.RoomEmployeeRepository;
import vn.com.thesis.uaa.service.RoomEmployeeService;
import vn.com.thesis.uaa.service.dto.RoomEmployeeDTO;
import vn.com.thesis.uaa.service.mapper.RoomEmployeeMapper;
import vn.com.thesis.uaa.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static vn.com.thesis.uaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RoomEmployeeResource REST controller.
 *
 * @see RoomEmployeeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UaaApp.class)
public class RoomEmployeeResourceIntTest {

    private static final String DEFAULT_ROOM_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ROOM_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_EMPLOYEE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EMPLOYEE_CODE = "BBBBBBBBBB";

    @Autowired
    private RoomEmployeeRepository roomEmployeeRepository;


    @Autowired
    private RoomEmployeeMapper roomEmployeeMapper;
    

    @Autowired
    private RoomEmployeeService roomEmployeeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRoomEmployeeMockMvc;

    private RoomEmployee roomEmployee;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RoomEmployeeResource roomEmployeeResource = new RoomEmployeeResource(roomEmployeeService);
        this.restRoomEmployeeMockMvc = MockMvcBuilders.standaloneSetup(roomEmployeeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RoomEmployee createEntity(EntityManager em) {
        RoomEmployee roomEmployee = new RoomEmployee()
            .roomCode(DEFAULT_ROOM_CODE)
            .employeeCode(DEFAULT_EMPLOYEE_CODE);
        return roomEmployee;
    }

    @Before
    public void initTest() {
        roomEmployee = createEntity(em);
    }

    @Test
    @Transactional
    public void createRoomEmployee() throws Exception {
        int databaseSizeBeforeCreate = roomEmployeeRepository.findAll().size();

        // Create the RoomEmployee
        RoomEmployeeDTO roomEmployeeDTO = roomEmployeeMapper.toDto(roomEmployee);
        restRoomEmployeeMockMvc.perform(post("/api/room-employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomEmployeeDTO)))
            .andExpect(status().isCreated());

        // Validate the RoomEmployee in the database
        List<RoomEmployee> roomEmployeeList = roomEmployeeRepository.findAll();
        assertThat(roomEmployeeList).hasSize(databaseSizeBeforeCreate + 1);
        RoomEmployee testRoomEmployee = roomEmployeeList.get(roomEmployeeList.size() - 1);
        assertThat(testRoomEmployee.getRoomCode()).isEqualTo(DEFAULT_ROOM_CODE);
        assertThat(testRoomEmployee.getEmployeeCode()).isEqualTo(DEFAULT_EMPLOYEE_CODE);
    }

    @Test
    @Transactional
    public void createRoomEmployeeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = roomEmployeeRepository.findAll().size();

        // Create the RoomEmployee with an existing ID
        roomEmployee.setId(1L);
        RoomEmployeeDTO roomEmployeeDTO = roomEmployeeMapper.toDto(roomEmployee);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoomEmployeeMockMvc.perform(post("/api/room-employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomEmployeeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RoomEmployee in the database
        List<RoomEmployee> roomEmployeeList = roomEmployeeRepository.findAll();
        assertThat(roomEmployeeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRoomEmployees() throws Exception {
        // Initialize the database
        roomEmployeeRepository.saveAndFlush(roomEmployee);

        // Get all the roomEmployeeList
        restRoomEmployeeMockMvc.perform(get("/api/room-employees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roomEmployee.getId().intValue())))
            .andExpect(jsonPath("$.[*].roomCode").value(hasItem(DEFAULT_ROOM_CODE.toString())))
            .andExpect(jsonPath("$.[*].employeeCode").value(hasItem(DEFAULT_EMPLOYEE_CODE.toString())));
    }
    

    @Test
    @Transactional
    public void getRoomEmployee() throws Exception {
        // Initialize the database
        roomEmployeeRepository.saveAndFlush(roomEmployee);

        // Get the roomEmployee
        restRoomEmployeeMockMvc.perform(get("/api/room-employees/{id}", roomEmployee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(roomEmployee.getId().intValue()))
            .andExpect(jsonPath("$.roomCode").value(DEFAULT_ROOM_CODE.toString()))
            .andExpect(jsonPath("$.employeeCode").value(DEFAULT_EMPLOYEE_CODE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingRoomEmployee() throws Exception {
        // Get the roomEmployee
        restRoomEmployeeMockMvc.perform(get("/api/room-employees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRoomEmployee() throws Exception {
        // Initialize the database
        roomEmployeeRepository.saveAndFlush(roomEmployee);

        int databaseSizeBeforeUpdate = roomEmployeeRepository.findAll().size();

        // Update the roomEmployee
        RoomEmployee updatedRoomEmployee = roomEmployeeRepository.findById(roomEmployee.getId()).get();
        // Disconnect from session so that the updates on updatedRoomEmployee are not directly saved in db
        em.detach(updatedRoomEmployee);
        updatedRoomEmployee
            .roomCode(UPDATED_ROOM_CODE)
            .employeeCode(UPDATED_EMPLOYEE_CODE);
        RoomEmployeeDTO roomEmployeeDTO = roomEmployeeMapper.toDto(updatedRoomEmployee);

        restRoomEmployeeMockMvc.perform(put("/api/room-employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomEmployeeDTO)))
            .andExpect(status().isOk());

        // Validate the RoomEmployee in the database
        List<RoomEmployee> roomEmployeeList = roomEmployeeRepository.findAll();
        assertThat(roomEmployeeList).hasSize(databaseSizeBeforeUpdate);
        RoomEmployee testRoomEmployee = roomEmployeeList.get(roomEmployeeList.size() - 1);
        assertThat(testRoomEmployee.getRoomCode()).isEqualTo(UPDATED_ROOM_CODE);
        assertThat(testRoomEmployee.getEmployeeCode()).isEqualTo(UPDATED_EMPLOYEE_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingRoomEmployee() throws Exception {
        int databaseSizeBeforeUpdate = roomEmployeeRepository.findAll().size();

        // Create the RoomEmployee
        RoomEmployeeDTO roomEmployeeDTO = roomEmployeeMapper.toDto(roomEmployee);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restRoomEmployeeMockMvc.perform(put("/api/room-employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomEmployeeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RoomEmployee in the database
        List<RoomEmployee> roomEmployeeList = roomEmployeeRepository.findAll();
        assertThat(roomEmployeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRoomEmployee() throws Exception {
        // Initialize the database
        roomEmployeeRepository.saveAndFlush(roomEmployee);

        int databaseSizeBeforeDelete = roomEmployeeRepository.findAll().size();

        // Get the roomEmployee
        restRoomEmployeeMockMvc.perform(delete("/api/room-employees/{id}", roomEmployee.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RoomEmployee> roomEmployeeList = roomEmployeeRepository.findAll();
        assertThat(roomEmployeeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoomEmployee.class);
        RoomEmployee roomEmployee1 = new RoomEmployee();
        roomEmployee1.setId(1L);
        RoomEmployee roomEmployee2 = new RoomEmployee();
        roomEmployee2.setId(roomEmployee1.getId());
        assertThat(roomEmployee1).isEqualTo(roomEmployee2);
        roomEmployee2.setId(2L);
        assertThat(roomEmployee1).isNotEqualTo(roomEmployee2);
        roomEmployee1.setId(null);
        assertThat(roomEmployee1).isNotEqualTo(roomEmployee2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoomEmployeeDTO.class);
        RoomEmployeeDTO roomEmployeeDTO1 = new RoomEmployeeDTO();
        roomEmployeeDTO1.setId(1L);
        RoomEmployeeDTO roomEmployeeDTO2 = new RoomEmployeeDTO();
        assertThat(roomEmployeeDTO1).isNotEqualTo(roomEmployeeDTO2);
        roomEmployeeDTO2.setId(roomEmployeeDTO1.getId());
        assertThat(roomEmployeeDTO1).isEqualTo(roomEmployeeDTO2);
        roomEmployeeDTO2.setId(2L);
        assertThat(roomEmployeeDTO1).isNotEqualTo(roomEmployeeDTO2);
        roomEmployeeDTO1.setId(null);
        assertThat(roomEmployeeDTO1).isNotEqualTo(roomEmployeeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(roomEmployeeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(roomEmployeeMapper.fromId(null)).isNull();
    }
}
