package vn.com.thesis.uaa.web.rest;

import vn.com.thesis.uaa.UaaApp;

import vn.com.thesis.uaa.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.uaa.domain.FuncitonSystem;
import vn.com.thesis.uaa.repository.FuncitonSystemRepository;
import vn.com.thesis.uaa.service.FuncitonSystemService;
import vn.com.thesis.uaa.service.dto.FuncitonSystemDTO;
import vn.com.thesis.uaa.service.mapper.FuncitonSystemMapper;
import vn.com.thesis.uaa.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static vn.com.thesis.uaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FuncitonSystemResource REST controller.
 *
 * @see FuncitonSystemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UaaApp.class)
public class FuncitonSystemResourceIntTest {

    private static final String DEFAULT_FUNCITION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FUNCITION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FUNCITION_CODE = "AAAAAAAAAA";
    private static final String UPDATED_FUNCITION_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_FUNCITION_LINK = "AAAAAAAAAA";
    private static final String UPDATED_FUNCITION_LINK = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final Long DEFAULT_PARENT_ID = 1L;
    private static final Long UPDATED_PARENT_ID = 2L;

    @Autowired
    private FuncitonSystemRepository funcitonSystemRepository;


    @Autowired
    private FuncitonSystemMapper funcitonSystemMapper;
    

    @Autowired
    private FuncitonSystemService funcitonSystemService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFuncitonSystemMockMvc;

    private FuncitonSystem funcitonSystem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FuncitonSystemResource funcitonSystemResource = new FuncitonSystemResource(funcitonSystemService);
        this.restFuncitonSystemMockMvc = MockMvcBuilders.standaloneSetup(funcitonSystemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FuncitonSystem createEntity(EntityManager em) {
        FuncitonSystem funcitonSystem = new FuncitonSystem()
            .funcitionName(DEFAULT_FUNCITION_NAME)
            .funcitionCode(DEFAULT_FUNCITION_CODE)
            .funcitionLink(DEFAULT_FUNCITION_LINK)
            .note(DEFAULT_NOTE)
            .parentId(DEFAULT_PARENT_ID);
        return funcitonSystem;
    }

    @Before
    public void initTest() {
        funcitonSystem = createEntity(em);
    }

    @Test
    @Transactional
    public void createFuncitonSystem() throws Exception {
        int databaseSizeBeforeCreate = funcitonSystemRepository.findAll().size();

        // Create the FuncitonSystem
        FuncitonSystemDTO funcitonSystemDTO = funcitonSystemMapper.toDto(funcitonSystem);
        restFuncitonSystemMockMvc.perform(post("/api/funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(funcitonSystemDTO)))
            .andExpect(status().isCreated());

        // Validate the FuncitonSystem in the database
        List<FuncitonSystem> funcitonSystemList = funcitonSystemRepository.findAll();
        assertThat(funcitonSystemList).hasSize(databaseSizeBeforeCreate + 1);
        FuncitonSystem testFuncitonSystem = funcitonSystemList.get(funcitonSystemList.size() - 1);
        assertThat(testFuncitonSystem.getFuncitionName()).isEqualTo(DEFAULT_FUNCITION_NAME);
        assertThat(testFuncitonSystem.getFuncitionCode()).isEqualTo(DEFAULT_FUNCITION_CODE);
        assertThat(testFuncitonSystem.getFuncitionLink()).isEqualTo(DEFAULT_FUNCITION_LINK);
        assertThat(testFuncitonSystem.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testFuncitonSystem.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
    }

    @Test
    @Transactional
    public void createFuncitonSystemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = funcitonSystemRepository.findAll().size();

        // Create the FuncitonSystem with an existing ID
        funcitonSystem.setId(1L);
        FuncitonSystemDTO funcitonSystemDTO = funcitonSystemMapper.toDto(funcitonSystem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFuncitonSystemMockMvc.perform(post("/api/funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(funcitonSystemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FuncitonSystem in the database
        List<FuncitonSystem> funcitonSystemList = funcitonSystemRepository.findAll();
        assertThat(funcitonSystemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllFuncitonSystems() throws Exception {
        // Initialize the database
        funcitonSystemRepository.saveAndFlush(funcitonSystem);

        // Get all the funcitonSystemList
        restFuncitonSystemMockMvc.perform(get("/api/funciton-systems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(funcitonSystem.getId().intValue())))
            .andExpect(jsonPath("$.[*].funcitionName").value(hasItem(DEFAULT_FUNCITION_NAME.toString())))
            .andExpect(jsonPath("$.[*].funcitionCode").value(hasItem(DEFAULT_FUNCITION_CODE.toString())))
            .andExpect(jsonPath("$.[*].funcitionLink").value(hasItem(DEFAULT_FUNCITION_LINK.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())));
    }
    

    @Test
    @Transactional
    public void getFuncitonSystem() throws Exception {
        // Initialize the database
        funcitonSystemRepository.saveAndFlush(funcitonSystem);

        // Get the funcitonSystem
        restFuncitonSystemMockMvc.perform(get("/api/funciton-systems/{id}", funcitonSystem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(funcitonSystem.getId().intValue()))
            .andExpect(jsonPath("$.funcitionName").value(DEFAULT_FUNCITION_NAME.toString()))
            .andExpect(jsonPath("$.funcitionCode").value(DEFAULT_FUNCITION_CODE.toString()))
            .andExpect(jsonPath("$.funcitionLink").value(DEFAULT_FUNCITION_LINK.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFuncitonSystem() throws Exception {
        // Get the funcitonSystem
        restFuncitonSystemMockMvc.perform(get("/api/funciton-systems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFuncitonSystem() throws Exception {
        // Initialize the database
        funcitonSystemRepository.saveAndFlush(funcitonSystem);

        int databaseSizeBeforeUpdate = funcitonSystemRepository.findAll().size();

        // Update the funcitonSystem
        FuncitonSystem updatedFuncitonSystem = funcitonSystemRepository.findById(funcitonSystem.getId()).get();
        // Disconnect from session so that the updates on updatedFuncitonSystem are not directly saved in db
        em.detach(updatedFuncitonSystem);
        updatedFuncitonSystem
            .funcitionName(UPDATED_FUNCITION_NAME)
            .funcitionCode(UPDATED_FUNCITION_CODE)
            .funcitionLink(UPDATED_FUNCITION_LINK)
            .note(UPDATED_NOTE)
            .parentId(UPDATED_PARENT_ID);
        FuncitonSystemDTO funcitonSystemDTO = funcitonSystemMapper.toDto(updatedFuncitonSystem);

        restFuncitonSystemMockMvc.perform(put("/api/funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(funcitonSystemDTO)))
            .andExpect(status().isOk());

        // Validate the FuncitonSystem in the database
        List<FuncitonSystem> funcitonSystemList = funcitonSystemRepository.findAll();
        assertThat(funcitonSystemList).hasSize(databaseSizeBeforeUpdate);
        FuncitonSystem testFuncitonSystem = funcitonSystemList.get(funcitonSystemList.size() - 1);
        assertThat(testFuncitonSystem.getFuncitionName()).isEqualTo(UPDATED_FUNCITION_NAME);
        assertThat(testFuncitonSystem.getFuncitionCode()).isEqualTo(UPDATED_FUNCITION_CODE);
        assertThat(testFuncitonSystem.getFuncitionLink()).isEqualTo(UPDATED_FUNCITION_LINK);
        assertThat(testFuncitonSystem.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testFuncitonSystem.getParentId()).isEqualTo(UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingFuncitonSystem() throws Exception {
        int databaseSizeBeforeUpdate = funcitonSystemRepository.findAll().size();

        // Create the FuncitonSystem
        FuncitonSystemDTO funcitonSystemDTO = funcitonSystemMapper.toDto(funcitonSystem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restFuncitonSystemMockMvc.perform(put("/api/funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(funcitonSystemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FuncitonSystem in the database
        List<FuncitonSystem> funcitonSystemList = funcitonSystemRepository.findAll();
        assertThat(funcitonSystemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFuncitonSystem() throws Exception {
        // Initialize the database
        funcitonSystemRepository.saveAndFlush(funcitonSystem);

        int databaseSizeBeforeDelete = funcitonSystemRepository.findAll().size();

        // Get the funcitonSystem
        restFuncitonSystemMockMvc.perform(delete("/api/funciton-systems/{id}", funcitonSystem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FuncitonSystem> funcitonSystemList = funcitonSystemRepository.findAll();
        assertThat(funcitonSystemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FuncitonSystem.class);
        FuncitonSystem funcitonSystem1 = new FuncitonSystem();
        funcitonSystem1.setId(1L);
        FuncitonSystem funcitonSystem2 = new FuncitonSystem();
        funcitonSystem2.setId(funcitonSystem1.getId());
        assertThat(funcitonSystem1).isEqualTo(funcitonSystem2);
        funcitonSystem2.setId(2L);
        assertThat(funcitonSystem1).isNotEqualTo(funcitonSystem2);
        funcitonSystem1.setId(null);
        assertThat(funcitonSystem1).isNotEqualTo(funcitonSystem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FuncitonSystemDTO.class);
        FuncitonSystemDTO funcitonSystemDTO1 = new FuncitonSystemDTO();
        funcitonSystemDTO1.setId(1L);
        FuncitonSystemDTO funcitonSystemDTO2 = new FuncitonSystemDTO();
        assertThat(funcitonSystemDTO1).isNotEqualTo(funcitonSystemDTO2);
        funcitonSystemDTO2.setId(funcitonSystemDTO1.getId());
        assertThat(funcitonSystemDTO1).isEqualTo(funcitonSystemDTO2);
        funcitonSystemDTO2.setId(2L);
        assertThat(funcitonSystemDTO1).isNotEqualTo(funcitonSystemDTO2);
        funcitonSystemDTO1.setId(null);
        assertThat(funcitonSystemDTO1).isNotEqualTo(funcitonSystemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(funcitonSystemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(funcitonSystemMapper.fromId(null)).isNull();
    }
}
