package vn.com.thesis.uaa.web.rest;

import vn.com.thesis.uaa.UaaApp;

import vn.com.thesis.uaa.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.uaa.domain.EmployeeFuncitonSystem;
import vn.com.thesis.uaa.repository.EmployeeFuncitonSystemRepository;
import vn.com.thesis.uaa.service.EmployeeFuncitonSystemService;
import vn.com.thesis.uaa.service.dto.EmployeeFuncitonSystemDTO;
import vn.com.thesis.uaa.service.mapper.EmployeeFuncitonSystemMapper;
import vn.com.thesis.uaa.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static vn.com.thesis.uaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EmployeeFuncitonSystemResource REST controller.
 *
 * @see EmployeeFuncitonSystemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UaaApp.class)
public class EmployeeFuncitonSystemResourceIntTest {

    private static final String DEFAULT_EMPLOYEE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EMPLOYEE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_FUNCITION_CODE = "AAAAAAAAAA";
    private static final String UPDATED_FUNCITION_CODE = "BBBBBBBBBB";

    @Autowired
    private EmployeeFuncitonSystemRepository employeeFuncitonSystemRepository;


    @Autowired
    private EmployeeFuncitonSystemMapper employeeFuncitonSystemMapper;
    

    @Autowired
    private EmployeeFuncitonSystemService employeeFuncitonSystemService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEmployeeFuncitonSystemMockMvc;

    private EmployeeFuncitonSystem employeeFuncitonSystem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EmployeeFuncitonSystemResource employeeFuncitonSystemResource = new EmployeeFuncitonSystemResource(employeeFuncitonSystemService);
        this.restEmployeeFuncitonSystemMockMvc = MockMvcBuilders.standaloneSetup(employeeFuncitonSystemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmployeeFuncitonSystem createEntity(EntityManager em) {
        EmployeeFuncitonSystem employeeFuncitonSystem = new EmployeeFuncitonSystem()
            .employeeCode(DEFAULT_EMPLOYEE_CODE)
            .funcitionCode(DEFAULT_FUNCITION_CODE);
        return employeeFuncitonSystem;
    }

    @Before
    public void initTest() {
        employeeFuncitonSystem = createEntity(em);
    }

    @Test
    @Transactional
    public void createEmployeeFuncitonSystem() throws Exception {
        int databaseSizeBeforeCreate = employeeFuncitonSystemRepository.findAll().size();

        // Create the EmployeeFuncitonSystem
        EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO = employeeFuncitonSystemMapper.toDto(employeeFuncitonSystem);
        restEmployeeFuncitonSystemMockMvc.perform(post("/api/employee-funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeFuncitonSystemDTO)))
            .andExpect(status().isCreated());

        // Validate the EmployeeFuncitonSystem in the database
        List<EmployeeFuncitonSystem> employeeFuncitonSystemList = employeeFuncitonSystemRepository.findAll();
        assertThat(employeeFuncitonSystemList).hasSize(databaseSizeBeforeCreate + 1);
        EmployeeFuncitonSystem testEmployeeFuncitonSystem = employeeFuncitonSystemList.get(employeeFuncitonSystemList.size() - 1);
        assertThat(testEmployeeFuncitonSystem.getEmployeeCode()).isEqualTo(DEFAULT_EMPLOYEE_CODE);
        assertThat(testEmployeeFuncitonSystem.getFuncitionCode()).isEqualTo(DEFAULT_FUNCITION_CODE);
    }

    @Test
    @Transactional
    public void createEmployeeFuncitonSystemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = employeeFuncitonSystemRepository.findAll().size();

        // Create the EmployeeFuncitonSystem with an existing ID
        employeeFuncitonSystem.setId(1L);
        EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO = employeeFuncitonSystemMapper.toDto(employeeFuncitonSystem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmployeeFuncitonSystemMockMvc.perform(post("/api/employee-funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeFuncitonSystemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmployeeFuncitonSystem in the database
        List<EmployeeFuncitonSystem> employeeFuncitonSystemList = employeeFuncitonSystemRepository.findAll();
        assertThat(employeeFuncitonSystemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEmployeeFuncitonSystems() throws Exception {
        // Initialize the database
        employeeFuncitonSystemRepository.saveAndFlush(employeeFuncitonSystem);

        // Get all the employeeFuncitonSystemList
        restEmployeeFuncitonSystemMockMvc.perform(get("/api/employee-funciton-systems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(employeeFuncitonSystem.getId().intValue())))
            .andExpect(jsonPath("$.[*].employeeCode").value(hasItem(DEFAULT_EMPLOYEE_CODE.toString())))
            .andExpect(jsonPath("$.[*].funcitionCode").value(hasItem(DEFAULT_FUNCITION_CODE.toString())));
    }
    

    @Test
    @Transactional
    public void getEmployeeFuncitonSystem() throws Exception {
        // Initialize the database
        employeeFuncitonSystemRepository.saveAndFlush(employeeFuncitonSystem);

        // Get the employeeFuncitonSystem
        restEmployeeFuncitonSystemMockMvc.perform(get("/api/employee-funciton-systems/{id}", employeeFuncitonSystem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(employeeFuncitonSystem.getId().intValue()))
            .andExpect(jsonPath("$.employeeCode").value(DEFAULT_EMPLOYEE_CODE.toString()))
            .andExpect(jsonPath("$.funcitionCode").value(DEFAULT_FUNCITION_CODE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingEmployeeFuncitonSystem() throws Exception {
        // Get the employeeFuncitonSystem
        restEmployeeFuncitonSystemMockMvc.perform(get("/api/employee-funciton-systems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEmployeeFuncitonSystem() throws Exception {
        // Initialize the database
        employeeFuncitonSystemRepository.saveAndFlush(employeeFuncitonSystem);

        int databaseSizeBeforeUpdate = employeeFuncitonSystemRepository.findAll().size();

        // Update the employeeFuncitonSystem
        EmployeeFuncitonSystem updatedEmployeeFuncitonSystem = employeeFuncitonSystemRepository.findById(employeeFuncitonSystem.getId()).get();
        // Disconnect from session so that the updates on updatedEmployeeFuncitonSystem are not directly saved in db
        em.detach(updatedEmployeeFuncitonSystem);
        updatedEmployeeFuncitonSystem
            .employeeCode(UPDATED_EMPLOYEE_CODE)
            .funcitionCode(UPDATED_FUNCITION_CODE);
        EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO = employeeFuncitonSystemMapper.toDto(updatedEmployeeFuncitonSystem);

        restEmployeeFuncitonSystemMockMvc.perform(put("/api/employee-funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeFuncitonSystemDTO)))
            .andExpect(status().isOk());

        // Validate the EmployeeFuncitonSystem in the database
        List<EmployeeFuncitonSystem> employeeFuncitonSystemList = employeeFuncitonSystemRepository.findAll();
        assertThat(employeeFuncitonSystemList).hasSize(databaseSizeBeforeUpdate);
        EmployeeFuncitonSystem testEmployeeFuncitonSystem = employeeFuncitonSystemList.get(employeeFuncitonSystemList.size() - 1);
        assertThat(testEmployeeFuncitonSystem.getEmployeeCode()).isEqualTo(UPDATED_EMPLOYEE_CODE);
        assertThat(testEmployeeFuncitonSystem.getFuncitionCode()).isEqualTo(UPDATED_FUNCITION_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingEmployeeFuncitonSystem() throws Exception {
        int databaseSizeBeforeUpdate = employeeFuncitonSystemRepository.findAll().size();

        // Create the EmployeeFuncitonSystem
        EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO = employeeFuncitonSystemMapper.toDto(employeeFuncitonSystem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restEmployeeFuncitonSystemMockMvc.perform(put("/api/employee-funciton-systems")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeFuncitonSystemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmployeeFuncitonSystem in the database
        List<EmployeeFuncitonSystem> employeeFuncitonSystemList = employeeFuncitonSystemRepository.findAll();
        assertThat(employeeFuncitonSystemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEmployeeFuncitonSystem() throws Exception {
        // Initialize the database
        employeeFuncitonSystemRepository.saveAndFlush(employeeFuncitonSystem);

        int databaseSizeBeforeDelete = employeeFuncitonSystemRepository.findAll().size();

        // Get the employeeFuncitonSystem
        restEmployeeFuncitonSystemMockMvc.perform(delete("/api/employee-funciton-systems/{id}", employeeFuncitonSystem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EmployeeFuncitonSystem> employeeFuncitonSystemList = employeeFuncitonSystemRepository.findAll();
        assertThat(employeeFuncitonSystemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmployeeFuncitonSystem.class);
        EmployeeFuncitonSystem employeeFuncitonSystem1 = new EmployeeFuncitonSystem();
        employeeFuncitonSystem1.setId(1L);
        EmployeeFuncitonSystem employeeFuncitonSystem2 = new EmployeeFuncitonSystem();
        employeeFuncitonSystem2.setId(employeeFuncitonSystem1.getId());
        assertThat(employeeFuncitonSystem1).isEqualTo(employeeFuncitonSystem2);
        employeeFuncitonSystem2.setId(2L);
        assertThat(employeeFuncitonSystem1).isNotEqualTo(employeeFuncitonSystem2);
        employeeFuncitonSystem1.setId(null);
        assertThat(employeeFuncitonSystem1).isNotEqualTo(employeeFuncitonSystem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmployeeFuncitonSystemDTO.class);
        EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO1 = new EmployeeFuncitonSystemDTO();
        employeeFuncitonSystemDTO1.setId(1L);
        EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO2 = new EmployeeFuncitonSystemDTO();
        assertThat(employeeFuncitonSystemDTO1).isNotEqualTo(employeeFuncitonSystemDTO2);
        employeeFuncitonSystemDTO2.setId(employeeFuncitonSystemDTO1.getId());
        assertThat(employeeFuncitonSystemDTO1).isEqualTo(employeeFuncitonSystemDTO2);
        employeeFuncitonSystemDTO2.setId(2L);
        assertThat(employeeFuncitonSystemDTO1).isNotEqualTo(employeeFuncitonSystemDTO2);
        employeeFuncitonSystemDTO1.setId(null);
        assertThat(employeeFuncitonSystemDTO1).isNotEqualTo(employeeFuncitonSystemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(employeeFuncitonSystemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(employeeFuncitonSystemMapper.fromId(null)).isNull();
    }
}
