package vn.com.thesis.uaa.repository;

import vn.com.thesis.uaa.domain.FuncitonSystem;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FuncitonSystem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FuncitonSystemRepository extends JpaRepository<FuncitonSystem, Long> {
	List<FuncitonSystem> findByParentId(Long parentId);
}
