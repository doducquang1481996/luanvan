package vn.com.thesis.uaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.uaa.service.FuncitonSystemService;
import vn.com.thesis.uaa.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.uaa.web.rest.util.HeaderUtil;
import vn.com.thesis.uaa.web.rest.util.PaginationUtil;
import vn.com.thesis.uaa.service.dto.FuncitonSystemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing FuncitonSystem.
 */
@RestController
@RequestMapping("/api")
public class FuncitonSystemResource {

    private final Logger log = LoggerFactory.getLogger(FuncitonSystemResource.class);

    private static final String ENTITY_NAME = "funcitonSystem";

    private final FuncitonSystemService funcitonSystemService;

    public FuncitonSystemResource(FuncitonSystemService funcitonSystemService) {
        this.funcitonSystemService = funcitonSystemService;
    }

    /**
     * POST  /funciton-systems : Create a new funcitonSystem.
     *
     * @param funcitonSystemDTO the funcitonSystemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new funcitonSystemDTO, or with status 400 (Bad Request) if the funcitonSystem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/funciton-systems")
    @Timed
    public ResponseEntity<FuncitonSystemDTO> createFuncitonSystem(@RequestBody FuncitonSystemDTO funcitonSystemDTO) throws URISyntaxException {
        log.debug("REST request to save FuncitonSystem : {}", funcitonSystemDTO);
        if (funcitonSystemDTO.getId() != null) {
            throw new BadRequestAlertException("A new funcitonSystem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FuncitonSystemDTO result = funcitonSystemService.save(funcitonSystemDTO);
        return ResponseEntity.created(new URI("/api/funciton-systems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /funciton-systems : Updates an existing funcitonSystem.
     *
     * @param funcitonSystemDTO the funcitonSystemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated funcitonSystemDTO,
     * or with status 400 (Bad Request) if the funcitonSystemDTO is not valid,
     * or with status 500 (Internal Server Error) if the funcitonSystemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/funciton-systems")
    @Timed
    public ResponseEntity<FuncitonSystemDTO> updateFuncitonSystem(@RequestBody FuncitonSystemDTO funcitonSystemDTO) throws URISyntaxException {
        log.debug("REST request to update FuncitonSystem : {}", funcitonSystemDTO);
        if (funcitonSystemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FuncitonSystemDTO result = funcitonSystemService.save(funcitonSystemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, funcitonSystemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /funciton-systems : get all the funcitonSystems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of funcitonSystems in body
     */
    @GetMapping("/funciton-systems")
    @Timed
    public ResponseEntity<List<FuncitonSystemDTO>> getAllFuncitonSystems(Pageable pageable) {
        log.debug("REST request to get a page of FuncitonSystems");
        Page<FuncitonSystemDTO> page = funcitonSystemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/funciton-systems");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /funciton-systems/:id : get the "id" funcitonSystem.
     *
     * @param id the id of the funcitonSystemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the funcitonSystemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/funciton-systems/{id}")
    @Timed
    public ResponseEntity<FuncitonSystemDTO> getFuncitonSystem(@PathVariable Long id) {
        log.debug("REST request to get FuncitonSystem : {}", id);
        Optional<FuncitonSystemDTO> funcitonSystemDTO = funcitonSystemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(funcitonSystemDTO);
    }

    /**
     * DELETE  /funciton-systems/:id : delete the "id" funcitonSystem.
     *
     * @param id the id of the funcitonSystemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/funciton-systems/{id}")
    @Timed
    public ResponseEntity<Void> deleteFuncitonSystem(@PathVariable Long id) {
        log.debug("REST request to delete FuncitonSystem : {}", id);
        funcitonSystemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
