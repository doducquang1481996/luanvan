package vn.com.thesis.uaa.service;

import vn.com.thesis.uaa.service.dto.EmployeeFuncitonSystemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing EmployeeFuncitonSystem.
 */
public interface EmployeeFuncitonSystemService {

    /**
     * Save a employeeFuncitonSystem.
     *
     * @param employeeFuncitonSystemDTO the entity to save
     * @return the persisted entity
     */
    EmployeeFuncitonSystemDTO save(EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO);

    /**
     * Get all the employeeFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EmployeeFuncitonSystemDTO> findAll(Pageable pageable);


    /**
     * Get the "id" employeeFuncitonSystem.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<EmployeeFuncitonSystemDTO> findOne(Long id);

    /**
     * Delete the "id" employeeFuncitonSystem.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
