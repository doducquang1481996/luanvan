package vn.com.thesis.uaa.repository;

import vn.com.thesis.uaa.domain.EmployeeFuncitonSystem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EmployeeFuncitonSystem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmployeeFuncitonSystemRepository extends JpaRepository<EmployeeFuncitonSystem, Long> {

}
