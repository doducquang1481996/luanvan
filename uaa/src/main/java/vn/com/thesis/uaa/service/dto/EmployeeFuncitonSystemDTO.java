package vn.com.thesis.uaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the EmployeeFuncitonSystem entity.
 */
public class EmployeeFuncitonSystemDTO implements Serializable {

    private Long id;

    private String employeeCode;

    private String funcitionCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getFuncitionCode() {
        return funcitionCode;
    }

    public void setFuncitionCode(String funcitionCode) {
        this.funcitionCode = funcitionCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO = (EmployeeFuncitonSystemDTO) o;
        if (employeeFuncitonSystemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), employeeFuncitonSystemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EmployeeFuncitonSystemDTO{" +
            "id=" + getId() +
            ", employeeCode='" + getEmployeeCode() + "'" +
            ", funcitionCode='" + getFuncitionCode() + "'" +
            "}";
    }
}
