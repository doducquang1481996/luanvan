package vn.com.thesis.uaa.service.mapper;

import vn.com.thesis.uaa.domain.*;
import vn.com.thesis.uaa.service.dto.FuncitonSystemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity FuncitonSystem and its DTO FuncitonSystemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FuncitonSystemMapper extends EntityMapper<FuncitonSystemDTO, FuncitonSystem> {



    default FuncitonSystem fromId(Long id) {
        if (id == null) {
            return null;
        }
        FuncitonSystem funcitonSystem = new FuncitonSystem();
        funcitonSystem.setId(id);
        return funcitonSystem;
    }
}
