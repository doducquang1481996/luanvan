package vn.com.thesis.uaa.service.mapper;

import vn.com.thesis.uaa.domain.*;
import vn.com.thesis.uaa.service.dto.RoomFuncitonSystemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RoomFuncitonSystem and its DTO RoomFuncitonSystemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RoomFuncitonSystemMapper extends EntityMapper<RoomFuncitonSystemDTO, RoomFuncitonSystem> {



    default RoomFuncitonSystem fromId(Long id) {
        if (id == null) {
            return null;
        }
        RoomFuncitonSystem roomFuncitonSystem = new RoomFuncitonSystem();
        roomFuncitonSystem.setId(id);
        return roomFuncitonSystem;
    }
}
