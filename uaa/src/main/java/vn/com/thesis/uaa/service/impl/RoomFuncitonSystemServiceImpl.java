package vn.com.thesis.uaa.service.impl;

import vn.com.thesis.uaa.service.RoomFuncitonSystemService;
import vn.com.thesis.uaa.domain.RoomFuncitonSystem;
import vn.com.thesis.uaa.repository.RoomFuncitonSystemRepository;
import vn.com.thesis.uaa.service.dto.RoomFuncitonSystemDTO;
import vn.com.thesis.uaa.service.mapper.RoomFuncitonSystemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing RoomFuncitonSystem.
 */
@Service
@Transactional
public class RoomFuncitonSystemServiceImpl implements RoomFuncitonSystemService {

    private final Logger log = LoggerFactory.getLogger(RoomFuncitonSystemServiceImpl.class);

    private final RoomFuncitonSystemRepository roomFuncitonSystemRepository;

    private final RoomFuncitonSystemMapper roomFuncitonSystemMapper;

    public RoomFuncitonSystemServiceImpl(RoomFuncitonSystemRepository roomFuncitonSystemRepository, RoomFuncitonSystemMapper roomFuncitonSystemMapper) {
        this.roomFuncitonSystemRepository = roomFuncitonSystemRepository;
        this.roomFuncitonSystemMapper = roomFuncitonSystemMapper;
    }

    /**
     * Save a roomFuncitonSystem.
     *
     * @param roomFuncitonSystemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RoomFuncitonSystemDTO save(RoomFuncitonSystemDTO roomFuncitonSystemDTO) {
        log.debug("Request to save RoomFuncitonSystem : {}", roomFuncitonSystemDTO);
        RoomFuncitonSystem roomFuncitonSystem = roomFuncitonSystemMapper.toEntity(roomFuncitonSystemDTO);
        roomFuncitonSystem = roomFuncitonSystemRepository.save(roomFuncitonSystem);
        return roomFuncitonSystemMapper.toDto(roomFuncitonSystem);
    }

    /**
     * Get all the roomFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RoomFuncitonSystemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RoomFuncitonSystems");
        return roomFuncitonSystemRepository.findAll(pageable)
            .map(roomFuncitonSystemMapper::toDto);
    }


    /**
     * Get one roomFuncitonSystem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RoomFuncitonSystemDTO> findOne(Long id) {
        log.debug("Request to get RoomFuncitonSystem : {}", id);
        return roomFuncitonSystemRepository.findById(id)
            .map(roomFuncitonSystemMapper::toDto);
    }

    /**
     * Delete the roomFuncitonSystem by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RoomFuncitonSystem : {}", id);
        roomFuncitonSystemRepository.deleteById(id);
    }
}
