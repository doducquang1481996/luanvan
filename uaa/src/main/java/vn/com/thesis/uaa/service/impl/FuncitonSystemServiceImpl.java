package vn.com.thesis.uaa.service.impl;

import vn.com.thesis.uaa.service.FuncitonSystemService;
import vn.com.thesis.uaa.domain.FuncitonSystem;
import vn.com.thesis.uaa.repository.FuncitonSystemRepository;
import vn.com.thesis.uaa.service.dto.FuncitonSystemDTO;
import vn.com.thesis.uaa.service.mapper.FuncitonSystemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing FuncitonSystem.
 */
@Service
@Transactional
public class FuncitonSystemServiceImpl implements FuncitonSystemService {

    private final Logger log = LoggerFactory.getLogger(FuncitonSystemServiceImpl.class);

    private final FuncitonSystemRepository funcitonSystemRepository;

    private final FuncitonSystemMapper funcitonSystemMapper;

    public FuncitonSystemServiceImpl(FuncitonSystemRepository funcitonSystemRepository, FuncitonSystemMapper funcitonSystemMapper) {
        this.funcitonSystemRepository = funcitonSystemRepository;
        this.funcitonSystemMapper = funcitonSystemMapper;
    }

    /**
     * Save a funcitonSystem.
     *
     * @param funcitonSystemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public FuncitonSystemDTO save(FuncitonSystemDTO funcitonSystemDTO) {
        log.debug("Request to save FuncitonSystem : {}", funcitonSystemDTO);
        FuncitonSystem funcitonSystem = funcitonSystemMapper.toEntity(funcitonSystemDTO);
        funcitonSystem = funcitonSystemRepository.save(funcitonSystem);
        return funcitonSystemMapper.toDto(funcitonSystem);
    }

    /**
     * Get all the funcitonSystems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FuncitonSystemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FuncitonSystems");
        return funcitonSystemRepository.findAll(pageable)
            .map(funcitonSystemMapper::toDto);
    }


    /**
     * Get one funcitonSystem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FuncitonSystemDTO> findOne(Long id) {
        log.debug("Request to get FuncitonSystem : {}", id);
        return funcitonSystemRepository.findById(id)
            .map(funcitonSystemMapper::toDto);
    }

    /**
     * Delete the funcitonSystem by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FuncitonSystem : {}", id);
        funcitonSystemRepository.deleteById(id);
    }
}
