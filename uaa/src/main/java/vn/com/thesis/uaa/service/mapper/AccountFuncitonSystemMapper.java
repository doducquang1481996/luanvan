package vn.com.thesis.uaa.service.mapper;

import vn.com.thesis.uaa.domain.*;
import vn.com.thesis.uaa.service.dto.AccountFuncitonSystemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AccountFuncitonSystem and its DTO AccountFuncitonSystemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AccountFuncitonSystemMapper extends EntityMapper<AccountFuncitonSystemDTO, AccountFuncitonSystem> {



    default AccountFuncitonSystem fromId(Long id) {
        if (id == null) {
            return null;
        }
        AccountFuncitonSystem accountFuncitonSystem = new AccountFuncitonSystem();
        accountFuncitonSystem.setId(id);
        return accountFuncitonSystem;
    }
}
