package vn.com.thesis.uaa.service;

import vn.com.thesis.uaa.service.dto.FuncitonSystemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing FuncitonSystem.
 */
public interface FuncitonSystemService {

    /**
     * Save a funcitonSystem.
     *
     * @param funcitonSystemDTO the entity to save
     * @return the persisted entity
     */
    FuncitonSystemDTO save(FuncitonSystemDTO funcitonSystemDTO);

    /**
     * Get all the funcitonSystems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<FuncitonSystemDTO> findAll(Pageable pageable);


    /**
     * Get the "id" funcitonSystem.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<FuncitonSystemDTO> findOne(Long id);

    /**
     * Delete the "id" funcitonSystem.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
