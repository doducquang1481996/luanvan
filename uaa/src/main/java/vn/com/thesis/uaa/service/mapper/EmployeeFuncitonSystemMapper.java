package vn.com.thesis.uaa.service.mapper;

import vn.com.thesis.uaa.domain.*;
import vn.com.thesis.uaa.service.dto.EmployeeFuncitonSystemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EmployeeFuncitonSystem and its DTO EmployeeFuncitonSystemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EmployeeFuncitonSystemMapper extends EntityMapper<EmployeeFuncitonSystemDTO, EmployeeFuncitonSystem> {



    default EmployeeFuncitonSystem fromId(Long id) {
        if (id == null) {
            return null;
        }
        EmployeeFuncitonSystem employeeFuncitonSystem = new EmployeeFuncitonSystem();
        employeeFuncitonSystem.setId(id);
        return employeeFuncitonSystem;
    }
}
