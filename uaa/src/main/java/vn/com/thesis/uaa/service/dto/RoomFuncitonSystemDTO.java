package vn.com.thesis.uaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RoomFuncitonSystem entity.
 */
public class RoomFuncitonSystemDTO implements Serializable {

    private Long id;

    private String roomCode;

    private String funcitionCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getFuncitionCode() {
        return funcitionCode;
    }

    public void setFuncitionCode(String funcitionCode) {
        this.funcitionCode = funcitionCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RoomFuncitonSystemDTO roomFuncitonSystemDTO = (RoomFuncitonSystemDTO) o;
        if (roomFuncitonSystemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roomFuncitonSystemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoomFuncitonSystemDTO{" +
            "id=" + getId() +
            ", roomCode='" + getRoomCode() + "'" +
            ", funcitionCode='" + getFuncitionCode() + "'" +
            "}";
    }
}
