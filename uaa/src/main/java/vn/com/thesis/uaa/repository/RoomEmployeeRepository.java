package vn.com.thesis.uaa.repository;

import vn.com.thesis.uaa.domain.RoomEmployee;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RoomEmployee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoomEmployeeRepository extends JpaRepository<RoomEmployee, Long> {

}
