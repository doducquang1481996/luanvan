package vn.com.thesis.uaa.service;

import vn.com.thesis.uaa.service.dto.RoomEmployeeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RoomEmployee.
 */
public interface RoomEmployeeService {

    /**
     * Save a roomEmployee.
     *
     * @param roomEmployeeDTO the entity to save
     * @return the persisted entity
     */
    RoomEmployeeDTO save(RoomEmployeeDTO roomEmployeeDTO);

    /**
     * Get all the roomEmployees.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RoomEmployeeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" roomEmployee.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RoomEmployeeDTO> findOne(Long id);

    /**
     * Delete the "id" roomEmployee.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
