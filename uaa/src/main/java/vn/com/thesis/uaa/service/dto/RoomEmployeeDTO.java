package vn.com.thesis.uaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RoomEmployee entity.
 */
public class RoomEmployeeDTO implements Serializable {

    private Long id;

    private String roomCode;

    private String employeeCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RoomEmployeeDTO roomEmployeeDTO = (RoomEmployeeDTO) o;
        if (roomEmployeeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roomEmployeeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoomEmployeeDTO{" +
            "id=" + getId() +
            ", roomCode='" + getRoomCode() + "'" +
            ", employeeCode='" + getEmployeeCode() + "'" +
            "}";
    }
}
