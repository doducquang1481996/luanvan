package vn.com.thesis.uaa.service.impl;

import vn.com.thesis.uaa.service.RoomEmployeeService;
import vn.com.thesis.uaa.domain.RoomEmployee;
import vn.com.thesis.uaa.repository.RoomEmployeeRepository;
import vn.com.thesis.uaa.service.dto.RoomEmployeeDTO;
import vn.com.thesis.uaa.service.mapper.RoomEmployeeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing RoomEmployee.
 */
@Service
@Transactional
public class RoomEmployeeServiceImpl implements RoomEmployeeService {

    private final Logger log = LoggerFactory.getLogger(RoomEmployeeServiceImpl.class);

    private final RoomEmployeeRepository roomEmployeeRepository;

    private final RoomEmployeeMapper roomEmployeeMapper;

    public RoomEmployeeServiceImpl(RoomEmployeeRepository roomEmployeeRepository, RoomEmployeeMapper roomEmployeeMapper) {
        this.roomEmployeeRepository = roomEmployeeRepository;
        this.roomEmployeeMapper = roomEmployeeMapper;
    }

    /**
     * Save a roomEmployee.
     *
     * @param roomEmployeeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RoomEmployeeDTO save(RoomEmployeeDTO roomEmployeeDTO) {
        log.debug("Request to save RoomEmployee : {}", roomEmployeeDTO);
        RoomEmployee roomEmployee = roomEmployeeMapper.toEntity(roomEmployeeDTO);
        roomEmployee = roomEmployeeRepository.save(roomEmployee);
        return roomEmployeeMapper.toDto(roomEmployee);
    }

    /**
     * Get all the roomEmployees.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RoomEmployeeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RoomEmployees");
        return roomEmployeeRepository.findAll(pageable)
            .map(roomEmployeeMapper::toDto);
    }


    /**
     * Get one roomEmployee by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RoomEmployeeDTO> findOne(Long id) {
        log.debug("Request to get RoomEmployee : {}", id);
        return roomEmployeeRepository.findById(id)
            .map(roomEmployeeMapper::toDto);
    }

    /**
     * Delete the roomEmployee by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RoomEmployee : {}", id);
        roomEmployeeRepository.deleteById(id);
    }
}
