package vn.com.thesis.uaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.uaa.service.EmployeeFuncitonSystemService;
import vn.com.thesis.uaa.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.uaa.web.rest.util.HeaderUtil;
import vn.com.thesis.uaa.web.rest.util.PaginationUtil;
import vn.com.thesis.uaa.service.dto.EmployeeFuncitonSystemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EmployeeFuncitonSystem.
 */
@RestController
@RequestMapping("/api")
public class EmployeeFuncitonSystemResource {

    private final Logger log = LoggerFactory.getLogger(EmployeeFuncitonSystemResource.class);

    private static final String ENTITY_NAME = "employeeFuncitonSystem";

    private final EmployeeFuncitonSystemService employeeFuncitonSystemService;

    public EmployeeFuncitonSystemResource(EmployeeFuncitonSystemService employeeFuncitonSystemService) {
        this.employeeFuncitonSystemService = employeeFuncitonSystemService;
    }

    /**
     * POST  /employee-funciton-systems : Create a new employeeFuncitonSystem.
     *
     * @param employeeFuncitonSystemDTO the employeeFuncitonSystemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new employeeFuncitonSystemDTO, or with status 400 (Bad Request) if the employeeFuncitonSystem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/employee-funciton-systems")
    @Timed
    public ResponseEntity<EmployeeFuncitonSystemDTO> createEmployeeFuncitonSystem(@RequestBody EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO) throws URISyntaxException {
        log.debug("REST request to save EmployeeFuncitonSystem : {}", employeeFuncitonSystemDTO);
        if (employeeFuncitonSystemDTO.getId() != null) {
            throw new BadRequestAlertException("A new employeeFuncitonSystem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmployeeFuncitonSystemDTO result = employeeFuncitonSystemService.save(employeeFuncitonSystemDTO);
        return ResponseEntity.created(new URI("/api/employee-funciton-systems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /employee-funciton-systems : Updates an existing employeeFuncitonSystem.
     *
     * @param employeeFuncitonSystemDTO the employeeFuncitonSystemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated employeeFuncitonSystemDTO,
     * or with status 400 (Bad Request) if the employeeFuncitonSystemDTO is not valid,
     * or with status 500 (Internal Server Error) if the employeeFuncitonSystemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/employee-funciton-systems")
    @Timed
    public ResponseEntity<EmployeeFuncitonSystemDTO> updateEmployeeFuncitonSystem(@RequestBody EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO) throws URISyntaxException {
        log.debug("REST request to update EmployeeFuncitonSystem : {}", employeeFuncitonSystemDTO);
        if (employeeFuncitonSystemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EmployeeFuncitonSystemDTO result = employeeFuncitonSystemService.save(employeeFuncitonSystemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, employeeFuncitonSystemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /employee-funciton-systems : get all the employeeFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of employeeFuncitonSystems in body
     */
    @GetMapping("/employee-funciton-systems")
    @Timed
    public ResponseEntity<List<EmployeeFuncitonSystemDTO>> getAllEmployeeFuncitonSystems(Pageable pageable) {
        log.debug("REST request to get a page of EmployeeFuncitonSystems");
        Page<EmployeeFuncitonSystemDTO> page = employeeFuncitonSystemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/employee-funciton-systems");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /employee-funciton-systems/:id : get the "id" employeeFuncitonSystem.
     *
     * @param id the id of the employeeFuncitonSystemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the employeeFuncitonSystemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/employee-funciton-systems/{id}")
    @Timed
    public ResponseEntity<EmployeeFuncitonSystemDTO> getEmployeeFuncitonSystem(@PathVariable Long id) {
        log.debug("REST request to get EmployeeFuncitonSystem : {}", id);
        Optional<EmployeeFuncitonSystemDTO> employeeFuncitonSystemDTO = employeeFuncitonSystemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(employeeFuncitonSystemDTO);
    }

    /**
     * DELETE  /employee-funciton-systems/:id : delete the "id" employeeFuncitonSystem.
     *
     * @param id the id of the employeeFuncitonSystemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/employee-funciton-systems/{id}")
    @Timed
    public ResponseEntity<Void> deleteEmployeeFuncitonSystem(@PathVariable Long id) {
        log.debug("REST request to delete EmployeeFuncitonSystem : {}", id);
        employeeFuncitonSystemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
