package vn.com.thesis.uaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.uaa.service.AccountFuncitonSystemService;
import vn.com.thesis.uaa.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.uaa.web.rest.util.HeaderUtil;
import vn.com.thesis.uaa.web.rest.util.PaginationUtil;
import vn.com.thesis.uaa.service.dto.AccountFuncitonSystemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AccountFuncitonSystem.
 */
@RestController
@RequestMapping("/api")
public class AccountFuncitonSystemResource {

    private final Logger log = LoggerFactory.getLogger(AccountFuncitonSystemResource.class);

    private static final String ENTITY_NAME = "accountFuncitonSystem";

    private final AccountFuncitonSystemService accountFuncitonSystemService;

    public AccountFuncitonSystemResource(AccountFuncitonSystemService accountFuncitonSystemService) {
        this.accountFuncitonSystemService = accountFuncitonSystemService;
    }

    /**
     * POST  /account-funciton-systems : Create a new accountFuncitonSystem.
     *
     * @param accountFuncitonSystemDTO the accountFuncitonSystemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accountFuncitonSystemDTO, or with status 400 (Bad Request) if the accountFuncitonSystem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/account-funciton-systems")
    @Timed
    public ResponseEntity<AccountFuncitonSystemDTO> createAccountFuncitonSystem(@RequestBody AccountFuncitonSystemDTO accountFuncitonSystemDTO) throws URISyntaxException {
        log.debug("REST request to save AccountFuncitonSystem : {}", accountFuncitonSystemDTO);
        if (accountFuncitonSystemDTO.getId() != null) {
            throw new BadRequestAlertException("A new accountFuncitonSystem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AccountFuncitonSystemDTO result = accountFuncitonSystemService.save(accountFuncitonSystemDTO);
        return ResponseEntity.created(new URI("/api/account-funciton-systems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /account-funciton-systems : Updates an existing accountFuncitonSystem.
     *
     * @param accountFuncitonSystemDTO the accountFuncitonSystemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accountFuncitonSystemDTO,
     * or with status 400 (Bad Request) if the accountFuncitonSystemDTO is not valid,
     * or with status 500 (Internal Server Error) if the accountFuncitonSystemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/account-funciton-systems")
    @Timed
    public ResponseEntity<AccountFuncitonSystemDTO> updateAccountFuncitonSystem(@RequestBody AccountFuncitonSystemDTO accountFuncitonSystemDTO) throws URISyntaxException {
        log.debug("REST request to update AccountFuncitonSystem : {}", accountFuncitonSystemDTO);
        if (accountFuncitonSystemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AccountFuncitonSystemDTO result = accountFuncitonSystemService.save(accountFuncitonSystemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, accountFuncitonSystemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /account-funciton-systems : get all the accountFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of accountFuncitonSystems in body
     */
    @GetMapping("/account-funciton-systems")
    @Timed
    public ResponseEntity<List<AccountFuncitonSystemDTO>> getAllAccountFuncitonSystems(Pageable pageable) {
        log.debug("REST request to get a page of AccountFuncitonSystems");
        Page<AccountFuncitonSystemDTO> page = accountFuncitonSystemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/account-funciton-systems");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /account-funciton-systems/:id : get the "id" accountFuncitonSystem.
     *
     * @param id the id of the accountFuncitonSystemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accountFuncitonSystemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/account-funciton-systems/{id}")
    @Timed
    public ResponseEntity<AccountFuncitonSystemDTO> getAccountFuncitonSystem(@PathVariable Long id) {
        log.debug("REST request to get AccountFuncitonSystem : {}", id);
        Optional<AccountFuncitonSystemDTO> accountFuncitonSystemDTO = accountFuncitonSystemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(accountFuncitonSystemDTO);
    }

    /**
     * DELETE  /account-funciton-systems/:id : delete the "id" accountFuncitonSystem.
     *
     * @param id the id of the accountFuncitonSystemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/account-funciton-systems/{id}")
    @Timed
    public ResponseEntity<Void> deleteAccountFuncitonSystem(@PathVariable Long id) {
        log.debug("REST request to delete AccountFuncitonSystem : {}", id);
        accountFuncitonSystemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
