package vn.com.thesis.uaa.repository;

import vn.com.thesis.uaa.domain.RoomFuncitonSystem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RoomFuncitonSystem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoomFuncitonSystemRepository extends JpaRepository<RoomFuncitonSystem, Long> {

}
