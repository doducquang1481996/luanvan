package vn.com.thesis.uaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.uaa.service.RoomFuncitonSystemService;
import vn.com.thesis.uaa.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.uaa.web.rest.util.HeaderUtil;
import vn.com.thesis.uaa.web.rest.util.PaginationUtil;
import vn.com.thesis.uaa.service.dto.RoomFuncitonSystemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RoomFuncitonSystem.
 */
@RestController
@RequestMapping("/api")
public class RoomFuncitonSystemResource {

    private final Logger log = LoggerFactory.getLogger(RoomFuncitonSystemResource.class);

    private static final String ENTITY_NAME = "roomFuncitonSystem";

    private final RoomFuncitonSystemService roomFuncitonSystemService;

    public RoomFuncitonSystemResource(RoomFuncitonSystemService roomFuncitonSystemService) {
        this.roomFuncitonSystemService = roomFuncitonSystemService;
    }

    /**
     * POST  /room-funciton-systems : Create a new roomFuncitonSystem.
     *
     * @param roomFuncitonSystemDTO the roomFuncitonSystemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new roomFuncitonSystemDTO, or with status 400 (Bad Request) if the roomFuncitonSystem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/room-funciton-systems")
    @Timed
    public ResponseEntity<RoomFuncitonSystemDTO> createRoomFuncitonSystem(@RequestBody RoomFuncitonSystemDTO roomFuncitonSystemDTO) throws URISyntaxException {
        log.debug("REST request to save RoomFuncitonSystem : {}", roomFuncitonSystemDTO);
        if (roomFuncitonSystemDTO.getId() != null) {
            throw new BadRequestAlertException("A new roomFuncitonSystem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RoomFuncitonSystemDTO result = roomFuncitonSystemService.save(roomFuncitonSystemDTO);
        return ResponseEntity.created(new URI("/api/room-funciton-systems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /room-funciton-systems : Updates an existing roomFuncitonSystem.
     *
     * @param roomFuncitonSystemDTO the roomFuncitonSystemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated roomFuncitonSystemDTO,
     * or with status 400 (Bad Request) if the roomFuncitonSystemDTO is not valid,
     * or with status 500 (Internal Server Error) if the roomFuncitonSystemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/room-funciton-systems")
    @Timed
    public ResponseEntity<RoomFuncitonSystemDTO> updateRoomFuncitonSystem(@RequestBody RoomFuncitonSystemDTO roomFuncitonSystemDTO) throws URISyntaxException {
        log.debug("REST request to update RoomFuncitonSystem : {}", roomFuncitonSystemDTO);
        if (roomFuncitonSystemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RoomFuncitonSystemDTO result = roomFuncitonSystemService.save(roomFuncitonSystemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, roomFuncitonSystemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /room-funciton-systems : get all the roomFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of roomFuncitonSystems in body
     */
    @GetMapping("/room-funciton-systems")
    @Timed
    public ResponseEntity<List<RoomFuncitonSystemDTO>> getAllRoomFuncitonSystems(Pageable pageable) {
        log.debug("REST request to get a page of RoomFuncitonSystems");
        Page<RoomFuncitonSystemDTO> page = roomFuncitonSystemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/room-funciton-systems");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /room-funciton-systems/:id : get the "id" roomFuncitonSystem.
     *
     * @param id the id of the roomFuncitonSystemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the roomFuncitonSystemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/room-funciton-systems/{id}")
    @Timed
    public ResponseEntity<RoomFuncitonSystemDTO> getRoomFuncitonSystem(@PathVariable Long id) {
        log.debug("REST request to get RoomFuncitonSystem : {}", id);
        Optional<RoomFuncitonSystemDTO> roomFuncitonSystemDTO = roomFuncitonSystemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(roomFuncitonSystemDTO);
    }

    /**
     * DELETE  /room-funciton-systems/:id : delete the "id" roomFuncitonSystem.
     *
     * @param id the id of the roomFuncitonSystemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/room-funciton-systems/{id}")
    @Timed
    public ResponseEntity<Void> deleteRoomFuncitonSystem(@PathVariable Long id) {
        log.debug("REST request to delete RoomFuncitonSystem : {}", id);
        roomFuncitonSystemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
