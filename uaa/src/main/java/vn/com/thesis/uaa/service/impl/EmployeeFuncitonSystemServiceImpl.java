package vn.com.thesis.uaa.service.impl;

import vn.com.thesis.uaa.service.EmployeeFuncitonSystemService;
import vn.com.thesis.uaa.domain.EmployeeFuncitonSystem;
import vn.com.thesis.uaa.repository.EmployeeFuncitonSystemRepository;
import vn.com.thesis.uaa.service.dto.EmployeeFuncitonSystemDTO;
import vn.com.thesis.uaa.service.mapper.EmployeeFuncitonSystemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing EmployeeFuncitonSystem.
 */
@Service
@Transactional
public class EmployeeFuncitonSystemServiceImpl implements EmployeeFuncitonSystemService {

    private final Logger log = LoggerFactory.getLogger(EmployeeFuncitonSystemServiceImpl.class);

    private final EmployeeFuncitonSystemRepository employeeFuncitonSystemRepository;

    private final EmployeeFuncitonSystemMapper employeeFuncitonSystemMapper;

    public EmployeeFuncitonSystemServiceImpl(EmployeeFuncitonSystemRepository employeeFuncitonSystemRepository, EmployeeFuncitonSystemMapper employeeFuncitonSystemMapper) {
        this.employeeFuncitonSystemRepository = employeeFuncitonSystemRepository;
        this.employeeFuncitonSystemMapper = employeeFuncitonSystemMapper;
    }

    /**
     * Save a employeeFuncitonSystem.
     *
     * @param employeeFuncitonSystemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EmployeeFuncitonSystemDTO save(EmployeeFuncitonSystemDTO employeeFuncitonSystemDTO) {
        log.debug("Request to save EmployeeFuncitonSystem : {}", employeeFuncitonSystemDTO);
        EmployeeFuncitonSystem employeeFuncitonSystem = employeeFuncitonSystemMapper.toEntity(employeeFuncitonSystemDTO);
        employeeFuncitonSystem = employeeFuncitonSystemRepository.save(employeeFuncitonSystem);
        return employeeFuncitonSystemMapper.toDto(employeeFuncitonSystem);
    }

    /**
     * Get all the employeeFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EmployeeFuncitonSystemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EmployeeFuncitonSystems");
        return employeeFuncitonSystemRepository.findAll(pageable)
            .map(employeeFuncitonSystemMapper::toDto);
    }


    /**
     * Get one employeeFuncitonSystem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EmployeeFuncitonSystemDTO> findOne(Long id) {
        log.debug("Request to get EmployeeFuncitonSystem : {}", id);
        return employeeFuncitonSystemRepository.findById(id)
            .map(employeeFuncitonSystemMapper::toDto);
    }

    /**
     * Delete the employeeFuncitonSystem by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EmployeeFuncitonSystem : {}", id);
        employeeFuncitonSystemRepository.deleteById(id);
    }
}
