package vn.com.thesis.uaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.uaa.service.RoomEmployeeService;
import vn.com.thesis.uaa.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.uaa.web.rest.util.HeaderUtil;
import vn.com.thesis.uaa.web.rest.util.PaginationUtil;
import vn.com.thesis.uaa.service.dto.RoomEmployeeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RoomEmployee.
 */
@RestController
@RequestMapping("/api")
public class RoomEmployeeResource {

    private final Logger log = LoggerFactory.getLogger(RoomEmployeeResource.class);

    private static final String ENTITY_NAME = "roomEmployee";

    private final RoomEmployeeService roomEmployeeService;

    public RoomEmployeeResource(RoomEmployeeService roomEmployeeService) {
        this.roomEmployeeService = roomEmployeeService;
    }

    /**
     * POST  /room-employees : Create a new roomEmployee.
     *
     * @param roomEmployeeDTO the roomEmployeeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new roomEmployeeDTO, or with status 400 (Bad Request) if the roomEmployee has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/room-employees")
    @Timed
    public ResponseEntity<RoomEmployeeDTO> createRoomEmployee(@RequestBody RoomEmployeeDTO roomEmployeeDTO) throws URISyntaxException {
        log.debug("REST request to save RoomEmployee : {}", roomEmployeeDTO);
        if (roomEmployeeDTO.getId() != null) {
            throw new BadRequestAlertException("A new roomEmployee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RoomEmployeeDTO result = roomEmployeeService.save(roomEmployeeDTO);
        return ResponseEntity.created(new URI("/api/room-employees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /room-employees : Updates an existing roomEmployee.
     *
     * @param roomEmployeeDTO the roomEmployeeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated roomEmployeeDTO,
     * or with status 400 (Bad Request) if the roomEmployeeDTO is not valid,
     * or with status 500 (Internal Server Error) if the roomEmployeeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/room-employees")
    @Timed
    public ResponseEntity<RoomEmployeeDTO> updateRoomEmployee(@RequestBody RoomEmployeeDTO roomEmployeeDTO) throws URISyntaxException {
        log.debug("REST request to update RoomEmployee : {}", roomEmployeeDTO);
        if (roomEmployeeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RoomEmployeeDTO result = roomEmployeeService.save(roomEmployeeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, roomEmployeeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /room-employees : get all the roomEmployees.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of roomEmployees in body
     */
    @GetMapping("/room-employees")
    @Timed
    public ResponseEntity<List<RoomEmployeeDTO>> getAllRoomEmployees(Pageable pageable) {
        log.debug("REST request to get a page of RoomEmployees");
        Page<RoomEmployeeDTO> page = roomEmployeeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/room-employees");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /room-employees/:id : get the "id" roomEmployee.
     *
     * @param id the id of the roomEmployeeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the roomEmployeeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/room-employees/{id}")
    @Timed
    public ResponseEntity<RoomEmployeeDTO> getRoomEmployee(@PathVariable Long id) {
        log.debug("REST request to get RoomEmployee : {}", id);
        Optional<RoomEmployeeDTO> roomEmployeeDTO = roomEmployeeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(roomEmployeeDTO);
    }

    /**
     * DELETE  /room-employees/:id : delete the "id" roomEmployee.
     *
     * @param id the id of the roomEmployeeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/room-employees/{id}")
    @Timed
    public ResponseEntity<Void> deleteRoomEmployee(@PathVariable Long id) {
        log.debug("REST request to delete RoomEmployee : {}", id);
        roomEmployeeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
