package vn.com.thesis.uaa.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A AccountFuncitonSystem.
 */
@Entity
@Table(name = "account_funciton_system")
public class AccountFuncitonSystem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "funcition_id")
    private Long funcitionId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public AccountFuncitonSystem accountId(Long accountId) {
        this.accountId = accountId;
        return this;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getFuncitionId() {
        return funcitionId;
    }

    public AccountFuncitonSystem funcitionId(Long funcitionId) {
        this.funcitionId = funcitionId;
        return this;
    }

    public void setFuncitionId(Long funcitionId) {
        this.funcitionId = funcitionId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountFuncitonSystem accountFuncitonSystem = (AccountFuncitonSystem) o;
        if (accountFuncitonSystem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accountFuncitonSystem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccountFuncitonSystem{" +
            "id=" + getId() +
            ", accountId=" + getAccountId() +
            ", funcitionId=" + getFuncitionId() +
            "}";
    }
}
