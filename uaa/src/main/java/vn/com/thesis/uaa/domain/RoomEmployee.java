package vn.com.thesis.uaa.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A RoomEmployee.
 */
@Entity
@Table(name = "room_employee")
public class RoomEmployee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "room_code")
    private String roomCode;

    @Column(name = "employee_code")
    private String employeeCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomCode() {
        return roomCode;
    }

    public RoomEmployee roomCode(String roomCode) {
        this.roomCode = roomCode;
        return this;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public RoomEmployee employeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
        return this;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RoomEmployee roomEmployee = (RoomEmployee) o;
        if (roomEmployee.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roomEmployee.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoomEmployee{" +
            "id=" + getId() +
            ", roomCode='" + getRoomCode() + "'" +
            ", employeeCode='" + getEmployeeCode() + "'" +
            "}";
    }
}
