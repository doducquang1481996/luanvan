package vn.com.thesis.uaa.repository;

import vn.com.thesis.uaa.domain.Employee;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Employee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	List<Employee> findByEmployeeCode(String employeeCode);
	
}	
