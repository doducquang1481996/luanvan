package vn.com.thesis.uaa.service.mapper;

import vn.com.thesis.uaa.domain.*;
import vn.com.thesis.uaa.service.dto.RoomEmployeeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RoomEmployee and its DTO RoomEmployeeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RoomEmployeeMapper extends EntityMapper<RoomEmployeeDTO, RoomEmployee> {



    default RoomEmployee fromId(Long id) {
        if (id == null) {
            return null;
        }
        RoomEmployee roomEmployee = new RoomEmployee();
        roomEmployee.setId(id);
        return roomEmployee;
    }
}
