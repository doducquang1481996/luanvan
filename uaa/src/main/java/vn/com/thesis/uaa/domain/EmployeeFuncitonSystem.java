package vn.com.thesis.uaa.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A EmployeeFuncitonSystem.
 */
@Entity
@Table(name = "employee_funciton_system")
public class EmployeeFuncitonSystem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "employee_code")
    private String employeeCode;

    @Column(name = "funcition_code")
    private String funcitionCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public EmployeeFuncitonSystem employeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
        return this;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getFuncitionCode() {
        return funcitionCode;
    }

    public EmployeeFuncitonSystem funcitionCode(String funcitionCode) {
        this.funcitionCode = funcitionCode;
        return this;
    }

    public void setFuncitionCode(String funcitionCode) {
        this.funcitionCode = funcitionCode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EmployeeFuncitonSystem employeeFuncitonSystem = (EmployeeFuncitonSystem) o;
        if (employeeFuncitonSystem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), employeeFuncitonSystem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EmployeeFuncitonSystem{" +
            "id=" + getId() +
            ", employeeCode='" + getEmployeeCode() + "'" +
            ", funcitionCode='" + getFuncitionCode() + "'" +
            "}";
    }
}
