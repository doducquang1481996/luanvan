package vn.com.thesis.uaa.service.impl;

import vn.com.thesis.uaa.service.AccountFuncitonSystemService;
import vn.com.thesis.uaa.domain.AccountFuncitonSystem;
import vn.com.thesis.uaa.repository.AccountFuncitonSystemRepository;
import vn.com.thesis.uaa.service.dto.AccountFuncitonSystemDTO;
import vn.com.thesis.uaa.service.mapper.AccountFuncitonSystemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing AccountFuncitonSystem.
 */
@Service
@Transactional
public class AccountFuncitonSystemServiceImpl implements AccountFuncitonSystemService {

    private final Logger log = LoggerFactory.getLogger(AccountFuncitonSystemServiceImpl.class);
    
    @Autowired
    private AccountFuncitonSystemRepository accountFuncitonSystemRepository;

    private final AccountFuncitonSystemMapper accountFuncitonSystemMapper;

    public AccountFuncitonSystemServiceImpl(AccountFuncitonSystemRepository accountFuncitonSystemRepository, AccountFuncitonSystemMapper accountFuncitonSystemMapper) {
        this.accountFuncitonSystemRepository = accountFuncitonSystemRepository;
        this.accountFuncitonSystemMapper = accountFuncitonSystemMapper;
    }

    /**
     * Save a accountFuncitonSystem.
     *
     * @param accountFuncitonSystemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AccountFuncitonSystemDTO save(AccountFuncitonSystemDTO accountFuncitonSystemDTO) {
        log.debug("Request to save AccountFuncitonSystem : {}", accountFuncitonSystemDTO);
        AccountFuncitonSystem accountFuncitonSystem = accountFuncitonSystemMapper.toEntity(accountFuncitonSystemDTO);
        accountFuncitonSystem = accountFuncitonSystemRepository.save(accountFuncitonSystem);
        return accountFuncitonSystemMapper.toDto(accountFuncitonSystem);
    }

    /**
     * Get all the accountFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AccountFuncitonSystemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AccountFuncitonSystems");
        return accountFuncitonSystemRepository.findAll(pageable)
            .map(accountFuncitonSystemMapper::toDto);
    }


    /**
     * Get one accountFuncitonSystem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AccountFuncitonSystemDTO> findOne(Long id) {
        log.debug("Request to get AccountFuncitonSystem : {}", id);
        return accountFuncitonSystemRepository.findById(id)
            .map(accountFuncitonSystemMapper::toDto);
    }

    /**
     * Delete the accountFuncitonSystem by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AccountFuncitonSystem : {}", id);
        accountFuncitonSystemRepository.deleteById(id);
    }

	@Override
	public List<AccountFuncitonSystemDTO> findByAccountId(Long accountId) {
		 log.debug("Request to get all AccountFuncitonSystems");
	        return accountFuncitonSystemRepository.findByAccountId(accountId);
	}

}
