import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './room-funciton-system.reducer';
import { IRoomFuncitonSystem } from 'app/shared/model/room-funciton-system.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRoomFuncitonSystemDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class RoomFuncitonSystemDetail extends React.Component<IRoomFuncitonSystemDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { roomFuncitonSystemEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            RoomFuncitonSystem [<b>{roomFuncitonSystemEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="roomId">Room Id</span>
            </dt>
            <dd>{roomFuncitonSystemEntity.roomId}</dd>
            <dt>
              <span id="funcitionId">Funcition Id</span>
            </dt>
            <dd>{roomFuncitonSystemEntity.funcitionId}</dd>
          </dl>
          <Button tag={Link} to="/entity/room-funciton-system" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/room-funciton-system/${roomFuncitonSystemEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ roomFuncitonSystem }: IRootState) => ({
  roomFuncitonSystemEntity: roomFuncitonSystem.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomFuncitonSystemDetail);
