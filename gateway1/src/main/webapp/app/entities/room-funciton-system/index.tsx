import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import RoomFuncitonSystem from './room-funciton-system';
import RoomFuncitonSystemDetail from './room-funciton-system-detail';
import RoomFuncitonSystemUpdate from './room-funciton-system-update';
import RoomFuncitonSystemDeleteDialog from './room-funciton-system-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={RoomFuncitonSystemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={RoomFuncitonSystemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={RoomFuncitonSystemDetail} />
      <ErrorBoundaryRoute path={match.url} component={RoomFuncitonSystem} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={RoomFuncitonSystemDeleteDialog} />
  </>
);

export default Routes;
