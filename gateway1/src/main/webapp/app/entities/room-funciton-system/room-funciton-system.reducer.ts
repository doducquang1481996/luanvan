import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IRoomFuncitonSystem, defaultValue } from 'app/shared/model/room-funciton-system.model';

export const ACTION_TYPES = {
  FETCH_ROOMFUNCITONSYSTEM_LIST: 'roomFuncitonSystem/FETCH_ROOMFUNCITONSYSTEM_LIST',
  FETCH_ROOMFUNCITONSYSTEM: 'roomFuncitonSystem/FETCH_ROOMFUNCITONSYSTEM',
  CREATE_ROOMFUNCITONSYSTEM: 'roomFuncitonSystem/CREATE_ROOMFUNCITONSYSTEM',
  UPDATE_ROOMFUNCITONSYSTEM: 'roomFuncitonSystem/UPDATE_ROOMFUNCITONSYSTEM',
  DELETE_ROOMFUNCITONSYSTEM: 'roomFuncitonSystem/DELETE_ROOMFUNCITONSYSTEM',
  RESET: 'roomFuncitonSystem/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IRoomFuncitonSystem>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type RoomFuncitonSystemState = Readonly<typeof initialState>;

// Reducer

export default (state: RoomFuncitonSystemState = initialState, action): RoomFuncitonSystemState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ROOMFUNCITONSYSTEM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ROOMFUNCITONSYSTEM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ROOMFUNCITONSYSTEM):
    case REQUEST(ACTION_TYPES.UPDATE_ROOMFUNCITONSYSTEM):
    case REQUEST(ACTION_TYPES.DELETE_ROOMFUNCITONSYSTEM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ROOMFUNCITONSYSTEM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ROOMFUNCITONSYSTEM):
    case FAILURE(ACTION_TYPES.CREATE_ROOMFUNCITONSYSTEM):
    case FAILURE(ACTION_TYPES.UPDATE_ROOMFUNCITONSYSTEM):
    case FAILURE(ACTION_TYPES.DELETE_ROOMFUNCITONSYSTEM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ROOMFUNCITONSYSTEM_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_ROOMFUNCITONSYSTEM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ROOMFUNCITONSYSTEM):
    case SUCCESS(ACTION_TYPES.UPDATE_ROOMFUNCITONSYSTEM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ROOMFUNCITONSYSTEM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'uaa/api/room-funciton-systems';

// Actions

export const getEntities: ICrudGetAllAction<IRoomFuncitonSystem> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ROOMFUNCITONSYSTEM_LIST,
    payload: axios.get<IRoomFuncitonSystem>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IRoomFuncitonSystem> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ROOMFUNCITONSYSTEM,
    payload: axios.get<IRoomFuncitonSystem>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IRoomFuncitonSystem> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ROOMFUNCITONSYSTEM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IRoomFuncitonSystem> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ROOMFUNCITONSYSTEM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IRoomFuncitonSystem> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ROOMFUNCITONSYSTEM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
