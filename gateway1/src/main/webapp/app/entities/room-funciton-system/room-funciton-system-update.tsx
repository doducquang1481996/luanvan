import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './room-funciton-system.reducer';
import { IRoomFuncitonSystem } from 'app/shared/model/room-funciton-system.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IRoomFuncitonSystemUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IRoomFuncitonSystemUpdateState {
  isNew: boolean;
}

export class RoomFuncitonSystemUpdate extends React.Component<IRoomFuncitonSystemUpdateProps, IRoomFuncitonSystemUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { roomFuncitonSystemEntity } = this.props;
      const entity = {
        ...roomFuncitonSystemEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/room-funciton-system');
  };

  render() {
    const { roomFuncitonSystemEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.roomFuncitonSystem.home.createOrEditLabel">Create or edit a RoomFuncitonSystem</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : roomFuncitonSystemEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="room-funciton-system-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="roomIdLabel" for="roomId">
                    Room Id
                  </Label>
                  <AvField id="room-funciton-system-roomId" type="number" className="form-control" name="roomId" />
                </AvGroup>
                <AvGroup>
                  <Label id="funcitionIdLabel" for="funcitionId">
                    Funcition Id
                  </Label>
                  <AvField id="room-funciton-system-funcitionId" type="number" className="form-control" name="funcitionId" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/room-funciton-system" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  roomFuncitonSystemEntity: storeState.roomFuncitonSystem.entity,
  loading: storeState.roomFuncitonSystem.loading,
  updating: storeState.roomFuncitonSystem.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomFuncitonSystemUpdate);
