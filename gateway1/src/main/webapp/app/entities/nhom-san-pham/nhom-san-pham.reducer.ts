import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INhomSanPham, defaultValue } from 'app/shared/model/nhom-san-pham.model';

export const ACTION_TYPES = {
  FETCH_NHOMSANPHAM_LIST: 'nhomSanPham/FETCH_NHOMSANPHAM_LIST',
  FETCH_NHOMSANPHAM: 'nhomSanPham/FETCH_NHOMSANPHAM',
  CREATE_NHOMSANPHAM: 'nhomSanPham/CREATE_NHOMSANPHAM',
  UPDATE_NHOMSANPHAM: 'nhomSanPham/UPDATE_NHOMSANPHAM',
  DELETE_NHOMSANPHAM: 'nhomSanPham/DELETE_NHOMSANPHAM',
  RESET: 'nhomSanPham/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INhomSanPham>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type NhomSanPhamState = Readonly<typeof initialState>;

// Reducer

export default (state: NhomSanPhamState = initialState, action): NhomSanPhamState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NHOMSANPHAM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NHOMSANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NHOMSANPHAM):
    case REQUEST(ACTION_TYPES.UPDATE_NHOMSANPHAM):
    case REQUEST(ACTION_TYPES.DELETE_NHOMSANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NHOMSANPHAM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NHOMSANPHAM):
    case FAILURE(ACTION_TYPES.CREATE_NHOMSANPHAM):
    case FAILURE(ACTION_TYPES.UPDATE_NHOMSANPHAM):
    case FAILURE(ACTION_TYPES.DELETE_NHOMSANPHAM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHOMSANPHAM_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHOMSANPHAM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NHOMSANPHAM):
    case SUCCESS(ACTION_TYPES.UPDATE_NHOMSANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NHOMSANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/nhom-san-phams';

// Actions

export const getEntities: ICrudGetAllAction<INhomSanPham> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_NHOMSANPHAM_LIST,
    payload: axios.get<INhomSanPham>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<INhomSanPham> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NHOMSANPHAM,
    payload: axios.get<INhomSanPham>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INhomSanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NHOMSANPHAM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INhomSanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NHOMSANPHAM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INhomSanPham> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NHOMSANPHAM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
