import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NhomSanPham from './nhom-san-pham';
import NhomSanPhamDetail from './nhom-san-pham-detail';
import NhomSanPhamUpdate from './nhom-san-pham-update';
import NhomSanPhamDeleteDialog from './nhom-san-pham-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NhomSanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NhomSanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NhomSanPhamDetail} />
      <ErrorBoundaryRoute path={match.url} component={NhomSanPham} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={NhomSanPhamDeleteDialog} />
  </>
);

export default Routes;
