import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './nhom-san-pham.reducer';
import { INhomSanPham } from 'app/shared/model/nhom-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface INhomSanPhamUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface INhomSanPhamUpdateState {
  isNew: boolean;
}

export class NhomSanPhamUpdate extends React.Component<INhomSanPhamUpdateProps, INhomSanPhamUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { nhomSanPhamEntity } = this.props;
      const entity = {
        ...nhomSanPhamEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/nhom-san-pham');
  };

  render() {
    const { nhomSanPhamEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.nhomSanPham.home.createOrEditLabel">Create or edit a NhomSanPham</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : nhomSanPhamEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="nhom-san-pham-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Tên Nhóm Sản Phẩm
                  </Label>
                  <AvField id="nhom-san-pham-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Mã Nhóm Sản Phẩm
                  </Label>
                  <AvField id="nhom-san-pham-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="moTaLabel" for="moTa">
                    Mô Tả
                  </Label>
                  <AvField id="nhom-san-pham-moTa" type="text" name="moTa" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngày Tạo
                  </Label>
                  <AvField id="nhom-san-pham-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Người Tạo
                  </Label>
                  <AvField id="nhom-san-pham-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngày Cập Nhật
                  </Label>
                  <AvField id="nhom-san-pham-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="nhom-san-pham-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/nhom-san-pham" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Trở Lại</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Lưu
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  nhomSanPhamEntity: storeState.nhomSanPham.entity,
  loading: storeState.nhomSanPham.loading,
  updating: storeState.nhomSanPham.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhomSanPhamUpdate);
