import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './nha-san-xuat.reducer';
import { INhaSanXuat } from 'app/shared/model/nha-san-xuat.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface INhaSanXuatUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface INhaSanXuatUpdateState {
  isNew: boolean;
}

export class NhaSanXuatUpdate extends React.Component<INhaSanXuatUpdateProps, INhaSanXuatUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { nhaSanXuatEntity } = this.props;
      const entity = {
        ...nhaSanXuatEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/nha-san-xuat');
  };

  render() {
    const { nhaSanXuatEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            {!isNew ? (
              <h2 id="gatewayApp.nhaSanXuat.home.createOrEditLabel">Cập Nhật Thông Tin Nhà Sản Xuất</h2>
            ) : (
              <h2 id="gatewayApp.nhaSanXuat.home.createOrEditLabel">Thêm mới Nhà Sản Xuất</h2>
            )}
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : nhaSanXuatEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="nha-san-xuat-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Ten
                  </Label>
                  <AvField id="nha-san-xuat-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Ma
                  </Label>
                  <AvField id="nha-san-xuat-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="diaChiLabel" for="diaChi">
                    Dia Chi
                  </Label>
                  <AvField id="nha-san-xuat-diaChi" type="text" name="diaChi" />
                </AvGroup>
                <AvGroup>
                  <Label id="soDienThoaiLabel" for="soDienThoai">
                    So Dien Thoai
                  </Label>
                  <AvField id="nha-san-xuat-soDienThoai" type="text" name="soDienThoai" />
                </AvGroup>
                <AvGroup>
                  <Label id="websiteLabel" for="website">
                    Website
                  </Label>
                  <AvField id="nha-san-xuat-website" type="text" name="website" />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="email">
                    Email
                  </Label>
                  <AvField id="nha-san-xuat-email" type="text" name="email" />
                </AvGroup>
                <AvGroup>
                  <Label id="faxLabel" for="fax">
                    Fax
                  </Label>
                  <AvField id="nha-san-xuat-fax" type="text" name="fax" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="nha-san-xuat-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="nha-san-xuat-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="nha-san-xuat-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="nha-san-xuat-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/nha-san-xuat" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  nhaSanXuatEntity: storeState.nhaSanXuat.entity,
  loading: storeState.nhaSanXuat.loading,
  updating: storeState.nhaSanXuat.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhaSanXuatUpdate);
