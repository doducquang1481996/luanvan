import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './nha-san-xuat.reducer';
import { INhaSanXuat } from 'app/shared/model/nha-san-xuat.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INhaSanXuatDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class NhaSanXuatDetail extends React.Component<INhaSanXuatDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { nhaSanXuatEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            NhaSanXuat [<b>{nhaSanXuatEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Nhà Cung Cấp</span>
            </dt>
            <dd>{nhaSanXuatEntity.ten}</dd>
            <dt>
              <span id="ma">Mã</span>
            </dt>
            <dd>{nhaSanXuatEntity.ma}</dd>
            <dt>
              <span id="diaChi">Địa Chỉ</span>
            </dt>
            <dd>{nhaSanXuatEntity.diaChi}</dd>
            <dt>
              <span id="soDienThoai">Số Điện Thoại</span>
            </dt>
            <dd>{nhaSanXuatEntity.soDienThoai}</dd>
            <dt>
              <span id="website">Website</span>
            </dt>
            <dd>{nhaSanXuatEntity.website}</dd>
            <dt>
              <span id="email">Email</span>
            </dt>
            <dd>{nhaSanXuatEntity.email}</dd>
            <dt>
              <span id="fax">Fax</span>
            </dt>
            <dd>{nhaSanXuatEntity.fax}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={nhaSanXuatEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{nhaSanXuatEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={nhaSanXuatEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{nhaSanXuatEntity.nguoiCapNhat}</dd>
          </dl>
          <Button tag={Link} to="/entity/nha-san-xuat" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/nha-san-xuat/${nhaSanXuatEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ nhaSanXuat }: IRootState) => ({
  nhaSanXuatEntity: nhaSanXuat.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhaSanXuatDetail);
