import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NhaSanXuat from './nha-san-xuat';
import NhaSanXuatDetail from './nha-san-xuat-detail';
import NhaSanXuatUpdate from './nha-san-xuat-update';
import NhaSanXuatDeleteDialog from './nha-san-xuat-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NhaSanXuatUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NhaSanXuatUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NhaSanXuatDetail} />
      <ErrorBoundaryRoute path={match.url} component={NhaSanXuat} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={NhaSanXuatDeleteDialog} />
  </>
);

export default Routes;
