import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INhaSanXuat, defaultValue } from 'app/shared/model/nha-san-xuat.model';

export const ACTION_TYPES = {
  FETCH_NHASANXUAT_LIST: 'nhaSanXuat/FETCH_NHASANXUAT_LIST',
  FETCH_NHASANXUAT: 'nhaSanXuat/FETCH_NHASANXUAT',
  CREATE_NHASANXUAT: 'nhaSanXuat/CREATE_NHASANXUAT',
  UPDATE_NHASANXUAT: 'nhaSanXuat/UPDATE_NHASANXUAT',
  DELETE_NHASANXUAT: 'nhaSanXuat/DELETE_NHASANXUAT',
  RESET: 'nhaSanXuat/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INhaSanXuat>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type NhaSanXuatState = Readonly<typeof initialState>;

// Reducer

export default (state: NhaSanXuatState = initialState, action): NhaSanXuatState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NHASANXUAT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NHASANXUAT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NHASANXUAT):
    case REQUEST(ACTION_TYPES.UPDATE_NHASANXUAT):
    case REQUEST(ACTION_TYPES.DELETE_NHASANXUAT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NHASANXUAT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NHASANXUAT):
    case FAILURE(ACTION_TYPES.CREATE_NHASANXUAT):
    case FAILURE(ACTION_TYPES.UPDATE_NHASANXUAT):
    case FAILURE(ACTION_TYPES.DELETE_NHASANXUAT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHASANXUAT_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHASANXUAT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NHASANXUAT):
    case SUCCESS(ACTION_TYPES.UPDATE_NHASANXUAT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NHASANXUAT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/nha-san-xuats';

// Actions

export const getEntities: ICrudGetAllAction<INhaSanXuat> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_NHASANXUAT_LIST,
    payload: axios.get<INhaSanXuat>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<INhaSanXuat> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NHASANXUAT,
    payload: axios.get<INhaSanXuat>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INhaSanXuat> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NHASANXUAT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INhaSanXuat> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NHASANXUAT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INhaSanXuat> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NHASANXUAT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
