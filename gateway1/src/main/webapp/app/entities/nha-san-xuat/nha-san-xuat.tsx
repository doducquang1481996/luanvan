import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './nha-san-xuat.reducer';
import { INhaSanXuat } from 'app/shared/model/nha-san-xuat.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface INhaSanXuatProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type INhaSanXuatState = IPaginationBaseState;

export class NhaSanXuat extends React.Component<INhaSanXuatProps, INhaSanXuatState> {
  state: INhaSanXuatState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { nhaSanXuatList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="nha-san-xuat-heading">
          Danh Sách Nhà Sản Xuất
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Tạo Mới Nhà Sản Xuất
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ten')}>
                  Tên Nhà Sản Xuất <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ma')}>
                  Mã Nhà Sản Xuất <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('diaChi')}>
                  Địa Chỉ <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('soDienThoai')}>
                  Số Điện Thoại <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('website')}>
                  Website <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('email')}>
                  Email <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('fax')}>
                  Fax <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayTao')}>
                  Ngày Tạo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiTao')}>
                  Người Tạo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayCapNhat')}>
                  Ngày Cập Nhật <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiCapNhat')}>
                  Người Cập Nhật <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {nhaSanXuatList.map((nhaSanXuat, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${nhaSanXuat.id}`} color="link" size="sm">
                      {nhaSanXuat.id}
                    </Button>
                  </td>
                  <td>{nhaSanXuat.ten}</td>
                  <td>{nhaSanXuat.ma}</td>
                  <td>{nhaSanXuat.diaChi}</td>
                  <td>{nhaSanXuat.soDienThoai}</td>
                  <td>{nhaSanXuat.website}</td>
                  <td>{nhaSanXuat.email}</td>
                  <td>{nhaSanXuat.fax}</td>
                  <td>
                    <TextFormat type="date" value={nhaSanXuat.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{nhaSanXuat.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={nhaSanXuat.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{nhaSanXuat.nguoiCapNhat}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${nhaSanXuat.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${nhaSanXuat.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${nhaSanXuat.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ nhaSanXuat }: IRootState) => ({
  nhaSanXuatList: nhaSanXuat.entities,
  totalItems: nhaSanXuat.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhaSanXuat);
