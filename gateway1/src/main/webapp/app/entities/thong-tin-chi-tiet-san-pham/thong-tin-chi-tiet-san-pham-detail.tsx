import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './thong-tin-chi-tiet-san-pham.reducer';
import { IThongTinChiTietSanPham } from 'app/shared/model/thong-tin-chi-tiet-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IThongTinChiTietSanPhamDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ThongTinChiTietSanPhamDetail extends React.Component<IThongTinChiTietSanPhamDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { thongTinChiTietSanPhamEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            ThongTinChiTietSanPham [<b>{thongTinChiTietSanPhamEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="sanPhamThuocTinhId">San Pham Thuoc Tinh Id</span>
            </dt>
            <dd>{thongTinChiTietSanPhamEntity.sanPhamThuocTinhId}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={thongTinChiTietSanPhamEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{thongTinChiTietSanPhamEntity.nguoiTao}</dd>
            <dt>San Pham Chi Tiet</dt>
            <dd>{thongTinChiTietSanPhamEntity.sanPhamChiTiet ? thongTinChiTietSanPhamEntity.sanPhamChiTiet.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/thong-tin-chi-tiet-san-pham" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/thong-tin-chi-tiet-san-pham/${thongTinChiTietSanPhamEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ thongTinChiTietSanPham }: IRootState) => ({
  thongTinChiTietSanPhamEntity: thongTinChiTietSanPham.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThongTinChiTietSanPhamDetail);
