import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IThongTinChiTietSanPham, defaultValue } from 'app/shared/model/thong-tin-chi-tiet-san-pham.model';

export const ACTION_TYPES = {
  FETCH_THONGTINCHITIETSANPHAM_LIST: 'thongTinChiTietSanPham/FETCH_THONGTINCHITIETSANPHAM_LIST',
  FETCH_THONGTINCHITIETSANPHAM: 'thongTinChiTietSanPham/FETCH_THONGTINCHITIETSANPHAM',
  CREATE_THONGTINCHITIETSANPHAM: 'thongTinChiTietSanPham/CREATE_THONGTINCHITIETSANPHAM',
  UPDATE_THONGTINCHITIETSANPHAM: 'thongTinChiTietSanPham/UPDATE_THONGTINCHITIETSANPHAM',
  DELETE_THONGTINCHITIETSANPHAM: 'thongTinChiTietSanPham/DELETE_THONGTINCHITIETSANPHAM',
  RESET: 'thongTinChiTietSanPham/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IThongTinChiTietSanPham>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ThongTinChiTietSanPhamState = Readonly<typeof initialState>;

// Reducer

export default (state: ThongTinChiTietSanPhamState = initialState, action): ThongTinChiTietSanPhamState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_THONGTINCHITIETSANPHAM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_THONGTINCHITIETSANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_THONGTINCHITIETSANPHAM):
    case REQUEST(ACTION_TYPES.UPDATE_THONGTINCHITIETSANPHAM):
    case REQUEST(ACTION_TYPES.DELETE_THONGTINCHITIETSANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_THONGTINCHITIETSANPHAM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_THONGTINCHITIETSANPHAM):
    case FAILURE(ACTION_TYPES.CREATE_THONGTINCHITIETSANPHAM):
    case FAILURE(ACTION_TYPES.UPDATE_THONGTINCHITIETSANPHAM):
    case FAILURE(ACTION_TYPES.DELETE_THONGTINCHITIETSANPHAM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_THONGTINCHITIETSANPHAM_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_THONGTINCHITIETSANPHAM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_THONGTINCHITIETSANPHAM):
    case SUCCESS(ACTION_TYPES.UPDATE_THONGTINCHITIETSANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_THONGTINCHITIETSANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/thong-tin-chi-tiet-san-phams';

// Actions

export const getEntities: ICrudGetAllAction<IThongTinChiTietSanPham> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_THONGTINCHITIETSANPHAM_LIST,
  payload: axios.get<IThongTinChiTietSanPham>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IThongTinChiTietSanPham> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_THONGTINCHITIETSANPHAM,
    payload: axios.get<IThongTinChiTietSanPham>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IThongTinChiTietSanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_THONGTINCHITIETSANPHAM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IThongTinChiTietSanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_THONGTINCHITIETSANPHAM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IThongTinChiTietSanPham> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_THONGTINCHITIETSANPHAM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
