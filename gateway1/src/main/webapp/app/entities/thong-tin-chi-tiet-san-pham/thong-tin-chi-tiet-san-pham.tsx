import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './thong-tin-chi-tiet-san-pham.reducer';
import { IThongTinChiTietSanPham } from 'app/shared/model/thong-tin-chi-tiet-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IThongTinChiTietSanPhamProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class ThongTinChiTietSanPham extends React.Component<IThongTinChiTietSanPhamProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { thongTinChiTietSanPhamList, match } = this.props;
    return (
      <div>
        <h2 id="thong-tin-chi-tiet-san-pham-heading">
          Thong Tin Chi Tiet San Phams
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Thong Tin Chi Tiet San Pham
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>San Pham Thuoc Tinh Id</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th>San Pham Chi Tiet</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {thongTinChiTietSanPhamList.map((thongTinChiTietSanPham, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${thongTinChiTietSanPham.id}`} color="link" size="sm">
                      {thongTinChiTietSanPham.id}
                    </Button>
                  </td>
                  <td>{thongTinChiTietSanPham.sanPhamThuocTinhId}</td>
                  <td>
                    <TextFormat type="date" value={thongTinChiTietSanPham.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{thongTinChiTietSanPham.nguoiTao}</td>
                  <td>
                    {thongTinChiTietSanPham.sanPhamChiTiet ? (
                      <Link to={`san-pham-chi-tiet/${thongTinChiTietSanPham.sanPhamChiTiet.id}`}>
                        {thongTinChiTietSanPham.sanPhamChiTiet.id}
                      </Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${thongTinChiTietSanPham.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${thongTinChiTietSanPham.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${thongTinChiTietSanPham.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ thongTinChiTietSanPham }: IRootState) => ({
  thongTinChiTietSanPhamList: thongTinChiTietSanPham.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThongTinChiTietSanPham);
