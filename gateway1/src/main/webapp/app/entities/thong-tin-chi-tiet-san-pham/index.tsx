import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ThongTinChiTietSanPham from './thong-tin-chi-tiet-san-pham';
import ThongTinChiTietSanPhamDetail from './thong-tin-chi-tiet-san-pham-detail';
import ThongTinChiTietSanPhamUpdate from './thong-tin-chi-tiet-san-pham-update';
import ThongTinChiTietSanPhamDeleteDialog from './thong-tin-chi-tiet-san-pham-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ThongTinChiTietSanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ThongTinChiTietSanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ThongTinChiTietSanPhamDetail} />
      <ErrorBoundaryRoute path={match.url} component={ThongTinChiTietSanPham} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ThongTinChiTietSanPhamDeleteDialog} />
  </>
);

export default Routes;
