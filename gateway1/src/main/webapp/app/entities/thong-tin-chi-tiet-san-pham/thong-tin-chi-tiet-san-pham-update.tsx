import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ISanPhamChiTiet } from 'app/shared/model/san-pham-chi-tiet.model';
import { getEntities as getSanPhamChiTiets } from 'app/entities/san-pham-chi-tiet/san-pham-chi-tiet.reducer';
import { getEntity, updateEntity, createEntity, reset } from './thong-tin-chi-tiet-san-pham.reducer';
import { IThongTinChiTietSanPham } from 'app/shared/model/thong-tin-chi-tiet-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IThongTinChiTietSanPhamUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IThongTinChiTietSanPhamUpdateState {
  isNew: boolean;
  sanPhamChiTietId: number;
}

export class ThongTinChiTietSanPhamUpdate extends React.Component<IThongTinChiTietSanPhamUpdateProps, IThongTinChiTietSanPhamUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      sanPhamChiTietId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getSanPhamChiTiets();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { thongTinChiTietSanPhamEntity } = this.props;
      const entity = {
        ...thongTinChiTietSanPhamEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/thong-tin-chi-tiet-san-pham');
  };

  render() {
    const { thongTinChiTietSanPhamEntity, sanPhamChiTiets, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.thongTinChiTietSanPham.home.createOrEditLabel">Create or edit a ThongTinChiTietSanPham</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : thongTinChiTietSanPhamEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="thong-tin-chi-tiet-san-pham-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="sanPhamThuocTinhIdLabel" for="sanPhamThuocTinhId">
                    San Pham Thuoc Tinh Id
                  </Label>
                  <AvField
                    id="thong-tin-chi-tiet-san-pham-sanPhamThuocTinhId"
                    type="number"
                    className="form-control"
                    name="sanPhamThuocTinhId"
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="thong-tin-chi-tiet-san-pham-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="thong-tin-chi-tiet-san-pham-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label for="sanPhamChiTiet.id">San Pham Chi Tiet</Label>
                  <AvInput id="thong-tin-chi-tiet-san-pham-sanPhamChiTiet" type="select" className="form-control" name="sanPhamChiTiet.id">
                    <option value="" key="0" />
                    {sanPhamChiTiets
                      ? sanPhamChiTiets.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/thong-tin-chi-tiet-san-pham" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  sanPhamChiTiets: storeState.sanPhamChiTiet.entities,
  thongTinChiTietSanPhamEntity: storeState.thongTinChiTietSanPham.entity,
  loading: storeState.thongTinChiTietSanPham.loading,
  updating: storeState.thongTinChiTietSanPham.updating
});

const mapDispatchToProps = {
  getSanPhamChiTiets,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThongTinChiTietSanPhamUpdate);
