import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './phieu-giam-dinh.reducer';
import { IPhieuGiamDinh } from 'app/shared/model/phieu-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPhieuGiamDinhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPhieuGiamDinhUpdateState {
  isNew: boolean;
}

export class PhieuGiamDinhUpdate extends React.Component<IPhieuGiamDinhUpdateProps, IPhieuGiamDinhUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { phieuGiamDinhEntity } = this.props;
      const entity = {
        ...phieuGiamDinhEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/phieu-giam-dinh');
  };

  render() {
    const { phieuGiamDinhEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.phieuGiamDinh.home.createOrEditLabel">Create or edit a PhieuGiamDinh</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : phieuGiamDinhEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="phieu-giam-dinh-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Ma
                  </Label>
                  <AvField id="phieu-giam-dinh-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Ten
                  </Label>
                  <AvField id="phieu-giam-dinh-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayGiamDinhLabel" for="ngayGiamDinh">
                    Ngay Giam Dinh
                  </Label>
                  <AvField id="phieu-giam-dinh-ngayGiamDinh" type="date" className="form-control" name="ngayGiamDinh" />
                </AvGroup>
                <AvGroup>
                  <Label id="phieuMuaHangIdLabel" for="phieuMuaHangId">
                    Phieu Mua Hang Id
                  </Label>
                  <AvField id="phieu-giam-dinh-phieuMuaHangId" type="number" className="form-control" name="phieuMuaHangId" />
                </AvGroup>
                <AvGroup>
                  <Label id="maPhieuMuaHangLabel" for="maPhieuMuaHang">
                    Ma Phieu Mua Hang
                  </Label>
                  <AvField id="phieu-giam-dinh-maPhieuMuaHang" type="text" name="maPhieuMuaHang" />
                </AvGroup>
                <AvGroup>
                  <Label id="trangThaiLabel" check>
                    <AvInput id="phieu-giam-dinh-trangThai" type="checkbox" className="form-control" name="trangThai" />
                    Trang Thai
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="phieu-giam-dinh-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="phieu-giam-dinh-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="phieu-giam-dinh-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="phieu-giam-dinh-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/phieu-giam-dinh" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  phieuGiamDinhEntity: storeState.phieuGiamDinh.entity,
  loading: storeState.phieuGiamDinh.loading,
  updating: storeState.phieuGiamDinh.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhieuGiamDinhUpdate);
