import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './phieu-giam-dinh.reducer';
import { IPhieuGiamDinh } from 'app/shared/model/phieu-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPhieuGiamDinhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class PhieuGiamDinh extends React.Component<IPhieuGiamDinhProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { phieuGiamDinhList, match } = this.props;
    return (
      <div>
        <h2 id="phieu-giam-dinh-heading">
          Phieu Giam Dinhs
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Phieu Giam Dinh
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Ma</th>
                <th>Ten</th>
                <th>Ngay Giam Dinh</th>
                <th>Phieu Mua Hang Id</th>
                <th>Ma Phieu Mua Hang</th>
                <th>Trang Thai</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th>Ngay Cap Nhat</th>
                <th>Nguoi Cap Nhat</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {phieuGiamDinhList.map((phieuGiamDinh, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${phieuGiamDinh.id}`} color="link" size="sm">
                      {phieuGiamDinh.id}
                    </Button>
                  </td>
                  <td>{phieuGiamDinh.ma}</td>
                  <td>{phieuGiamDinh.ten}</td>
                  <td>
                    <TextFormat type="date" value={phieuGiamDinh.ngayGiamDinh} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{phieuGiamDinh.phieuMuaHangId}</td>
                  <td>{phieuGiamDinh.maPhieuMuaHang}</td>
                  <td>{phieuGiamDinh.trangThai ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={phieuGiamDinh.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{phieuGiamDinh.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={phieuGiamDinh.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{phieuGiamDinh.nguoiCapNhat}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${phieuGiamDinh.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${phieuGiamDinh.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${phieuGiamDinh.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ phieuGiamDinh }: IRootState) => ({
  phieuGiamDinhList: phieuGiamDinh.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhieuGiamDinh);
