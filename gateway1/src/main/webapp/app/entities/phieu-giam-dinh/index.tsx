import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PhieuGiamDinh from './phieu-giam-dinh';
import PhieuGiamDinhDetail from './phieu-giam-dinh-detail';
import PhieuGiamDinhUpdate from './phieu-giam-dinh-update';
import PhieuGiamDinhDeleteDialog from './phieu-giam-dinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PhieuGiamDinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PhieuGiamDinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PhieuGiamDinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={PhieuGiamDinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={PhieuGiamDinhDeleteDialog} />
  </>
);

export default Routes;
