import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPhieuGiamDinh, defaultValue } from 'app/shared/model/phieu-giam-dinh.model';

export const ACTION_TYPES = {
  FETCH_PHIEUGIAMDINH_LIST: 'phieuGiamDinh/FETCH_PHIEUGIAMDINH_LIST',
  FETCH_PHIEUGIAMDINH: 'phieuGiamDinh/FETCH_PHIEUGIAMDINH',
  CREATE_PHIEUGIAMDINH: 'phieuGiamDinh/CREATE_PHIEUGIAMDINH',
  UPDATE_PHIEUGIAMDINH: 'phieuGiamDinh/UPDATE_PHIEUGIAMDINH',
  DELETE_PHIEUGIAMDINH: 'phieuGiamDinh/DELETE_PHIEUGIAMDINH',
  RESET: 'phieuGiamDinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPhieuGiamDinh>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type PhieuGiamDinhState = Readonly<typeof initialState>;

// Reducer

export default (state: PhieuGiamDinhState = initialState, action): PhieuGiamDinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PHIEUGIAMDINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PHIEUGIAMDINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PHIEUGIAMDINH):
    case REQUEST(ACTION_TYPES.UPDATE_PHIEUGIAMDINH):
    case REQUEST(ACTION_TYPES.DELETE_PHIEUGIAMDINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PHIEUGIAMDINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PHIEUGIAMDINH):
    case FAILURE(ACTION_TYPES.CREATE_PHIEUGIAMDINH):
    case FAILURE(ACTION_TYPES.UPDATE_PHIEUGIAMDINH):
    case FAILURE(ACTION_TYPES.DELETE_PHIEUGIAMDINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PHIEUGIAMDINH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PHIEUGIAMDINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PHIEUGIAMDINH):
    case SUCCESS(ACTION_TYPES.UPDATE_PHIEUGIAMDINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PHIEUGIAMDINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'storehouse/api/phieu-giam-dinhs';

// Actions

export const getEntities: ICrudGetAllAction<IPhieuGiamDinh> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PHIEUGIAMDINH_LIST,
  payload: axios.get<IPhieuGiamDinh>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IPhieuGiamDinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PHIEUGIAMDINH,
    payload: axios.get<IPhieuGiamDinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IPhieuGiamDinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PHIEUGIAMDINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPhieuGiamDinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PHIEUGIAMDINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPhieuGiamDinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PHIEUGIAMDINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
