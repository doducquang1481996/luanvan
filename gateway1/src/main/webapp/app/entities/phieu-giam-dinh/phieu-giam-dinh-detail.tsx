import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './phieu-giam-dinh.reducer';
import { IPhieuGiamDinh } from 'app/shared/model/phieu-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPhieuGiamDinhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PhieuGiamDinhDetail extends React.Component<IPhieuGiamDinhDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { phieuGiamDinhEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            PhieuGiamDinh [<b>{phieuGiamDinhEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ma">Ma</span>
            </dt>
            <dd>{phieuGiamDinhEntity.ma}</dd>
            <dt>
              <span id="ten">Ten</span>
            </dt>
            <dd>{phieuGiamDinhEntity.ten}</dd>
            <dt>
              <span id="ngayGiamDinh">Ngay Giam Dinh</span>
            </dt>
            <dd>
              <TextFormat value={phieuGiamDinhEntity.ngayGiamDinh} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="phieuMuaHangId">Phieu Mua Hang Id</span>
            </dt>
            <dd>{phieuGiamDinhEntity.phieuMuaHangId}</dd>
            <dt>
              <span id="maPhieuMuaHang">Ma Phieu Mua Hang</span>
            </dt>
            <dd>{phieuGiamDinhEntity.maPhieuMuaHang}</dd>
            <dt>
              <span id="trangThai">Trang Thai</span>
            </dt>
            <dd>{phieuGiamDinhEntity.trangThai ? 'true' : 'false'}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={phieuGiamDinhEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{phieuGiamDinhEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngay Cap Nhat</span>
            </dt>
            <dd>
              <TextFormat value={phieuGiamDinhEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Nguoi Cap Nhat</span>
            </dt>
            <dd>{phieuGiamDinhEntity.nguoiCapNhat}</dd>
          </dl>
          <Button tag={Link} to="/entity/phieu-giam-dinh" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/phieu-giam-dinh/${phieuGiamDinhEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ phieuGiamDinh }: IRootState) => ({
  phieuGiamDinhEntity: phieuGiamDinh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhieuGiamDinhDetail);
