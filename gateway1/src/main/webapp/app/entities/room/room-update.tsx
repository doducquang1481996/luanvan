import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './room.reducer';
import { getSession } from 'app/shared/reducers/authentication';
import { IRoom } from 'app/shared/model/room.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IRoomUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IRoomUpdateState {
  isNew: boolean;
}

export class RoomUpdate extends React.Component<IRoomUpdateProps, IRoomUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
    this.props.getSession();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { roomEntity } = this.props;
      const entity = {
        ...roomEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/room');
  };

  render() {
    const { roomEntity, loading, updating } = this.props;
    const { isNew } = this.state;
    const { account } = this.props;
    // console.log('test' + account.firstName);

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            {!isNew ? (
              <h2 id="gatewayApp.room.home.createOrEditLabel">Cập nhật Thông Tin Phòng Ban</h2>
            ) : (
              <h2 id="gatewayApp.room.home.createOrEditLabel">Thêm Mới Phòng Ban</h2>
            )}
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : roomEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="room-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="roomNameLabel" for="roomName">
                    Tên Phòng Ban
                  </Label>
                  <AvField id="room-roomName" type="text" name="roomName" />
                </AvGroup>
                <AvGroup>
                  <Label id="roomCodeLabel" for="roomCode">
                    Mã Phòng Ban
                  </Label>
                  <AvField id="room-roomCode" type="text" name="roomCode" />
                </AvGroup>
                <AvGroup>
                  <AvField id="room-parentId" type="hidden" className="form-control" name="parentId" value="0" />
                </AvGroup>
                {!isNew ? (
                  <AvGroup>
                    <Label id="updatePersonLabel">Người Cập Nhật</Label>
                    <AvField type="text" name="updatePerson" value={account.login} required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="updatePersonLabel">Người Tạo</Label>
                  {!isNew ? (
                    <AvField id="account-craetePerson" type="text" className="form-control" name="createPerson" required readOnly />
                  ) : (
                    <AvField type="text" name="createPerson" className="form-control" value={account.login} required readOnly />
                  )}
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/room" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  roomEntity: storeState.room.entity,
  loading: storeState.room.loading,
  updating: storeState.room.updating,
  account: storeState.authentication.account
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
  getSession
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomUpdate);
