import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './loai-san-pham-loai-thuoc-tinh.reducer';
import { ILoaiSanPhamLoaiThuocTinh } from 'app/shared/model/loai-san-pham-loai-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILoaiSanPhamLoaiThuocTinhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class LoaiSanPhamLoaiThuocTinhDetail extends React.Component<ILoaiSanPhamLoaiThuocTinhDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { loaiSanPhamLoaiThuocTinhEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            LoaiSanPhamLoaiThuocTinh [<b>{loaiSanPhamLoaiThuocTinhEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="loaiSanPhamId">Loai San Pham Id</span>
            </dt>
            <dd>{loaiSanPhamLoaiThuocTinhEntity.loaiSanPhamId}</dd>
            <dt>
              <span id="loaiThuocTinhId">Loai Thuoc Tinh Id</span>
            </dt>
            <dd>{loaiSanPhamLoaiThuocTinhEntity.loaiThuocTinhId}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={loaiSanPhamLoaiThuocTinhEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{loaiSanPhamLoaiThuocTinhEntity.nguoiTao}</dd>
          </dl>
          <Button tag={Link} to="/entity/loai-san-pham-loai-thuoc-tinh" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/loai-san-pham-loai-thuoc-tinh/${loaiSanPhamLoaiThuocTinhEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ loaiSanPhamLoaiThuocTinh }: IRootState) => ({
  loaiSanPhamLoaiThuocTinhEntity: loaiSanPhamLoaiThuocTinh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiSanPhamLoaiThuocTinhDetail);
