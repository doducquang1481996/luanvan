import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './loai-san-pham-loai-thuoc-tinh.reducer';
import { ILoaiSanPhamLoaiThuocTinh } from 'app/shared/model/loai-san-pham-loai-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILoaiSanPhamLoaiThuocTinhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class LoaiSanPhamLoaiThuocTinh extends React.Component<ILoaiSanPhamLoaiThuocTinhProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { loaiSanPhamLoaiThuocTinhList, match } = this.props;
    return (
      <div>
        <h2 id="loai-san-pham-loai-thuoc-tinh-heading">
          Loai San Pham Loai Thuoc Tinhs
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Loai San Pham Loai Thuoc Tinh
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Loai San Pham Id</th>
                <th>Loai Thuoc Tinh Id</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {loaiSanPhamLoaiThuocTinhList.map((loaiSanPhamLoaiThuocTinh, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${loaiSanPhamLoaiThuocTinh.id}`} color="link" size="sm">
                      {loaiSanPhamLoaiThuocTinh.id}
                    </Button>
                  </td>
                  <td>{loaiSanPhamLoaiThuocTinh.loaiSanPhamId}</td>
                  <td>{loaiSanPhamLoaiThuocTinh.loaiThuocTinhId}</td>
                  <td>
                    <TextFormat type="date" value={loaiSanPhamLoaiThuocTinh.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{loaiSanPhamLoaiThuocTinh.nguoiTao}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${loaiSanPhamLoaiThuocTinh.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${loaiSanPhamLoaiThuocTinh.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${loaiSanPhamLoaiThuocTinh.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ loaiSanPhamLoaiThuocTinh }: IRootState) => ({
  loaiSanPhamLoaiThuocTinhList: loaiSanPhamLoaiThuocTinh.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiSanPhamLoaiThuocTinh);
