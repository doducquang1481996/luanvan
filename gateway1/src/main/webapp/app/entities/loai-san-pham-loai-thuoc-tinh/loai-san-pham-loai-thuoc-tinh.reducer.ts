import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILoaiSanPhamLoaiThuocTinh, defaultValue } from 'app/shared/model/loai-san-pham-loai-thuoc-tinh.model';

export const ACTION_TYPES = {
  FETCH_LOAISANPHAMLOAITHUOCTINH_LIST: 'loaiSanPhamLoaiThuocTinh/FETCH_LOAISANPHAMLOAITHUOCTINH_LIST',
  FETCH_LOAISANPHAMLOAITHUOCTINH: 'loaiSanPhamLoaiThuocTinh/FETCH_LOAISANPHAMLOAITHUOCTINH',
  CREATE_LOAISANPHAMLOAITHUOCTINH: 'loaiSanPhamLoaiThuocTinh/CREATE_LOAISANPHAMLOAITHUOCTINH',
  UPDATE_LOAISANPHAMLOAITHUOCTINH: 'loaiSanPhamLoaiThuocTinh/UPDATE_LOAISANPHAMLOAITHUOCTINH',
  DELETE_LOAISANPHAMLOAITHUOCTINH: 'loaiSanPhamLoaiThuocTinh/DELETE_LOAISANPHAMLOAITHUOCTINH',
  RESET: 'loaiSanPhamLoaiThuocTinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILoaiSanPhamLoaiThuocTinh>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type LoaiSanPhamLoaiThuocTinhState = Readonly<typeof initialState>;

// Reducer

export default (state: LoaiSanPhamLoaiThuocTinhState = initialState, action): LoaiSanPhamLoaiThuocTinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LOAISANPHAMLOAITHUOCTINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LOAISANPHAMLOAITHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_LOAISANPHAMLOAITHUOCTINH):
    case REQUEST(ACTION_TYPES.UPDATE_LOAISANPHAMLOAITHUOCTINH):
    case REQUEST(ACTION_TYPES.DELETE_LOAISANPHAMLOAITHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_LOAISANPHAMLOAITHUOCTINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LOAISANPHAMLOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.CREATE_LOAISANPHAMLOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.UPDATE_LOAISANPHAMLOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.DELETE_LOAISANPHAMLOAITHUOCTINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_LOAISANPHAMLOAITHUOCTINH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_LOAISANPHAMLOAITHUOCTINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_LOAISANPHAMLOAITHUOCTINH):
    case SUCCESS(ACTION_TYPES.UPDATE_LOAISANPHAMLOAITHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_LOAISANPHAMLOAITHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/loai-san-pham-loai-thuoc-tinhs';

// Actions

export const getEntities: ICrudGetAllAction<ILoaiSanPhamLoaiThuocTinh> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_LOAISANPHAMLOAITHUOCTINH_LIST,
  payload: axios.get<ILoaiSanPhamLoaiThuocTinh>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<ILoaiSanPhamLoaiThuocTinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LOAISANPHAMLOAITHUOCTINH,
    payload: axios.get<ILoaiSanPhamLoaiThuocTinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ILoaiSanPhamLoaiThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LOAISANPHAMLOAITHUOCTINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILoaiSanPhamLoaiThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LOAISANPHAMLOAITHUOCTINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILoaiSanPhamLoaiThuocTinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LOAISANPHAMLOAITHUOCTINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
