import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import LoaiSanPhamLoaiThuocTinh from './loai-san-pham-loai-thuoc-tinh';
import LoaiSanPhamLoaiThuocTinhDetail from './loai-san-pham-loai-thuoc-tinh-detail';
import LoaiSanPhamLoaiThuocTinhUpdate from './loai-san-pham-loai-thuoc-tinh-update';
import LoaiSanPhamLoaiThuocTinhDeleteDialog from './loai-san-pham-loai-thuoc-tinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LoaiSanPhamLoaiThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LoaiSanPhamLoaiThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LoaiSanPhamLoaiThuocTinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={LoaiSanPhamLoaiThuocTinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={LoaiSanPhamLoaiThuocTinhDeleteDialog} />
  </>
);

export default Routes;
