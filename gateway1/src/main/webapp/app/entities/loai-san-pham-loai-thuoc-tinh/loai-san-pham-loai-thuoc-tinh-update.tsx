import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './loai-san-pham-loai-thuoc-tinh.reducer';
import { ILoaiSanPhamLoaiThuocTinh } from 'app/shared/model/loai-san-pham-loai-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILoaiSanPhamLoaiThuocTinhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ILoaiSanPhamLoaiThuocTinhUpdateState {
  isNew: boolean;
}

export class LoaiSanPhamLoaiThuocTinhUpdate extends React.Component<
  ILoaiSanPhamLoaiThuocTinhUpdateProps,
  ILoaiSanPhamLoaiThuocTinhUpdateState
> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { loaiSanPhamLoaiThuocTinhEntity } = this.props;
      const entity = {
        ...loaiSanPhamLoaiThuocTinhEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/loai-san-pham-loai-thuoc-tinh');
  };

  render() {
    const { loaiSanPhamLoaiThuocTinhEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.loaiSanPhamLoaiThuocTinh.home.createOrEditLabel">Create or edit a LoaiSanPhamLoaiThuocTinh</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : loaiSanPhamLoaiThuocTinhEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="loai-san-pham-loai-thuoc-tinh-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="loaiSanPhamIdLabel" for="loaiSanPhamId">
                    Loai San Pham Id
                  </Label>
                  <AvField id="loai-san-pham-loai-thuoc-tinh-loaiSanPhamId" type="number" className="form-control" name="loaiSanPhamId" />
                </AvGroup>
                <AvGroup>
                  <Label id="loaiThuocTinhIdLabel" for="loaiThuocTinhId">
                    Loai Thuoc Tinh Id
                  </Label>
                  <AvField
                    id="loai-san-pham-loai-thuoc-tinh-loaiThuocTinhId"
                    type="number"
                    className="form-control"
                    name="loaiThuocTinhId"
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="loai-san-pham-loai-thuoc-tinh-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="loai-san-pham-loai-thuoc-tinh-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/loai-san-pham-loai-thuoc-tinh" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  loaiSanPhamLoaiThuocTinhEntity: storeState.loaiSanPhamLoaiThuocTinh.entity,
  loading: storeState.loaiSanPhamLoaiThuocTinh.loading,
  updating: storeState.loaiSanPhamLoaiThuocTinh.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiSanPhamLoaiThuocTinhUpdate);
