import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './nha-cung-cap.reducer';
import { INhaCungCap } from 'app/shared/model/nha-cung-cap.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface INhaCungCapUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface INhaCungCapUpdateState {
  isNew: boolean;
}

export class NhaCungCapUpdate extends React.Component<INhaCungCapUpdateProps, INhaCungCapUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { nhaCungCapEntity } = this.props;
      const entity = {
        ...nhaCungCapEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/nha-cung-cap');
  };

  render() {
    const { nhaCungCapEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.nhaCungCap.home.createOrEditLabel">Create or edit a NhaCungCap</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : nhaCungCapEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="nha-cung-cap-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Ten
                  </Label>
                  <AvField id="nha-cung-cap-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Ma
                  </Label>
                  <AvField id="nha-cung-cap-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="diaChiLabel" for="diaChi">
                    Dia Chi
                  </Label>
                  <AvField id="nha-cung-cap-diaChi" type="text" name="diaChi" />
                </AvGroup>
                <AvGroup>
                  <Label id="soDienThoaiLabel" for="soDienThoai">
                    So Dien Thoai
                  </Label>
                  <AvField id="nha-cung-cap-soDienThoai" type="text" name="soDienThoai" />
                </AvGroup>
                <AvGroup>
                  <Label id="websiteLabel" for="website">
                    Website
                  </Label>
                  <AvField id="nha-cung-cap-website" type="text" name="website" />
                </AvGroup>
                <AvGroup>
                  <Label id="faxLabel" for="fax">
                    Fax
                  </Label>
                  <AvField id="nha-cung-cap-fax" type="text" name="fax" />
                </AvGroup>
                <AvGroup>
                  <Label id="maSoThueLabel" for="maSoThue">
                    Ma So Thue
                  </Label>
                  <AvField id="nha-cung-cap-maSoThue" type="text" name="maSoThue" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiPhuTrachLabel" for="nguoiPhuTrach">
                    Nguoi Phu Trach
                  </Label>
                  <AvField id="nha-cung-cap-nguoiPhuTrach" type="text" name="nguoiPhuTrach" />
                </AvGroup>
                <AvGroup>
                  <Label id="activeLabel" check>
                    <AvInput id="nha-cung-cap-active" type="checkbox" className="form-control" name="active" />
                    Active
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="warningLabel" check>
                    <AvInput id="nha-cung-cap-warning" type="checkbox" className="form-control" name="warning" />
                    Warning
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="nha-cung-cap-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="nha-cung-cap-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="nha-cung-cap-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="nha-cung-cap-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/nha-cung-cap" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  nhaCungCapEntity: storeState.nhaCungCap.entity,
  loading: storeState.nhaCungCap.loading,
  updating: storeState.nhaCungCap.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhaCungCapUpdate);
