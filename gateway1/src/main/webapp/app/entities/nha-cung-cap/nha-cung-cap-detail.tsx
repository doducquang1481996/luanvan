import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './nha-cung-cap.reducer';
import { INhaCungCap } from 'app/shared/model/nha-cung-cap.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INhaCungCapDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class NhaCungCapDetail extends React.Component<INhaCungCapDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { nhaCungCapEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Thông Tin Nhà Cung Cấp [<b>{nhaCungCapEntity.ten}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Nhà Cung Cấp</span>
            </dt>
            <dd>{nhaCungCapEntity.ten}</dd>
            <dt>
              <span id="ma">Mã</span>
            </dt>
            <dd>{nhaCungCapEntity.ma}</dd>
            <dt>
              <span id="diaChi">Địa Chỉ</span>
            </dt>
            <dd>{nhaCungCapEntity.diaChi}</dd>
            <dt>
              <span id="soDienThoai">Số Điện Thoại</span>
            </dt>
            <dd>{nhaCungCapEntity.soDienThoai}</dd>
            <dt>
              <span id="website">Website</span>
            </dt>
            <dd>{nhaCungCapEntity.website}</dd>
            <dt>
              <span id="fax">Fax</span>
            </dt>
            <dd>{nhaCungCapEntity.fax}</dd>
            <dt>
              <span id="maSoThue">Mã Số Thuế</span>
            </dt>
            <dd>{nhaCungCapEntity.maSoThue}</dd>
            <dt>
              <span id="nguoiPhuTrach">Người Phụ Trách</span>
            </dt>
            <dd>{nhaCungCapEntity.nguoiPhuTrach}</dd>
            <dt>
              <span id="active">Hoạt Động</span>
            </dt>
            <dd>{nhaCungCapEntity.active ? 'true' : 'false'}</dd>
            <dt>
              <span id="warning">Warning</span>
            </dt>
            <dd>{nhaCungCapEntity.warning ? 'true' : 'false'}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={nhaCungCapEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{nhaCungCapEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={nhaCungCapEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{nhaCungCapEntity.nguoiCapNhat}</dd>
          </dl>
          <Button tag={Link} to="/entity/nha-cung-cap" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/nha-cung-cap/${nhaCungCapEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ nhaCungCap }: IRootState) => ({
  nhaCungCapEntity: nhaCungCap.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhaCungCapDetail);
