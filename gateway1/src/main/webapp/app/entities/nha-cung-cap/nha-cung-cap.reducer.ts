import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INhaCungCap, defaultValue } from 'app/shared/model/nha-cung-cap.model';

export const ACTION_TYPES = {
  FETCH_NHACUNGCAP_LIST: 'nhaCungCap/FETCH_NHACUNGCAP_LIST',
  FETCH_NHACUNGCAP: 'nhaCungCap/FETCH_NHACUNGCAP',
  CREATE_NHACUNGCAP: 'nhaCungCap/CREATE_NHACUNGCAP',
  UPDATE_NHACUNGCAP: 'nhaCungCap/UPDATE_NHACUNGCAP',
  DELETE_NHACUNGCAP: 'nhaCungCap/DELETE_NHACUNGCAP',
  RESET: 'nhaCungCap/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INhaCungCap>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type NhaCungCapState = Readonly<typeof initialState>;

// Reducer

export default (state: NhaCungCapState = initialState, action): NhaCungCapState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NHACUNGCAP_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NHACUNGCAP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NHACUNGCAP):
    case REQUEST(ACTION_TYPES.UPDATE_NHACUNGCAP):
    case REQUEST(ACTION_TYPES.DELETE_NHACUNGCAP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NHACUNGCAP_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NHACUNGCAP):
    case FAILURE(ACTION_TYPES.CREATE_NHACUNGCAP):
    case FAILURE(ACTION_TYPES.UPDATE_NHACUNGCAP):
    case FAILURE(ACTION_TYPES.DELETE_NHACUNGCAP):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHACUNGCAP_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHACUNGCAP):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NHACUNGCAP):
    case SUCCESS(ACTION_TYPES.UPDATE_NHACUNGCAP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NHACUNGCAP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/nha-cung-caps';

// Actions

export const getEntities: ICrudGetAllAction<INhaCungCap> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_NHACUNGCAP_LIST,
    payload: axios.get<INhaCungCap>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<INhaCungCap> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NHACUNGCAP,
    payload: axios.get<INhaCungCap>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INhaCungCap> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NHACUNGCAP,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INhaCungCap> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NHACUNGCAP,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INhaCungCap> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NHACUNGCAP,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
