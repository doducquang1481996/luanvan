import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NhaCungCap from './nha-cung-cap';
import NhaCungCapDetail from './nha-cung-cap-detail';
import NhaCungCapUpdate from './nha-cung-cap-update';
import NhaCungCapDeleteDialog from './nha-cung-cap-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NhaCungCapUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NhaCungCapUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NhaCungCapDetail} />
      <ErrorBoundaryRoute path={match.url} component={NhaCungCap} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={NhaCungCapDeleteDialog} />
  </>
);

export default Routes;
