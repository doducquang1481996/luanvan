import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './nha-cung-cap.reducer';
import { INhaCungCap } from 'app/shared/model/nha-cung-cap.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface INhaCungCapProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type INhaCungCapState = IPaginationBaseState;

export class NhaCungCap extends React.Component<INhaCungCapProps, INhaCungCapState> {
  state: INhaCungCapState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { nhaCungCapList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="nha-cung-cap-heading">
          Danh Sách Nhà Cung Cấp
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Nha Cung Cap
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ten')}>
                  Tên Nhà Cung Cấp <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ma')}>
                  Mã <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('diaChi')}>
                  Địa Chỉ <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('soDienThoai')}>
                  Số Điện Thoại <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('website')}>
                  Website <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('fax')}>
                  Fax <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('maSoThue')}>
                  Mã Số Thuế <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiPhuTrach')}>
                  Người Phụ Trách <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('active')}>
                  Hoạt động <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('warning')}>
                  Cảnh Báo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayTao')}>
                  Ngày Tạo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiTao')}>
                  Người Tạo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayCapNhat')}>
                  Ngày Cập Nhật <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiCapNhat')}>
                  Người Cập Nhật <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {nhaCungCapList.map((nhaCungCap, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${nhaCungCap.id}`} color="link" size="sm">
                      {nhaCungCap.id}
                    </Button>
                  </td>
                  <td>{nhaCungCap.ten}</td>
                  <td>{nhaCungCap.ma}</td>
                  <td>{nhaCungCap.diaChi}</td>
                  <td>{nhaCungCap.soDienThoai}</td>
                  <td>{nhaCungCap.website}</td>
                  <td>{nhaCungCap.fax}</td>
                  <td>{nhaCungCap.maSoThue}</td>
                  <td>{nhaCungCap.nguoiPhuTrach}</td>
                  <td>{nhaCungCap.active ? 'true' : 'false'}</td>
                  <td>{nhaCungCap.warning ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={nhaCungCap.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{nhaCungCap.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={nhaCungCap.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{nhaCungCap.nguoiCapNhat}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${nhaCungCap.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${nhaCungCap.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${nhaCungCap.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ nhaCungCap }: IRootState) => ({
  nhaCungCapList: nhaCungCap.entities,
  totalItems: nhaCungCap.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhaCungCap);
