import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './nhom-thuoc-tinh.reducer';
import { INhomThuocTinh } from 'app/shared/model/nhom-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INhomThuocTinhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class NhomThuocTinhDetail extends React.Component<INhomThuocTinhDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { nhomThuocTinhEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Thông Tin Nhóm Thuộc Tính [<b>{nhomThuocTinhEntity.ma}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Thuộc Tính</span>
            </dt>
            <dd>{nhomThuocTinhEntity.ten}</dd>
            <dt>
              <span id="ma">Mã Thuộc Tính</span>
            </dt>
            <dd>{nhomThuocTinhEntity.ma}</dd>
            <dt>
              <span id="moTa">Mô Tả</span>
            </dt>
            <dd>{nhomThuocTinhEntity.moTa}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={nhomThuocTinhEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{nhomThuocTinhEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={nhomThuocTinhEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{nhomThuocTinhEntity.nguoiCapNhat}</dd>
          </dl>
          <Button tag={Link} to="/entity/nhom-thuoc-tinh" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/nhom-thuoc-tinh/${nhomThuocTinhEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ nhomThuocTinh }: IRootState) => ({
  nhomThuocTinhEntity: nhomThuocTinh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhomThuocTinhDetail);
