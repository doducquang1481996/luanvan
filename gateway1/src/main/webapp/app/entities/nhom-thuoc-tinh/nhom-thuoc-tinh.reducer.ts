import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INhomThuocTinh, defaultValue } from 'app/shared/model/nhom-thuoc-tinh.model';

export const ACTION_TYPES = {
  FETCH_NHOMTHUOCTINH_LIST: 'nhomThuocTinh/FETCH_NHOMTHUOCTINH_LIST',
  FETCH_NHOMTHUOCTINH: 'nhomThuocTinh/FETCH_NHOMTHUOCTINH',
  CREATE_NHOMTHUOCTINH: 'nhomThuocTinh/CREATE_NHOMTHUOCTINH',
  UPDATE_NHOMTHUOCTINH: 'nhomThuocTinh/UPDATE_NHOMTHUOCTINH',
  DELETE_NHOMTHUOCTINH: 'nhomThuocTinh/DELETE_NHOMTHUOCTINH',
  RESET: 'nhomThuocTinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INhomThuocTinh>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type NhomThuocTinhState = Readonly<typeof initialState>;

// Reducer

export default (state: NhomThuocTinhState = initialState, action): NhomThuocTinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NHOMTHUOCTINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NHOMTHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NHOMTHUOCTINH):
    case REQUEST(ACTION_TYPES.UPDATE_NHOMTHUOCTINH):
    case REQUEST(ACTION_TYPES.DELETE_NHOMTHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NHOMTHUOCTINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NHOMTHUOCTINH):
    case FAILURE(ACTION_TYPES.CREATE_NHOMTHUOCTINH):
    case FAILURE(ACTION_TYPES.UPDATE_NHOMTHUOCTINH):
    case FAILURE(ACTION_TYPES.DELETE_NHOMTHUOCTINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHOMTHUOCTINH_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHOMTHUOCTINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NHOMTHUOCTINH):
    case SUCCESS(ACTION_TYPES.UPDATE_NHOMTHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NHOMTHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/nhom-thuoc-tinhs';

// Actions

export const getEntities: ICrudGetAllAction<INhomThuocTinh> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_NHOMTHUOCTINH_LIST,
    payload: axios.get<INhomThuocTinh>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<INhomThuocTinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NHOMTHUOCTINH,
    payload: axios.get<INhomThuocTinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INhomThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NHOMTHUOCTINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INhomThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NHOMTHUOCTINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INhomThuocTinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NHOMTHUOCTINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
