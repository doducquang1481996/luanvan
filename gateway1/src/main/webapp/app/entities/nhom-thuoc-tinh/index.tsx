import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NhomThuocTinh from './nhom-thuoc-tinh';
import NhomThuocTinhDetail from './nhom-thuoc-tinh-detail';
import NhomThuocTinhUpdate from './nhom-thuoc-tinh-update';
import NhomThuocTinhDeleteDialog from './nhom-thuoc-tinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NhomThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NhomThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NhomThuocTinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={NhomThuocTinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={NhomThuocTinhDeleteDialog} />
  </>
);

export default Routes;
