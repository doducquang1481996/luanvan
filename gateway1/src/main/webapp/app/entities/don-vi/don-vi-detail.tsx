import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './don-vi.reducer';
import { IDonVi } from 'app/shared/model/don-vi.model';
import { getEntities as getByNhomDonVi } from '../nhom-don-vi/nhom-don-vi.reducer';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDonViDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class DonViDetail extends React.Component<IDonViDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { donViEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Thông Tin Đơn Vị [<b>{donViEntity.ma}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Đơn Vị</span>
            </dt>
            <dd>{donViEntity.ten}</dd>
            <dt>
              <span id="ma">Mã Đơn Vị</span>
            </dt>
            <dd>{donViEntity.ma}</dd>
            <dt>
              <span id="moTa">Mô Tả</span>
            </dt>
            <dd>{donViEntity.moTa}</dd>
            <dt>
              <span id="quyDoi">Quy Đổi</span>
            </dt>
            <dd>{donViEntity.quyDoi}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={donViEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{donViEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={donViEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{donViEntity.nguoiCapNhat}</dd>
            <dt>Nhóm Đơn Vị</dt>
            <dd>{donViEntity.nhomDonViId ? donViEntity.nhomDonViId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/don-vi" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/don-vi/${donViEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ donVi }: IRootState) => ({
  donViEntity: donVi.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DonViDetail);
