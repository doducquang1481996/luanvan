import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { INhomDonVi } from 'app/shared/model/nhom-don-vi.model';
import { getEntities as getNhomDonVis } from 'app/entities/nhom-don-vi/nhom-don-vi.reducer';
import { getEntity, updateEntity, createEntity, reset } from './don-vi.reducer';
import { IDonVi } from 'app/shared/model/don-vi.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IDonViUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IDonViUpdateState {
  isNew: boolean;
  nhomDonViId: number;
}

export class DonViUpdate extends React.Component<IDonViUpdateProps, IDonViUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      nhomDonViId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getNhomDonVis();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { donViEntity } = this.props;
      const entity = {
        ...donViEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/don-vi');
  };

  render() {
    const { donViEntity, nhomDonVis, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.donVi.home.createOrEditLabel">Create or edit a DonVi</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : donViEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="don-vi-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Ten
                  </Label>
                  <AvField id="don-vi-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Ma
                  </Label>
                  <AvField id="don-vi-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="moTaLabel" for="moTa">
                    Mo Ta
                  </Label>
                  <AvField id="don-vi-moTa" type="text" name="moTa" />
                </AvGroup>
                <AvGroup>
                  <Label id="quyDoiLabel" for="quyDoi">
                    Quy Doi
                  </Label>
                  <AvField id="don-vi-quyDoi" type="number" className="form-control" name="quyDoi" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="don-vi-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="don-vi-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="don-vi-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="don-vi-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label for="nhomDonVi.id">Nhom Don Vi</Label>
                  <AvInput id="don-vi-nhomDonVi" type="select" className="form-control" name="nhomDonViId">
                    <option value="" key="0" />
                    {nhomDonVis
                      ? nhomDonVis.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.ten}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/don-vi" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  nhomDonVis: storeState.nhomDonVi.entities,
  donViEntity: storeState.donVi.entity,
  loading: storeState.donVi.loading,
  updating: storeState.donVi.updating
});

const mapDispatchToProps = {
  getNhomDonVis,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DonViUpdate);
