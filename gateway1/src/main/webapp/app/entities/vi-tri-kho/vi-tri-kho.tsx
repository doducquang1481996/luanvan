import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './vi-tri-kho.reducer';
import { IViTriKho } from 'app/shared/model/vi-tri-kho.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IViTriKhoProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IViTriKhoState = IPaginationBaseState;

export class ViTriKho extends React.Component<IViTriKhoProps, IViTriKhoState> {
  state: IViTriKhoState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { viTriKhoList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="vi-tri-kho-heading">
          Vi Tri Khos
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Thêm Mới Vị Trí Kho
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ten')}>
                  Tên Vị Trí <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ma')}>
                  Mã Vị Trí <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('moTa')}>
                  Mô Tả <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayTao')}>
                  Ngày Tạo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiTao')}>
                  Người Tạo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayCapNhat')}>
                  Ngày Cập Nhật <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiCapNhat')}>
                  Người Cập Nhật <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Kho <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {viTriKhoList.map((viTriKho, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${viTriKho.id}`} color="link" size="sm">
                      {viTriKho.ten}
                    </Button>
                  </td>
                  <td>{viTriKho.ten}</td>
                  <td>{viTriKho.ma}</td>
                  <td>{viTriKho.moTa}</td>
                  <td>
                    <TextFormat type="date" value={viTriKho.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{viTriKho.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={viTriKho.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{viTriKho.nguoiCapNhat}</td>
                  <td>{viTriKho.khoId ? <Link to={`kho/${viTriKho.khoId}`}>{viTriKho.khoId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${viTriKho.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${viTriKho.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${viTriKho.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ viTriKho }: IRootState) => ({
  viTriKhoList: viTriKho.entities,
  totalItems: viTriKho.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViTriKho);
