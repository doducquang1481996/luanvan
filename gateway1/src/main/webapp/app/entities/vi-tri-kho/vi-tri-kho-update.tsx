import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IKho } from 'app/shared/model/kho.model';
import { getEntities as getKhos } from 'app/entities/kho/kho.reducer';
import { getEntity, updateEntity, createEntity, reset } from './vi-tri-kho.reducer';
import { IViTriKho } from 'app/shared/model/vi-tri-kho.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IViTriKhoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IViTriKhoUpdateState {
  isNew: boolean;
  khoId: number;
}

export class ViTriKhoUpdate extends React.Component<IViTriKhoUpdateProps, IViTriKhoUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      khoId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getKhos();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { viTriKhoEntity } = this.props;
      const entity = {
        ...viTriKhoEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/vi-tri-kho');
  };

  render() {
    const { viTriKhoEntity, khos, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.viTriKho.home.createOrEditLabel">Create or edit a ViTriKho</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : viTriKhoEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="vi-tri-kho-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Tên Vị Trí Kho
                  </Label>
                  <AvField id="vi-tri-kho-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Mã Vị Trí Kho
                  </Label>
                  <AvField id="vi-tri-kho-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="moTaLabel" for="moTa">
                    Mô Tả
                  </Label>
                  <AvField id="vi-tri-kho-moTa" type="text" name="moTa" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngày Tạo
                  </Label>
                  <AvField id="vi-tri-kho-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Người Tạo
                  </Label>
                  <AvField id="vi-tri-kho-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngày Cập Nhật
                  </Label>
                  <AvField id="vi-tri-kho-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Người Cập Nhật
                  </Label>
                  <AvField id="vi-tri-kho-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label for="kho.id">Kho</Label>
                  <AvInput id="vi-tri-kho-kho" type="select" className="form-control" name="khoId">
                    <option value="" key="0" />
                    {khos
                      ? khos.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.ten}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/vi-tri-kho" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Trở Lại</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Lưu
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  khos: storeState.kho.entities,
  viTriKhoEntity: storeState.viTriKho.entity,
  loading: storeState.viTriKho.loading,
  updating: storeState.viTriKho.updating
});

const mapDispatchToProps = {
  getKhos,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViTriKhoUpdate);
