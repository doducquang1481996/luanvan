import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './vi-tri-kho.reducer';
import { IViTriKho } from 'app/shared/model/vi-tri-kho.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IViTriKhoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ViTriKhoDetail extends React.Component<IViTriKhoDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { viTriKhoEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Thông Tin Vị Trí Kho [<b>{viTriKhoEntity.ma}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Vị Trí</span>
            </dt>
            <dd>{viTriKhoEntity.ten}</dd>
            <dt>
              <span id="ma">Mã Vị Trí</span>
            </dt>
            <dd>{viTriKhoEntity.ma}</dd>
            <dt>
              <span id="moTa">Mô Tả</span>
            </dt>
            <dd>{viTriKhoEntity.moTa}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={viTriKhoEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{viTriKhoEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={viTriKhoEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{viTriKhoEntity.nguoiCapNhat}</dd>
            <dt>Kho</dt>
            <dd>{viTriKhoEntity.khoId ? viTriKhoEntity.khoId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/vi-tri-kho" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/vi-tri-kho/${viTriKhoEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ viTriKho }: IRootState) => ({
  viTriKhoEntity: viTriKho.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViTriKhoDetail);
