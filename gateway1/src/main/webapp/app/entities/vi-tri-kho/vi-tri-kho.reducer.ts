import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IViTriKho, defaultValue } from 'app/shared/model/vi-tri-kho.model';

export const ACTION_TYPES = {
  FETCH_VITRIKHO_LIST: 'viTriKho/FETCH_VITRIKHO_LIST',
  FETCH_VITRIKHO: 'viTriKho/FETCH_VITRIKHO',
  CREATE_VITRIKHO: 'viTriKho/CREATE_VITRIKHO',
  UPDATE_VITRIKHO: 'viTriKho/UPDATE_VITRIKHO',
  DELETE_VITRIKHO: 'viTriKho/DELETE_VITRIKHO',
  RESET: 'viTriKho/RESET',
  GET_BY_KHO: 'viTriKho/GET_BY_KHO'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IViTriKho>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ViTriKhoState = Readonly<typeof initialState>;

// Reducer

export default (state: ViTriKhoState = initialState, action): ViTriKhoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_VITRIKHO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_VITRIKHO):
    case REQUEST(ACTION_TYPES.GET_BY_KHO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_VITRIKHO):
    case REQUEST(ACTION_TYPES.UPDATE_VITRIKHO):
    case REQUEST(ACTION_TYPES.DELETE_VITRIKHO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_VITRIKHO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_VITRIKHO):
    case FAILURE(ACTION_TYPES.CREATE_VITRIKHO):
    case FAILURE(ACTION_TYPES.UPDATE_VITRIKHO):
    case FAILURE(ACTION_TYPES.DELETE_VITRIKHO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_VITRIKHO_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_VITRIKHO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_VITRIKHO):
    case SUCCESS(ACTION_TYPES.UPDATE_VITRIKHO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_VITRIKHO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case SUCCESS(ACTION_TYPES.GET_BY_KHO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/vi-tri-khos';

// Actions

export const getEntities: ICrudGetAllAction<IViTriKho> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_VITRIKHO_LIST,
    payload: axios.get<IViTriKho>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IViTriKho> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_VITRIKHO,
    payload: axios.get<IViTriKho>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IViTriKho> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_VITRIKHO,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IViTriKho> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_VITRIKHO,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IViTriKho> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_VITRIKHO,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

export const getByKho = (page, size, sort) => {
  const requestUrl = `${apiUrl}/khoId${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_VITRIKHO_LIST,
    payload: axios.get<IViTriKho>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};
