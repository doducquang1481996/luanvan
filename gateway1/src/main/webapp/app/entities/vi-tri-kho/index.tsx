import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ViTriKho from './vi-tri-kho';
import ViTriKhoDetail from './vi-tri-kho-detail';
import ViTriKhoUpdate from './vi-tri-kho-update';
import ViTriKhoDeleteDialog from './vi-tri-kho-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ViTriKhoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ViTriKhoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ViTriKhoDetail} />
      <ErrorBoundaryRoute path={match.url} component={ViTriKho} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ViTriKhoDeleteDialog} />
  </>
);

export default Routes;
