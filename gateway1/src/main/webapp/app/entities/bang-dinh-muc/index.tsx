import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import BangDinhMuc from './bang-dinh-muc';
import BangDinhMucDetail from './bang-dinh-muc-detail';
import BangDinhMucUpdate from './bang-dinh-muc-update';
import BangDinhMucDeleteDialog from './bang-dinh-muc-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={BangDinhMucUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={BangDinhMucUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={BangDinhMucDetail} />
      <ErrorBoundaryRoute path={match.url} component={BangDinhMuc} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={BangDinhMucDeleteDialog} />
  </>
);

export default Routes;
