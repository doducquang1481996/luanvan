import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IBangDinhMuc, defaultValue } from 'app/shared/model/bang-dinh-muc.model';

export const ACTION_TYPES = {
  FETCH_BANGDINHMUC_LIST: 'bangDinhMuc/FETCH_BANGDINHMUC_LIST',
  FETCH_BANGDINHMUC: 'bangDinhMuc/FETCH_BANGDINHMUC',
  CREATE_BANGDINHMUC: 'bangDinhMuc/CREATE_BANGDINHMUC',
  UPDATE_BANGDINHMUC: 'bangDinhMuc/UPDATE_BANGDINHMUC',
  DELETE_BANGDINHMUC: 'bangDinhMuc/DELETE_BANGDINHMUC',
  RESET: 'bangDinhMuc/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IBangDinhMuc>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type BangDinhMucState = Readonly<typeof initialState>;

// Reducer

export default (state: BangDinhMucState = initialState, action): BangDinhMucState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_BANGDINHMUC_LIST):
    case REQUEST(ACTION_TYPES.FETCH_BANGDINHMUC):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_BANGDINHMUC):
    case REQUEST(ACTION_TYPES.UPDATE_BANGDINHMUC):
    case REQUEST(ACTION_TYPES.DELETE_BANGDINHMUC):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_BANGDINHMUC_LIST):
    case FAILURE(ACTION_TYPES.FETCH_BANGDINHMUC):
    case FAILURE(ACTION_TYPES.CREATE_BANGDINHMUC):
    case FAILURE(ACTION_TYPES.UPDATE_BANGDINHMUC):
    case FAILURE(ACTION_TYPES.DELETE_BANGDINHMUC):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_BANGDINHMUC_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_BANGDINHMUC):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_BANGDINHMUC):
    case SUCCESS(ACTION_TYPES.UPDATE_BANGDINHMUC):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_BANGDINHMUC):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'storehouse/api/bang-dinh-mucs';

// Actions

export const getEntities: ICrudGetAllAction<IBangDinhMuc> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_BANGDINHMUC_LIST,
  payload: axios.get<IBangDinhMuc>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IBangDinhMuc> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_BANGDINHMUC,
    payload: axios.get<IBangDinhMuc>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IBangDinhMuc> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_BANGDINHMUC,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IBangDinhMuc> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_BANGDINHMUC,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IBangDinhMuc> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_BANGDINHMUC,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
