import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './bang-dinh-muc.reducer';
import { IBangDinhMuc } from 'app/shared/model/bang-dinh-muc.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IBangDinhMucDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class BangDinhMucDetail extends React.Component<IBangDinhMucDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { bangDinhMucEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Bảng Đinh Mức [<b>{bangDinhMucEntity.ma}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="phieuMuaHangId">Phiếu Mua Hàng</span>
            </dt>
            <dd>{bangDinhMucEntity.phieuMuaHangId}</dd>
            <dt>
              <span id="ma">Mã</span>
            </dt>
            <dd>{bangDinhMucEntity.ma}</dd>
            <dt>
              <span id="trangThai">Trạng Thái</span>
            </dt>
            <dd>{bangDinhMucEntity.trangThai ? 'true' : 'false'}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={bangDinhMucEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{bangDinhMucEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={bangDinhMucEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{bangDinhMucEntity.nguoiCapNhat}</dd>
          </dl>
          <Button tag={Link} to="/entity/bang-dinh-muc" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/bang-dinh-muc/${bangDinhMucEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ bangDinhMuc }: IRootState) => ({
  bangDinhMucEntity: bangDinhMuc.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BangDinhMucDetail);
