import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IThongTinChiTietSanPham } from 'app/shared/model/thong-tin-chi-tiet-san-pham.model';
import { getEntities as getThongTinChiTietSanPhams } from 'app/entities/thong-tin-chi-tiet-san-pham/thong-tin-chi-tiet-san-pham.reducer';
import { getEntity, updateEntity, createEntity, reset } from './san-pham-thuoc-tinh.reducer';
import { ISanPhamThuocTinh } from 'app/shared/model/san-pham-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISanPhamThuocTinhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISanPhamThuocTinhUpdateState {
  isNew: boolean;
  thongTinChiTietSanPhamId: number;
}

export class SanPhamThuocTinhUpdate extends React.Component<ISanPhamThuocTinhUpdateProps, ISanPhamThuocTinhUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      thongTinChiTietSanPhamId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getThongTinChiTietSanPhams();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { sanPhamThuocTinhEntity } = this.props;
      const entity = {
        ...sanPhamThuocTinhEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/san-pham-thuoc-tinh');
  };

  render() {
    const { sanPhamThuocTinhEntity, thongTinChiTietSanPhams, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.sanPhamThuocTinh.home.createOrEditLabel">Create or edit a SanPhamThuocTinh</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : sanPhamThuocTinhEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="san-pham-thuoc-tinh-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="sanPhamIdLabel" for="sanPhamId">
                    San Pham Id
                  </Label>
                  <AvField id="san-pham-thuoc-tinh-sanPhamId" type="number" className="form-control" name="sanPhamId" />
                </AvGroup>
                <AvGroup>
                  <Label id="loaiThuocTinhIdLabel" for="loaiThuocTinhId">
                    Loai Thuoc Tinh Id
                  </Label>
                  <AvField id="san-pham-thuoc-tinh-loaiThuocTinhId" type="number" className="form-control" name="loaiThuocTinhId" />
                </AvGroup>
                <AvGroup>
                  <Label id="thuocTinhIdLabel" for="thuocTinhId">
                    Thuoc Tinh Id
                  </Label>
                  <AvField id="san-pham-thuoc-tinh-thuocTinhId" type="number" className="form-control" name="thuocTinhId" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="san-pham-thuoc-tinh-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="san-pham-thuoc-tinh-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label for="thongTinChiTietSanPham.id">Thong Tin Chi Tiet San Pham</Label>
                  <AvInput
                    id="san-pham-thuoc-tinh-thongTinChiTietSanPham"
                    type="select"
                    className="form-control"
                    name="thongTinChiTietSanPham.id"
                  >
                    <option value="" key="0" />
                    {thongTinChiTietSanPhams
                      ? thongTinChiTietSanPhams.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/san-pham-thuoc-tinh" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  thongTinChiTietSanPhams: storeState.thongTinChiTietSanPham.entities,
  sanPhamThuocTinhEntity: storeState.sanPhamThuocTinh.entity,
  loading: storeState.sanPhamThuocTinh.loading,
  updating: storeState.sanPhamThuocTinh.updating
});

const mapDispatchToProps = {
  getThongTinChiTietSanPhams,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamThuocTinhUpdate);
