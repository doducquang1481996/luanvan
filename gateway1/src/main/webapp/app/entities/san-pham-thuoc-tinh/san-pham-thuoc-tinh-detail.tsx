import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './san-pham-thuoc-tinh.reducer';
import { ISanPhamThuocTinh } from 'app/shared/model/san-pham-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISanPhamThuocTinhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class SanPhamThuocTinhDetail extends React.Component<ISanPhamThuocTinhDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { sanPhamThuocTinhEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            SanPhamThuocTinh [<b>{sanPhamThuocTinhEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="sanPhamId">San Pham Id</span>
            </dt>
            <dd>{sanPhamThuocTinhEntity.sanPhamId}</dd>
            <dt>
              <span id="loaiThuocTinhId">Loai Thuoc Tinh Id</span>
            </dt>
            <dd>{sanPhamThuocTinhEntity.loaiThuocTinhId}</dd>
            <dt>
              <span id="thuocTinhId">Thuoc Tinh Id</span>
            </dt>
            <dd>{sanPhamThuocTinhEntity.thuocTinhId}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={sanPhamThuocTinhEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{sanPhamThuocTinhEntity.nguoiTao}</dd>
            <dt>Thong Tin Chi Tiet San Pham</dt>
            <dd>{sanPhamThuocTinhEntity.thongTinChiTietSanPham ? sanPhamThuocTinhEntity.thongTinChiTietSanPham.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/san-pham-thuoc-tinh" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/san-pham-thuoc-tinh/${sanPhamThuocTinhEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ sanPhamThuocTinh }: IRootState) => ({
  sanPhamThuocTinhEntity: sanPhamThuocTinh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamThuocTinhDetail);
