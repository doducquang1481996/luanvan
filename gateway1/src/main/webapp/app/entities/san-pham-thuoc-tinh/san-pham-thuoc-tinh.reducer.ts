import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISanPhamThuocTinh, defaultValue } from 'app/shared/model/san-pham-thuoc-tinh.model';

export const ACTION_TYPES = {
  FETCH_SANPHAMTHUOCTINH_LIST: 'sanPhamThuocTinh/FETCH_SANPHAMTHUOCTINH_LIST',
  FETCH_SANPHAMTHUOCTINH: 'sanPhamThuocTinh/FETCH_SANPHAMTHUOCTINH',
  CREATE_SANPHAMTHUOCTINH: 'sanPhamThuocTinh/CREATE_SANPHAMTHUOCTINH',
  UPDATE_SANPHAMTHUOCTINH: 'sanPhamThuocTinh/UPDATE_SANPHAMTHUOCTINH',
  DELETE_SANPHAMTHUOCTINH: 'sanPhamThuocTinh/DELETE_SANPHAMTHUOCTINH',
  RESET: 'sanPhamThuocTinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISanPhamThuocTinh>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type SanPhamThuocTinhState = Readonly<typeof initialState>;

// Reducer

export default (state: SanPhamThuocTinhState = initialState, action): SanPhamThuocTinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SANPHAMTHUOCTINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SANPHAMTHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SANPHAMTHUOCTINH):
    case REQUEST(ACTION_TYPES.UPDATE_SANPHAMTHUOCTINH):
    case REQUEST(ACTION_TYPES.DELETE_SANPHAMTHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SANPHAMTHUOCTINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SANPHAMTHUOCTINH):
    case FAILURE(ACTION_TYPES.CREATE_SANPHAMTHUOCTINH):
    case FAILURE(ACTION_TYPES.UPDATE_SANPHAMTHUOCTINH):
    case FAILURE(ACTION_TYPES.DELETE_SANPHAMTHUOCTINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SANPHAMTHUOCTINH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SANPHAMTHUOCTINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SANPHAMTHUOCTINH):
    case SUCCESS(ACTION_TYPES.UPDATE_SANPHAMTHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SANPHAMTHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/san-pham-thuoc-tinhs';

// Actions

export const getEntities: ICrudGetAllAction<ISanPhamThuocTinh> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_SANPHAMTHUOCTINH_LIST,
  payload: axios.get<ISanPhamThuocTinh>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<ISanPhamThuocTinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SANPHAMTHUOCTINH,
    payload: axios.get<ISanPhamThuocTinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISanPhamThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SANPHAMTHUOCTINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISanPhamThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SANPHAMTHUOCTINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISanPhamThuocTinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SANPHAMTHUOCTINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
