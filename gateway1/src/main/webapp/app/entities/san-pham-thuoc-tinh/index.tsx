import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SanPhamThuocTinh from './san-pham-thuoc-tinh';
import SanPhamThuocTinhDetail from './san-pham-thuoc-tinh-detail';
import SanPhamThuocTinhUpdate from './san-pham-thuoc-tinh-update';
import SanPhamThuocTinhDeleteDialog from './san-pham-thuoc-tinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SanPhamThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SanPhamThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SanPhamThuocTinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={SanPhamThuocTinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SanPhamThuocTinhDeleteDialog} />
  </>
);

export default Routes;
