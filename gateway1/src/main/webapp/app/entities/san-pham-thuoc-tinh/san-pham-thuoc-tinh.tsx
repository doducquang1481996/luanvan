import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './san-pham-thuoc-tinh.reducer';
import { ISanPhamThuocTinh } from 'app/shared/model/san-pham-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISanPhamThuocTinhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class SanPhamThuocTinh extends React.Component<ISanPhamThuocTinhProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { sanPhamThuocTinhList, match } = this.props;
    return (
      <div>
        <h2 id="san-pham-thuoc-tinh-heading">
          San Pham Thuoc Tinhs
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new San Pham Thuoc Tinh
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>San Pham Id</th>
                <th>Loai Thuoc Tinh Id</th>
                <th>Thuoc Tinh Id</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th>Thong Tin Chi Tiet San Pham</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {sanPhamThuocTinhList.map((sanPhamThuocTinh, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${sanPhamThuocTinh.id}`} color="link" size="sm">
                      {sanPhamThuocTinh.id}
                    </Button>
                  </td>
                  <td>{sanPhamThuocTinh.sanPhamId}</td>
                  <td>{sanPhamThuocTinh.loaiThuocTinhId}</td>
                  <td>{sanPhamThuocTinh.thuocTinhId}</td>
                  <td>
                    <TextFormat type="date" value={sanPhamThuocTinh.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{sanPhamThuocTinh.nguoiTao}</td>
                  <td>
                    {sanPhamThuocTinh.thongTinChiTietSanPham ? (
                      <Link to={`thong-tin-chi-tiet-san-pham/${sanPhamThuocTinh.thongTinChiTietSanPham.id}`}>
                        {sanPhamThuocTinh.thongTinChiTietSanPham.id}
                      </Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${sanPhamThuocTinh.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${sanPhamThuocTinh.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${sanPhamThuocTinh.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ sanPhamThuocTinh }: IRootState) => ({
  sanPhamThuocTinhList: sanPhamThuocTinh.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamThuocTinh);
