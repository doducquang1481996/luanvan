import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, setFileData, openFile, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IRoom } from 'app/shared/model/room.model';
import { getEntities as getRooms } from 'app/entities/room/room.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './employee.reducer';
import { getSession } from 'app/shared/reducers/authentication';
import { IEmployee } from 'app/shared/model/employee.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IEmployeeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IEmployeeUpdateState {
  isNew: boolean;
  roomId: number;
}

export class EmployeeUpdate extends React.Component<IEmployeeUpdateProps, IEmployeeUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      roomId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
    this.props.getSession();
    this.props.getRooms();
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { employeeEntity } = this.props;
      const entity = {
        ...employeeEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/employee');
  };

  render() {
    const { employeeEntity, rooms, loading, updating } = this.props;
    const { isNew } = this.state;
    const { account } = this.props;

    const { image, imageContentType } = employeeEntity;

    return (
      <div>
        {!isNew ? (
          <Row className="justify-content-center">
            <Col md="8">
              <h2 id="gatewayApp.employee.home.createOrEditLabel">Cập Nhật Thông Tin Nhân Viên</h2>
            </Col>
          </Row>
        ) : (
          <Row className="justify-content-center">
            <Col md="8">
              <h2 id="gatewayApp.employee.home.createOrEditLabel">Tạo Mới Thông Tin Nhân Viên</h2>
            </Col>
          </Row>
        )}
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : employeeEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="employee-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="employeeNameLabel" for="employeeName">
                    Tên Nhân Viên:
                  </Label>
                  <AvField id="employee-employeeName" type="text" name="employeeName" />
                </AvGroup>
                <AvGroup>
                  <Label id="employeeCodeLabel" for="employeeCode">
                    Mã Nhân Viên:
                  </Label>
                  <AvField id="employee-employeeCode" type="text" name="employeeCode" />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="email">
                    Email
                  </Label>
                  <AvField id="employee-email" type="text" name="email" />
                </AvGroup>
                <AvGroup>
                  <Label id="dateBirthLabel" for="dateBirth">
                    Ngày Sinh:
                  </Label>
                  <AvField id="employee-dateBirth" type="date" className="form-control" name="dateBirth" />
                </AvGroup>
                <AvGroup>
                  <Label id="cmndLabel" for="cmnd">
                    Số CMND
                  </Label>
                  <AvField id="employee-cmnd" type="text" name="cmnd" />
                </AvGroup>
                <AvGroup>
                  <Label id="numberPhoneLabel" for="numberPhone">
                    Số Điện Thoại:
                  </Label>
                  <AvField id="employee-numberPhone" type="text" name="numberPhone" />
                </AvGroup>
                <AvGroup>
                  <AvGroup>
                    <Label id="imageLabel" for="image">
                      Hình Ảnh
                    </Label>
                    <br />
                    {image ? (
                      <div>
                        <a onClick={openFile(imageContentType, image)}>Open</a>
                        <br />
                        <Row>
                          <Col md="11">
                            <span>
                              {imageContentType}, {byteSize(image)}
                            </span>
                          </Col>
                          <Col md="1">
                            <Button color="danger" onClick={this.clearBlob('image')}>
                              <FontAwesomeIcon icon="times-circle" />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ) : null}
                    <input id="file_image" type="file" onChange={this.onBlobChange(false, 'image')} />
                    <AvInput type="hidden" name="image" value={image} />
                  </AvGroup>
                </AvGroup>
                <AvGroup>
                  <Label id="locationLabel" for="location">
                    Địa Chỉ Thường Trú:
                  </Label>
                  <AvField id="employee-location" type="text" name="location" />
                </AvGroup>
                <AvGroup>
                  <Label id="placeOfBirthLabel" for="placeOfBirth">
                    Nơi Sinh:
                  </Label>
                  <AvField id="employee-placeOfBirth" type="text" name="placeOfBirth" />
                </AvGroup>
                <AvGroup>
                  <Label id="sexLabel" for="sex">
                    Giới Tính :
                  </Label>
                  <AvInput id="employee-room" type="select" className="form-control" name="sex">
                    <option value="Nam" key="Nam">
                      Nam
                    </option>
                    <option value="Nữ" key="Nữ">
                      Nữ
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="dayToDoLabel" for="dayToDo">
                    Ngày Vào Làm:
                  </Label>
                  <AvField id="employee-dayToDo" type="date" className="form-control" name="dayToDo" />
                </AvGroup>
                <AvGroup>
                  <AvInput id="employee-active" type="hidden" className="form-control" name="active" value="true" />
                </AvGroup>
                {!isNew ? (
                  <AvGroup>
                    <Label id="updatePersonLabel">Người Cập Nhật</Label>
                    <AvField type="text" name="updatePerson" value={account.login} required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="updatePersonLabel">Người Tạo</Label>
                  {!isNew ? (
                    <AvField id="employee-craetePerson" type="text" className="form-control" name="createPerson" required readOnly />
                  ) : (
                    <AvField type="text" name="createPerson" className="form-control" value={account.login} required readOnly />
                  )}
                </AvGroup>
                <AvGroup>
                  <Label for="room.id">Phòng Ban</Label>
                  <AvInput id="employee-room" type="select" className="form-control" name="roomId">
                    <option value="" key="0" />
                    {rooms
                      ? rooms.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.roomName}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/employee" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Trở Lại</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Lưu
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  rooms: storeState.room.entities,
  employeeEntity: storeState.employee.entity,
  loading: storeState.employee.loading,
  updating: storeState.employee.updating,
  account: storeState.authentication.account
});

const mapDispatchToProps = {
  getRooms,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
  getSession
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeUpdate);
