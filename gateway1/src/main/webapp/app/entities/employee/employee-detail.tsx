import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './employee.reducer';
import { IEmployee } from 'app/shared/model/employee.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEmployeeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class EmployeeDetail extends React.Component<IEmployeeDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { employeeEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Employee [<b>{employeeEntity.employeeCode}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="employeeName">Tên Nhân Viên</span>
            </dt>
            <dd>{employeeEntity.employeeName}</dd>
            <dt>
              <span id="employeeCode">Mã Nhân Viên</span>
            </dt>
            <dd>{employeeEntity.employeeCode}</dd>
            <dt>
              <span id="email">Email</span>
            </dt>
            <dd>{employeeEntity.email}</dd>
            <dt>
              <span id="dateBirth">Date Birth</span>
            </dt>
            <dd>
              <TextFormat value={employeeEntity.dateBirth} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="cmnd">CMND</span>
            </dt>
            <dd>{employeeEntity.cmnd}</dd>
            <dt>
              <span id="numberPhone">Số Điện Thoại</span>
            </dt>
            <dd>{employeeEntity.numberPhone}</dd>
            <dt>
              <span id="image">Hình Ảnh</span>
            </dt>
            <dd>
              {employeeEntity.image ? (
                <div>
                  <a onClick={openFile(employeeEntity.imageContentType, employeeEntity.image)}>Xem Ảnh&nbsp;</a>
                  <span>
                    {employeeEntity.imageContentType}, {byteSize(employeeEntity.image)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="location">Địa chỉ</span>
            </dt>
            <dd>{employeeEntity.location}</dd>
            <dt>
              <span id="placeOfBirth">Ngày Sinh</span>
            </dt>
            <dd>{employeeEntity.placeOfBirth}</dd>
            <dt>
              <span id="sex">Giới Tính</span>
            </dt>
            <dd>{employeeEntity.sex}</dd>
            <dt>
              <span id="dayToDo">Ngày Vào Làm</span>
            </dt>
            <dd>
              <TextFormat value={employeeEntity.dayToDo} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="active">Trạng Thái</span>
            </dt>
            <dd>{employeeEntity.active ? 'true' : 'false'}</dd>
            <dt>
              <span id="createDate">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={employeeEntity.createDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="createPerson">Người Tạo</span>
            </dt>
            <dd>{employeeEntity.createPerson}</dd>
            <dt>
              <span id="updateDate">Ngày Cập Nhật Mới</span>
            </dt>
            <dd>
              <TextFormat value={employeeEntity.updateDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="updatePerson">Người Cập Nhật</span>
            </dt>
            <dd>{employeeEntity.updatePerson}</dd>
            <dt>Phòng Ban</dt>
            <dd>{employeeEntity.roomId ? employeeEntity.roomId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/employee" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/employee/${employeeEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ employee }: IRootState) => ({
  employeeEntity: employee.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeDetail);
