import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IChiTietGiamDinh, defaultValue } from 'app/shared/model/chi-tiet-giam-dinh.model';

export const ACTION_TYPES = {
  FETCH_CHITIETGIAMDINH_LIST: 'chiTietGiamDinh/FETCH_CHITIETGIAMDINH_LIST',
  FETCH_CHITIETGIAMDINH: 'chiTietGiamDinh/FETCH_CHITIETGIAMDINH',
  CREATE_CHITIETGIAMDINH: 'chiTietGiamDinh/CREATE_CHITIETGIAMDINH',
  UPDATE_CHITIETGIAMDINH: 'chiTietGiamDinh/UPDATE_CHITIETGIAMDINH',
  DELETE_CHITIETGIAMDINH: 'chiTietGiamDinh/DELETE_CHITIETGIAMDINH',
  RESET: 'chiTietGiamDinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IChiTietGiamDinh>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ChiTietGiamDinhState = Readonly<typeof initialState>;

// Reducer

export default (state: ChiTietGiamDinhState = initialState, action): ChiTietGiamDinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CHITIETGIAMDINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CHITIETGIAMDINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CHITIETGIAMDINH):
    case REQUEST(ACTION_TYPES.UPDATE_CHITIETGIAMDINH):
    case REQUEST(ACTION_TYPES.DELETE_CHITIETGIAMDINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CHITIETGIAMDINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CHITIETGIAMDINH):
    case FAILURE(ACTION_TYPES.CREATE_CHITIETGIAMDINH):
    case FAILURE(ACTION_TYPES.UPDATE_CHITIETGIAMDINH):
    case FAILURE(ACTION_TYPES.DELETE_CHITIETGIAMDINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHITIETGIAMDINH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHITIETGIAMDINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CHITIETGIAMDINH):
    case SUCCESS(ACTION_TYPES.UPDATE_CHITIETGIAMDINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CHITIETGIAMDINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'storehouse/api/chi-tiet-giam-dinhs';

// Actions

export const getEntities: ICrudGetAllAction<IChiTietGiamDinh> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_CHITIETGIAMDINH_LIST,
  payload: axios.get<IChiTietGiamDinh>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IChiTietGiamDinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CHITIETGIAMDINH,
    payload: axios.get<IChiTietGiamDinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IChiTietGiamDinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CHITIETGIAMDINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IChiTietGiamDinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CHITIETGIAMDINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IChiTietGiamDinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHITIETGIAMDINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
