import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IChiTietPhieuGiamDinh } from 'app/shared/model/chi-tiet-phieu-giam-dinh.model';
import { getEntities as getChiTietPhieuGiamDinhs } from 'app/entities/chi-tiet-phieu-giam-dinh/chi-tiet-phieu-giam-dinh.reducer';
import { getEntity, updateEntity, createEntity, reset } from './chi-tiet-giam-dinh.reducer';
import { IChiTietGiamDinh } from 'app/shared/model/chi-tiet-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IChiTietGiamDinhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IChiTietGiamDinhUpdateState {
  isNew: boolean;
  chiTietPhieuGiamDinhId: number;
}

export class ChiTietGiamDinhUpdate extends React.Component<IChiTietGiamDinhUpdateProps, IChiTietGiamDinhUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      chiTietPhieuGiamDinhId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getChiTietPhieuGiamDinhs();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { chiTietGiamDinhEntity } = this.props;
      const entity = {
        ...chiTietGiamDinhEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/chi-tiet-giam-dinh');
  };

  render() {
    const { chiTietGiamDinhEntity, chiTietPhieuGiamDinhs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.chiTietGiamDinh.home.createOrEditLabel">Create or edit a ChiTietGiamDinh</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : chiTietGiamDinhEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="chi-tiet-giam-dinh-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="soLuongLabel" for="soLuong">
                    So Luong
                  </Label>
                  <AvField id="chi-tiet-giam-dinh-soLuong" type="number" className="form-control" name="soLuong" />
                </AvGroup>
                <AvGroup>
                  <Label id="datYeuCauLabel" for="datYeuCau">
                    Dat Yeu Cau
                  </Label>
                  <AvField id="chi-tiet-giam-dinh-datYeuCau" type="number" className="form-control" name="datYeuCau" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="chi-tiet-giam-dinh-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="chi-tiet-giam-dinh-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="chi-tiet-giam-dinh-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="chi-tiet-giam-dinh-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label for="chiTietPhieuGiamDinh.id">Chi Tiet Phieu Giam Dinh</Label>
                  <AvInput
                    id="chi-tiet-giam-dinh-chiTietPhieuGiamDinh"
                    type="select"
                    className="form-control"
                    name="chiTietPhieuGiamDinh.id"
                  >
                    <option value="" key="0" />
                    {chiTietPhieuGiamDinhs
                      ? chiTietPhieuGiamDinhs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/chi-tiet-giam-dinh" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  chiTietPhieuGiamDinhs: storeState.chiTietPhieuGiamDinh.entities,
  chiTietGiamDinhEntity: storeState.chiTietGiamDinh.entity,
  loading: storeState.chiTietGiamDinh.loading,
  updating: storeState.chiTietGiamDinh.updating
});

const mapDispatchToProps = {
  getChiTietPhieuGiamDinhs,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietGiamDinhUpdate);
