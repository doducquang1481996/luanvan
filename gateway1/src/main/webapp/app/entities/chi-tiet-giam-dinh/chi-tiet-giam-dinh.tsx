import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './chi-tiet-giam-dinh.reducer';
import { IChiTietGiamDinh } from 'app/shared/model/chi-tiet-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChiTietGiamDinhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class ChiTietGiamDinh extends React.Component<IChiTietGiamDinhProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { chiTietGiamDinhList, match } = this.props;
    return (
      <div>
        <h2 id="chi-tiet-giam-dinh-heading">
          Chi Tiet Giam Dinhs
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Chi Tiet Giam Dinh
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>So Luong</th>
                <th>Dat Yeu Cau</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th>Ngay Cap Nhat</th>
                <th>Nguoi Cap Nhat</th>
                <th>Chi Tiet Phieu Giam Dinh</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {chiTietGiamDinhList.map((chiTietGiamDinh, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${chiTietGiamDinh.id}`} color="link" size="sm">
                      {chiTietGiamDinh.id}
                    </Button>
                  </td>
                  <td>{chiTietGiamDinh.soLuong}</td>
                  <td>{chiTietGiamDinh.datYeuCau}</td>
                  <td>
                    <TextFormat type="date" value={chiTietGiamDinh.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{chiTietGiamDinh.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={chiTietGiamDinh.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{chiTietGiamDinh.nguoiCapNhat}</td>
                  <td>
                    {chiTietGiamDinh.chiTietPhieuGiamDinh ? (
                      <Link to={`chi-tiet-phieu-giam-dinh/${chiTietGiamDinh.chiTietPhieuGiamDinh.id}`}>
                        {chiTietGiamDinh.chiTietPhieuGiamDinh.id}
                      </Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${chiTietGiamDinh.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chiTietGiamDinh.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chiTietGiamDinh.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ chiTietGiamDinh }: IRootState) => ({
  chiTietGiamDinhList: chiTietGiamDinh.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietGiamDinh);
