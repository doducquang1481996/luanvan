import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ChiTietGiamDinh from './chi-tiet-giam-dinh';
import ChiTietGiamDinhDetail from './chi-tiet-giam-dinh-detail';
import ChiTietGiamDinhUpdate from './chi-tiet-giam-dinh-update';
import ChiTietGiamDinhDeleteDialog from './chi-tiet-giam-dinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ChiTietGiamDinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ChiTietGiamDinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ChiTietGiamDinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={ChiTietGiamDinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ChiTietGiamDinhDeleteDialog} />
  </>
);

export default Routes;
