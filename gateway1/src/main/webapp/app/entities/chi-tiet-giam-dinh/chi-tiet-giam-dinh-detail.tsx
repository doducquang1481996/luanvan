import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './chi-tiet-giam-dinh.reducer';
import { IChiTietGiamDinh } from 'app/shared/model/chi-tiet-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChiTietGiamDinhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ChiTietGiamDinhDetail extends React.Component<IChiTietGiamDinhDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { chiTietGiamDinhEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            ChiTietGiamDinh [<b>{chiTietGiamDinhEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="soLuong">So Luong</span>
            </dt>
            <dd>{chiTietGiamDinhEntity.soLuong}</dd>
            <dt>
              <span id="datYeuCau">Dat Yeu Cau</span>
            </dt>
            <dd>{chiTietGiamDinhEntity.datYeuCau}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={chiTietGiamDinhEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{chiTietGiamDinhEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngay Cap Nhat</span>
            </dt>
            <dd>
              <TextFormat value={chiTietGiamDinhEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Nguoi Cap Nhat</span>
            </dt>
            <dd>{chiTietGiamDinhEntity.nguoiCapNhat}</dd>
            <dt>Chi Tiet Phieu Giam Dinh</dt>
            <dd>{chiTietGiamDinhEntity.chiTietPhieuGiamDinh ? chiTietGiamDinhEntity.chiTietPhieuGiamDinh.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/chi-tiet-giam-dinh" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/chi-tiet-giam-dinh/${chiTietGiamDinhEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ chiTietGiamDinh }: IRootState) => ({
  chiTietGiamDinhEntity: chiTietGiamDinh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietGiamDinhDetail);
