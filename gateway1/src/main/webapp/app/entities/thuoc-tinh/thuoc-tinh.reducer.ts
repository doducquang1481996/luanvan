import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IThuocTinh, defaultValue } from 'app/shared/model/thuoc-tinh.model';

export const ACTION_TYPES = {
  FETCH_THUOCTINH_LIST: 'thuocTinh/FETCH_THUOCTINH_LIST',
  FETCH_THUOCTINH: 'thuocTinh/FETCH_THUOCTINH',
  CREATE_THUOCTINH: 'thuocTinh/CREATE_THUOCTINH',
  UPDATE_THUOCTINH: 'thuocTinh/UPDATE_THUOCTINH',
  DELETE_THUOCTINH: 'thuocTinh/DELETE_THUOCTINH',
  RESET: 'thuocTinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IThuocTinh>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ThuocTinhState = Readonly<typeof initialState>;

// Reducer

export default (state: ThuocTinhState = initialState, action): ThuocTinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_THUOCTINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_THUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_THUOCTINH):
    case REQUEST(ACTION_TYPES.UPDATE_THUOCTINH):
    case REQUEST(ACTION_TYPES.DELETE_THUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_THUOCTINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_THUOCTINH):
    case FAILURE(ACTION_TYPES.CREATE_THUOCTINH):
    case FAILURE(ACTION_TYPES.UPDATE_THUOCTINH):
    case FAILURE(ACTION_TYPES.DELETE_THUOCTINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_THUOCTINH_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_THUOCTINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_THUOCTINH):
    case SUCCESS(ACTION_TYPES.UPDATE_THUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_THUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/thuoc-tinhs';

// Actions

export const getEntities: ICrudGetAllAction<IThuocTinh> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_THUOCTINH_LIST,
    payload: axios.get<IThuocTinh>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IThuocTinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_THUOCTINH,
    payload: axios.get<IThuocTinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_THUOCTINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_THUOCTINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IThuocTinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_THUOCTINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
