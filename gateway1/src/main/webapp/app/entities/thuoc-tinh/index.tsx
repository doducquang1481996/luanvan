import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ThuocTinh from './thuoc-tinh';
import ThuocTinhDetail from './thuoc-tinh-detail';
import ThuocTinhUpdate from './thuoc-tinh-update';
import ThuocTinhDeleteDialog from './thuoc-tinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ThuocTinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={ThuocTinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ThuocTinhDeleteDialog} />
  </>
);

export default Routes;
