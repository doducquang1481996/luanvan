import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ILoaiThuocTinh } from 'app/shared/model/loai-thuoc-tinh.model';
import { getEntities as getLoaiThuocTinhs } from 'app/entities/loai-thuoc-tinh/loai-thuoc-tinh.reducer';
import { getEntity, updateEntity, createEntity, reset } from './thuoc-tinh.reducer';
import { IThuocTinh } from 'app/shared/model/thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IThuocTinhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IThuocTinhUpdateState {
  isNew: boolean;
  loaiThuocTinhId: number;
}

export class ThuocTinhUpdate extends React.Component<IThuocTinhUpdateProps, IThuocTinhUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      loaiThuocTinhId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getLoaiThuocTinhs();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { thuocTinhEntity } = this.props;
      const entity = {
        ...thuocTinhEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/thuoc-tinh');
  };

  render() {
    const { thuocTinhEntity, loaiThuocTinhs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            {!isNew ? (
              <h2 id="gatewayApp.thuocTinh.home.createOrEditLabel">Cập nhật thuộc tính</h2>
            ) : (
              <h2 id="gatewayApp.thuocTinh.home.createOrEditLabel">Tạo mới thuộc tính</h2>
            )};
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : thuocTinhEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="thuoc-tinh-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Tên Thuộc Tính
                  </Label>
                  <AvField id="thuoc-tinh-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Mã Thuộc Tính
                  </Label>
                  <AvField id="thuoc-tinh-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="moTaLabel" for="moTa">
                    Mô Tả
                  </Label>
                  <AvField id="thuoc-tinh-moTa" type="text" name="moTa" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngày Tạo
                  </Label>
                  <AvField id="thuoc-tinh-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Người Tạo
                  </Label>
                  <AvField id="thuoc-tinh-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngày Cập Nhật
                  </Label>
                  <AvField id="thuoc-tinh-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Người Cập Nhật
                  </Label>
                  <AvField id="thuoc-tinh-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label for="loaiThuocTinh.id">Loại Thuốc Tính</Label>
                  <AvInput id="thuoc-tinh-loaiThuocTinh" type="select" className="form-control" name="loaiThuocTinhId">
                    <option value="" key="0" />
                    {loaiThuocTinhs
                      ? loaiThuocTinhs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.ten}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/thuoc-tinh" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Trở Lại</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Lưu
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  loaiThuocTinhs: storeState.loaiThuocTinh.entities,
  thuocTinhEntity: storeState.thuocTinh.entity,
  loading: storeState.thuocTinh.loading,
  updating: storeState.thuocTinh.updating
});

const mapDispatchToProps = {
  getLoaiThuocTinhs,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThuocTinhUpdate);
