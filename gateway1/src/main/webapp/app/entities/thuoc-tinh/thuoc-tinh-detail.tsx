import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './thuoc-tinh.reducer';
import { IThuocTinh } from 'app/shared/model/thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IThuocTinhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ThuocTinhDetail extends React.Component<IThuocTinhDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { thuocTinhEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Thông Tin Thuộc Tính [<b>{thuocTinhEntity.ten}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Thuộc Tính</span>
            </dt>
            <dd>{thuocTinhEntity.ten}</dd>
            <dt>
              <span id="ma">Mã Thuộc Tính</span>
            </dt>
            <dd>{thuocTinhEntity.ma}</dd>
            <dt>
              <span id="moTa">Mô Tả</span>
            </dt>
            <dd>{thuocTinhEntity.moTa}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={thuocTinhEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{thuocTinhEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={thuocTinhEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{thuocTinhEntity.nguoiCapNhat}</dd>
            <dt>Loại Thuộc Tính</dt>
            <dd>{thuocTinhEntity.loaiThuocTinhId ? thuocTinhEntity.loaiThuocTinhId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/thuoc-tinh" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/thuoc-tinh/${thuocTinhEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ thuocTinh }: IRootState) => ({
  thuocTinhEntity: thuocTinh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThuocTinhDetail);
