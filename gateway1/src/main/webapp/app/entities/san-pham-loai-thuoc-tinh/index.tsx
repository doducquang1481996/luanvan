import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SanPhamLoaiThuocTinh from './san-pham-loai-thuoc-tinh';
import SanPhamLoaiThuocTinhDetail from './san-pham-loai-thuoc-tinh-detail';
import SanPhamLoaiThuocTinhUpdate from './san-pham-loai-thuoc-tinh-update';
import SanPhamLoaiThuocTinhDeleteDialog from './san-pham-loai-thuoc-tinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SanPhamLoaiThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SanPhamLoaiThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SanPhamLoaiThuocTinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={SanPhamLoaiThuocTinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SanPhamLoaiThuocTinhDeleteDialog} />
  </>
);

export default Routes;
