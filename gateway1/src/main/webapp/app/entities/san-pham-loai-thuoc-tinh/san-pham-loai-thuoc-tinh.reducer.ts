import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISanPhamLoaiThuocTinh, defaultValue } from 'app/shared/model/san-pham-loai-thuoc-tinh.model';

export const ACTION_TYPES = {
  FETCH_SANPHAMLOAITHUOCTINH_LIST: 'sanPhamLoaiThuocTinh/FETCH_SANPHAMLOAITHUOCTINH_LIST',
  FETCH_SANPHAMLOAITHUOCTINH: 'sanPhamLoaiThuocTinh/FETCH_SANPHAMLOAITHUOCTINH',
  CREATE_SANPHAMLOAITHUOCTINH: 'sanPhamLoaiThuocTinh/CREATE_SANPHAMLOAITHUOCTINH',
  UPDATE_SANPHAMLOAITHUOCTINH: 'sanPhamLoaiThuocTinh/UPDATE_SANPHAMLOAITHUOCTINH',
  DELETE_SANPHAMLOAITHUOCTINH: 'sanPhamLoaiThuocTinh/DELETE_SANPHAMLOAITHUOCTINH',
  RESET: 'sanPhamLoaiThuocTinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISanPhamLoaiThuocTinh>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type SanPhamLoaiThuocTinhState = Readonly<typeof initialState>;

// Reducer

export default (state: SanPhamLoaiThuocTinhState = initialState, action): SanPhamLoaiThuocTinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SANPHAMLOAITHUOCTINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SANPHAMLOAITHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SANPHAMLOAITHUOCTINH):
    case REQUEST(ACTION_TYPES.UPDATE_SANPHAMLOAITHUOCTINH):
    case REQUEST(ACTION_TYPES.DELETE_SANPHAMLOAITHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SANPHAMLOAITHUOCTINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SANPHAMLOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.CREATE_SANPHAMLOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.UPDATE_SANPHAMLOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.DELETE_SANPHAMLOAITHUOCTINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SANPHAMLOAITHUOCTINH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SANPHAMLOAITHUOCTINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SANPHAMLOAITHUOCTINH):
    case SUCCESS(ACTION_TYPES.UPDATE_SANPHAMLOAITHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SANPHAMLOAITHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/san-pham-loai-thuoc-tinhs';

// Actions

export const getEntities: ICrudGetAllAction<ISanPhamLoaiThuocTinh> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_SANPHAMLOAITHUOCTINH_LIST,
  payload: axios.get<ISanPhamLoaiThuocTinh>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<ISanPhamLoaiThuocTinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SANPHAMLOAITHUOCTINH,
    payload: axios.get<ISanPhamLoaiThuocTinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISanPhamLoaiThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SANPHAMLOAITHUOCTINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISanPhamLoaiThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SANPHAMLOAITHUOCTINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISanPhamLoaiThuocTinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SANPHAMLOAITHUOCTINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
