import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './san-pham-loai-thuoc-tinh.reducer';
import { ISanPhamLoaiThuocTinh } from 'app/shared/model/san-pham-loai-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISanPhamLoaiThuocTinhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISanPhamLoaiThuocTinhUpdateState {
  isNew: boolean;
}

export class SanPhamLoaiThuocTinhUpdate extends React.Component<ISanPhamLoaiThuocTinhUpdateProps, ISanPhamLoaiThuocTinhUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { sanPhamLoaiThuocTinhEntity } = this.props;
      const entity = {
        ...sanPhamLoaiThuocTinhEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/san-pham-loai-thuoc-tinh');
  };

  render() {
    const { sanPhamLoaiThuocTinhEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.sanPhamLoaiThuocTinh.home.createOrEditLabel">Create or edit a SanPhamLoaiThuocTinh</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : sanPhamLoaiThuocTinhEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="san-pham-loai-thuoc-tinh-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="sanPhamIdLabel" for="sanPhamId">
                    San Pham Id
                  </Label>
                  <AvField id="san-pham-loai-thuoc-tinh-sanPhamId" type="number" className="form-control" name="sanPhamId" />
                </AvGroup>
                <AvGroup>
                  <Label id="loaiThuocTinhIdLabel" for="loaiThuocTinhId">
                    Loai Thuoc Tinh Id
                  </Label>
                  <AvField id="san-pham-loai-thuoc-tinh-loaiThuocTinhId" type="number" className="form-control" name="loaiThuocTinhId" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="san-pham-loai-thuoc-tinh-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="san-pham-loai-thuoc-tinh-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/san-pham-loai-thuoc-tinh" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  sanPhamLoaiThuocTinhEntity: storeState.sanPhamLoaiThuocTinh.entity,
  loading: storeState.sanPhamLoaiThuocTinh.loading,
  updating: storeState.sanPhamLoaiThuocTinh.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamLoaiThuocTinhUpdate);
