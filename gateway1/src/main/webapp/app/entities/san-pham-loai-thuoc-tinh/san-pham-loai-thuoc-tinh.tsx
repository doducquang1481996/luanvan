import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './san-pham-loai-thuoc-tinh.reducer';
import { ISanPhamLoaiThuocTinh } from 'app/shared/model/san-pham-loai-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISanPhamLoaiThuocTinhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class SanPhamLoaiThuocTinh extends React.Component<ISanPhamLoaiThuocTinhProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { sanPhamLoaiThuocTinhList, match } = this.props;
    return (
      <div>
        <h2 id="san-pham-loai-thuoc-tinh-heading">
          San Pham Loai Thuoc Tinhs
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new San Pham Loai Thuoc Tinh
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>San Pham Id</th>
                <th>Loai Thuoc Tinh Id</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {sanPhamLoaiThuocTinhList.map((sanPhamLoaiThuocTinh, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${sanPhamLoaiThuocTinh.id}`} color="link" size="sm">
                      {sanPhamLoaiThuocTinh.id}
                    </Button>
                  </td>
                  <td>{sanPhamLoaiThuocTinh.sanPhamId}</td>
                  <td>{sanPhamLoaiThuocTinh.loaiThuocTinhId}</td>
                  <td>
                    <TextFormat type="date" value={sanPhamLoaiThuocTinh.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{sanPhamLoaiThuocTinh.nguoiTao}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${sanPhamLoaiThuocTinh.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${sanPhamLoaiThuocTinh.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${sanPhamLoaiThuocTinh.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ sanPhamLoaiThuocTinh }: IRootState) => ({
  sanPhamLoaiThuocTinhList: sanPhamLoaiThuocTinh.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamLoaiThuocTinh);
