import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IChiTietPhieuGiamDinh, defaultValue } from 'app/shared/model/chi-tiet-phieu-giam-dinh.model';

export const ACTION_TYPES = {
  FETCH_CHITIETPHIEUGIAMDINH_LIST: 'chiTietPhieuGiamDinh/FETCH_CHITIETPHIEUGIAMDINH_LIST',
  FETCH_CHITIETPHIEUGIAMDINH: 'chiTietPhieuGiamDinh/FETCH_CHITIETPHIEUGIAMDINH',
  CREATE_CHITIETPHIEUGIAMDINH: 'chiTietPhieuGiamDinh/CREATE_CHITIETPHIEUGIAMDINH',
  UPDATE_CHITIETPHIEUGIAMDINH: 'chiTietPhieuGiamDinh/UPDATE_CHITIETPHIEUGIAMDINH',
  DELETE_CHITIETPHIEUGIAMDINH: 'chiTietPhieuGiamDinh/DELETE_CHITIETPHIEUGIAMDINH',
  RESET: 'chiTietPhieuGiamDinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IChiTietPhieuGiamDinh>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ChiTietPhieuGiamDinhState = Readonly<typeof initialState>;

// Reducer

export default (state: ChiTietPhieuGiamDinhState = initialState, action): ChiTietPhieuGiamDinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CHITIETPHIEUGIAMDINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CHITIETPHIEUGIAMDINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CHITIETPHIEUGIAMDINH):
    case REQUEST(ACTION_TYPES.UPDATE_CHITIETPHIEUGIAMDINH):
    case REQUEST(ACTION_TYPES.DELETE_CHITIETPHIEUGIAMDINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CHITIETPHIEUGIAMDINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CHITIETPHIEUGIAMDINH):
    case FAILURE(ACTION_TYPES.CREATE_CHITIETPHIEUGIAMDINH):
    case FAILURE(ACTION_TYPES.UPDATE_CHITIETPHIEUGIAMDINH):
    case FAILURE(ACTION_TYPES.DELETE_CHITIETPHIEUGIAMDINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHITIETPHIEUGIAMDINH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHITIETPHIEUGIAMDINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CHITIETPHIEUGIAMDINH):
    case SUCCESS(ACTION_TYPES.UPDATE_CHITIETPHIEUGIAMDINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CHITIETPHIEUGIAMDINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'storehouse/api/chi-tiet-phieu-giam-dinhs';

// Actions

export const getEntities: ICrudGetAllAction<IChiTietPhieuGiamDinh> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_CHITIETPHIEUGIAMDINH_LIST,
  payload: axios.get<IChiTietPhieuGiamDinh>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IChiTietPhieuGiamDinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CHITIETPHIEUGIAMDINH,
    payload: axios.get<IChiTietPhieuGiamDinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IChiTietPhieuGiamDinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CHITIETPHIEUGIAMDINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IChiTietPhieuGiamDinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CHITIETPHIEUGIAMDINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IChiTietPhieuGiamDinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHITIETPHIEUGIAMDINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
