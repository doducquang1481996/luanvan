import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ChiTietPhieuGiamDinh from './chi-tiet-phieu-giam-dinh';
import ChiTietPhieuGiamDinhDetail from './chi-tiet-phieu-giam-dinh-detail';
import ChiTietPhieuGiamDinhUpdate from './chi-tiet-phieu-giam-dinh-update';
import ChiTietPhieuGiamDinhDeleteDialog from './chi-tiet-phieu-giam-dinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ChiTietPhieuGiamDinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ChiTietPhieuGiamDinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ChiTietPhieuGiamDinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={ChiTietPhieuGiamDinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ChiTietPhieuGiamDinhDeleteDialog} />
  </>
);

export default Routes;
