import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IPhieuGiamDinh } from 'app/shared/model/phieu-giam-dinh.model';
import { getEntities as getPhieuGiamDinhs } from 'app/entities/phieu-giam-dinh/phieu-giam-dinh.reducer';
import { getEntity, updateEntity, createEntity, reset } from './chi-tiet-phieu-giam-dinh.reducer';
import { IChiTietPhieuGiamDinh } from 'app/shared/model/chi-tiet-phieu-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IChiTietPhieuGiamDinhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IChiTietPhieuGiamDinhUpdateState {
  isNew: boolean;
  phieuGiamDinhId: number;
}

export class ChiTietPhieuGiamDinhUpdate extends React.Component<IChiTietPhieuGiamDinhUpdateProps, IChiTietPhieuGiamDinhUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      phieuGiamDinhId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPhieuGiamDinhs();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { chiTietPhieuGiamDinhEntity } = this.props;
      const entity = {
        ...chiTietPhieuGiamDinhEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/chi-tiet-phieu-giam-dinh');
  };

  render() {
    const { chiTietPhieuGiamDinhEntity, phieuGiamDinhs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.chiTietPhieuGiamDinh.home.createOrEditLabel">Create or edit a ChiTietPhieuGiamDinh</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : chiTietPhieuGiamDinhEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="chi-tiet-phieu-giam-dinh-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Ma
                  </Label>
                  <AvField id="chi-tiet-phieu-giam-dinh-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="chiTietPhieuMuaHangIdLabel" for="chiTietPhieuMuaHangId">
                    Chi Tiet Phieu Mua Hang Id
                  </Label>
                  <AvField
                    id="chi-tiet-phieu-giam-dinh-chiTietPhieuMuaHangId"
                    type="number"
                    className="form-control"
                    name="chiTietPhieuMuaHangId"
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="tinhTrangLabel" check>
                    <AvInput id="chi-tiet-phieu-giam-dinh-tinhTrang" type="checkbox" className="form-control" name="tinhTrang" />
                    Tinh Trang
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="chi-tiet-phieu-giam-dinh-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="chi-tiet-phieu-giam-dinh-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="chi-tiet-phieu-giam-dinh-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="chi-tiet-phieu-giam-dinh-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label for="phieuGiamDinh.id">Phieu Giam Dinh</Label>
                  <AvInput id="chi-tiet-phieu-giam-dinh-phieuGiamDinh" type="select" className="form-control" name="phieuGiamDinh.id">
                    <option value="" key="0" />
                    {phieuGiamDinhs
                      ? phieuGiamDinhs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/chi-tiet-phieu-giam-dinh" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  phieuGiamDinhs: storeState.phieuGiamDinh.entities,
  chiTietPhieuGiamDinhEntity: storeState.chiTietPhieuGiamDinh.entity,
  loading: storeState.chiTietPhieuGiamDinh.loading,
  updating: storeState.chiTietPhieuGiamDinh.updating
});

const mapDispatchToProps = {
  getPhieuGiamDinhs,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietPhieuGiamDinhUpdate);
