import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './chi-tiet-phieu-giam-dinh.reducer';
import { IChiTietPhieuGiamDinh } from 'app/shared/model/chi-tiet-phieu-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChiTietPhieuGiamDinhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ChiTietPhieuGiamDinhDetail extends React.Component<IChiTietPhieuGiamDinhDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { chiTietPhieuGiamDinhEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            ChiTietPhieuGiamDinh [<b>{chiTietPhieuGiamDinhEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ma">Ma</span>
            </dt>
            <dd>{chiTietPhieuGiamDinhEntity.ma}</dd>
            <dt>
              <span id="chiTietPhieuMuaHangId">Chi Tiet Phieu Mua Hang Id</span>
            </dt>
            <dd>{chiTietPhieuGiamDinhEntity.chiTietPhieuMuaHangId}</dd>
            <dt>
              <span id="tinhTrang">Tinh Trang</span>
            </dt>
            <dd>{chiTietPhieuGiamDinhEntity.tinhTrang ? 'true' : 'false'}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={chiTietPhieuGiamDinhEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{chiTietPhieuGiamDinhEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngay Cap Nhat</span>
            </dt>
            <dd>
              <TextFormat value={chiTietPhieuGiamDinhEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Nguoi Cap Nhat</span>
            </dt>
            <dd>{chiTietPhieuGiamDinhEntity.nguoiCapNhat}</dd>
            <dt>Phieu Giam Dinh</dt>
            <dd>{chiTietPhieuGiamDinhEntity.phieuGiamDinh ? chiTietPhieuGiamDinhEntity.phieuGiamDinh.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/chi-tiet-phieu-giam-dinh" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/chi-tiet-phieu-giam-dinh/${chiTietPhieuGiamDinhEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ chiTietPhieuGiamDinh }: IRootState) => ({
  chiTietPhieuGiamDinhEntity: chiTietPhieuGiamDinh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietPhieuGiamDinhDetail);
