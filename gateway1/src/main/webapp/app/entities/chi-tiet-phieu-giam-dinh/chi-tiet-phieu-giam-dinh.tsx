import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './chi-tiet-phieu-giam-dinh.reducer';
import { IChiTietPhieuGiamDinh } from 'app/shared/model/chi-tiet-phieu-giam-dinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChiTietPhieuGiamDinhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class ChiTietPhieuGiamDinh extends React.Component<IChiTietPhieuGiamDinhProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { chiTietPhieuGiamDinhList, match } = this.props;
    return (
      <div>
        <h2 id="chi-tiet-phieu-giam-dinh-heading">
          Chi Tiet Phieu Giam Dinhs
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Chi Tiet Phieu Giam Dinh
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Ma</th>
                <th>Chi Tiet Phieu Mua Hang Id</th>
                <th>Tinh Trang</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th>Ngay Cap Nhat</th>
                <th>Nguoi Cap Nhat</th>
                <th>Phieu Giam Dinh</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {chiTietPhieuGiamDinhList.map((chiTietPhieuGiamDinh, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${chiTietPhieuGiamDinh.id}`} color="link" size="sm">
                      {chiTietPhieuGiamDinh.id}
                    </Button>
                  </td>
                  <td>{chiTietPhieuGiamDinh.ma}</td>
                  <td>{chiTietPhieuGiamDinh.chiTietPhieuMuaHangId}</td>
                  <td>{chiTietPhieuGiamDinh.tinhTrang ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={chiTietPhieuGiamDinh.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{chiTietPhieuGiamDinh.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={chiTietPhieuGiamDinh.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{chiTietPhieuGiamDinh.nguoiCapNhat}</td>
                  <td>
                    {chiTietPhieuGiamDinh.phieuGiamDinh ? (
                      <Link to={`phieu-giam-dinh/${chiTietPhieuGiamDinh.phieuGiamDinh.id}`}>{chiTietPhieuGiamDinh.phieuGiamDinh.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${chiTietPhieuGiamDinh.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chiTietPhieuGiamDinh.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chiTietPhieuGiamDinh.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ chiTietPhieuGiamDinh }: IRootState) => ({
  chiTietPhieuGiamDinhList: chiTietPhieuGiamDinh.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietPhieuGiamDinh);
