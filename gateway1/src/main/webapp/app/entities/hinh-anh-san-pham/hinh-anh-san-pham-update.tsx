import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, setFileData, openFile, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, setBlob, reset } from './hinh-anh-san-pham.reducer';
import { IHinhAnhSanPham } from 'app/shared/model/hinh-anh-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IHinhAnhSanPhamUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IHinhAnhSanPhamUpdateState {
  isNew: boolean;
}

export class HinhAnhSanPhamUpdate extends React.Component<IHinhAnhSanPhamUpdateProps, IHinhAnhSanPhamUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { hinhAnhSanPhamEntity } = this.props;
      const entity = {
        ...hinhAnhSanPhamEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/hinh-anh-san-pham');
  };

  render() {
    const { hinhAnhSanPhamEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    const { hinhAnh, hinhAnhContentType } = hinhAnhSanPhamEntity;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.hinhAnhSanPham.home.createOrEditLabel">Create or edit a HinhAnhSanPham</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : hinhAnhSanPhamEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="hinh-anh-san-pham-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="sanPhamIdLabel" for="sanPhamId">
                    San Pham Id
                  </Label>
                  <AvField id="hinh-anh-san-pham-sanPhamId" type="number" className="form-control" name="sanPhamId" />
                </AvGroup>
                <AvGroup>
                  <AvGroup>
                    <Label id="hinhAnhLabel" for="hinhAnh">
                      Hinh Anh
                    </Label>
                    <br />
                    {hinhAnh ? (
                      <div>
                        <a onClick={openFile(hinhAnhContentType, hinhAnh)}>Open</a>
                        <br />
                        <Row>
                          <Col md="11">
                            <span>
                              {hinhAnhContentType}, {byteSize(hinhAnh)}
                            </span>
                          </Col>
                          <Col md="1">
                            <Button color="danger" onClick={this.clearBlob('hinhAnh')}>
                              <FontAwesomeIcon icon="times-circle" />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ) : null}
                    <input id="file_hinhAnh" type="file" onChange={this.onBlobChange(false, 'hinhAnh')} />
                    <AvInput type="hidden" name="hinhAnh" value={hinhAnh} />
                  </AvGroup>
                </AvGroup>
                <AvGroup>
                  <Label id="tieuDeLabel" for="tieuDe">
                    Tieu De
                  </Label>
                  <AvField id="hinh-anh-san-pham-tieuDe" type="text" name="tieuDe" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="hinh-anh-san-pham-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="hinh-anh-san-pham-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/hinh-anh-san-pham" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  hinhAnhSanPhamEntity: storeState.hinhAnhSanPham.entity,
  loading: storeState.hinhAnhSanPham.loading,
  updating: storeState.hinhAnhSanPham.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HinhAnhSanPhamUpdate);
