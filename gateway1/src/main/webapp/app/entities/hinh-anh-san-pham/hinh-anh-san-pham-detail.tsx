import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './hinh-anh-san-pham.reducer';
import { IHinhAnhSanPham } from 'app/shared/model/hinh-anh-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IHinhAnhSanPhamDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class HinhAnhSanPhamDetail extends React.Component<IHinhAnhSanPhamDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { hinhAnhSanPhamEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            HinhAnhSanPham [<b>{hinhAnhSanPhamEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="sanPhamId">San Pham Id</span>
            </dt>
            <dd>{hinhAnhSanPhamEntity.sanPhamId}</dd>
            <dt>
              <span id="hinhAnh">Hinh Anh</span>
            </dt>
            <dd>
              {hinhAnhSanPhamEntity.hinhAnh ? (
                <div>
                  <a onClick={openFile(hinhAnhSanPhamEntity.hinhAnhContentType, hinhAnhSanPhamEntity.hinhAnh)}>Open&nbsp;</a>
                  <span>
                    {hinhAnhSanPhamEntity.hinhAnhContentType}, {byteSize(hinhAnhSanPhamEntity.hinhAnh)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="tieuDe">Tieu De</span>
            </dt>
            <dd>{hinhAnhSanPhamEntity.tieuDe}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={hinhAnhSanPhamEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{hinhAnhSanPhamEntity.nguoiTao}</dd>
          </dl>
          <Button tag={Link} to="/entity/hinh-anh-san-pham" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/hinh-anh-san-pham/${hinhAnhSanPhamEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ hinhAnhSanPham }: IRootState) => ({
  hinhAnhSanPhamEntity: hinhAnhSanPham.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HinhAnhSanPhamDetail);
