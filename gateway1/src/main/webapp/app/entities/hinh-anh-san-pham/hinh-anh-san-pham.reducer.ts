import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IHinhAnhSanPham, defaultValue } from 'app/shared/model/hinh-anh-san-pham.model';

export const ACTION_TYPES = {
  FETCH_HINHANHSANPHAM_LIST: 'hinhAnhSanPham/FETCH_HINHANHSANPHAM_LIST',
  FETCH_HINHANHSANPHAM: 'hinhAnhSanPham/FETCH_HINHANHSANPHAM',
  CREATE_HINHANHSANPHAM: 'hinhAnhSanPham/CREATE_HINHANHSANPHAM',
  UPDATE_HINHANHSANPHAM: 'hinhAnhSanPham/UPDATE_HINHANHSANPHAM',
  DELETE_HINHANHSANPHAM: 'hinhAnhSanPham/DELETE_HINHANHSANPHAM',
  SET_BLOB: 'hinhAnhSanPham/SET_BLOB',
  RESET: 'hinhAnhSanPham/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IHinhAnhSanPham>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type HinhAnhSanPhamState = Readonly<typeof initialState>;

// Reducer

export default (state: HinhAnhSanPhamState = initialState, action): HinhAnhSanPhamState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_HINHANHSANPHAM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_HINHANHSANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_HINHANHSANPHAM):
    case REQUEST(ACTION_TYPES.UPDATE_HINHANHSANPHAM):
    case REQUEST(ACTION_TYPES.DELETE_HINHANHSANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_HINHANHSANPHAM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_HINHANHSANPHAM):
    case FAILURE(ACTION_TYPES.CREATE_HINHANHSANPHAM):
    case FAILURE(ACTION_TYPES.UPDATE_HINHANHSANPHAM):
    case FAILURE(ACTION_TYPES.DELETE_HINHANHSANPHAM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_HINHANHSANPHAM_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_HINHANHSANPHAM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_HINHANHSANPHAM):
    case SUCCESS(ACTION_TYPES.UPDATE_HINHANHSANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_HINHANHSANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB:
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/hinh-anh-san-phams';

// Actions

export const getEntities: ICrudGetAllAction<IHinhAnhSanPham> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_HINHANHSANPHAM_LIST,
  payload: axios.get<IHinhAnhSanPham>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IHinhAnhSanPham> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_HINHANHSANPHAM,
    payload: axios.get<IHinhAnhSanPham>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IHinhAnhSanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_HINHANHSANPHAM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IHinhAnhSanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_HINHANHSANPHAM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IHinhAnhSanPham> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_HINHANHSANPHAM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
