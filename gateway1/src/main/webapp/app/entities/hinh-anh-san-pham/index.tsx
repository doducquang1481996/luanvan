import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import HinhAnhSanPham from './hinh-anh-san-pham';
import HinhAnhSanPhamDetail from './hinh-anh-san-pham-detail';
import HinhAnhSanPhamUpdate from './hinh-anh-san-pham-update';
import HinhAnhSanPhamDeleteDialog from './hinh-anh-san-pham-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={HinhAnhSanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={HinhAnhSanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={HinhAnhSanPhamDetail} />
      <ErrorBoundaryRoute path={match.url} component={HinhAnhSanPham} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={HinhAnhSanPhamDeleteDialog} />
  </>
);

export default Routes;
