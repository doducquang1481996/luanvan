import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { openFile, byteSize, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './hinh-anh-san-pham.reducer';
import { IHinhAnhSanPham } from 'app/shared/model/hinh-anh-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IHinhAnhSanPhamProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class HinhAnhSanPham extends React.Component<IHinhAnhSanPhamProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { hinhAnhSanPhamList, match } = this.props;
    return (
      <div>
        <h2 id="hinh-anh-san-pham-heading">
          Hinh Anh San Phams
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Hinh Anh San Pham
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>San Pham Id</th>
                <th>Hinh Anh</th>
                <th>Tieu De</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {hinhAnhSanPhamList.map((hinhAnhSanPham, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${hinhAnhSanPham.id}`} color="link" size="sm">
                      {hinhAnhSanPham.id}
                    </Button>
                  </td>
                  <td>{hinhAnhSanPham.sanPhamId}</td>
                  <td>
                    {hinhAnhSanPham.hinhAnh ? (
                      <div>
                        <a onClick={openFile(hinhAnhSanPham.hinhAnhContentType, hinhAnhSanPham.hinhAnh)}>Open &nbsp;</a>
                        <span>
                          {hinhAnhSanPham.hinhAnhContentType}, {byteSize(hinhAnhSanPham.hinhAnh)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>{hinhAnhSanPham.tieuDe}</td>
                  <td>
                    <TextFormat type="date" value={hinhAnhSanPham.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{hinhAnhSanPham.nguoiTao}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${hinhAnhSanPham.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${hinhAnhSanPham.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${hinhAnhSanPham.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ hinhAnhSanPham }: IRootState) => ({
  hinhAnhSanPhamList: hinhAnhSanPham.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HinhAnhSanPham);
