import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './san-pham.reducer';
import { ISanPham } from 'app/shared/model/san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISanPhamDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class SanPhamDetail extends React.Component<ISanPhamDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { sanPhamEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Thông Tin Sản Phẩm [<b>{sanPhamEntity.ma}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Sản Phẩm</span>
            </dt>
            <dd>{sanPhamEntity.ten}</dd>
            <dt>
              <span id="ma">Mã Sản Phẩm</span>
            </dt>
            <dd>{sanPhamEntity.ma}</dd>
            <dt>
              <span id="moTa">Mô Tả</span>
            </dt>
            <dd>{sanPhamEntity.moTa}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={sanPhamEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{sanPhamEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={sanPhamEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{sanPhamEntity.nguoiCapNhat}</dd>
            <dt>Loại Sản Phẩm</dt>
            <dd>{sanPhamEntity.loaiSanPhamId ? sanPhamEntity.loaiSanPhamId : ''}</dd>
            <dt>Nhà Sản Xuất</dt>
            <dd>{sanPhamEntity.nhaSanXuatId ? sanPhamEntity.nhaSanXuatId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/san-pham" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/san-pham/${sanPhamEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ sanPham }: IRootState) => ({
  sanPhamEntity: sanPham.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamDetail);
