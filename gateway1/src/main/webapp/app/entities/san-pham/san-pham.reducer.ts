import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISanPham, defaultValue } from 'app/shared/model/san-pham.model';

export const ACTION_TYPES = {
  FETCH_SANPHAM_LIST: 'sanPham/FETCH_SANPHAM_LIST',
  FETCH_SANPHAM: 'sanPham/FETCH_SANPHAM',
  CREATE_SANPHAM: 'sanPham/CREATE_SANPHAM',
  UPDATE_SANPHAM: 'sanPham/UPDATE_SANPHAM',
  DELETE_SANPHAM: 'sanPham/DELETE_SANPHAM',
  RESET: 'sanPham/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISanPham>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type SanPhamState = Readonly<typeof initialState>;

// Reducer

export default (state: SanPhamState = initialState, action): SanPhamState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SANPHAM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SANPHAM):
    case REQUEST(ACTION_TYPES.UPDATE_SANPHAM):
    case REQUEST(ACTION_TYPES.DELETE_SANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SANPHAM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SANPHAM):
    case FAILURE(ACTION_TYPES.CREATE_SANPHAM):
    case FAILURE(ACTION_TYPES.UPDATE_SANPHAM):
    case FAILURE(ACTION_TYPES.DELETE_SANPHAM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SANPHAM_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SANPHAM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SANPHAM):
    case SUCCESS(ACTION_TYPES.UPDATE_SANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/san-phams';

// Actions

export const getEntities: ICrudGetAllAction<ISanPham> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_SANPHAM_LIST,
    payload: axios.get<ISanPham>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ISanPham> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SANPHAM,
    payload: axios.get<ISanPham>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SANPHAM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SANPHAM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISanPham> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SANPHAM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
