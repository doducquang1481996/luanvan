import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ILoaiSanPham } from 'app/shared/model/loai-san-pham.model';
import { getEntities as getLoaiSanPhams } from 'app/entities/loai-san-pham/loai-san-pham.reducer';
import { INhaSanXuat } from 'app/shared/model/nha-san-xuat.model';
import { getEntities as getNhaSanXuats } from 'app/entities/nha-san-xuat/nha-san-xuat.reducer';
import { getEntity, updateEntity, createEntity, reset } from './san-pham.reducer';
import { ISanPham } from 'app/shared/model/san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISanPhamUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISanPhamUpdateState {
  isNew: boolean;
  loaiSanPhamId: number;
  nhaSanXuatId: number;
}

export class SanPhamUpdate extends React.Component<ISanPhamUpdateProps, ISanPhamUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      loaiSanPhamId: 0,
      nhaSanXuatId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getLoaiSanPhams();
    this.props.getNhaSanXuats();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { sanPhamEntity } = this.props;
      const entity = {
        ...sanPhamEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/san-pham');
  };

  render() {
    const { sanPhamEntity, loaiSanPhams, nhaSanXuats, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.sanPham.home.createOrEditLabel">Tạo Mới Sản Phẩm</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : sanPhamEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="san-pham-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Tên Sản Phẩm
                  </Label>
                  <AvField id="san-pham-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Mã Sản Phẩm
                  </Label>
                  <AvField id="san-pham-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="moTaLabel" for="moTa">
                    Mô Tả
                  </Label>
                  <AvField id="san-pham-moTa" type="text" name="moTa" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngày Tạo
                  </Label>
                  <AvField id="san-pham-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Người Tạo
                  </Label>
                  <AvField id="san-pham-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngày Cập Nhật
                  </Label>
                  <AvField id="san-pham-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Người Cập Nhật
                  </Label>
                  <AvField id="san-pham-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label for="loaiSanPham.id">Loại Sản Phẩm</Label>
                  <AvInput id="san-pham-loaiSanPham" type="select" className="form-control" name="loaiSanPhamId">
                    <option value="" key="0" />
                    {loaiSanPhams
                      ? loaiSanPhams.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.ten}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="nhaSanXuat.id">Nhà Sản Xuất</Label>
                  <AvInput id="san-pham-nhaSanXuat" type="select" className="form-control" name="nhaSanXuatId">
                    <option value="" key="0" />
                    {nhaSanXuats
                      ? nhaSanXuats.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.ten}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/san-pham" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  loaiSanPhams: storeState.loaiSanPham.entities,
  nhaSanXuats: storeState.nhaSanXuat.entities,
  sanPhamEntity: storeState.sanPham.entity,
  loading: storeState.sanPham.loading,
  updating: storeState.sanPham.updating
});

const mapDispatchToProps = {
  getLoaiSanPhams,
  getNhaSanXuats,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamUpdate);
