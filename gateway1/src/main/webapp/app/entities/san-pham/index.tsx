import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SanPham from './san-pham';
import SanPhamDetail from './san-pham-detail';
import SanPhamUpdate from './san-pham-update';
import SanPhamDeleteDialog from './san-pham-delete-dialog';
import LoaiSanPham from '../loai-san-pham/loai-san-pham';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SanPhamDetail} />
      <ErrorBoundaryRoute exact path={`${match.url}/new-loai-san-pham`} component={LoaiSanPham} />
      <ErrorBoundaryRoute path={match.url} component={SanPham} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SanPhamDeleteDialog} />
  </>
);

export default Routes;
