import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PhieuMuaHang from './phieu-mua-hang';
import PhieuMuaHangDetail from './phieu-mua-hang-detail';
import PhieuMuaHangUpdate from './phieu-mua-hang-update';
import PhieuMuaHangDeleteDialog from './phieu-mua-hang-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PhieuMuaHangUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PhieuMuaHangUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PhieuMuaHangDetail} />
      <ErrorBoundaryRoute path={match.url} component={PhieuMuaHang} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={PhieuMuaHangDeleteDialog} />
  </>
);

export default Routes;
