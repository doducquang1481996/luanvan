import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPhieuMuaHang, defaultValue } from 'app/shared/model/phieu-mua-hang.model';

export const ACTION_TYPES = {
  FETCH_PHIEUMUAHANG_LIST: 'phieuMuaHang/FETCH_PHIEUMUAHANG_LIST',
  FETCH_PHIEUMUAHANG: 'phieuMuaHang/FETCH_PHIEUMUAHANG',
  CREATE_PHIEUMUAHANG: 'phieuMuaHang/CREATE_PHIEUMUAHANG',
  UPDATE_PHIEUMUAHANG: 'phieuMuaHang/UPDATE_PHIEUMUAHANG',
  DELETE_PHIEUMUAHANG: 'phieuMuaHang/DELETE_PHIEUMUAHANG',
  RESET: 'phieuMuaHang/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPhieuMuaHang>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type PhieuMuaHangState = Readonly<typeof initialState>;

// Reducer

export default (state: PhieuMuaHangState = initialState, action): PhieuMuaHangState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PHIEUMUAHANG_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PHIEUMUAHANG):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PHIEUMUAHANG):
    case REQUEST(ACTION_TYPES.UPDATE_PHIEUMUAHANG):
    case REQUEST(ACTION_TYPES.DELETE_PHIEUMUAHANG):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PHIEUMUAHANG_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PHIEUMUAHANG):
    case FAILURE(ACTION_TYPES.CREATE_PHIEUMUAHANG):
    case FAILURE(ACTION_TYPES.UPDATE_PHIEUMUAHANG):
    case FAILURE(ACTION_TYPES.DELETE_PHIEUMUAHANG):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PHIEUMUAHANG_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PHIEUMUAHANG):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PHIEUMUAHANG):
    case SUCCESS(ACTION_TYPES.UPDATE_PHIEUMUAHANG):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PHIEUMUAHANG):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'storehouse/api/phieu-mua-hangs';

// Actions

export const getEntities: ICrudGetAllAction<IPhieuMuaHang> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PHIEUMUAHANG_LIST,
  payload: axios.get<IPhieuMuaHang>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IPhieuMuaHang> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PHIEUMUAHANG,
    payload: axios.get<IPhieuMuaHang>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IPhieuMuaHang> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PHIEUMUAHANG,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPhieuMuaHang> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PHIEUMUAHANG,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPhieuMuaHang> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PHIEUMUAHANG,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
