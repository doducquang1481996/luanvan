import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './phieu-mua-hang.reducer';
import { IPhieuMuaHang } from 'app/shared/model/phieu-mua-hang.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPhieuMuaHangUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPhieuMuaHangUpdateState {
  isNew: boolean;
}

export class PhieuMuaHangUpdate extends React.Component<IPhieuMuaHangUpdateProps, IPhieuMuaHangUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { phieuMuaHangEntity } = this.props;
      const entity = {
        ...phieuMuaHangEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/phieu-mua-hang');
  };

  render() {
    const { phieuMuaHangEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.phieuMuaHang.home.createOrEditLabel">Create or edit a PhieuMuaHang</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : phieuMuaHangEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="phieu-mua-hang-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Ten
                  </Label>
                  <AvField id="phieu-mua-hang-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Ma
                  </Label>
                  <AvField id="phieu-mua-hang-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="nhaCungCapLabel" for="nhaCungCap">
                    Nhà Cung Cấp
                  </Label>
                  <AvField id="phieu-mua-hang-nhaCungCap" type="number" className="form-control" name="nhaCungCap" />
                </AvGroup>
                <AvGroup>
                  <Label id="tenNhaCungCapLabel" for="tenNhaCungCap">
                    Tên Nhà Cung Cấp
                  </Label>
                  <AvField id="phieu-mua-hang-tenNhaCungCap" type="text" name="tenNhaCungCap" />
                </AvGroup>
                <AvGroup>
                  <Label id="trangThaiLabel" check>
                    <AvInput id="phieu-mua-hang-trangThai" type="checkbox" className="form-control" name="trangThai" />
                    Trạng Thái
                  </Label>
                  <Label id="duyetLabel" check>
                    <AvInput id="phieu-mua-hang-duyet" type="checkbox" className="form-control" name="duyet" />
                    Duyệt
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="ngayDatMuaLabel" for="ngayDatMua">
                    Ngày Đặt Mua
                  </Label>
                  <AvField id="phieu-mua-hang-ngayDatMua" type="date" className="form-control" name="ngayDatMua" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayNhanLabel" for="ngayNhan">
                    Ngày Nhận
                  </Label>
                  <AvField id="phieu-mua-hang-ngayNhan" type="date" className="form-control" name="ngayNhan" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngày Tạo
                  </Label>
                  <AvField id="phieu-mua-hang-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Người Tạo
                  </Label>
                  <AvField id="phieu-mua-hang-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngày Cập Nhật
                  </Label>
                  <AvField id="phieu-mua-hang-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Người Cập Nhật
                  </Label>
                  <AvField id="phieu-mua-hang-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/phieu-mua-hang" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  phieuMuaHangEntity: storeState.phieuMuaHang.entity,
  loading: storeState.phieuMuaHang.loading,
  updating: storeState.phieuMuaHang.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhieuMuaHangUpdate);
