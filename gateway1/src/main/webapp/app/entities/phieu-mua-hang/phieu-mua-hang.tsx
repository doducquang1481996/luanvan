import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './phieu-mua-hang.reducer';
import { IPhieuMuaHang } from 'app/shared/model/phieu-mua-hang.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPhieuMuaHangProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class PhieuMuaHang extends React.Component<IPhieuMuaHangProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { phieuMuaHangList, match } = this.props;
    return (
      <div>
        <h2 id="phieu-mua-hang-heading">
          Danh Sách Phiêu Mua Hàng
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Tạo mới phiếu mua hàng
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Tên</th>
                <th>Mã</th>
                <th>Nhà Cung Cấp</th>
                <th>Tên Nhà Cung Cấp</th>
                <th>Trạng thái</th>
                <th>Duyệt</th>
                <th>Ngày Đặt Mua</th>
                <th>Ngày Nhận</th>
                <th>Ngày Tạo</th>
                <th>Người Tạo</th>
                <th>Ngày Cập Nhật</th>
                <th>Người Cập Nhật</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {phieuMuaHangList.map((phieuMuaHang, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${phieuMuaHang.id}`} color="link" size="sm">
                      {phieuMuaHang.id}
                    </Button>
                  </td>
                  <td>{phieuMuaHang.ten}</td>
                  <td>{phieuMuaHang.ma}</td>
                  <td>{phieuMuaHang.nhaCungCap}</td>
                  <td>{phieuMuaHang.tenNhaCungCap}</td>
                  <td>{phieuMuaHang.trangThai ? 'true' : 'false'}</td>
                  <td>{phieuMuaHang.duyet ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={phieuMuaHang.ngayDatMua} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={phieuMuaHang.ngayNhan} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={phieuMuaHang.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{phieuMuaHang.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={phieuMuaHang.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{phieuMuaHang.nguoiCapNhat}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${phieuMuaHang.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${phieuMuaHang.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${phieuMuaHang.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ phieuMuaHang }: IRootState) => ({
  phieuMuaHangList: phieuMuaHang.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhieuMuaHang);
