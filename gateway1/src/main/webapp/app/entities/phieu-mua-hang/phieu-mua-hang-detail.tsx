import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './phieu-mua-hang.reducer';
import { IPhieuMuaHang } from 'app/shared/model/phieu-mua-hang.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPhieuMuaHangDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PhieuMuaHangDetail extends React.Component<IPhieuMuaHangDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { phieuMuaHangEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Danh Sách Phiếu Mua Hàng [<b>{phieuMuaHangEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Phiếu Mua Hàng</span>
            </dt>
            <dd>{phieuMuaHangEntity.ten}</dd>
            <dt>
              <span id="ma">Mã</span>
            </dt>
            <dd>{phieuMuaHangEntity.ma}</dd>
            {/* <dt>
              <span id="nhaCungCap">Nha Cung Cap</span>
            </dt> */}
            <dd>{phieuMuaHangEntity.nhaCungCap}</dd>
            <dt>
              <span id="tenNhaCungCap">Tên Nhà Cung Cấp</span>
            </dt>
            <dd>{phieuMuaHangEntity.tenNhaCungCap}</dd>
            <dt>
              <span id="trangThai">Trại Thái</span>
            </dt>
            <dd>{phieuMuaHangEntity.trangThai ? 'true' : 'false'}</dd>
            <dt>
              <span id="duyet">Duyệt</span>
            </dt>
            <dd>{phieuMuaHangEntity.duyet ? 'true' : 'false'}</dd>
            <dt>
              <span id="ngayDatMua">Ngày Đặt Mua</span>
            </dt>
            <dd>
              <TextFormat value={phieuMuaHangEntity.ngayDatMua} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="ngayNhan">Ngày Nhận</span>
            </dt>
            <dd>
              <TextFormat value={phieuMuaHangEntity.ngayNhan} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={phieuMuaHangEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{phieuMuaHangEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={phieuMuaHangEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{phieuMuaHangEntity.nguoiCapNhat}</dd>
          </dl>
          <Button tag={Link} to="/entity/phieu-mua-hang" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/phieu-mua-hang/${phieuMuaHangEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ phieuMuaHang }: IRootState) => ({
  phieuMuaHangEntity: phieuMuaHang.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhieuMuaHangDetail);
