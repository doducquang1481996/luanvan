import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './funciton-system.reducer';
import { IFuncitonSystem } from 'app/shared/model/funciton-system.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFuncitonSystemDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class FuncitonSystemDetail extends React.Component<IFuncitonSystemDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { funcitonSystemEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            FuncitonSystem [<b>{funcitonSystemEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="funcitionName">Funcition Name</span>
            </dt>
            <dd>{funcitonSystemEntity.funcitionName}</dd>
            <dt>
              <span id="funcitionCode">Funcition Code</span>
            </dt>
            <dd>{funcitonSystemEntity.funcitionCode}</dd>
            <dt>
              <span id="funcitionLink">Funcition Link</span>
            </dt>
            <dd>{funcitonSystemEntity.funcitionLink}</dd>
            <dt>
              <span id="note">Note</span>
            </dt>
            <dd>{funcitonSystemEntity.note}</dd>
            <dt>
              <span id="parentId">Parent Id</span>
            </dt>
            <dd>{funcitonSystemEntity.parentId}</dd>
          </dl>
          <Button tag={Link} to="/entity/funciton-system" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/funciton-system/${funcitonSystemEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ funcitonSystem }: IRootState) => ({
  funcitonSystemEntity: funcitonSystem.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FuncitonSystemDetail);
