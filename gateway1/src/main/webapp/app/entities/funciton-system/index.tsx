import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import FuncitonSystem from './funciton-system';
import FuncitonSystemDetail from './funciton-system-detail';
import FuncitonSystemUpdate from './funciton-system-update';
import FuncitonSystemDeleteDialog from './funciton-system-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FuncitonSystemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FuncitonSystemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FuncitonSystemDetail} />
      <ErrorBoundaryRoute path={match.url} component={FuncitonSystem} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={FuncitonSystemDeleteDialog} />
  </>
);

export default Routes;
