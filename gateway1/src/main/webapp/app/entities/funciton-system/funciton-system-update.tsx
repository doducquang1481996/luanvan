import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './funciton-system.reducer';
import { IFuncitonSystem } from 'app/shared/model/funciton-system.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IFuncitonSystemUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IFuncitonSystemUpdateState {
  isNew: boolean;
}

export class FuncitonSystemUpdate extends React.Component<IFuncitonSystemUpdateProps, IFuncitonSystemUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { funcitonSystemEntity } = this.props;
      const entity = {
        ...funcitonSystemEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/funciton-system');
  };

  render() {
    const { funcitonSystemEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.funcitonSystem.home.createOrEditLabel">Create or edit a FuncitonSystem</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : funcitonSystemEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="funciton-system-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="funcitionNameLabel" for="funcitionName">
                    Funcition Name
                  </Label>
                  <AvField id="funciton-system-funcitionName" type="text" name="funcitionName" />
                </AvGroup>
                <AvGroup>
                  <Label id="funcitionCodeLabel" for="funcitionCode">
                    Funcition Code
                  </Label>
                  <AvField id="funciton-system-funcitionCode" type="text" name="funcitionCode" />
                </AvGroup>
                <AvGroup>
                  <Label id="funcitionLinkLabel" for="funcitionLink">
                    Funcition Link
                  </Label>
                  <AvField id="funciton-system-funcitionLink" type="text" name="funcitionLink" />
                </AvGroup>
                <AvGroup>
                  <Label id="noteLabel" for="note">
                    Note
                  </Label>
                  <AvField id="funciton-system-note" type="text" name="note" />
                </AvGroup>
                <AvGroup>
                  <Label id="parentIdLabel" for="parentId">
                    Parent Id
                  </Label>
                  <AvField id="funciton-system-parentId" type="number" className="form-control" name="parentId" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/funciton-system" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  funcitonSystemEntity: storeState.funcitonSystem.entity,
  loading: storeState.funcitonSystem.loading,
  updating: storeState.funcitonSystem.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FuncitonSystemUpdate);
