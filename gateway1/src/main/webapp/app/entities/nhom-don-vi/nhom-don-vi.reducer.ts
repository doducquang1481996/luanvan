import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INhomDonVi, defaultValue } from 'app/shared/model/nhom-don-vi.model';

export const ACTION_TYPES = {
  FETCH_NHOMDONVI_LIST: 'nhomDonVi/FETCH_NHOMDONVI_LIST',
  FETCH_NHOMDONVI: 'nhomDonVi/FETCH_NHOMDONVI',
  CREATE_NHOMDONVI: 'nhomDonVi/CREATE_NHOMDONVI',
  UPDATE_NHOMDONVI: 'nhomDonVi/UPDATE_NHOMDONVI',
  DELETE_NHOMDONVI: 'nhomDonVi/DELETE_NHOMDONVI',
  RESET: 'nhomDonVi/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INhomDonVi>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type NhomDonViState = Readonly<typeof initialState>;

// Reducer

export default (state: NhomDonViState = initialState, action): NhomDonViState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NHOMDONVI_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NHOMDONVI):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NHOMDONVI):
    case REQUEST(ACTION_TYPES.UPDATE_NHOMDONVI):
    case REQUEST(ACTION_TYPES.DELETE_NHOMDONVI):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NHOMDONVI_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NHOMDONVI):
    case FAILURE(ACTION_TYPES.CREATE_NHOMDONVI):
    case FAILURE(ACTION_TYPES.UPDATE_NHOMDONVI):
    case FAILURE(ACTION_TYPES.DELETE_NHOMDONVI):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHOMDONVI_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHOMDONVI):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NHOMDONVI):
    case SUCCESS(ACTION_TYPES.UPDATE_NHOMDONVI):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NHOMDONVI):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/nhom-don-vis';

// Actions

export const getEntities: ICrudGetAllAction<INhomDonVi> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_NHOMDONVI_LIST,
    payload: axios.get<INhomDonVi>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<INhomDonVi> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NHOMDONVI,
    payload: axios.get<INhomDonVi>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INhomDonVi> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NHOMDONVI,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INhomDonVi> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NHOMDONVI,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INhomDonVi> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NHOMDONVI,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
