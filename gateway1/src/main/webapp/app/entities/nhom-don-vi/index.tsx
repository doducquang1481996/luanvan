import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NhomDonVi from './nhom-don-vi';
import NhomDonViDetail from './nhom-don-vi-detail';
import NhomDonViUpdate from './nhom-don-vi-update';
import NhomDonViDeleteDialog from './nhom-don-vi-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NhomDonViUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NhomDonViUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NhomDonViDetail} />
      <ErrorBoundaryRoute path={match.url} component={NhomDonVi} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={NhomDonViDeleteDialog} />
  </>
);

export default Routes;
