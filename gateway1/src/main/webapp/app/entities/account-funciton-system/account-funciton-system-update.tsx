import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './account-funciton-system.reducer';
import { IAccountFuncitonSystem } from 'app/shared/model/account-funciton-system.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IAccountFuncitonSystemUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IAccountFuncitonSystemUpdateState {
  isNew: boolean;
}

export class AccountFuncitonSystemUpdate extends React.Component<IAccountFuncitonSystemUpdateProps, IAccountFuncitonSystemUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { accountFuncitonSystemEntity } = this.props;
      const entity = {
        ...accountFuncitonSystemEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/account-funciton-system');
  };

  render() {
    const { accountFuncitonSystemEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.accountFuncitonSystem.home.createOrEditLabel">Create or edit a AccountFuncitonSystem</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : accountFuncitonSystemEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="account-funciton-system-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="accountIdLabel" for="accountId">
                    Account Id
                  </Label>
                  <AvField id="account-funciton-system-accountId" type="number" className="form-control" name="accountId" />
                </AvGroup>
                <AvGroup>
                  <Label id="funcitionIdLabel" for="funcitionId">
                    Funcition Id
                  </Label>
                  <AvField id="account-funciton-system-funcitionId" type="number" className="form-control" name="funcitionId" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/account-funciton-system" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  accountFuncitonSystemEntity: storeState.accountFuncitonSystem.entity,
  loading: storeState.accountFuncitonSystem.loading,
  updating: storeState.accountFuncitonSystem.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountFuncitonSystemUpdate);
