import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import AccountFuncitonSystem from './account-funciton-system';
import AccountFuncitonSystemDetail from './account-funciton-system-detail';
import AccountFuncitonSystemUpdate from './account-funciton-system-update';
import AccountFuncitonSystemDeleteDialog from './account-funciton-system-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={AccountFuncitonSystemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={AccountFuncitonSystemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={AccountFuncitonSystemDetail} />
      <ErrorBoundaryRoute path={match.url} component={AccountFuncitonSystem} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={AccountFuncitonSystemDeleteDialog} />
  </>
);

export default Routes;
