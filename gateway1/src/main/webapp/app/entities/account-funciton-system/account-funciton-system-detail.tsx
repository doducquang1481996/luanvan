import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './account-funciton-system.reducer';
import { IAccountFuncitonSystem } from 'app/shared/model/account-funciton-system.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAccountFuncitonSystemDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class AccountFuncitonSystemDetail extends React.Component<IAccountFuncitonSystemDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { accountFuncitonSystemEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            AccountFuncitonSystem [<b>{accountFuncitonSystemEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="accountId">Account Id</span>
            </dt>
            <dd>{accountFuncitonSystemEntity.accountId}</dd>
            <dt>
              <span id="funcitionId">Funcition Id</span>
            </dt>
            <dd>{accountFuncitonSystemEntity.funcitionId}</dd>
          </dl>
          <Button tag={Link} to="/entity/account-funciton-system" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/account-funciton-system/${accountFuncitonSystemEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ accountFuncitonSystem }: IRootState) => ({
  accountFuncitonSystemEntity: accountFuncitonSystem.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountFuncitonSystemDetail);
