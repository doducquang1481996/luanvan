import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IAccountFuncitonSystem, defaultValue } from 'app/shared/model/account-funciton-system.model';

export const ACTION_TYPES = {
  FETCH_ACCOUNTFUNCITONSYSTEM_LIST: 'accountFuncitonSystem/FETCH_ACCOUNTFUNCITONSYSTEM_LIST',
  FETCH_ACCOUNTFUNCITONSYSTEM: 'accountFuncitonSystem/FETCH_ACCOUNTFUNCITONSYSTEM',
  CREATE_ACCOUNTFUNCITONSYSTEM: 'accountFuncitonSystem/CREATE_ACCOUNTFUNCITONSYSTEM',
  UPDATE_ACCOUNTFUNCITONSYSTEM: 'accountFuncitonSystem/UPDATE_ACCOUNTFUNCITONSYSTEM',
  DELETE_ACCOUNTFUNCITONSYSTEM: 'accountFuncitonSystem/DELETE_ACCOUNTFUNCITONSYSTEM',
  RESET: 'accountFuncitonSystem/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IAccountFuncitonSystem>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type AccountFuncitonSystemState = Readonly<typeof initialState>;

// Reducer

export default (state: AccountFuncitonSystemState = initialState, action): AccountFuncitonSystemState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ACCOUNTFUNCITONSYSTEM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ACCOUNTFUNCITONSYSTEM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ACCOUNTFUNCITONSYSTEM):
    case REQUEST(ACTION_TYPES.UPDATE_ACCOUNTFUNCITONSYSTEM):
    case REQUEST(ACTION_TYPES.DELETE_ACCOUNTFUNCITONSYSTEM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ACCOUNTFUNCITONSYSTEM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ACCOUNTFUNCITONSYSTEM):
    case FAILURE(ACTION_TYPES.CREATE_ACCOUNTFUNCITONSYSTEM):
    case FAILURE(ACTION_TYPES.UPDATE_ACCOUNTFUNCITONSYSTEM):
    case FAILURE(ACTION_TYPES.DELETE_ACCOUNTFUNCITONSYSTEM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ACCOUNTFUNCITONSYSTEM_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_ACCOUNTFUNCITONSYSTEM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ACCOUNTFUNCITONSYSTEM):
    case SUCCESS(ACTION_TYPES.UPDATE_ACCOUNTFUNCITONSYSTEM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ACCOUNTFUNCITONSYSTEM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/account-funciton-systems';

// Actions

export const getEntities: ICrudGetAllAction<IAccountFuncitonSystem> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ACCOUNTFUNCITONSYSTEM_LIST,
    payload: axios.get<IAccountFuncitonSystem>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IAccountFuncitonSystem> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ACCOUNTFUNCITONSYSTEM,
    payload: axios.get<IAccountFuncitonSystem>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IAccountFuncitonSystem> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ACCOUNTFUNCITONSYSTEM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IAccountFuncitonSystem> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ACCOUNTFUNCITONSYSTEM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IAccountFuncitonSystem> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ACCOUNTFUNCITONSYSTEM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
