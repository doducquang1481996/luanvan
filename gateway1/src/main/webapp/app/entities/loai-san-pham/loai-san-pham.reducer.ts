import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILoaiSanPham, defaultValue } from 'app/shared/model/loai-san-pham.model';

export const ACTION_TYPES = {
  FETCH_LOAISANPHAM_LIST: 'loaiSanPham/FETCH_LOAISANPHAM_LIST',
  FETCH_LOAISANPHAM: 'loaiSanPham/FETCH_LOAISANPHAM',
  CREATE_LOAISANPHAM: 'loaiSanPham/CREATE_LOAISANPHAM',
  UPDATE_LOAISANPHAM: 'loaiSanPham/UPDATE_LOAISANPHAM',
  DELETE_LOAISANPHAM: 'loaiSanPham/DELETE_LOAISANPHAM',
  RESET: 'loaiSanPham/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILoaiSanPham>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type LoaiSanPhamState = Readonly<typeof initialState>;

// Reducer

export default (state: LoaiSanPhamState = initialState, action): LoaiSanPhamState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LOAISANPHAM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LOAISANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_LOAISANPHAM):
    case REQUEST(ACTION_TYPES.UPDATE_LOAISANPHAM):
    case REQUEST(ACTION_TYPES.DELETE_LOAISANPHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_LOAISANPHAM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LOAISANPHAM):
    case FAILURE(ACTION_TYPES.CREATE_LOAISANPHAM):
    case FAILURE(ACTION_TYPES.UPDATE_LOAISANPHAM):
    case FAILURE(ACTION_TYPES.DELETE_LOAISANPHAM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_LOAISANPHAM_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_LOAISANPHAM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_LOAISANPHAM):
    case SUCCESS(ACTION_TYPES.UPDATE_LOAISANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_LOAISANPHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/loai-san-phams';

// Actions

export const getEntities: ICrudGetAllAction<ILoaiSanPham> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_LOAISANPHAM_LIST,
    payload: axios.get<ILoaiSanPham>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ILoaiSanPham> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LOAISANPHAM,
    payload: axios.get<ILoaiSanPham>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ILoaiSanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LOAISANPHAM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILoaiSanPham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LOAISANPHAM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILoaiSanPham> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LOAISANPHAM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
