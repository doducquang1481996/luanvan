import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { INhomSanPham } from 'app/shared/model/nhom-san-pham.model';
import { getEntities as getNhomSanPhams } from 'app/entities/nhom-san-pham/nhom-san-pham.reducer';
import { getEntity, updateEntity, createEntity, reset } from './loai-san-pham.reducer';
import { ILoaiSanPham } from 'app/shared/model/loai-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILoaiSanPhamUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ILoaiSanPhamUpdateState {
  isNew: boolean;
  nhomSanPhamId: number;
}

export class LoaiSanPhamUpdate extends React.Component<ILoaiSanPhamUpdateProps, ILoaiSanPhamUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      nhomSanPhamId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getNhomSanPhams();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { loaiSanPhamEntity } = this.props;
      const entity = {
        ...loaiSanPhamEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/loai-san-pham');
  };

  render() {
    const { loaiSanPhamEntity, nhomSanPhams, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.loaiSanPham.home.createOrEditLabel">Create or edit a LoaiSanPham</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : loaiSanPhamEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="loai-san-pham-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Tên Loại Sản Phẩm
                  </Label>
                  <AvField id="loai-san-pham-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Mã Loại Sản Phẩm
                  </Label>
                  <AvField id="loai-san-pham-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="moTaLabel" for="moTa">
                    Mô Tả
                  </Label>
                  <AvField id="loai-san-pham-moTa" type="text" name="moTa" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngày Tạo
                  </Label>
                  <AvField id="loai-san-pham-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Người Tạo
                  </Label>
                  <AvField id="loai-san-pham-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngày Cập Nhật
                  </Label>
                  <AvField id="loai-san-pham-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Người Cập Nhật
                  </Label>
                  <AvField id="loai-san-pham-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="activeLabel" check>
                    <AvInput id="loai-san-pham-active" type="checkbox" className="form-control" name="active" />
                    Trạng Thái
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label for="nhomSanPham.id">Nhóm Sản Phẩm</Label>
                  <AvInput id="loai-san-pham-nhomSanPham" type="select" className="form-control" name="nhomSanPhamId">
                    <option value="" key="0" />
                    {nhomSanPhams
                      ? nhomSanPhams.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.ten}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/loai-san-pham" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Trở Lại</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Lưu
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  nhomSanPhams: storeState.nhomSanPham.entities,
  loaiSanPhamEntity: storeState.loaiSanPham.entity,
  loading: storeState.loaiSanPham.loading,
  updating: storeState.loaiSanPham.updating
});

const mapDispatchToProps = {
  getNhomSanPhams,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiSanPhamUpdate);
