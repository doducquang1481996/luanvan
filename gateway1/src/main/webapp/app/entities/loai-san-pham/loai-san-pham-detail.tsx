import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './loai-san-pham.reducer';
import { ILoaiSanPham } from 'app/shared/model/loai-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILoaiSanPhamDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class LoaiSanPhamDetail extends React.Component<ILoaiSanPhamDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { loaiSanPhamEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Danh Sách Loại Sản Phẩm [<b>{loaiSanPhamEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Tên Loại Sản Phẩm</span>
            </dt>
            <dd>{loaiSanPhamEntity.ten}</dd>
            <dt>
              <span id="ma">Mã Loại Sản Phẩm</span>
            </dt>
            <dd>{loaiSanPhamEntity.ma}</dd>
            <dt>
              <span id="moTa">Mô Tả</span>
            </dt>
            <dd>{loaiSanPhamEntity.moTa}</dd>
            <dt>
              <span id="ngayTao">Ngày Tạo</span>
            </dt>
            <dd>
              <TextFormat value={loaiSanPhamEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Người Tạo</span>
            </dt>
            <dd>{loaiSanPhamEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngày Cập Nhật</span>
            </dt>
            <dd>
              <TextFormat value={loaiSanPhamEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Người Cập Nhật</span>
            </dt>
            <dd>{loaiSanPhamEntity.nguoiCapNhat}</dd>
            <dt>
              <span id="active">Trạng Thái</span>
            </dt>
            <dd>{loaiSanPhamEntity.active ? 'true' : 'false'}</dd>
            <dt>Nhóm Sản Phẩm</dt>
            <dd>{loaiSanPhamEntity.nhomSanPhamId ? loaiSanPhamEntity.nhomSanPhamId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/loai-san-pham" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/loai-san-pham/${loaiSanPhamEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ loaiSanPham }: IRootState) => ({
  loaiSanPhamEntity: loaiSanPham.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiSanPhamDetail);
