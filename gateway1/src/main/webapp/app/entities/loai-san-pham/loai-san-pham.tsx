import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './loai-san-pham.reducer';
import { ILoaiSanPham } from 'app/shared/model/loai-san-pham.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface ILoaiSanPhamProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type ILoaiSanPhamState = IPaginationBaseState;

export class LoaiSanPham extends React.Component<ILoaiSanPhamProps, ILoaiSanPhamState> {
  state: ILoaiSanPhamState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { loaiSanPhamList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="loai-san-pham-heading">
          Loai San Phams
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Loai San Pham
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ten')}>
                  Tên Loại Sản Phẩm <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ma')}>
                  Mã Loại Sản Phẩm <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('moTa')}>
                  Mô Tả <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayTao')}>
                  Ngày Tạo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiTao')}>
                  Người Tạo <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayCapNhat')}>
                  Ngày Cập Nhật <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiCapNhat')}>
                  Người Cập Nhật <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('active')}>
                  Trạng Thai <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Nhóm Sản Phẩm <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {loaiSanPhamList.map((loaiSanPham, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${loaiSanPham.id}`} color="link" size="sm">
                      {loaiSanPham.id}
                    </Button>
                  </td>
                  <td>{loaiSanPham.ten}</td>
                  <td>{loaiSanPham.ma}</td>
                  <td>{loaiSanPham.moTa}</td>
                  <td>
                    <TextFormat type="date" value={loaiSanPham.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{loaiSanPham.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={loaiSanPham.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{loaiSanPham.nguoiCapNhat}</td>
                  <td>{loaiSanPham.active ? 'true' : 'false'}</td>
                  <td>
                    {loaiSanPham.nhomSanPhamId ? (
                      <Link to={`nhom-san-pham/${loaiSanPham.nhomSanPhamId}`}>{loaiSanPham.nhomSanPhamId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${loaiSanPham.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${loaiSanPham.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${loaiSanPham.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ loaiSanPham }: IRootState) => ({
  loaiSanPhamList: loaiSanPham.entities,
  totalItems: loaiSanPham.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiSanPham);
