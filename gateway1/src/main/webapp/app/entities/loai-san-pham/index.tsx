import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import LoaiSanPham from './loai-san-pham';
import LoaiSanPhamDetail from './loai-san-pham-detail';
import LoaiSanPhamUpdate from './loai-san-pham-update';
import LoaiSanPhamDeleteDialog from './loai-san-pham-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LoaiSanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LoaiSanPhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LoaiSanPhamDetail} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LoaiSanPhamDetail} />
      <ErrorBoundaryRoute path={match.url} component={LoaiSanPham} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={LoaiSanPhamDeleteDialog} />
  </>
);

export default Routes;
