import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IPhieuMuaHang } from 'app/shared/model/phieu-mua-hang.model';
import { getEntities as getPhieuMuaHangs } from 'app/entities/phieu-mua-hang/phieu-mua-hang.reducer';
import { getEntity, updateEntity, createEntity, reset } from './chi-tiet-phieu-mua-hang.reducer';
import { IChiTietPhieuMuaHang } from 'app/shared/model/chi-tiet-phieu-mua-hang.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IChiTietPhieuMuaHangUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IChiTietPhieuMuaHangUpdateState {
  isNew: boolean;
  phieuMuaHangId: number;
}

export class ChiTietPhieuMuaHangUpdate extends React.Component<IChiTietPhieuMuaHangUpdateProps, IChiTietPhieuMuaHangUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      phieuMuaHangId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPhieuMuaHangs();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { chiTietPhieuMuaHangEntity } = this.props;
      const entity = {
        ...chiTietPhieuMuaHangEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/chi-tiet-phieu-mua-hang');
  };

  render() {
    const { chiTietPhieuMuaHangEntity, phieuMuaHangs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.chiTietPhieuMuaHang.home.createOrEditLabel">Create or edit a ChiTietPhieuMuaHang</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : chiTietPhieuMuaHangEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="chi-tiet-phieu-mua-hang-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="sanPhamIdLabel" for="sanPhamId">
                    San Pham Id
                  </Label>
                  <AvField id="chi-tiet-phieu-mua-hang-sanPhamId" type="number" className="form-control" name="sanPhamId" />
                </AvGroup>
                <AvGroup>
                  <Label id="chiTietSanPhamIdLabel" for="chiTietSanPhamId">
                    Chi Tiet San Pham Id
                  </Label>
                  <AvField id="chi-tiet-phieu-mua-hang-chiTietSanPhamId" type="number" className="form-control" name="chiTietSanPhamId" />
                </AvGroup>
                <AvGroup>
                  <Label id="giaMuaLabel" for="giaMua">
                    Gia Mua
                  </Label>
                  <AvField id="chi-tiet-phieu-mua-hang-giaMua" type="number" className="form-control" name="giaMua" />
                </AvGroup>
                <AvGroup>
                  <Label id="soLuongLabel" for="soLuong">
                    So Luong
                  </Label>
                  <AvField id="chi-tiet-phieu-mua-hang-soLuong" type="number" className="form-control" name="soLuong" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="chi-tiet-phieu-mua-hang-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="chi-tiet-phieu-mua-hang-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="chi-tiet-phieu-mua-hang-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="chi-tiet-phieu-mua-hang-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label for="phieuMuaHang.id">Phieu Mua Hang</Label>
                  <AvInput id="chi-tiet-phieu-mua-hang-phieuMuaHang" type="select" className="form-control" name="phieuMuaHang.id">
                    <option value="" key="0" />
                    {phieuMuaHangs
                      ? phieuMuaHangs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/chi-tiet-phieu-mua-hang" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  phieuMuaHangs: storeState.phieuMuaHang.entities,
  chiTietPhieuMuaHangEntity: storeState.chiTietPhieuMuaHang.entity,
  loading: storeState.chiTietPhieuMuaHang.loading,
  updating: storeState.chiTietPhieuMuaHang.updating
});

const mapDispatchToProps = {
  getPhieuMuaHangs,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietPhieuMuaHangUpdate);
