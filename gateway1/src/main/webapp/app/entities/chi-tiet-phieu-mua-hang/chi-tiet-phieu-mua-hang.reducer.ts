import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IChiTietPhieuMuaHang, defaultValue } from 'app/shared/model/chi-tiet-phieu-mua-hang.model';

export const ACTION_TYPES = {
  FETCH_CHITIETPHIEUMUAHANG_LIST: 'chiTietPhieuMuaHang/FETCH_CHITIETPHIEUMUAHANG_LIST',
  FETCH_CHITIETPHIEUMUAHANG: 'chiTietPhieuMuaHang/FETCH_CHITIETPHIEUMUAHANG',
  CREATE_CHITIETPHIEUMUAHANG: 'chiTietPhieuMuaHang/CREATE_CHITIETPHIEUMUAHANG',
  UPDATE_CHITIETPHIEUMUAHANG: 'chiTietPhieuMuaHang/UPDATE_CHITIETPHIEUMUAHANG',
  DELETE_CHITIETPHIEUMUAHANG: 'chiTietPhieuMuaHang/DELETE_CHITIETPHIEUMUAHANG',
  RESET: 'chiTietPhieuMuaHang/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IChiTietPhieuMuaHang>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ChiTietPhieuMuaHangState = Readonly<typeof initialState>;

// Reducer

export default (state: ChiTietPhieuMuaHangState = initialState, action): ChiTietPhieuMuaHangState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CHITIETPHIEUMUAHANG_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CHITIETPHIEUMUAHANG):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CHITIETPHIEUMUAHANG):
    case REQUEST(ACTION_TYPES.UPDATE_CHITIETPHIEUMUAHANG):
    case REQUEST(ACTION_TYPES.DELETE_CHITIETPHIEUMUAHANG):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CHITIETPHIEUMUAHANG_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CHITIETPHIEUMUAHANG):
    case FAILURE(ACTION_TYPES.CREATE_CHITIETPHIEUMUAHANG):
    case FAILURE(ACTION_TYPES.UPDATE_CHITIETPHIEUMUAHANG):
    case FAILURE(ACTION_TYPES.DELETE_CHITIETPHIEUMUAHANG):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHITIETPHIEUMUAHANG_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHITIETPHIEUMUAHANG):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CHITIETPHIEUMUAHANG):
    case SUCCESS(ACTION_TYPES.UPDATE_CHITIETPHIEUMUAHANG):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CHITIETPHIEUMUAHANG):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'storehouse/api/chi-tiet-phieu-mua-hangs';

// Actions

export const getEntities: ICrudGetAllAction<IChiTietPhieuMuaHang> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_CHITIETPHIEUMUAHANG_LIST,
  payload: axios.get<IChiTietPhieuMuaHang>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IChiTietPhieuMuaHang> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CHITIETPHIEUMUAHANG,
    payload: axios.get<IChiTietPhieuMuaHang>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IChiTietPhieuMuaHang> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CHITIETPHIEUMUAHANG,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IChiTietPhieuMuaHang> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CHITIETPHIEUMUAHANG,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IChiTietPhieuMuaHang> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHITIETPHIEUMUAHANG,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
