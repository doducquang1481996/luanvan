import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './chi-tiet-phieu-mua-hang.reducer';
import { IChiTietPhieuMuaHang } from 'app/shared/model/chi-tiet-phieu-mua-hang.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChiTietPhieuMuaHangProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class ChiTietPhieuMuaHang extends React.Component<IChiTietPhieuMuaHangProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { chiTietPhieuMuaHangList, match } = this.props;
    return (
      <div>
        <h2 id="chi-tiet-phieu-mua-hang-heading">
          Danh Sách chi Tiết Sản Phẩm Của Phiếu
          {/* <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Chi Tiet Phieu Mua Hang
          </Link> */}
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>San Pham Id</th>
                <th>Chi Tiet San Pham Id</th>
                <th>Gia Mua</th>
                <th>So Luong</th>
                <th>Ngay Tao</th>
                <th>Nguoi Tao</th>
                <th>Ngay Cap Nhat</th>
                <th>Nguoi Cap Nhat</th>
                <th>Phieu Mua Hang</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {chiTietPhieuMuaHangList.map((chiTietPhieuMuaHang, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${chiTietPhieuMuaHang.id}`} color="link" size="sm">
                      {chiTietPhieuMuaHang.id}
                    </Button>
                  </td>
                  <td>{chiTietPhieuMuaHang.sanPhamId}</td>
                  <td>{chiTietPhieuMuaHang.chiTietSanPhamId}</td>
                  <td>{chiTietPhieuMuaHang.giaMua}</td>
                  <td>{chiTietPhieuMuaHang.soLuong}</td>
                  <td>
                    <TextFormat type="date" value={chiTietPhieuMuaHang.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{chiTietPhieuMuaHang.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={chiTietPhieuMuaHang.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{chiTietPhieuMuaHang.nguoiCapNhat}</td>
                  <td>
                    {chiTietPhieuMuaHang.phieuMuaHang ? (
                      <Link to={`phieu-mua-hang/${chiTietPhieuMuaHang.phieuMuaHang.id}`}>{chiTietPhieuMuaHang.phieuMuaHang.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${chiTietPhieuMuaHang.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chiTietPhieuMuaHang.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chiTietPhieuMuaHang.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ chiTietPhieuMuaHang }: IRootState) => ({
  chiTietPhieuMuaHangList: chiTietPhieuMuaHang.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietPhieuMuaHang);
