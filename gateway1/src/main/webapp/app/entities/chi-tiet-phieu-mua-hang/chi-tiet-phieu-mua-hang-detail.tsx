import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './chi-tiet-phieu-mua-hang.reducer';
import { IChiTietPhieuMuaHang } from 'app/shared/model/chi-tiet-phieu-mua-hang.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChiTietPhieuMuaHangDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ChiTietPhieuMuaHangDetail extends React.Component<IChiTietPhieuMuaHangDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { chiTietPhieuMuaHangEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            ChiTietPhieuMuaHang [<b>{chiTietPhieuMuaHangEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="sanPhamId">San Pham Id</span>
            </dt>
            <dd>{chiTietPhieuMuaHangEntity.sanPhamId}</dd>
            <dt>
              <span id="chiTietSanPhamId">Chi Tiet San Pham Id</span>
            </dt>
            <dd>{chiTietPhieuMuaHangEntity.chiTietSanPhamId}</dd>
            <dt>
              <span id="giaMua">Gia Mua</span>
            </dt>
            <dd>{chiTietPhieuMuaHangEntity.giaMua}</dd>
            <dt>
              <span id="soLuong">So Luong</span>
            </dt>
            <dd>{chiTietPhieuMuaHangEntity.soLuong}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={chiTietPhieuMuaHangEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{chiTietPhieuMuaHangEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngay Cap Nhat</span>
            </dt>
            <dd>
              <TextFormat value={chiTietPhieuMuaHangEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Nguoi Cap Nhat</span>
            </dt>
            <dd>{chiTietPhieuMuaHangEntity.nguoiCapNhat}</dd>
            <dt>Phieu Mua Hang</dt>
            <dd>{chiTietPhieuMuaHangEntity.phieuMuaHang ? chiTietPhieuMuaHangEntity.phieuMuaHang.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/chi-tiet-phieu-mua-hang" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/chi-tiet-phieu-mua-hang/${chiTietPhieuMuaHangEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ chiTietPhieuMuaHang }: IRootState) => ({
  chiTietPhieuMuaHangEntity: chiTietPhieuMuaHang.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChiTietPhieuMuaHangDetail);
