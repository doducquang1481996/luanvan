import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ChiTietPhieuMuaHang from './chi-tiet-phieu-mua-hang';
import ChiTietPhieuMuaHangDetail from './chi-tiet-phieu-mua-hang-detail';
import ChiTietPhieuMuaHangUpdate from './chi-tiet-phieu-mua-hang-update';
import ChiTietPhieuMuaHangDeleteDialog from './chi-tiet-phieu-mua-hang-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ChiTietPhieuMuaHangUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ChiTietPhieuMuaHangUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ChiTietPhieuMuaHangDetail} />
      <ErrorBoundaryRoute path={match.url} component={ChiTietPhieuMuaHang} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ChiTietPhieuMuaHangDeleteDialog} />
  </>
);

export default Routes;
