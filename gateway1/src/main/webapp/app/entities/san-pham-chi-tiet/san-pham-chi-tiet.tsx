import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './san-pham-chi-tiet.reducer';
import { ISanPhamChiTiet } from 'app/shared/model/san-pham-chi-tiet.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISanPhamChiTietProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class SanPhamChiTiet extends React.Component<ISanPhamChiTietProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { sanPhamChiTietList, match } = this.props;
    return (
      <div>
        <h2 id="san-pham-chi-tiet-heading">
          San Pham Chi Tiets
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new San Pham Chi Tiet
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>San Pham Id</th>
                <th>Ma San Pham Id</th>
                <th>Tenchi Tiet</th>
                <th>Ma San Pham Chi Tiet</th>
                <th>Don Vi Id</th>
                <th>Nguoi Tao</th>
                <th>Ngay Tao</th>
                <th>Nguoi Cap Nhat</th>
                <th>Ngay Cap Nhat</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {sanPhamChiTietList.map((sanPhamChiTiet, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${sanPhamChiTiet.id}`} color="link" size="sm">
                      {sanPhamChiTiet.id}
                    </Button>
                  </td>
                  <td>{sanPhamChiTiet.sanPhamId}</td>
                  <td>{sanPhamChiTiet.maSanPhamId}</td>
                  <td>{sanPhamChiTiet.tenchiTiet}</td>
                  <td>{sanPhamChiTiet.maSanPhamChiTiet}</td>
                  <td>{sanPhamChiTiet.donViId}</td>
                  <td>{sanPhamChiTiet.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={sanPhamChiTiet.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{sanPhamChiTiet.nguoiCapNhat}</td>
                  <td>{sanPhamChiTiet.ngayCapNhat}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${sanPhamChiTiet.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${sanPhamChiTiet.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${sanPhamChiTiet.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ sanPhamChiTiet }: IRootState) => ({
  sanPhamChiTietList: sanPhamChiTiet.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamChiTiet);
