import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './san-pham-chi-tiet.reducer';
import { ISanPhamChiTiet } from 'app/shared/model/san-pham-chi-tiet.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISanPhamChiTietDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class SanPhamChiTietDetail extends React.Component<ISanPhamChiTietDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { sanPhamChiTietEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            SanPhamChiTiet [<b>{sanPhamChiTietEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="sanPhamId">San Pham Id</span>
            </dt>
            <dd>{sanPhamChiTietEntity.sanPhamId}</dd>
            <dt>
              <span id="maSanPhamId">Ma San Pham Id</span>
            </dt>
            <dd>{sanPhamChiTietEntity.maSanPhamId}</dd>
            <dt>
              <span id="tenchiTiet">Tenchi Tiet</span>
            </dt>
            <dd>{sanPhamChiTietEntity.tenchiTiet}</dd>
            <dt>
              <span id="maSanPhamChiTiet">Ma San Pham Chi Tiet</span>
            </dt>
            <dd>{sanPhamChiTietEntity.maSanPhamChiTiet}</dd>
            <dt>
              <span id="donViId">Don Vi Id</span>
            </dt>
            <dd>{sanPhamChiTietEntity.donViId}</dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{sanPhamChiTietEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={sanPhamChiTietEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Nguoi Cap Nhat</span>
            </dt>
            <dd>{sanPhamChiTietEntity.nguoiCapNhat}</dd>
            <dt>
              <span id="ngayCapNhat">Ngay Cap Nhat</span>
            </dt>
            <dd>{sanPhamChiTietEntity.ngayCapNhat}</dd>
          </dl>
          <Button tag={Link} to="/entity/san-pham-chi-tiet" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/san-pham-chi-tiet/${sanPhamChiTietEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ sanPhamChiTiet }: IRootState) => ({
  sanPhamChiTietEntity: sanPhamChiTiet.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamChiTietDetail);
