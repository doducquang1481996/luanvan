import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SanPhamChiTiet from './san-pham-chi-tiet';
import SanPhamChiTietDetail from './san-pham-chi-tiet-detail';
import SanPhamChiTietUpdate from './san-pham-chi-tiet-update';
import SanPhamChiTietDeleteDialog from './san-pham-chi-tiet-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SanPhamChiTietUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SanPhamChiTietUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SanPhamChiTietDetail} />
      <ErrorBoundaryRoute path={match.url} component={SanPhamChiTiet} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SanPhamChiTietDeleteDialog} />
  </>
);

export default Routes;
