import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISanPhamChiTiet, defaultValue } from 'app/shared/model/san-pham-chi-tiet.model';

export const ACTION_TYPES = {
  FETCH_SANPHAMCHITIET_LIST: 'sanPhamChiTiet/FETCH_SANPHAMCHITIET_LIST',
  FETCH_SANPHAMCHITIET: 'sanPhamChiTiet/FETCH_SANPHAMCHITIET',
  CREATE_SANPHAMCHITIET: 'sanPhamChiTiet/CREATE_SANPHAMCHITIET',
  UPDATE_SANPHAMCHITIET: 'sanPhamChiTiet/UPDATE_SANPHAMCHITIET',
  DELETE_SANPHAMCHITIET: 'sanPhamChiTiet/DELETE_SANPHAMCHITIET',
  RESET: 'sanPhamChiTiet/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISanPhamChiTiet>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type SanPhamChiTietState = Readonly<typeof initialState>;

// Reducer

export default (state: SanPhamChiTietState = initialState, action): SanPhamChiTietState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SANPHAMCHITIET_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SANPHAMCHITIET):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SANPHAMCHITIET):
    case REQUEST(ACTION_TYPES.UPDATE_SANPHAMCHITIET):
    case REQUEST(ACTION_TYPES.DELETE_SANPHAMCHITIET):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SANPHAMCHITIET_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SANPHAMCHITIET):
    case FAILURE(ACTION_TYPES.CREATE_SANPHAMCHITIET):
    case FAILURE(ACTION_TYPES.UPDATE_SANPHAMCHITIET):
    case FAILURE(ACTION_TYPES.DELETE_SANPHAMCHITIET):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SANPHAMCHITIET_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SANPHAMCHITIET):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SANPHAMCHITIET):
    case SUCCESS(ACTION_TYPES.UPDATE_SANPHAMCHITIET):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SANPHAMCHITIET):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/san-pham-chi-tiets';

// Actions

export const getEntities: ICrudGetAllAction<ISanPhamChiTiet> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_SANPHAMCHITIET_LIST,
  payload: axios.get<ISanPhamChiTiet>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<ISanPhamChiTiet> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SANPHAMCHITIET,
    payload: axios.get<ISanPhamChiTiet>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISanPhamChiTiet> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SANPHAMCHITIET,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISanPhamChiTiet> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SANPHAMCHITIET,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISanPhamChiTiet> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SANPHAMCHITIET,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
