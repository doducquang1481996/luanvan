import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './san-pham-chi-tiet.reducer';
import { ISanPhamChiTiet } from 'app/shared/model/san-pham-chi-tiet.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISanPhamChiTietUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISanPhamChiTietUpdateState {
  isNew: boolean;
}

export class SanPhamChiTietUpdate extends React.Component<ISanPhamChiTietUpdateProps, ISanPhamChiTietUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { sanPhamChiTietEntity } = this.props;
      const entity = {
        ...sanPhamChiTietEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/san-pham-chi-tiet');
  };

  render() {
    const { sanPhamChiTietEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.sanPhamChiTiet.home.createOrEditLabel">Create or edit a SanPhamChiTiet</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : sanPhamChiTietEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="san-pham-chi-tiet-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="sanPhamIdLabel" for="sanPhamId">
                    San Pham Id
                  </Label>
                  <AvField id="san-pham-chi-tiet-sanPhamId" type="number" className="form-control" name="sanPhamId" />
                </AvGroup>
                <AvGroup>
                  <Label id="maSanPhamIdLabel" for="maSanPhamId">
                    Ma San Pham Id
                  </Label>
                  <AvField id="san-pham-chi-tiet-maSanPhamId" type="text" name="maSanPhamId" />
                </AvGroup>
                <AvGroup>
                  <Label id="tenchiTietLabel" for="tenchiTiet">
                    Tenchi Tiet
                  </Label>
                  <AvField id="san-pham-chi-tiet-tenchiTiet" type="text" name="tenchiTiet" />
                </AvGroup>
                <AvGroup>
                  <Label id="maSanPhamChiTietLabel" for="maSanPhamChiTiet">
                    Ma San Pham Chi Tiet
                  </Label>
                  <AvField id="san-pham-chi-tiet-maSanPhamChiTiet" type="text" name="maSanPhamChiTiet" />
                </AvGroup>
                <AvGroup>
                  <Label id="donViIdLabel" for="donViId">
                    Don Vi Id
                  </Label>
                  <AvField id="san-pham-chi-tiet-donViId" type="number" className="form-control" name="donViId" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="san-pham-chi-tiet-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="san-pham-chi-tiet-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="san-pham-chi-tiet-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="san-pham-chi-tiet-ngayCapNhat" type="text" name="ngayCapNhat" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/san-pham-chi-tiet" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  sanPhamChiTietEntity: storeState.sanPhamChiTiet.entity,
  loading: storeState.sanPhamChiTiet.loading,
  updating: storeState.sanPhamChiTiet.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SanPhamChiTietUpdate);
