import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILoaiThuocTinh, defaultValue } from 'app/shared/model/loai-thuoc-tinh.model';

export const ACTION_TYPES = {
  FETCH_LOAITHUOCTINH_LIST: 'loaiThuocTinh/FETCH_LOAITHUOCTINH_LIST',
  FETCH_LOAITHUOCTINH: 'loaiThuocTinh/FETCH_LOAITHUOCTINH',
  CREATE_LOAITHUOCTINH: 'loaiThuocTinh/CREATE_LOAITHUOCTINH',
  UPDATE_LOAITHUOCTINH: 'loaiThuocTinh/UPDATE_LOAITHUOCTINH',
  DELETE_LOAITHUOCTINH: 'loaiThuocTinh/DELETE_LOAITHUOCTINH',
  RESET: 'loaiThuocTinh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILoaiThuocTinh>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type LoaiThuocTinhState = Readonly<typeof initialState>;

// Reducer

export default (state: LoaiThuocTinhState = initialState, action): LoaiThuocTinhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LOAITHUOCTINH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LOAITHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_LOAITHUOCTINH):
    case REQUEST(ACTION_TYPES.UPDATE_LOAITHUOCTINH):
    case REQUEST(ACTION_TYPES.DELETE_LOAITHUOCTINH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_LOAITHUOCTINH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.CREATE_LOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.UPDATE_LOAITHUOCTINH):
    case FAILURE(ACTION_TYPES.DELETE_LOAITHUOCTINH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_LOAITHUOCTINH_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_LOAITHUOCTINH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_LOAITHUOCTINH):
    case SUCCESS(ACTION_TYPES.UPDATE_LOAITHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_LOAITHUOCTINH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/loai-thuoc-tinhs';

// Actions

export const getEntities: ICrudGetAllAction<ILoaiThuocTinh> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_LOAITHUOCTINH_LIST,
    payload: axios.get<ILoaiThuocTinh>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ILoaiThuocTinh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LOAITHUOCTINH,
    payload: axios.get<ILoaiThuocTinh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ILoaiThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LOAITHUOCTINH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILoaiThuocTinh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LOAITHUOCTINH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILoaiThuocTinh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LOAITHUOCTINH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
