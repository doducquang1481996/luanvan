import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './loai-thuoc-tinh.reducer';
import { ILoaiThuocTinh } from 'app/shared/model/loai-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILoaiThuocTinhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class LoaiThuocTinhDetail extends React.Component<ILoaiThuocTinhDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { loaiThuocTinhEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            LoaiThuocTinh [<b>{loaiThuocTinhEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ten">Ten</span>
            </dt>
            <dd>{loaiThuocTinhEntity.ten}</dd>
            <dt>
              <span id="ma">Ma</span>
            </dt>
            <dd>{loaiThuocTinhEntity.ma}</dd>
            <dt>
              <span id="moTa">Mo Ta</span>
            </dt>
            <dd>{loaiThuocTinhEntity.moTa}</dd>
            <dt>
              <span id="ngayTao">Ngay Tao</span>
            </dt>
            <dd>
              <TextFormat value={loaiThuocTinhEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiTao">Nguoi Tao</span>
            </dt>
            <dd>{loaiThuocTinhEntity.nguoiTao}</dd>
            <dt>
              <span id="ngayCapNhat">Ngay Cap Nhat</span>
            </dt>
            <dd>
              <TextFormat value={loaiThuocTinhEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="nguoiCapNhat">Nguoi Cap Nhat</span>
            </dt>
            <dd>{loaiThuocTinhEntity.nguoiCapNhat}</dd>
            <dt>Nhom Thuoc Tinh</dt>
            <dd>{loaiThuocTinhEntity.nhomThuocTinhId ? loaiThuocTinhEntity.nhomThuocTinhId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/loai-thuoc-tinh" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/loai-thuoc-tinh/${loaiThuocTinhEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ loaiThuocTinh }: IRootState) => ({
  loaiThuocTinhEntity: loaiThuocTinh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiThuocTinhDetail);
