import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { INhomThuocTinh } from 'app/shared/model/nhom-thuoc-tinh.model';
import { getEntities as getNhomThuocTinhs } from 'app/entities/nhom-thuoc-tinh/nhom-thuoc-tinh.reducer';
import { getEntity, updateEntity, createEntity, reset } from './loai-thuoc-tinh.reducer';
import { ILoaiThuocTinh } from 'app/shared/model/loai-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILoaiThuocTinhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ILoaiThuocTinhUpdateState {
  isNew: boolean;
  nhomThuocTinhId: number;
}

export class LoaiThuocTinhUpdate extends React.Component<ILoaiThuocTinhUpdateProps, ILoaiThuocTinhUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      nhomThuocTinhId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getNhomThuocTinhs();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { loaiThuocTinhEntity } = this.props;
      const entity = {
        ...loaiThuocTinhEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/loai-thuoc-tinh');
  };

  render() {
    const { loaiThuocTinhEntity, nhomThuocTinhs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.loaiThuocTinh.home.createOrEditLabel">Create or edit a LoaiThuocTinh</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : loaiThuocTinhEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="loai-thuoc-tinh-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Ten
                  </Label>
                  <AvField id="loai-thuoc-tinh-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Ma
                  </Label>
                  <AvField id="loai-thuoc-tinh-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="moTaLabel" for="moTa">
                    Mo Ta
                  </Label>
                  <AvField id="loai-thuoc-tinh-moTa" type="text" name="moTa" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayTaoLabel" for="ngayTao">
                    Ngay Tao
                  </Label>
                  <AvField id="loai-thuoc-tinh-ngayTao" type="date" className="form-control" name="ngayTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Nguoi Tao
                  </Label>
                  <AvField id="loai-thuoc-tinh-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <AvGroup>
                  <Label id="ngayCapNhatLabel" for="ngayCapNhat">
                    Ngay Cap Nhat
                  </Label>
                  <AvField id="loai-thuoc-tinh-ngayCapNhat" type="date" className="form-control" name="ngayCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiCapNhatLabel" for="nguoiCapNhat">
                    Nguoi Cap Nhat
                  </Label>
                  <AvField id="loai-thuoc-tinh-nguoiCapNhat" type="text" name="nguoiCapNhat" />
                </AvGroup>
                <AvGroup>
                  <Label for="nhomThuocTinh.id">Nhom Thuoc Tinh</Label>
                  <AvInput id="loai-thuoc-tinh-nhomThuocTinh" type="select" className="form-control" name="nhomThuocTinhId">
                    <option value="" key="0" />
                    {nhomThuocTinhs
                      ? nhomThuocTinhs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.ten}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/loai-thuoc-tinh" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  nhomThuocTinhs: storeState.nhomThuocTinh.entities,
  loaiThuocTinhEntity: storeState.loaiThuocTinh.entity,
  loading: storeState.loaiThuocTinh.loading,
  updating: storeState.loaiThuocTinh.updating
});

const mapDispatchToProps = {
  getNhomThuocTinhs,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiThuocTinhUpdate);
