import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './loai-thuoc-tinh.reducer';
import { ILoaiThuocTinh } from 'app/shared/model/loai-thuoc-tinh.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface ILoaiThuocTinhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type ILoaiThuocTinhState = IPaginationBaseState;

export class LoaiThuocTinh extends React.Component<ILoaiThuocTinhProps, ILoaiThuocTinhState> {
  state: ILoaiThuocTinhState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { loaiThuocTinhList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="loai-thuoc-tinh-heading">
          Loai Thuoc Tinhs
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Loai Thuoc Tinh
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ten')}>
                  Ten <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ma')}>
                  Ma <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('moTa')}>
                  Mo Ta <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayTao')}>
                  Ngay Tao <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiTao')}>
                  Nguoi Tao <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ngayCapNhat')}>
                  Ngay Cap Nhat <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nguoiCapNhat')}>
                  Nguoi Cap Nhat <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Nhom Thuoc Tinh <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {loaiThuocTinhList.map((loaiThuocTinh, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${loaiThuocTinh.id}`} color="link" size="sm">
                      {loaiThuocTinh.id}
                    </Button>
                  </td>
                  <td>{loaiThuocTinh.ten}</td>
                  <td>{loaiThuocTinh.ma}</td>
                  <td>{loaiThuocTinh.moTa}</td>
                  <td>
                    <TextFormat type="date" value={loaiThuocTinh.ngayTao} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{loaiThuocTinh.nguoiTao}</td>
                  <td>
                    <TextFormat type="date" value={loaiThuocTinh.ngayCapNhat} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{loaiThuocTinh.nguoiCapNhat}</td>
                  <td>
                    {loaiThuocTinh.nhomThuocTinhId ? (
                      <Link to={`nhom-thuoc-tinh/${loaiThuocTinh.nhomThuocTinhId}`}>{loaiThuocTinh.nhomThuocTinhId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${loaiThuocTinh.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${loaiThuocTinh.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${loaiThuocTinh.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ loaiThuocTinh }: IRootState) => ({
  loaiThuocTinhList: loaiThuocTinh.entities,
  totalItems: loaiThuocTinh.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoaiThuocTinh);
