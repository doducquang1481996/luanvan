import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import LoaiThuocTinh from './loai-thuoc-tinh';
import LoaiThuocTinhDetail from './loai-thuoc-tinh-detail';
import LoaiThuocTinhUpdate from './loai-thuoc-tinh-update';
import LoaiThuocTinhDeleteDialog from './loai-thuoc-tinh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LoaiThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LoaiThuocTinhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LoaiThuocTinhDetail} />
      <ErrorBoundaryRoute path={match.url} component={LoaiThuocTinh} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={LoaiThuocTinhDeleteDialog} />
  </>
);

export default Routes;
