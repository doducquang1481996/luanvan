import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './kho.reducer';
import { IKho } from 'app/shared/model/kho.model';
import { getSession } from 'app/shared/reducers/authentication';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IKhoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IKhoUpdateState {
  isNew: boolean;
}

export class KhoUpdate extends React.Component<IKhoUpdateProps, IKhoUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { khoEntity } = this.props;
      const entity = {
        ...khoEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/kho');
  };

  render() {
    const { khoEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.kho.home.createOrEditLabel">Tạo Mới Kho</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : khoEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="kho-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tenLabel" for="ten">
                    Tên Kho
                  </Label>
                  <AvField id="kho-ten" type="text" name="ten" />
                </AvGroup>
                <AvGroup>
                  <Label id="maLabel" for="ma">
                    Mã Kho
                  </Label>
                  <AvField id="kho-ma" type="text" name="ma" />
                </AvGroup>
                <AvGroup>
                  <Label id="ghiChiLabel" for="ghiChi">
                    Ghi chú
                  </Label>
                  <AvField id="kho-ghiChi" type="text" name="ghiChi" />
                </AvGroup>
                <AvGroup>
                  <Label id="nguoiTaoLabel" for="nguoiTao">
                    Người Tạo
                  </Label>
                  <AvField id="kho-nguoiTao" type="text" name="nguoiTao" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/kho" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Trở Lại</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Lưu
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  khoEntity: storeState.kho.entity,
  loading: storeState.kho.loading,
  updating: storeState.kho.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KhoUpdate);
