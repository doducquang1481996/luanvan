import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './kho.reducer';
import { IKho } from 'app/shared/model/kho.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IKhoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class KhoDetail extends React.Component<IKhoDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { khoEntity } = this.props;
    return (
      <div>
        <Row>
          <Col md="8">
            <h2>
              Thông Tin Kho [<b>{khoEntity.ten}</b>]
            </h2>
            <dl className="jh-entity-details">
              <dt>
                <span id="ten">Tên Kho</span>
              </dt>
              <dd>{khoEntity.ten}</dd>
              <dt>
                <span id="ma">Mã Kho</span>
              </dt>
              <dd>{khoEntity.ma}</dd>
              <dt>
                <span id="ghiChi">Ghi chú</span>
              </dt>
              <dd>{khoEntity.ghiChi}</dd>
              <dt>
                <span id="ngayTao">Ngày Tạo</span>
              </dt>
              <dd>
                <TextFormat value={khoEntity.ngayTao} type="date" format={APP_LOCAL_DATE_FORMAT} />
              </dd>
              <dt>
                <span id="nguoiTao">Người Tạo</span>
              </dt>
              <dd>{khoEntity.nguoiTao}</dd>
              <dt>
                <span id="ngayCapNhat">Ngày Cập Nhật</span>
              </dt>
              <dd>
                <TextFormat value={khoEntity.ngayCapNhat} type="date" format={APP_LOCAL_DATE_FORMAT} />
              </dd>
              <dt>
                <span id="nguoiCapNhat">Người Cập Nhật</span>
              </dt>
              <dd>{khoEntity.nguoiCapNhat}</dd>
            </dl>
            <Button tag={Link} to="/entity/kho" replace color="info">
              <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Trở Lại</span>
            </Button>&nbsp;
            <Button tag={Link} to={`/entity/kho/${khoEntity.id}/edit`} replace color="primary">
              <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Cập Nhật</span>
            </Button>&nbsp;
            <Button tag={Link} to={`/entity/kho/${khoEntity.id}/edit/add-vi-tri-kho`} replace color="primary">
              <FontAwesomeIcon icon="plus" /> <span className="d-none d-md-inline">Thêm Vị Trí</span>
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ kho }: IRootState) => ({
  khoEntity: kho.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KhoDetail);
