import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Kho from './kho';
import KhoDetail from './kho-detail';
import KhoUpdate from './kho-update';
import KhoDeleteDialog from './kho-delete-dialog';
import ViTriKhoUpdate from '../vi-tri-kho/vi-tri-kho-update';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={KhoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={KhoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={KhoDetail} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit/add-vi-tri-kho`} component={ViTriKhoUpdate} />
      <ErrorBoundaryRoute path={match.url} component={Kho} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={KhoDeleteDialog} />
  </>
);

export default Routes;
