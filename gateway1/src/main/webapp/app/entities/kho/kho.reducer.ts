import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IKho, defaultValue } from 'app/shared/model/kho.model';

export const ACTION_TYPES = {
  FETCH_KHO_LIST: 'kho/FETCH_KHO_LIST',
  FETCH_KHO: 'kho/FETCH_KHO',
  CREATE_KHO: 'kho/CREATE_KHO',
  UPDATE_KHO: 'kho/UPDATE_KHO',
  DELETE_KHO: 'kho/DELETE_KHO',
  RESET: 'kho/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IKho>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type KhoState = Readonly<typeof initialState>;

// Reducer

export default (state: KhoState = initialState, action): KhoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_KHO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_KHO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_KHO):
    case REQUEST(ACTION_TYPES.UPDATE_KHO):
    case REQUEST(ACTION_TYPES.DELETE_KHO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_KHO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_KHO):
    case FAILURE(ACTION_TYPES.CREATE_KHO):
    case FAILURE(ACTION_TYPES.UPDATE_KHO):
    case FAILURE(ACTION_TYPES.DELETE_KHO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_KHO_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_KHO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_KHO):
    case SUCCESS(ACTION_TYPES.UPDATE_KHO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_KHO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'library/api/khos';

// Actions

export const getEntities: ICrudGetAllAction<IKho> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_KHO_LIST,
    payload: axios.get<IKho>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IKho> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_KHO,
    payload: axios.get<IKho>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IKho> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_KHO,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IKho> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_KHO,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IKho> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_KHO,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
