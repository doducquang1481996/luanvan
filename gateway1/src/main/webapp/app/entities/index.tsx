import React from 'react';
import { Switch } from 'react-router-dom';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Room from './room';
import Employee from './employee';
import FuncitonSystem from './funciton-system';
import NhaCungCap from './nha-cung-cap';
import NhaSanXuat from './nha-san-xuat';
import RoomFuncitonSystem from './room-funciton-system';
import AccountFuncitonSystem from './account-funciton-system';
import NhomSanPham from './nhom-san-pham';
import LoaiSanPham from './loai-san-pham';
import Kho from './kho';
import ViTriKho from './vi-tri-kho';
import NhomThuocTinh from './nhom-thuoc-tinh';
import LoaiThuocTinh from './loai-thuoc-tinh';
import DonVi from './don-vi';
import NhomDonVi from './nhom-don-vi';
import SanPham from './san-pham';
import ThuocTinh from './thuoc-tinh';
import LoaiSanPhamLoaiThuocTinh from './loai-san-pham-loai-thuoc-tinh';
import SanPhamThuocTinh from './san-pham-thuoc-tinh';
import SanPhamLoaiThuocTinh from './san-pham-loai-thuoc-tinh';
import HinhAnhSanPham from './hinh-anh-san-pham';
import SanPhamChiTiet from './san-pham-chi-tiet';
import ThongTinChiTietSanPham from './thong-tin-chi-tiet-san-pham';
import PhieuMuaHang from './phieu-mua-hang';
import ChiTietPhieuMuaHang from './chi-tiet-phieu-mua-hang';
import BangDinhMuc from './bang-dinh-muc';
import PhieuGiamDinh from './phieu-giam-dinh';
import ChiTietPhieuGiamDinh from './chi-tiet-phieu-giam-dinh';
import ChiTietGiamDinh from './chi-tiet-giam-dinh';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}/room`} component={Room} />
      <ErrorBoundaryRoute path={`${match.url}/employee`} component={Employee} />
      <ErrorBoundaryRoute path={`${match.url}/funciton-system`} component={FuncitonSystem} />
      <ErrorBoundaryRoute path={`${match.url}/nha-cung-cap`} component={NhaCungCap} />
      <ErrorBoundaryRoute path={`${match.url}/nha-san-xuat`} component={NhaSanXuat} />
      <ErrorBoundaryRoute path={`${match.url}/room-funciton-system`} component={RoomFuncitonSystem} />
      <ErrorBoundaryRoute path={`${match.url}/account-funciton-system`} component={AccountFuncitonSystem} />
      <ErrorBoundaryRoute path={`${match.url}/nhom-san-pham`} component={NhomSanPham} />
      <ErrorBoundaryRoute path={`${match.url}/loai-san-pham`} component={LoaiSanPham} />
      <ErrorBoundaryRoute path={`${match.url}/kho`} component={Kho} />
      {/* <ErrorBoundaryRoute path={`${match.url}/vi-tri-kho`} component={ViTriKho} /> */}
      <ErrorBoundaryRoute path={`${match.url}/nhom-thuoc-tinh`} component={NhomThuocTinh} />
      <ErrorBoundaryRoute path={`${match.url}/loai-thuoc-tinh`} component={LoaiThuocTinh} />
      <ErrorBoundaryRoute path={`${match.url}/don-vi`} component={DonVi} />
      <ErrorBoundaryRoute path={`${match.url}/nhom-don-vi`} component={NhomDonVi} />
      <ErrorBoundaryRoute path={`${match.url}/san-pham`} component={SanPham} />
      <ErrorBoundaryRoute path={`${match.url}/thuoc-tinh`} component={ThuocTinh} />
      <ErrorBoundaryRoute path={`${match.url}/loai-san-pham-loai-thuoc-tinh`} component={LoaiSanPhamLoaiThuocTinh} />
      <ErrorBoundaryRoute path={`${match.url}/san-pham-thuoc-tinh`} component={SanPhamThuocTinh} />
      <ErrorBoundaryRoute path={`${match.url}/san-pham-loai-thuoc-tinh`} component={SanPhamLoaiThuocTinh} />
      <ErrorBoundaryRoute path={`${match.url}/hinh-anh-san-pham`} component={HinhAnhSanPham} />
      <ErrorBoundaryRoute path={`${match.url}/san-pham-chi-tiet`} component={SanPhamChiTiet} />
      <ErrorBoundaryRoute path={`${match.url}/thong-tin-chi-tiet-san-pham`} component={ThongTinChiTietSanPham} />
      <ErrorBoundaryRoute path={`${match.url}/phieu-mua-hang`} component={PhieuMuaHang} />
      <ErrorBoundaryRoute path={`${match.url}/chi-tiet-phieu-mua-hang`} component={ChiTietPhieuMuaHang} />
      <ErrorBoundaryRoute path={`${match.url}/bang-dinh-muc`} component={BangDinhMuc} />
      <ErrorBoundaryRoute path={`${match.url}/phieu-giam-dinh`} component={PhieuGiamDinh} />
      <ErrorBoundaryRoute path={`${match.url}/chi-tiet-phieu-giam-dinh`} component={ChiTietPhieuGiamDinh} />
      <ErrorBoundaryRoute path={`${match.url}/chi-tiet-giam-dinh`} component={ChiTietGiamDinh} />
      {/* jhipster-needle-add-route-path - JHipster will routes here */}
    </Switch>
  </div>
);

export default Routes;
