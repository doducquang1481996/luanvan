import React from 'react';

import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Alert, Row, Col } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Link } from 'react-router-dom';

export interface ILoginModalProps {
  showModal: boolean;
  loginError: boolean;
  handleLogin: Function;
  handleClose: Function;
}

class LoginModal extends React.Component<ILoginModalProps> {
  handleSubmit = (event, errors, { username, password, rememberMe }) => {
    const { handleLogin } = this.props;
    handleLogin(username, password, rememberMe);
  };

  render() {
    const { loginError, handleClose } = this.props;

    return (
      <Modal isOpen={this.props.showModal} toggle={handleClose} backdrop="static" id="login-page" autoFocus={false}>
        <AvForm onSubmit={this.handleSubmit}>
          <ModalHeader id="login-title" toggle={handleClose}>
            Đăng nhập
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col md="12">
                {loginError ? (
                  <Alert color="danger">
                    <strong>Đăng nhập thất bại!</strong> Làm ơn kiểm tra lại tài khoản và mật khẩu.
                  </Alert>
                ) : null}
              </Col>
              <Col md="12">
                <AvField
                  name="username"
                  label="Tài khoản"
                  placeholder="Tên Tài khoản"
                  required
                  errorMessage="Chưa nhập tài khoản!"
                  autoFocus
                />
                <AvField
                  name="password"
                  type="Mật khẩu"
                  label="Mật khẩu"
                  placeholder="Your password"
                  required
                  errorMessage="Chưa nhập mât khẩu!"
                />
                <AvGroup check inline>
                  <Label className="form-check-label">
                    <AvInput type="checkbox" name="rememberMe" /> Nhớ tài khoản
                  </Label>
                </AvGroup>
              </Col>
            </Row>
            <div className="mt-1">&nbsp;</div>
            <Alert color="warning">
              <Link to="/reset/request">Quên mật khẩu?</Link>
            </Alert>
            {/* <Alert color="warning">
              <span>You don't have an account yet?</span> <Link to="/register">Register a new account</Link>
            </Alert> */}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={handleClose} tabIndex="1">
              Trở Lại
            </Button>{' '}
            <Button color="primary" type="submit">
              Đăng nhập
            </Button>
          </ModalFooter>
        </AvForm>
      </Modal>
    );
  }
}

export default LoginModal;
