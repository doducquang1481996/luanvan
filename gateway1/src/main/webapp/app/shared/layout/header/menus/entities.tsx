import React from 'react';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from '../header-components';

export const EntitiesMenu = props => (
  // tslint:disable-next-line:jsx-self-close
  <NavDropdown icon="th-list" name="Chức Năng" id="entity-menu">
    <DropdownItem tag={Link} to="/entity/room">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản lý phòng ban
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/employee">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản lý nhân viên
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/kho">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Kho
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/loai-san-pham">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Loại Sản Phẩm
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/loai-thuoc-tinh">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Loại Thuộc Tính
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/nha-cung-cap">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Nhà Cung Cấp
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/nha-san-xuat">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Nhà Sản Xuất
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/don-vi">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Đơn Vị
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/nhom-don-vi">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Nhóm Đơn Vị
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/nhom-thuoc-tinh">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Nhóm Thuộc Tính
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/san-pham">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản lý sản phẩm
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/thuoc-tinh">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản lý thuộc tính
    </DropdownItem>
    {/* <DropdownItem tag={Link} to="/entity/loai-san-pham-loai-thuoc-tinh">
      <FontAwesomeIcon icon="asterisk" />&nbsp;Loai San Pham Loai Thuoc Tinh
    </DropdownItem> */}
    {/* <DropdownItem tag={Link} to="/entity/san-pham-thuoc-tinh">
      <FontAwesomeIcon icon="asterisk" />&nbsp;San Pham Thuoc Tinh
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/san-pham-loai-thuoc-tinh">
      <FontAwesomeIcon icon="asterisk" />&nbsp;San Pham Loai Thuoc Tinh
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/hinh-anh-san-pham">
      <FontAwesomeIcon icon="asterisk" />&nbsp;Hinh Anh San Pham
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/san-pham-chi-tiet">
      <FontAwesomeIcon icon="asterisk" />&nbsp;San Pham Chi Tiet
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/thong-tin-chi-tiet-san-pham">
      <FontAwesomeIcon icon="asterisk" />&nbsp;Thong Tin Chi Tiet San Pham
    </DropdownItem> */}
    <DropdownItem tag={Link} to="/entity/phieu-mua-hang">
      <FontAwesomeIcon icon="indent" />&nbsp;Quản Lý Mua Hàng
    </DropdownItem>
    {/* <DropdownItem tag={Link} to="/entity/chi-tiet-phieu-mua-hang">
      <FontAwesomeIcon icon="asterisk" />&nbsp;Chi Tiet Phieu Mua Hang
    </DropdownItem> */}
    {/* <DropdownItem tag={Link} to="/entity/bang-dinh-muc">
      <FontAwesomeIcon icon="asterisk" />&nbsp;Bang Dinh Muc
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/phieu-giam-dinh">
      <FontAwesomeIcon icon="asterisk" />&nbsp;Phieu Giam Dinh
    </DropdownItem> */}
    {/* <DropdownItem tag={Link} to="/entity/chi-tiet-phieu-giam-dinh">
      <FontAwesomeIcon icon="asterisk" />&nbsp;Chi Tiet Phieu Giam Dinh
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/chi-tiet-giam-dinh">
      <FontAwesomeIcon icon="asterisk" />&nbsp;Chi Tiet Giam Dinh
    </DropdownItem> */}
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
