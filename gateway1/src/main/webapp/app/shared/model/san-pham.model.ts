import { Moment } from 'moment';

export interface ISanPham {
  id?: number;
  ten?: string;
  ma?: string;
  moTa?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  loaiSanPhamId?: number;
  nhaSanXuatId?: number;
}

export const defaultValue: Readonly<ISanPham> = {};
