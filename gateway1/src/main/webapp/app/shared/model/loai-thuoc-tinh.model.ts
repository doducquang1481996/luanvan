import { Moment } from 'moment';

export interface ILoaiThuocTinh {
  id?: number;
  ten?: string;
  ma?: string;
  moTa?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  nhomThuocTinhId?: number;
}

export const defaultValue: Readonly<ILoaiThuocTinh> = {};
