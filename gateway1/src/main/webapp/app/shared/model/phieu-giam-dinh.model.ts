import { Moment } from 'moment';

export interface IPhieuGiamDinh {
  id?: number;
  ma?: string;
  ten?: string;
  ngayGiamDinh?: Moment;
  phieuMuaHangId?: number;
  maPhieuMuaHang?: string;
  trangThai?: boolean;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
}

export const defaultValue: Readonly<IPhieuGiamDinh> = {
  trangThai: false
};
