import { Moment } from 'moment';

export interface INhaCungCap {
  id?: number;
  ten?: string;
  ma?: string;
  diaChi?: string;
  soDienThoai?: string;
  website?: string;
  fax?: string;
  maSoThue?: string;
  nguoiPhuTrach?: string;
  active?: boolean;
  warning?: boolean;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
}

export const defaultValue: Readonly<INhaCungCap> = {
  active: false,
  warning: false
};
