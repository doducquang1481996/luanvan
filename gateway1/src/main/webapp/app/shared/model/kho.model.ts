import { Moment } from 'moment';

export interface IKho {
  id?: number;
  ten?: string;
  ma?: string;
  ghiChi?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
}

export const defaultValue: Readonly<IKho> = {};
