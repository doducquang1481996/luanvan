import { Moment } from 'moment';

export interface IPhieuMuaHang {
  id?: number;
  ten?: string;
  ma?: string;
  nhaCungCap?: number;
  tenNhaCungCap?: string;
  trangThai?: boolean;
  duyet?: boolean;
  ngayDatMua?: Moment;
  ngayNhan?: Moment;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
}

export const defaultValue: Readonly<IPhieuMuaHang> = {
  trangThai: false,
  duyet: false
};
