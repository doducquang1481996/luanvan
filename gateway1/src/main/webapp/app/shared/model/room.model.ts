import { Moment } from 'moment';
import { IEmployee } from 'app/shared/model//employee.model';

export interface IRoom {
  id?: number;
  roomName?: string;
  roomCode?: string;
  parentId?: number;
  createDate?: Moment;
  createPerson?: string;
  updateDate?: Moment;
  updatePerson?: string;
  rooms?: IEmployee[];
}

export const defaultValue: Readonly<IRoom> = {};
