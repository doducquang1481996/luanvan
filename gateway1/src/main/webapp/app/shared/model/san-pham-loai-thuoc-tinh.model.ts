import { Moment } from 'moment';

export interface ISanPhamLoaiThuocTinh {
  id?: number;
  sanPhamId?: number;
  loaiThuocTinhId?: number;
  ngayTao?: Moment;
  nguoiTao?: string;
}

export const defaultValue: Readonly<ISanPhamLoaiThuocTinh> = {};
