import { Moment } from 'moment';

export interface IHinhAnhSanPham {
  id?: number;
  sanPhamId?: number;
  hinhAnhContentType?: string;
  hinhAnh?: any;
  tieuDe?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
}

export const defaultValue: Readonly<IHinhAnhSanPham> = {};
