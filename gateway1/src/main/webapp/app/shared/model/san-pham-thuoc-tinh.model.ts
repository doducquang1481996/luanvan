import { Moment } from 'moment';
import { IThongTinChiTietSanPham } from 'app/shared/model//thong-tin-chi-tiet-san-pham.model';

export interface ISanPhamThuocTinh {
  id?: number;
  sanPhamId?: number;
  loaiThuocTinhId?: number;
  thuocTinhId?: number;
  ngayTao?: Moment;
  nguoiTao?: string;
  thongTinChiTietSanPham?: IThongTinChiTietSanPham;
}

export const defaultValue: Readonly<ISanPhamThuocTinh> = {};
