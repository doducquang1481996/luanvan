import { Moment } from 'moment';

export interface IDonVi {
  id?: number;
  ten?: string;
  ma?: string;
  moTa?: string;
  quyDoi?: number;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  nhomDonViId?: number;
}

export const defaultValue: Readonly<IDonVi> = {};
