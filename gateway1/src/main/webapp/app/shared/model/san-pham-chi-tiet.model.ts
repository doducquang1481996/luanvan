import { Moment } from 'moment';

export interface ISanPhamChiTiet {
  id?: number;
  sanPhamId?: number;
  maSanPhamId?: string;
  tenchiTiet?: string;
  maSanPhamChiTiet?: string;
  donViId?: number;
  nguoiTao?: string;
  ngayTao?: Moment;
  nguoiCapNhat?: string;
  ngayCapNhat?: string;
}

export const defaultValue: Readonly<ISanPhamChiTiet> = {};
