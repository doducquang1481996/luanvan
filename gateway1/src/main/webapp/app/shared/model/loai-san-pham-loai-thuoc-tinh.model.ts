import { Moment } from 'moment';

export interface ILoaiSanPhamLoaiThuocTinh {
  id?: number;
  loaiSanPhamId?: number;
  loaiThuocTinhId?: number;
  ngayTao?: Moment;
  nguoiTao?: string;
}

export const defaultValue: Readonly<ILoaiSanPhamLoaiThuocTinh> = {};
