import { Moment } from 'moment';

export interface IThuocTinh {
  id?: number;
  ten?: string;
  ma?: string;
  moTa?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  loaiThuocTinhId?: number;
}

export const defaultValue: Readonly<IThuocTinh> = {};
