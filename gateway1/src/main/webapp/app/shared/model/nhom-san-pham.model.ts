import { Moment } from 'moment';

export interface INhomSanPham {
  id?: number;
  ten?: string;
  ma?: string;
  moTa?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
}

export const defaultValue: Readonly<INhomSanPham> = {};
