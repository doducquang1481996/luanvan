import { Moment } from 'moment';

export interface IEmployee {
  id?: number;
  employeeName?: string;
  employeeCode?: string;
  email?: string;
  dateBirth?: Moment;
  cmnd?: string;
  numberPhone?: string;
  imageContentType?: string;
  image?: any;
  location?: string;
  placeOfBirth?: string;
  sex?: string;
  dayToDo?: Moment;
  active?: boolean;
  createDate?: Moment;
  createPerson?: string;
  updateDate?: Moment;
  updatePerson?: string;
  roomId?: number;
}

export const defaultValue: Readonly<IEmployee> = {
  active: false
};
