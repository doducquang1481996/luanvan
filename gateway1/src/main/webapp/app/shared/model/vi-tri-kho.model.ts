import { Moment } from 'moment';

export interface IViTriKho {
  id?: number;
  ten?: string;
  ma?: string;
  moTa?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  khoId?: number;
}

export const defaultValue: Readonly<IViTriKho> = {};
