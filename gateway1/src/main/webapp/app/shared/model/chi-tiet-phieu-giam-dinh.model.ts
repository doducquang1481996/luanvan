import { Moment } from 'moment';
import { IPhieuGiamDinh } from 'app/shared/model//phieu-giam-dinh.model';

export interface IChiTietPhieuGiamDinh {
  id?: number;
  ma?: string;
  chiTietPhieuMuaHangId?: number;
  tinhTrang?: boolean;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  phieuGiamDinh?: IPhieuGiamDinh;
}

export const defaultValue: Readonly<IChiTietPhieuGiamDinh> = {
  tinhTrang: false
};
