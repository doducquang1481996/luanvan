import { Moment } from 'moment';

export interface ILoaiSanPham {
  id?: number;
  ten?: string;
  ma?: string;
  moTa?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  active?: boolean;
  nhomSanPhamId?: number;
}

export const defaultValue: Readonly<ILoaiSanPham> = {
  active: false
};
