import { Moment } from 'moment';

export interface IBangDinhMuc {
  id?: number;
  phieuMuaHangId?: number;
  ma?: string;
  trangThai?: boolean;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
}

export const defaultValue: Readonly<IBangDinhMuc> = {
  trangThai: false
};
