import { Moment } from 'moment';
import { IChiTietPhieuGiamDinh } from 'app/shared/model//chi-tiet-phieu-giam-dinh.model';

export interface IChiTietGiamDinh {
  id?: number;
  soLuong?: number;
  datYeuCau?: number;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  chiTietPhieuGiamDinh?: IChiTietPhieuGiamDinh;
}

export const defaultValue: Readonly<IChiTietGiamDinh> = {};
