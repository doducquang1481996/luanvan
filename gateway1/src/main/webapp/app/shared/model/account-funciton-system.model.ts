export interface IAccountFuncitonSystem {
  id?: number;
  accountId?: number;
  funcitionId?: number;
}

export const defaultValue: Readonly<IAccountFuncitonSystem> = {};
