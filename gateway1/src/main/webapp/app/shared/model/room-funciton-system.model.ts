export interface IRoomFuncitonSystem {
  id?: number;
  roomId?: number;
  funcitionId?: number;
}

export const defaultValue: Readonly<IRoomFuncitonSystem> = {};
