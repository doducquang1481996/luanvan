export interface IFuncitonSystem {
  id?: number;
  funcitionName?: string;
  funcitionCode?: string;
  funcitionLink?: string;
  note?: string;
  parentId?: number;
}

export const defaultValue: Readonly<IFuncitonSystem> = {};
