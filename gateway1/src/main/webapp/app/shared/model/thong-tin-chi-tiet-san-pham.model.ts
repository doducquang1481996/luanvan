import { Moment } from 'moment';
import { ISanPhamThuocTinh } from 'app/shared/model//san-pham-thuoc-tinh.model';
import { ISanPhamChiTiet } from 'app/shared/model//san-pham-chi-tiet.model';

export interface IThongTinChiTietSanPham {
  id?: number;
  sanPhamThuocTinhId?: number;
  ngayTao?: Moment;
  nguoiTao?: string;
  sanPhamThuocTinhs?: ISanPhamThuocTinh[];
  sanPhamChiTiet?: ISanPhamChiTiet;
}

export const defaultValue: Readonly<IThongTinChiTietSanPham> = {};
