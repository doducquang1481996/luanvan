import { Moment } from 'moment';
import { IPhieuMuaHang } from 'app/shared/model//phieu-mua-hang.model';

export interface IChiTietPhieuMuaHang {
  id?: number;
  sanPhamId?: number;
  chiTietSanPhamId?: number;
  giaMua?: number;
  soLuong?: number;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
  phieuMuaHang?: IPhieuMuaHang;
}

export const defaultValue: Readonly<IChiTietPhieuMuaHang> = {};
