import { Moment } from 'moment';

export interface INhaSanXuat {
  id?: number;
  ten?: string;
  ma?: string;
  diaChi?: string;
  soDienThoai?: string;
  website?: string;
  email?: string;
  fax?: string;
  ngayTao?: Moment;
  nguoiTao?: string;
  ngayCapNhat?: Moment;
  nguoiCapNhat?: string;
}

export const defaultValue: Readonly<INhaSanXuat> = {};
