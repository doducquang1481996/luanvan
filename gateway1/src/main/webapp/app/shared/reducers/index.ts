import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';

// prettier-ignore
// prettier-ignore
import room, {
  RoomState
} from 'app/entities/room/room.reducer';
// prettier-ignore
import employee, {
  EmployeeState
} from 'app/entities/employee/employee.reducer';
// prettier-ignore
import funcitonSystem, {
  FuncitonSystemState
} from 'app/entities/funciton-system/funciton-system.reducer';
// prettier-ignore
import nhaCungCap, {
  NhaCungCapState
} from 'app/entities/nha-cung-cap/nha-cung-cap.reducer';
// prettier-ignore
import nhaSanXuat, {
  NhaSanXuatState
} from 'app/entities/nha-san-xuat/nha-san-xuat.reducer';

// prettier-ignore
import roomFuncitonSystem, {
  RoomFuncitonSystemState
} from 'app/entities/room-funciton-system/room-funciton-system.reducer';
// prettier-ignore
import accountFuncitonSystem, {
  AccountFuncitonSystemState
} from 'app/entities/account-funciton-system/account-funciton-system.reducer';
// prettier-ignore
import nhomSanPham, {
  NhomSanPhamState
} from 'app/entities/nhom-san-pham/nhom-san-pham.reducer';
// prettier-ignore
import loaiSanPham, {
  LoaiSanPhamState
} from 'app/entities/loai-san-pham/loai-san-pham.reducer';
// prettier-ignore
import kho, {
  KhoState
} from 'app/entities/kho/kho.reducer';
// prettier-ignore
import viTriKho, {
  ViTriKhoState
} from 'app/entities/vi-tri-kho/vi-tri-kho.reducer';
// prettier-ignore
import nhomThuocTinh, {
  NhomThuocTinhState
} from 'app/entities/nhom-thuoc-tinh/nhom-thuoc-tinh.reducer';
// prettier-ignore
import loaiThuocTinh, {
  LoaiThuocTinhState
} from 'app/entities/loai-thuoc-tinh/loai-thuoc-tinh.reducer';
// prettier-ignore
import donVi, {
  DonViState
} from 'app/entities/don-vi/don-vi.reducer';
// prettier-ignore
import nhomDonVi, {
  NhomDonViState
} from 'app/entities/nhom-don-vi/nhom-don-vi.reducer';
// prettier-ignore
import sanPham, {
  SanPhamState
} from 'app/entities/san-pham/san-pham.reducer';
// prettier-ignore
import thuocTinh, {
  ThuocTinhState
} from 'app/entities/thuoc-tinh/thuoc-tinh.reducer';
// prettier-ignore
import loaiSanPhamLoaiThuocTinh, {
  LoaiSanPhamLoaiThuocTinhState
} from 'app/entities/loai-san-pham-loai-thuoc-tinh/loai-san-pham-loai-thuoc-tinh.reducer';
// prettier-ignore
import sanPhamThuocTinh, {
  SanPhamThuocTinhState
} from 'app/entities/san-pham-thuoc-tinh/san-pham-thuoc-tinh.reducer';
// prettier-ignore
import sanPhamLoaiThuocTinh, {
  SanPhamLoaiThuocTinhState
} from 'app/entities/san-pham-loai-thuoc-tinh/san-pham-loai-thuoc-tinh.reducer';
// prettier-ignore
import hinhAnhSanPham, {
  HinhAnhSanPhamState
} from 'app/entities/hinh-anh-san-pham/hinh-anh-san-pham.reducer';
// prettier-ignore
import sanPhamChiTiet, {
  SanPhamChiTietState
} from 'app/entities/san-pham-chi-tiet/san-pham-chi-tiet.reducer';
// prettier-ignore
import thongTinChiTietSanPham, {
  ThongTinChiTietSanPhamState
} from 'app/entities/thong-tin-chi-tiet-san-pham/thong-tin-chi-tiet-san-pham.reducer';
// prettier-ignore
import phieuMuaHang, {
  PhieuMuaHangState
} from 'app/entities/phieu-mua-hang/phieu-mua-hang.reducer';
// prettier-ignore
import chiTietPhieuMuaHang, {
  ChiTietPhieuMuaHangState
} from 'app/entities/chi-tiet-phieu-mua-hang/chi-tiet-phieu-mua-hang.reducer';
// prettier-ignore
import bangDinhMuc, {
  BangDinhMucState
} from 'app/entities/bang-dinh-muc/bang-dinh-muc.reducer';
// prettier-ignore
import phieuGiamDinh, {
  PhieuGiamDinhState
} from 'app/entities/phieu-giam-dinh/phieu-giam-dinh.reducer';
// prettier-ignore
import chiTietPhieuGiamDinh, {
  ChiTietPhieuGiamDinhState
} from 'app/entities/chi-tiet-phieu-giam-dinh/chi-tiet-phieu-giam-dinh.reducer';
// prettier-ignore
import chiTietGiamDinh, {
  ChiTietGiamDinhState
} from 'app/entities/chi-tiet-giam-dinh/chi-tiet-giam-dinh.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly room: RoomState;
  readonly employee: EmployeeState;
  readonly funcitonSystem: FuncitonSystemState;
  readonly nhaCungCap: NhaCungCapState;
  readonly nhaSanXuat: NhaSanXuatState;
  readonly roomFuncitonSystem: RoomFuncitonSystemState;
  readonly accountFuncitonSystem: AccountFuncitonSystemState;
  readonly nhomSanPham: NhomSanPhamState;
  readonly loaiSanPham: LoaiSanPhamState;
  readonly kho: KhoState;
  readonly viTriKho: ViTriKhoState;
  readonly nhomThuocTinh: NhomThuocTinhState;
  readonly loaiThuocTinh: LoaiThuocTinhState;
  readonly donVi: DonViState;
  readonly nhomDonVi: NhomDonViState;
  readonly sanPham: SanPhamState;
  readonly thuocTinh: ThuocTinhState;
  readonly loaiSanPhamLoaiThuocTinh: LoaiSanPhamLoaiThuocTinhState;
  readonly sanPhamThuocTinh: SanPhamThuocTinhState;
  readonly sanPhamLoaiThuocTinh: SanPhamLoaiThuocTinhState;
  readonly hinhAnhSanPham: HinhAnhSanPhamState;
  readonly sanPhamChiTiet: SanPhamChiTietState;
  readonly thongTinChiTietSanPham: ThongTinChiTietSanPhamState;
  readonly phieuMuaHang: PhieuMuaHangState;
  readonly chiTietPhieuMuaHang: ChiTietPhieuMuaHangState;
  readonly bangDinhMuc: BangDinhMucState;
  readonly phieuGiamDinh: PhieuGiamDinhState;
  readonly chiTietPhieuGiamDinh: ChiTietPhieuGiamDinhState;
  readonly chiTietGiamDinh: ChiTietGiamDinhState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  room,
  employee,
  funcitonSystem,
  nhaCungCap,
  nhaSanXuat,
  roomFuncitonSystem,
  accountFuncitonSystem,
  nhomSanPham,
  loaiSanPham,
  kho,
  viTriKho,
  nhomThuocTinh,
  loaiThuocTinh,
  donVi,
  nhomDonVi,
  sanPham,
  thuocTinh,
  loaiSanPhamLoaiThuocTinh,
  sanPhamThuocTinh,
  sanPhamLoaiThuocTinh,
  hinhAnhSanPham,
  sanPhamChiTiet,
  thongTinChiTietSanPham,
  phieuMuaHang,
  chiTietPhieuMuaHang,
  bangDinhMuc,
  phieuGiamDinh,
  chiTietPhieuGiamDinh,
  chiTietGiamDinh,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar
});

export default rootReducer;
