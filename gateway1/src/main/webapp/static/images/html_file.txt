Trước khi đăng nhập:

import './home.css';

import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { Row, Col, Alert } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';
import { getSession } from 'app/shared/reducers/authentication';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
export const Food = () => (
  <div>
    Favorite Food: <FontAwesomeIcon icon="stroopwafel" />
  </div>
)

export interface IHomeProp extends StateProps, DispatchProps { }

export class Home extends React.Component<IHomeProp> {
  componentDidMount() {
    this.props.getSession();
  }

 SỬA TỪ ĐOẠN NÀY
  
  render() {
    const { account } = this.props;
    return (
      <Row>
        <Col md="9">
          <h2>Xin chào!</h2>
          <p className="lead">Đây là trang chủ của bạn</p>
          {account && account.login ? (
            <div>
              <Alert color="success">Bạn đã đăng nhập bằng tài khoản {account.login}.</Alert>
            </div>
          ) : (
              <div>
                <Alert color="warning">
                Vui lòng
                <Link to="/login" className="alert-link">
                    {' '}
                    Đăng nhập
                </Link>
              </Alert>

	
	ĐÂY LÀ ĐOẠN ĐÃ ĐĂNG NHẬP THÌ CÓ LIST NÀY		  
			  
			  
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-md-9 col-sm-9">
              <div className="sidebar">
              
                <ul className="list">
                <li className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                <FontAwesomeIcon icon = "shopping-basket" /><span>Quản lý sản phẩm</span></a>
              
                  <ul className="dropdown-menu-store">
                    <li><a href="#">Nhà sản xuất</a></li>
                    <li><a href="#">Danh sách sản phẩm</a></li>
                    <li><a href="#">Nhà cung cấp</a></li>
                  </ul>
                </li>
                
                <li className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                <FontAwesomeIcon icon = "truck" /><span>Quản lý nhập kho</span></a>
              
                  <ul className="dropdown-menu-store">
                    <li><a href="#">Phiếu giám định</a></li>
                    <li><a href="#">Phiếu nhập hàng</a></li>
                    <li><a href="#">Lịch sử nhập kho</a></li>
                  </ul>
                </li>
                
                <li className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                <FontAwesomeIcon icon = "times-circle" /><span>Quản lý xuất kho</span></a>
              
                  <ul className="dropdown-menu-store">
                    <li><a href="#">Phiếu xuất kho</a></li>
                    <li><a href="#">Phiếu soạn hàng</a></li>
                    <li><a href="#">Đơn hàng</a></li>
                  </ul>
                </li>

                <li className="list-item"><a href="#" className="item"><FontAwesomeIcon icon = "file" /><span>Thống kê</span></a></li>
                <li className="list-item"><a href="#" className="item"><FontAwesomeIcon icon = "plus-square" /><span>Quản lý thuộc tính</span></a></li>

                  
                </ul>
              </div>
            </div>
          </div>
        </div>

              </div>
            )}
                
 
        </Col>
        <Col md="3" className="pad">
          <span className="hipster rounded" />
        </Col>

        
          
      </Row>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = { getSession };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
