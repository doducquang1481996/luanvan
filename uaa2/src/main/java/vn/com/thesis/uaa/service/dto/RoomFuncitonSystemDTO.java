package vn.com.thesis.uaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RoomFuncitonSystem entity.
 */
public class RoomFuncitonSystemDTO implements Serializable {

    private Long id;

    private Long roomId;

    private Long funcitionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getFuncitionId() {
        return funcitionId;
    }

    public void setFuncitionId(Long funcitionId) {
        this.funcitionId = funcitionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RoomFuncitonSystemDTO roomFuncitonSystemDTO = (RoomFuncitonSystemDTO) o;
        if (roomFuncitonSystemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roomFuncitonSystemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoomFuncitonSystemDTO{" +
            "id=" + getId() +
            ", roomId=" + getRoomId() +
            ", funcitionId=" + getFuncitionId() +
            "}";
    }
}
