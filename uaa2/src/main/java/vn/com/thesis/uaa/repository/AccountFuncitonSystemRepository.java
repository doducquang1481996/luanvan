package vn.com.thesis.uaa.repository;

import vn.com.thesis.uaa.domain.AccountFuncitonSystem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AccountFuncitonSystem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccountFuncitonSystemRepository extends JpaRepository<AccountFuncitonSystem, Long> {

}
