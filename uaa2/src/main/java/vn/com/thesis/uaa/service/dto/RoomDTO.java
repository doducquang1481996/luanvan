package vn.com.thesis.uaa.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

import vn.com.thesis.uaa.domain.Room;

/**
 * A DTO for the Room entity.
 */
public class RoomDTO implements Serializable {

    private Long id;

    private String roomName;

    private String roomCode;

    private Long parentId;

    private LocalDate createDate;

    private String createPerson;

    private LocalDate updateDate;

    private String updatePerson;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RoomDTO roomDTO = (RoomDTO) o;
        if (roomDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roomDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
            "id=" + getId() +
            ", roomName='" + getRoomName() + "'" +
            ", roomCode='" + getRoomCode() + "'" +
            ", parentId=" + getParentId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createPerson='" + getCreatePerson() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updatePerson='" + getUpdatePerson() + "'" +
            "}";
    }

	public RoomDTO(Room room) {
		super();
		this.id = room.getId();
		this.roomName = room.getRoomName();
		this.roomCode = room.getRoomCode();
		this.parentId = room.getParentId();
		this.createDate = room.getCreateDate();
		this.createPerson = room.getCreatePerson();
		this.updateDate = room.getUpdateDate();
		this.updatePerson = room.getUpdatePerson();
	}

	public RoomDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
}
