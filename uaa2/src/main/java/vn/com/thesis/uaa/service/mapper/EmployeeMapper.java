package vn.com.thesis.uaa.service.mapper;

import vn.com.thesis.uaa.domain.*;
import vn.com.thesis.uaa.service.dto.EmployeeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Employee and its DTO EmployeeDTO.
 */
@Mapper(componentModel = "spring", uses = {RoomMapper.class})
public interface EmployeeMapper extends EntityMapper<EmployeeDTO, Employee> {

    @Mapping(source = "room.id", target = "roomId")
    EmployeeDTO toDto(Employee employee);

    @Mapping(source = "roomId", target = "room")
    Employee toEntity(EmployeeDTO employeeDTO);

    default Employee fromId(Long id) {
        if (id == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(id);
        return employee;
    }
}
