package vn.com.thesis.uaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AccountFuncitonSystem entity.
 */
public class AccountFuncitonSystemDTO implements Serializable {

    private Long id;

    private Long accountId;

    private Long funcitionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getFuncitionId() {
        return funcitionId;
    }

    public void setFuncitionId(Long funcitionId) {
        this.funcitionId = funcitionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountFuncitonSystemDTO accountFuncitonSystemDTO = (AccountFuncitonSystemDTO) o;
        if (accountFuncitonSystemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accountFuncitonSystemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccountFuncitonSystemDTO{" +
            "id=" + getId() +
            ", accountId=" + getAccountId() +
            ", funcitionId=" + getFuncitionId() +
            "}";
    }
}
