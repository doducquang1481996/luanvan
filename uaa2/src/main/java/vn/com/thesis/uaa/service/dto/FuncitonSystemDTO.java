package vn.com.thesis.uaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the FuncitonSystem entity.
 */
public class FuncitonSystemDTO implements Serializable {

    private Long id;

    private String funcitionName;

    private String funcitionCode;

    private String funcitionLink;

    private String note;

    private Long parentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFuncitionName() {
        return funcitionName;
    }

    public void setFuncitionName(String funcitionName) {
        this.funcitionName = funcitionName;
    }

    public String getFuncitionCode() {
        return funcitionCode;
    }

    public void setFuncitionCode(String funcitionCode) {
        this.funcitionCode = funcitionCode;
    }

    public String getFuncitionLink() {
        return funcitionLink;
    }

    public void setFuncitionLink(String funcitionLink) {
        this.funcitionLink = funcitionLink;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FuncitonSystemDTO funcitonSystemDTO = (FuncitonSystemDTO) o;
        if (funcitonSystemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), funcitonSystemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FuncitonSystemDTO{" +
            "id=" + getId() +
            ", funcitionName='" + getFuncitionName() + "'" +
            ", funcitionCode='" + getFuncitionCode() + "'" +
            ", funcitionLink='" + getFuncitionLink() + "'" +
            ", note='" + getNote() + "'" +
            ", parentId=" + getParentId() +
            "}";
    }
}
