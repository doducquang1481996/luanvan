package vn.com.thesis.uaa.service;

import vn.com.thesis.uaa.service.dto.RoomFuncitonSystemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RoomFuncitonSystem.
 */
public interface RoomFuncitonSystemService {

    /**
     * Save a roomFuncitonSystem.
     *
     * @param roomFuncitonSystemDTO the entity to save
     * @return the persisted entity
     */
    RoomFuncitonSystemDTO save(RoomFuncitonSystemDTO roomFuncitonSystemDTO);

    /**
     * Get all the roomFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RoomFuncitonSystemDTO> findAll(Pageable pageable);


    /**
     * Get the "id" roomFuncitonSystem.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RoomFuncitonSystemDTO> findOne(Long id);

    /**
     * Delete the "id" roomFuncitonSystem.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
