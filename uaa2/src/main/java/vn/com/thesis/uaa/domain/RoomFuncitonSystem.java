package vn.com.thesis.uaa.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A RoomFuncitonSystem.
 */
@Entity
@Table(name = "room_funciton_system")
public class RoomFuncitonSystem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "room_id")
    private Long roomId;

    @Column(name = "funcition_id")
    private Long funcitionId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoomId() {
        return roomId;
    }

    public RoomFuncitonSystem roomId(Long roomId) {
        this.roomId = roomId;
        return this;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getFuncitionId() {
        return funcitionId;
    }

    public RoomFuncitonSystem funcitionId(Long funcitionId) {
        this.funcitionId = funcitionId;
        return this;
    }

    public void setFuncitionId(Long funcitionId) {
        this.funcitionId = funcitionId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RoomFuncitonSystem roomFuncitonSystem = (RoomFuncitonSystem) o;
        if (roomFuncitonSystem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roomFuncitonSystem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoomFuncitonSystem{" +
            "id=" + getId() +
            ", roomId=" + getRoomId() +
            ", funcitionId=" + getFuncitionId() +
            "}";
    }
}
