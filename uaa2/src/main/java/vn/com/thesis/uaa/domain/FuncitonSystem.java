package vn.com.thesis.uaa.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A FuncitonSystem.
 */
@Entity
@Table(name = "funciton_system")
public class FuncitonSystem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "funcition_name")
    private String funcitionName;

    @Column(name = "funcition_code")
    private String funcitionCode;

    @Column(name = "funcition_link")
    private String funcitionLink;

    @Column(name = "note")
    private String note;

    @Column(name = "parent_id")
    private Long parentId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFuncitionName() {
        return funcitionName;
    }

    public FuncitonSystem funcitionName(String funcitionName) {
        this.funcitionName = funcitionName;
        return this;
    }

    public void setFuncitionName(String funcitionName) {
        this.funcitionName = funcitionName;
    }

    public String getFuncitionCode() {
        return funcitionCode;
    }

    public FuncitonSystem funcitionCode(String funcitionCode) {
        this.funcitionCode = funcitionCode;
        return this;
    }

    public void setFuncitionCode(String funcitionCode) {
        this.funcitionCode = funcitionCode;
    }

    public String getFuncitionLink() {
        return funcitionLink;
    }

    public FuncitonSystem funcitionLink(String funcitionLink) {
        this.funcitionLink = funcitionLink;
        return this;
    }

    public void setFuncitionLink(String funcitionLink) {
        this.funcitionLink = funcitionLink;
    }

    public String getNote() {
        return note;
    }

    public FuncitonSystem note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getParentId() {
        return parentId;
    }

    public FuncitonSystem parentId(Long parentId) {
        this.parentId = parentId;
        return this;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FuncitonSystem funcitonSystem = (FuncitonSystem) o;
        if (funcitonSystem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), funcitonSystem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FuncitonSystem{" +
            "id=" + getId() +
            ", funcitionName='" + getFuncitionName() + "'" +
            ", funcitionCode='" + getFuncitionCode() + "'" +
            ", funcitionLink='" + getFuncitionLink() + "'" +
            ", note='" + getNote() + "'" +
            ", parentId=" + getParentId() +
            "}";
    }
}
