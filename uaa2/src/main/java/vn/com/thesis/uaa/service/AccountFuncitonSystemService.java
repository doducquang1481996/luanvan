package vn.com.thesis.uaa.service;

import vn.com.thesis.uaa.service.dto.AccountFuncitonSystemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing AccountFuncitonSystem.
 */
public interface AccountFuncitonSystemService {

    /**
     * Save a accountFuncitonSystem.
     *
     * @param accountFuncitonSystemDTO the entity to save
     * @return the persisted entity
     */
    AccountFuncitonSystemDTO save(AccountFuncitonSystemDTO accountFuncitonSystemDTO);

    /**
     * Get all the accountFuncitonSystems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AccountFuncitonSystemDTO> findAll(Pageable pageable);


    /**
     * Get the "id" accountFuncitonSystem.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AccountFuncitonSystemDTO> findOne(Long id);

    /**
     * Delete the "id" accountFuncitonSystem.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
