package vn.com.thesis.uaa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Room.
 */
@Entity
@Table(name = "room")
public class Room implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "room_name")
    private String roomName;

    @Column(name = "room_code")
    private String roomCode;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_person")
    private String createPerson;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "update_person")
    private String updatePerson;

    @OneToMany(mappedBy = "room")
    private Set<Employee> rooms = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public Room roomName(String roomName) {
        this.roomName = roomName;
        return this;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomCode() {
        return roomCode;
    }

    public Room roomCode(String roomCode) {
        this.roomCode = roomCode;
        return this;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public Long getParentId() {
        return parentId;
    }

    public Room parentId(Long parentId) {
        this.parentId = parentId;
        return this;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public Room createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public Room createPerson(String createPerson) {
        this.createPerson = createPerson;
        return this;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public Room updateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public Room updatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
        return this;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Set<Employee> getRooms() {
        return rooms;
    }

    public Room rooms(Set<Employee> employees) {
        this.rooms = employees;
        return this;
    }

    public Room addRoom(Employee employee) {
        this.rooms.add(employee);
        employee.setRoom(this);
        return this;
    }

    public Room removeRoom(Employee employee) {
        this.rooms.remove(employee);
        employee.setRoom(null);
        return this;
    }

    public void setRooms(Set<Employee> employees) {
        this.rooms = employees;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Room room = (Room) o;
        if (room.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), room.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Room{" +
            "id=" + getId() +
            ", roomName='" + getRoomName() + "'" +
            ", roomCode='" + getRoomCode() + "'" +
            ", parentId=" + getParentId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createPerson='" + getCreatePerson() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updatePerson='" + getUpdatePerson() + "'" +
            "}";
    }
}
