package vn.com.thesis.uaa.repository;

import vn.com.thesis.uaa.domain.Room;
import vn.com.thesis.uaa.service.dto.RoomDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Room entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
	
	Page<RoomDTO> findByParentId(Long parentId,Pageable pageable);
}
