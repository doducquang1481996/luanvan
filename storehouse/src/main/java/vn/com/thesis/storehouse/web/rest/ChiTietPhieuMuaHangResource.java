package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.ChiTietPhieuMuaHang;
import vn.com.thesis.storehouse.repository.ChiTietPhieuMuaHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChiTietPhieuMuaHang.
 */
@RestController
@RequestMapping("/api")
public class ChiTietPhieuMuaHangResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietPhieuMuaHangResource.class);

    private static final String ENTITY_NAME = "chiTietPhieuMuaHang";

    private final ChiTietPhieuMuaHangRepository chiTietPhieuMuaHangRepository;

    public ChiTietPhieuMuaHangResource(ChiTietPhieuMuaHangRepository chiTietPhieuMuaHangRepository) {
        this.chiTietPhieuMuaHangRepository = chiTietPhieuMuaHangRepository;
    }

    /**
     * POST  /chi-tiet-phieu-mua-hangs : Create a new chiTietPhieuMuaHang.
     *
     * @param chiTietPhieuMuaHang the chiTietPhieuMuaHang to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chiTietPhieuMuaHang, or with status 400 (Bad Request) if the chiTietPhieuMuaHang has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chi-tiet-phieu-mua-hangs")
    @Timed
    public ResponseEntity<ChiTietPhieuMuaHang> createChiTietPhieuMuaHang(@RequestBody ChiTietPhieuMuaHang chiTietPhieuMuaHang) throws URISyntaxException {
        log.debug("REST request to save ChiTietPhieuMuaHang : {}", chiTietPhieuMuaHang);
        if (chiTietPhieuMuaHang.getId() != null) {
            throw new BadRequestAlertException("A new chiTietPhieuMuaHang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietPhieuMuaHang result = chiTietPhieuMuaHangRepository.save(chiTietPhieuMuaHang);
        return ResponseEntity.created(new URI("/api/chi-tiet-phieu-mua-hangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chi-tiet-phieu-mua-hangs : Updates an existing chiTietPhieuMuaHang.
     *
     * @param chiTietPhieuMuaHang the chiTietPhieuMuaHang to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chiTietPhieuMuaHang,
     * or with status 400 (Bad Request) if the chiTietPhieuMuaHang is not valid,
     * or with status 500 (Internal Server Error) if the chiTietPhieuMuaHang couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chi-tiet-phieu-mua-hangs")
    @Timed
    public ResponseEntity<ChiTietPhieuMuaHang> updateChiTietPhieuMuaHang(@RequestBody ChiTietPhieuMuaHang chiTietPhieuMuaHang) throws URISyntaxException {
        log.debug("REST request to update ChiTietPhieuMuaHang : {}", chiTietPhieuMuaHang);
        if (chiTietPhieuMuaHang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiTietPhieuMuaHang result = chiTietPhieuMuaHangRepository.save(chiTietPhieuMuaHang);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chiTietPhieuMuaHang.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chi-tiet-phieu-mua-hangs : get all the chiTietPhieuMuaHangs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chiTietPhieuMuaHangs in body
     */
    @GetMapping("/chi-tiet-phieu-mua-hangs")
    @Timed
    public List<ChiTietPhieuMuaHang> getAllChiTietPhieuMuaHangs() {
        log.debug("REST request to get all ChiTietPhieuMuaHangs");
        return chiTietPhieuMuaHangRepository.findAll();
    }

    /**
     * GET  /chi-tiet-phieu-mua-hangs/:id : get the "id" chiTietPhieuMuaHang.
     *
     * @param id the id of the chiTietPhieuMuaHang to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chiTietPhieuMuaHang, or with status 404 (Not Found)
     */
    @GetMapping("/chi-tiet-phieu-mua-hangs/{id}")
    @Timed
    public ResponseEntity<ChiTietPhieuMuaHang> getChiTietPhieuMuaHang(@PathVariable Long id) {
        log.debug("REST request to get ChiTietPhieuMuaHang : {}", id);
        Optional<ChiTietPhieuMuaHang> chiTietPhieuMuaHang = chiTietPhieuMuaHangRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chiTietPhieuMuaHang);
    }

    /**
     * DELETE  /chi-tiet-phieu-mua-hangs/:id : delete the "id" chiTietPhieuMuaHang.
     *
     * @param id the id of the chiTietPhieuMuaHang to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chi-tiet-phieu-mua-hangs/{id}")
    @Timed
    public ResponseEntity<Void> deleteChiTietPhieuMuaHang(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietPhieuMuaHang : {}", id);

        chiTietPhieuMuaHangRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
