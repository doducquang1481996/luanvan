package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.ChiTietBangDinhMuc;
import vn.com.thesis.storehouse.repository.ChiTietBangDinhMucRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChiTietBangDinhMuc.
 */
@RestController
@RequestMapping("/api")
public class ChiTietBangDinhMucResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietBangDinhMucResource.class);

    private static final String ENTITY_NAME = "chiTietBangDinhMuc";

    private final ChiTietBangDinhMucRepository chiTietBangDinhMucRepository;

    public ChiTietBangDinhMucResource(ChiTietBangDinhMucRepository chiTietBangDinhMucRepository) {
        this.chiTietBangDinhMucRepository = chiTietBangDinhMucRepository;
    }

    /**
     * POST  /chi-tiet-bang-dinh-mucs : Create a new chiTietBangDinhMuc.
     *
     * @param chiTietBangDinhMuc the chiTietBangDinhMuc to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chiTietBangDinhMuc, or with status 400 (Bad Request) if the chiTietBangDinhMuc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chi-tiet-bang-dinh-mucs")
    @Timed
    public ResponseEntity<ChiTietBangDinhMuc> createChiTietBangDinhMuc(@RequestBody ChiTietBangDinhMuc chiTietBangDinhMuc) throws URISyntaxException {
        log.debug("REST request to save ChiTietBangDinhMuc : {}", chiTietBangDinhMuc);
        if (chiTietBangDinhMuc.getId() != null) {
            throw new BadRequestAlertException("A new chiTietBangDinhMuc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietBangDinhMuc result = chiTietBangDinhMucRepository.save(chiTietBangDinhMuc);
        return ResponseEntity.created(new URI("/api/chi-tiet-bang-dinh-mucs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chi-tiet-bang-dinh-mucs : Updates an existing chiTietBangDinhMuc.
     *
     * @param chiTietBangDinhMuc the chiTietBangDinhMuc to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chiTietBangDinhMuc,
     * or with status 400 (Bad Request) if the chiTietBangDinhMuc is not valid,
     * or with status 500 (Internal Server Error) if the chiTietBangDinhMuc couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chi-tiet-bang-dinh-mucs")
    @Timed
    public ResponseEntity<ChiTietBangDinhMuc> updateChiTietBangDinhMuc(@RequestBody ChiTietBangDinhMuc chiTietBangDinhMuc) throws URISyntaxException {
        log.debug("REST request to update ChiTietBangDinhMuc : {}", chiTietBangDinhMuc);
        if (chiTietBangDinhMuc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiTietBangDinhMuc result = chiTietBangDinhMucRepository.save(chiTietBangDinhMuc);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chiTietBangDinhMuc.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chi-tiet-bang-dinh-mucs : get all the chiTietBangDinhMucs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chiTietBangDinhMucs in body
     */
    @GetMapping("/chi-tiet-bang-dinh-mucs")
    @Timed
    public List<ChiTietBangDinhMuc> getAllChiTietBangDinhMucs() {
        log.debug("REST request to get all ChiTietBangDinhMucs");
        return chiTietBangDinhMucRepository.findAll();
    }

    /**
     * GET  /chi-tiet-bang-dinh-mucs/:id : get the "id" chiTietBangDinhMuc.
     *
     * @param id the id of the chiTietBangDinhMuc to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chiTietBangDinhMuc, or with status 404 (Not Found)
     */
    @GetMapping("/chi-tiet-bang-dinh-mucs/{id}")
    @Timed
    public ResponseEntity<ChiTietBangDinhMuc> getChiTietBangDinhMuc(@PathVariable Long id) {
        log.debug("REST request to get ChiTietBangDinhMuc : {}", id);
        Optional<ChiTietBangDinhMuc> chiTietBangDinhMuc = chiTietBangDinhMucRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chiTietBangDinhMuc);
    }

    /**
     * DELETE  /chi-tiet-bang-dinh-mucs/:id : delete the "id" chiTietBangDinhMuc.
     *
     * @param id the id of the chiTietBangDinhMuc to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chi-tiet-bang-dinh-mucs/{id}")
    @Timed
    public ResponseEntity<Void> deleteChiTietBangDinhMuc(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietBangDinhMuc : {}", id);

        chiTietBangDinhMucRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
