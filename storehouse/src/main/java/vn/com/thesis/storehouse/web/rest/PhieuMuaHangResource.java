package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.PhieuMuaHang;
import vn.com.thesis.storehouse.repository.PhieuMuaHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PhieuMuaHang.
 */
@RestController
@RequestMapping("/api")
public class PhieuMuaHangResource {

    private final Logger log = LoggerFactory.getLogger(PhieuMuaHangResource.class);

    private static final String ENTITY_NAME = "phieuMuaHang";

    private final PhieuMuaHangRepository phieuMuaHangRepository;

    public PhieuMuaHangResource(PhieuMuaHangRepository phieuMuaHangRepository) {
        this.phieuMuaHangRepository = phieuMuaHangRepository;
    }

    /**
     * POST  /phieu-mua-hangs : Create a new phieuMuaHang.
     *
     * @param phieuMuaHang the phieuMuaHang to create
     * @return the ResponseEntity with status 201 (Created) and with body the new phieuMuaHang, or with status 400 (Bad Request) if the phieuMuaHang has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/phieu-mua-hangs")
    @Timed
    public ResponseEntity<PhieuMuaHang> createPhieuMuaHang(@RequestBody PhieuMuaHang phieuMuaHang) throws URISyntaxException {
        log.debug("REST request to save PhieuMuaHang : {}", phieuMuaHang);
        if (phieuMuaHang.getId() != null) {
            throw new BadRequestAlertException("A new phieuMuaHang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhieuMuaHang result = phieuMuaHangRepository.save(phieuMuaHang);
        return ResponseEntity.created(new URI("/api/phieu-mua-hangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /phieu-mua-hangs : Updates an existing phieuMuaHang.
     *
     * @param phieuMuaHang the phieuMuaHang to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated phieuMuaHang,
     * or with status 400 (Bad Request) if the phieuMuaHang is not valid,
     * or with status 500 (Internal Server Error) if the phieuMuaHang couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/phieu-mua-hangs")
    @Timed
    public ResponseEntity<PhieuMuaHang> updatePhieuMuaHang(@RequestBody PhieuMuaHang phieuMuaHang) throws URISyntaxException {
        log.debug("REST request to update PhieuMuaHang : {}", phieuMuaHang);
        if (phieuMuaHang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhieuMuaHang result = phieuMuaHangRepository.save(phieuMuaHang);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, phieuMuaHang.getId().toString()))
            .body(result);
    }

    /**
     * GET  /phieu-mua-hangs : get all the phieuMuaHangs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of phieuMuaHangs in body
     */
    @GetMapping("/phieu-mua-hangs")
    @Timed
    public List<PhieuMuaHang> getAllPhieuMuaHangs() {
        log.debug("REST request to get all PhieuMuaHangs");
        return phieuMuaHangRepository.findAll();
    }

    /**
     * GET  /phieu-mua-hangs/:id : get the "id" phieuMuaHang.
     *
     * @param id the id of the phieuMuaHang to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the phieuMuaHang, or with status 404 (Not Found)
     */
    @GetMapping("/phieu-mua-hangs/{id}")
    @Timed
    public ResponseEntity<PhieuMuaHang> getPhieuMuaHang(@PathVariable Long id) {
        log.debug("REST request to get PhieuMuaHang : {}", id);
        Optional<PhieuMuaHang> phieuMuaHang = phieuMuaHangRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(phieuMuaHang);
    }

    /**
     * DELETE  /phieu-mua-hangs/:id : delete the "id" phieuMuaHang.
     *
     * @param id the id of the phieuMuaHang to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/phieu-mua-hangs/{id}")
    @Timed
    public ResponseEntity<Void> deletePhieuMuaHang(@PathVariable Long id) {
        log.debug("REST request to delete PhieuMuaHang : {}", id);

        phieuMuaHangRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
