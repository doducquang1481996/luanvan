package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.PhieuGiamDinh;
import vn.com.thesis.storehouse.repository.PhieuGiamDinhRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PhieuGiamDinh.
 */
@RestController
@RequestMapping("/api")
public class PhieuGiamDinhResource {

    private final Logger log = LoggerFactory.getLogger(PhieuGiamDinhResource.class);

    private static final String ENTITY_NAME = "phieuGiamDinh";

    private final PhieuGiamDinhRepository phieuGiamDinhRepository;

    public PhieuGiamDinhResource(PhieuGiamDinhRepository phieuGiamDinhRepository) {
        this.phieuGiamDinhRepository = phieuGiamDinhRepository;
    }

    /**
     * POST  /phieu-giam-dinhs : Create a new phieuGiamDinh.
     *
     * @param phieuGiamDinh the phieuGiamDinh to create
     * @return the ResponseEntity with status 201 (Created) and with body the new phieuGiamDinh, or with status 400 (Bad Request) if the phieuGiamDinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/phieu-giam-dinhs")
    @Timed
    public ResponseEntity<PhieuGiamDinh> createPhieuGiamDinh(@RequestBody PhieuGiamDinh phieuGiamDinh) throws URISyntaxException {
        log.debug("REST request to save PhieuGiamDinh : {}", phieuGiamDinh);
        if (phieuGiamDinh.getId() != null) {
            throw new BadRequestAlertException("A new phieuGiamDinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhieuGiamDinh result = phieuGiamDinhRepository.save(phieuGiamDinh);
        return ResponseEntity.created(new URI("/api/phieu-giam-dinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /phieu-giam-dinhs : Updates an existing phieuGiamDinh.
     *
     * @param phieuGiamDinh the phieuGiamDinh to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated phieuGiamDinh,
     * or with status 400 (Bad Request) if the phieuGiamDinh is not valid,
     * or with status 500 (Internal Server Error) if the phieuGiamDinh couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/phieu-giam-dinhs")
    @Timed
    public ResponseEntity<PhieuGiamDinh> updatePhieuGiamDinh(@RequestBody PhieuGiamDinh phieuGiamDinh) throws URISyntaxException {
        log.debug("REST request to update PhieuGiamDinh : {}", phieuGiamDinh);
        if (phieuGiamDinh.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhieuGiamDinh result = phieuGiamDinhRepository.save(phieuGiamDinh);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, phieuGiamDinh.getId().toString()))
            .body(result);
    }

    /**
     * GET  /phieu-giam-dinhs : get all the phieuGiamDinhs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of phieuGiamDinhs in body
     */
    @GetMapping("/phieu-giam-dinhs")
    @Timed
    public List<PhieuGiamDinh> getAllPhieuGiamDinhs() {
        log.debug("REST request to get all PhieuGiamDinhs");
        return phieuGiamDinhRepository.findAll();
    }

    /**
     * GET  /phieu-giam-dinhs/:id : get the "id" phieuGiamDinh.
     *
     * @param id the id of the phieuGiamDinh to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the phieuGiamDinh, or with status 404 (Not Found)
     */
    @GetMapping("/phieu-giam-dinhs/{id}")
    @Timed
    public ResponseEntity<PhieuGiamDinh> getPhieuGiamDinh(@PathVariable Long id) {
        log.debug("REST request to get PhieuGiamDinh : {}", id);
        Optional<PhieuGiamDinh> phieuGiamDinh = phieuGiamDinhRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(phieuGiamDinh);
    }

    /**
     * DELETE  /phieu-giam-dinhs/:id : delete the "id" phieuGiamDinh.
     *
     * @param id the id of the phieuGiamDinh to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/phieu-giam-dinhs/{id}")
    @Timed
    public ResponseEntity<Void> deletePhieuGiamDinh(@PathVariable Long id) {
        log.debug("REST request to delete PhieuGiamDinh : {}", id);

        phieuGiamDinhRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
