package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.ChiTietBangDinhMuc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChiTietBangDinhMuc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietBangDinhMucRepository extends JpaRepository<ChiTietBangDinhMuc, Long> {

}
