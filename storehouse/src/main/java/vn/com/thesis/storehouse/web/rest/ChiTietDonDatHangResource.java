package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.ChiTietDonDatHang;
import vn.com.thesis.storehouse.repository.ChiTietDonDatHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChiTietDonDatHang.
 */
@RestController
@RequestMapping("/api")
public class ChiTietDonDatHangResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietDonDatHangResource.class);

    private static final String ENTITY_NAME = "chiTietDonDatHang";

    private final ChiTietDonDatHangRepository chiTietDonDatHangRepository;

    public ChiTietDonDatHangResource(ChiTietDonDatHangRepository chiTietDonDatHangRepository) {
        this.chiTietDonDatHangRepository = chiTietDonDatHangRepository;
    }

    /**
     * POST  /chi-tiet-don-dat-hangs : Create a new chiTietDonDatHang.
     *
     * @param chiTietDonDatHang the chiTietDonDatHang to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chiTietDonDatHang, or with status 400 (Bad Request) if the chiTietDonDatHang has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chi-tiet-don-dat-hangs")
    @Timed
    public ResponseEntity<ChiTietDonDatHang> createChiTietDonDatHang(@RequestBody ChiTietDonDatHang chiTietDonDatHang) throws URISyntaxException {
        log.debug("REST request to save ChiTietDonDatHang : {}", chiTietDonDatHang);
        if (chiTietDonDatHang.getId() != null) {
            throw new BadRequestAlertException("A new chiTietDonDatHang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietDonDatHang result = chiTietDonDatHangRepository.save(chiTietDonDatHang);
        return ResponseEntity.created(new URI("/api/chi-tiet-don-dat-hangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chi-tiet-don-dat-hangs : Updates an existing chiTietDonDatHang.
     *
     * @param chiTietDonDatHang the chiTietDonDatHang to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chiTietDonDatHang,
     * or with status 400 (Bad Request) if the chiTietDonDatHang is not valid,
     * or with status 500 (Internal Server Error) if the chiTietDonDatHang couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chi-tiet-don-dat-hangs")
    @Timed
    public ResponseEntity<ChiTietDonDatHang> updateChiTietDonDatHang(@RequestBody ChiTietDonDatHang chiTietDonDatHang) throws URISyntaxException {
        log.debug("REST request to update ChiTietDonDatHang : {}", chiTietDonDatHang);
        if (chiTietDonDatHang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiTietDonDatHang result = chiTietDonDatHangRepository.save(chiTietDonDatHang);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chiTietDonDatHang.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chi-tiet-don-dat-hangs : get all the chiTietDonDatHangs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chiTietDonDatHangs in body
     */
    @GetMapping("/chi-tiet-don-dat-hangs")
    @Timed
    public List<ChiTietDonDatHang> getAllChiTietDonDatHangs() {
        log.debug("REST request to get all ChiTietDonDatHangs");
        return chiTietDonDatHangRepository.findAll();
    }

    /**
     * GET  /chi-tiet-don-dat-hangs/:id : get the "id" chiTietDonDatHang.
     *
     * @param id the id of the chiTietDonDatHang to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chiTietDonDatHang, or with status 404 (Not Found)
     */
    @GetMapping("/chi-tiet-don-dat-hangs/{id}")
    @Timed
    public ResponseEntity<ChiTietDonDatHang> getChiTietDonDatHang(@PathVariable Long id) {
        log.debug("REST request to get ChiTietDonDatHang : {}", id);
        Optional<ChiTietDonDatHang> chiTietDonDatHang = chiTietDonDatHangRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chiTietDonDatHang);
    }

    /**
     * DELETE  /chi-tiet-don-dat-hangs/:id : delete the "id" chiTietDonDatHang.
     *
     * @param id the id of the chiTietDonDatHang to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chi-tiet-don-dat-hangs/{id}")
    @Timed
    public ResponseEntity<Void> deleteChiTietDonDatHang(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietDonDatHang : {}", id);

        chiTietDonDatHangRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
