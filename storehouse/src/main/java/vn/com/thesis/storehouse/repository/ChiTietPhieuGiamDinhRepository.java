package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.ChiTietPhieuGiamDinh;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChiTietPhieuGiamDinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietPhieuGiamDinhRepository extends JpaRepository<ChiTietPhieuGiamDinh, Long> {

}
