package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.ChiTietGiamDinh;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChiTietGiamDinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietGiamDinhRepository extends JpaRepository<ChiTietGiamDinh, Long> {

}
