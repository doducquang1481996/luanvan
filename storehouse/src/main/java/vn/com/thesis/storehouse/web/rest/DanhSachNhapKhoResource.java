package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.DanhSachNhapKho;
import vn.com.thesis.storehouse.repository.DanhSachNhapKhoRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DanhSachNhapKho.
 */
@RestController
@RequestMapping("/api")
public class DanhSachNhapKhoResource {

    private final Logger log = LoggerFactory.getLogger(DanhSachNhapKhoResource.class);

    private static final String ENTITY_NAME = "danhSachNhapKho";

    private final DanhSachNhapKhoRepository danhSachNhapKhoRepository;

    public DanhSachNhapKhoResource(DanhSachNhapKhoRepository danhSachNhapKhoRepository) {
        this.danhSachNhapKhoRepository = danhSachNhapKhoRepository;
    }

    /**
     * POST  /danh-sach-nhap-khos : Create a new danhSachNhapKho.
     *
     * @param danhSachNhapKho the danhSachNhapKho to create
     * @return the ResponseEntity with status 201 (Created) and with body the new danhSachNhapKho, or with status 400 (Bad Request) if the danhSachNhapKho has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/danh-sach-nhap-khos")
    @Timed
    public ResponseEntity<DanhSachNhapKho> createDanhSachNhapKho(@RequestBody DanhSachNhapKho danhSachNhapKho) throws URISyntaxException {
        log.debug("REST request to save DanhSachNhapKho : {}", danhSachNhapKho);
        if (danhSachNhapKho.getId() != null) {
            throw new BadRequestAlertException("A new danhSachNhapKho cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DanhSachNhapKho result = danhSachNhapKhoRepository.save(danhSachNhapKho);
        return ResponseEntity.created(new URI("/api/danh-sach-nhap-khos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /danh-sach-nhap-khos : Updates an existing danhSachNhapKho.
     *
     * @param danhSachNhapKho the danhSachNhapKho to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated danhSachNhapKho,
     * or with status 400 (Bad Request) if the danhSachNhapKho is not valid,
     * or with status 500 (Internal Server Error) if the danhSachNhapKho couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/danh-sach-nhap-khos")
    @Timed
    public ResponseEntity<DanhSachNhapKho> updateDanhSachNhapKho(@RequestBody DanhSachNhapKho danhSachNhapKho) throws URISyntaxException {
        log.debug("REST request to update DanhSachNhapKho : {}", danhSachNhapKho);
        if (danhSachNhapKho.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DanhSachNhapKho result = danhSachNhapKhoRepository.save(danhSachNhapKho);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, danhSachNhapKho.getId().toString()))
            .body(result);
    }

    /**
     * GET  /danh-sach-nhap-khos : get all the danhSachNhapKhos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of danhSachNhapKhos in body
     */
    @GetMapping("/danh-sach-nhap-khos")
    @Timed
    public List<DanhSachNhapKho> getAllDanhSachNhapKhos() {
        log.debug("REST request to get all DanhSachNhapKhos");
        return danhSachNhapKhoRepository.findAll();
    }

    /**
     * GET  /danh-sach-nhap-khos/:id : get the "id" danhSachNhapKho.
     *
     * @param id the id of the danhSachNhapKho to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the danhSachNhapKho, or with status 404 (Not Found)
     */
    @GetMapping("/danh-sach-nhap-khos/{id}")
    @Timed
    public ResponseEntity<DanhSachNhapKho> getDanhSachNhapKho(@PathVariable Long id) {
        log.debug("REST request to get DanhSachNhapKho : {}", id);
        Optional<DanhSachNhapKho> danhSachNhapKho = danhSachNhapKhoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(danhSachNhapKho);
    }

    /**
     * DELETE  /danh-sach-nhap-khos/:id : delete the "id" danhSachNhapKho.
     *
     * @param id the id of the danhSachNhapKho to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/danh-sach-nhap-khos/{id}")
    @Timed
    public ResponseEntity<Void> deleteDanhSachNhapKho(@PathVariable Long id) {
        log.debug("REST request to delete DanhSachNhapKho : {}", id);

        danhSachNhapKhoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
