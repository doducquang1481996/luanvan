package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.ChiTietPhieuGiamDinh;
import vn.com.thesis.storehouse.repository.ChiTietPhieuGiamDinhRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChiTietPhieuGiamDinh.
 */
@RestController
@RequestMapping("/api")
public class ChiTietPhieuGiamDinhResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietPhieuGiamDinhResource.class);

    private static final String ENTITY_NAME = "chiTietPhieuGiamDinh";

    private final ChiTietPhieuGiamDinhRepository chiTietPhieuGiamDinhRepository;

    public ChiTietPhieuGiamDinhResource(ChiTietPhieuGiamDinhRepository chiTietPhieuGiamDinhRepository) {
        this.chiTietPhieuGiamDinhRepository = chiTietPhieuGiamDinhRepository;
    }

    /**
     * POST  /chi-tiet-phieu-giam-dinhs : Create a new chiTietPhieuGiamDinh.
     *
     * @param chiTietPhieuGiamDinh the chiTietPhieuGiamDinh to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chiTietPhieuGiamDinh, or with status 400 (Bad Request) if the chiTietPhieuGiamDinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chi-tiet-phieu-giam-dinhs")
    @Timed
    public ResponseEntity<ChiTietPhieuGiamDinh> createChiTietPhieuGiamDinh(@RequestBody ChiTietPhieuGiamDinh chiTietPhieuGiamDinh) throws URISyntaxException {
        log.debug("REST request to save ChiTietPhieuGiamDinh : {}", chiTietPhieuGiamDinh);
        if (chiTietPhieuGiamDinh.getId() != null) {
            throw new BadRequestAlertException("A new chiTietPhieuGiamDinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietPhieuGiamDinh result = chiTietPhieuGiamDinhRepository.save(chiTietPhieuGiamDinh);
        return ResponseEntity.created(new URI("/api/chi-tiet-phieu-giam-dinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chi-tiet-phieu-giam-dinhs : Updates an existing chiTietPhieuGiamDinh.
     *
     * @param chiTietPhieuGiamDinh the chiTietPhieuGiamDinh to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chiTietPhieuGiamDinh,
     * or with status 400 (Bad Request) if the chiTietPhieuGiamDinh is not valid,
     * or with status 500 (Internal Server Error) if the chiTietPhieuGiamDinh couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chi-tiet-phieu-giam-dinhs")
    @Timed
    public ResponseEntity<ChiTietPhieuGiamDinh> updateChiTietPhieuGiamDinh(@RequestBody ChiTietPhieuGiamDinh chiTietPhieuGiamDinh) throws URISyntaxException {
        log.debug("REST request to update ChiTietPhieuGiamDinh : {}", chiTietPhieuGiamDinh);
        if (chiTietPhieuGiamDinh.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiTietPhieuGiamDinh result = chiTietPhieuGiamDinhRepository.save(chiTietPhieuGiamDinh);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chiTietPhieuGiamDinh.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chi-tiet-phieu-giam-dinhs : get all the chiTietPhieuGiamDinhs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chiTietPhieuGiamDinhs in body
     */
    @GetMapping("/chi-tiet-phieu-giam-dinhs")
    @Timed
    public List<ChiTietPhieuGiamDinh> getAllChiTietPhieuGiamDinhs() {
        log.debug("REST request to get all ChiTietPhieuGiamDinhs");
        return chiTietPhieuGiamDinhRepository.findAll();
    }

    /**
     * GET  /chi-tiet-phieu-giam-dinhs/:id : get the "id" chiTietPhieuGiamDinh.
     *
     * @param id the id of the chiTietPhieuGiamDinh to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chiTietPhieuGiamDinh, or with status 404 (Not Found)
     */
    @GetMapping("/chi-tiet-phieu-giam-dinhs/{id}")
    @Timed
    public ResponseEntity<ChiTietPhieuGiamDinh> getChiTietPhieuGiamDinh(@PathVariable Long id) {
        log.debug("REST request to get ChiTietPhieuGiamDinh : {}", id);
        Optional<ChiTietPhieuGiamDinh> chiTietPhieuGiamDinh = chiTietPhieuGiamDinhRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chiTietPhieuGiamDinh);
    }

    /**
     * DELETE  /chi-tiet-phieu-giam-dinhs/:id : delete the "id" chiTietPhieuGiamDinh.
     *
     * @param id the id of the chiTietPhieuGiamDinh to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chi-tiet-phieu-giam-dinhs/{id}")
    @Timed
    public ResponseEntity<Void> deleteChiTietPhieuGiamDinh(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietPhieuGiamDinh : {}", id);

        chiTietPhieuGiamDinhRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
