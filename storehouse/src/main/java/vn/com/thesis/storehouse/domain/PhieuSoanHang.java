package vn.com.thesis.storehouse.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PhieuSoanHang.
 */
@Entity
@Table(name = "phieu_soan_hang")
public class PhieuSoanHang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ma")
    private String ma;

    @Column(name = "don_dat_hang_id")
    private Long donDatHangId;

    @Column(name = "ma_don_dat_hang")
    private String maDonDatHang;

    @Column(name = "ngay_soan_hang")
    private LocalDate ngaySoanHang;

    @Column(name = "ngay_giao_hang")
    private LocalDate ngayGiaoHang;

    @Column(name = "xuat_hang")
    private Boolean xuatHang;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public PhieuSoanHang ma(String ma) {
        this.ma = ma;
        return this;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public Long getDonDatHangId() {
        return donDatHangId;
    }

    public PhieuSoanHang donDatHangId(Long donDatHangId) {
        this.donDatHangId = donDatHangId;
        return this;
    }

    public void setDonDatHangId(Long donDatHangId) {
        this.donDatHangId = donDatHangId;
    }

    public String getMaDonDatHang() {
        return maDonDatHang;
    }

    public PhieuSoanHang maDonDatHang(String maDonDatHang) {
        this.maDonDatHang = maDonDatHang;
        return this;
    }

    public void setMaDonDatHang(String maDonDatHang) {
        this.maDonDatHang = maDonDatHang;
    }

    public LocalDate getNgaySoanHang() {
        return ngaySoanHang;
    }

    public PhieuSoanHang ngaySoanHang(LocalDate ngaySoanHang) {
        this.ngaySoanHang = ngaySoanHang;
        return this;
    }

    public void setNgaySoanHang(LocalDate ngaySoanHang) {
        this.ngaySoanHang = ngaySoanHang;
    }

    public LocalDate getNgayGiaoHang() {
        return ngayGiaoHang;
    }

    public PhieuSoanHang ngayGiaoHang(LocalDate ngayGiaoHang) {
        this.ngayGiaoHang = ngayGiaoHang;
        return this;
    }

    public void setNgayGiaoHang(LocalDate ngayGiaoHang) {
        this.ngayGiaoHang = ngayGiaoHang;
    }

    public Boolean isXuatHang() {
        return xuatHang;
    }

    public PhieuSoanHang xuatHang(Boolean xuatHang) {
        this.xuatHang = xuatHang;
        return this;
    }

    public void setXuatHang(Boolean xuatHang) {
        this.xuatHang = xuatHang;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public PhieuSoanHang ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public PhieuSoanHang ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public PhieuSoanHang nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public PhieuSoanHang ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public PhieuSoanHang nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PhieuSoanHang phieuSoanHang = (PhieuSoanHang) o;
        if (phieuSoanHang.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phieuSoanHang.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhieuSoanHang{" +
            "id=" + getId() +
            ", ma='" + getMa() + "'" +
            ", donDatHangId=" + getDonDatHangId() +
            ", maDonDatHang='" + getMaDonDatHang() + "'" +
            ", ngaySoanHang='" + getNgaySoanHang() + "'" +
            ", ngayGiaoHang='" + getNgayGiaoHang() + "'" +
            ", xuatHang='" + isXuatHang() + "'" +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
