package vn.com.thesis.storehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ChiTietPhieuSoanHang.
 */
@Entity
@Table(name = "chi_tiet_phieu_soan_hang")
public class ChiTietPhieuSoanHang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "chi_tiet_nhap_kho_id")
    private Long chiTietNhapKhoId;

    @Column(name = "so_luong")
    private Integer soLuong;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Column(name = "gia_ban")
    private String giaBan;

    @Column(name = "don_vi_ban")
    private String donViBan;

    @Column(name = "don_vi_goc")
    private String donViGoc;

    @Column(name = "quy_doi")
    private Double quyDoi;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    @ManyToOne
    @JsonIgnoreProperties("")
    private PhieuSoanHang phieuSoanHang;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChiTietNhapKhoId() {
        return chiTietNhapKhoId;
    }

    public ChiTietPhieuSoanHang chiTietNhapKhoId(Long chiTietNhapKhoId) {
        this.chiTietNhapKhoId = chiTietNhapKhoId;
        return this;
    }

    public void setChiTietNhapKhoId(Long chiTietNhapKhoId) {
        this.chiTietNhapKhoId = chiTietNhapKhoId;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public ChiTietPhieuSoanHang soLuong(Integer soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public ChiTietPhieuSoanHang ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getGiaBan() {
        return giaBan;
    }

    public ChiTietPhieuSoanHang giaBan(String giaBan) {
        this.giaBan = giaBan;
        return this;
    }

    public void setGiaBan(String giaBan) {
        this.giaBan = giaBan;
    }

    public String getDonViBan() {
        return donViBan;
    }

    public ChiTietPhieuSoanHang donViBan(String donViBan) {
        this.donViBan = donViBan;
        return this;
    }

    public void setDonViBan(String donViBan) {
        this.donViBan = donViBan;
    }

    public String getDonViGoc() {
        return donViGoc;
    }

    public ChiTietPhieuSoanHang donViGoc(String donViGoc) {
        this.donViGoc = donViGoc;
        return this;
    }

    public void setDonViGoc(String donViGoc) {
        this.donViGoc = donViGoc;
    }

    public Double getQuyDoi() {
        return quyDoi;
    }

    public ChiTietPhieuSoanHang quyDoi(Double quyDoi) {
        this.quyDoi = quyDoi;
        return this;
    }

    public void setQuyDoi(Double quyDoi) {
        this.quyDoi = quyDoi;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public ChiTietPhieuSoanHang ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public ChiTietPhieuSoanHang nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public ChiTietPhieuSoanHang ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public ChiTietPhieuSoanHang nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public PhieuSoanHang getPhieuSoanHang() {
        return phieuSoanHang;
    }

    public ChiTietPhieuSoanHang phieuSoanHang(PhieuSoanHang phieuSoanHang) {
        this.phieuSoanHang = phieuSoanHang;
        return this;
    }

    public void setPhieuSoanHang(PhieuSoanHang phieuSoanHang) {
        this.phieuSoanHang = phieuSoanHang;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChiTietPhieuSoanHang chiTietPhieuSoanHang = (ChiTietPhieuSoanHang) o;
        if (chiTietPhieuSoanHang.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chiTietPhieuSoanHang.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChiTietPhieuSoanHang{" +
            "id=" + getId() +
            ", chiTietNhapKhoId=" + getChiTietNhapKhoId() +
            ", soLuong=" + getSoLuong() +
            ", ghiChu='" + getGhiChu() + "'" +
            ", giaBan='" + getGiaBan() + "'" +
            ", donViBan='" + getDonViBan() + "'" +
            ", donViGoc='" + getDonViGoc() + "'" +
            ", quyDoi=" + getQuyDoi() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
