package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.PhieuGiamDinh;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PhieuGiamDinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhieuGiamDinhRepository extends JpaRepository<PhieuGiamDinh, Long> {

}
