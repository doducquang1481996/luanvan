package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.ChiTietNhapKho;
import vn.com.thesis.storehouse.repository.ChiTietNhapKhoRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChiTietNhapKho.
 */
@RestController
@RequestMapping("/api")
public class ChiTietNhapKhoResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietNhapKhoResource.class);

    private static final String ENTITY_NAME = "chiTietNhapKho";

    private final ChiTietNhapKhoRepository chiTietNhapKhoRepository;

    public ChiTietNhapKhoResource(ChiTietNhapKhoRepository chiTietNhapKhoRepository) {
        this.chiTietNhapKhoRepository = chiTietNhapKhoRepository;
    }

    /**
     * POST  /chi-tiet-nhap-khos : Create a new chiTietNhapKho.
     *
     * @param chiTietNhapKho the chiTietNhapKho to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chiTietNhapKho, or with status 400 (Bad Request) if the chiTietNhapKho has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chi-tiet-nhap-khos")
    @Timed
    public ResponseEntity<ChiTietNhapKho> createChiTietNhapKho(@RequestBody ChiTietNhapKho chiTietNhapKho) throws URISyntaxException {
        log.debug("REST request to save ChiTietNhapKho : {}", chiTietNhapKho);
        if (chiTietNhapKho.getId() != null) {
            throw new BadRequestAlertException("A new chiTietNhapKho cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietNhapKho result = chiTietNhapKhoRepository.save(chiTietNhapKho);
        return ResponseEntity.created(new URI("/api/chi-tiet-nhap-khos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chi-tiet-nhap-khos : Updates an existing chiTietNhapKho.
     *
     * @param chiTietNhapKho the chiTietNhapKho to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chiTietNhapKho,
     * or with status 400 (Bad Request) if the chiTietNhapKho is not valid,
     * or with status 500 (Internal Server Error) if the chiTietNhapKho couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chi-tiet-nhap-khos")
    @Timed
    public ResponseEntity<ChiTietNhapKho> updateChiTietNhapKho(@RequestBody ChiTietNhapKho chiTietNhapKho) throws URISyntaxException {
        log.debug("REST request to update ChiTietNhapKho : {}", chiTietNhapKho);
        if (chiTietNhapKho.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiTietNhapKho result = chiTietNhapKhoRepository.save(chiTietNhapKho);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chiTietNhapKho.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chi-tiet-nhap-khos : get all the chiTietNhapKhos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chiTietNhapKhos in body
     */
    @GetMapping("/chi-tiet-nhap-khos")
    @Timed
    public List<ChiTietNhapKho> getAllChiTietNhapKhos() {
        log.debug("REST request to get all ChiTietNhapKhos");
        return chiTietNhapKhoRepository.findAll();
    }

    /**
     * GET  /chi-tiet-nhap-khos/:id : get the "id" chiTietNhapKho.
     *
     * @param id the id of the chiTietNhapKho to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chiTietNhapKho, or with status 404 (Not Found)
     */
    @GetMapping("/chi-tiet-nhap-khos/{id}")
    @Timed
    public ResponseEntity<ChiTietNhapKho> getChiTietNhapKho(@PathVariable Long id) {
        log.debug("REST request to get ChiTietNhapKho : {}", id);
        Optional<ChiTietNhapKho> chiTietNhapKho = chiTietNhapKhoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chiTietNhapKho);
    }

    /**
     * DELETE  /chi-tiet-nhap-khos/:id : delete the "id" chiTietNhapKho.
     *
     * @param id the id of the chiTietNhapKho to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chi-tiet-nhap-khos/{id}")
    @Timed
    public ResponseEntity<Void> deleteChiTietNhapKho(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietNhapKho : {}", id);

        chiTietNhapKhoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
