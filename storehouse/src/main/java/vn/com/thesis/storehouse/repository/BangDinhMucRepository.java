package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.BangDinhMuc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BangDinhMuc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BangDinhMucRepository extends JpaRepository<BangDinhMuc, Long> {

}
