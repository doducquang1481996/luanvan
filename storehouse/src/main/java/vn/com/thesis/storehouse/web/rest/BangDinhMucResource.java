package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.BangDinhMuc;
import vn.com.thesis.storehouse.repository.BangDinhMucRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BangDinhMuc.
 */
@RestController
@RequestMapping("/api")
public class BangDinhMucResource {

    private final Logger log = LoggerFactory.getLogger(BangDinhMucResource.class);

    private static final String ENTITY_NAME = "bangDinhMuc";

    private final BangDinhMucRepository bangDinhMucRepository;

    public BangDinhMucResource(BangDinhMucRepository bangDinhMucRepository) {
        this.bangDinhMucRepository = bangDinhMucRepository;
    }

    /**
     * POST  /bang-dinh-mucs : Create a new bangDinhMuc.
     *
     * @param bangDinhMuc the bangDinhMuc to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bangDinhMuc, or with status 400 (Bad Request) if the bangDinhMuc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bang-dinh-mucs")
    @Timed
    public ResponseEntity<BangDinhMuc> createBangDinhMuc(@RequestBody BangDinhMuc bangDinhMuc) throws URISyntaxException {
        log.debug("REST request to save BangDinhMuc : {}", bangDinhMuc);
        if (bangDinhMuc.getId() != null) {
            throw new BadRequestAlertException("A new bangDinhMuc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BangDinhMuc result = bangDinhMucRepository.save(bangDinhMuc);
        return ResponseEntity.created(new URI("/api/bang-dinh-mucs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bang-dinh-mucs : Updates an existing bangDinhMuc.
     *
     * @param bangDinhMuc the bangDinhMuc to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bangDinhMuc,
     * or with status 400 (Bad Request) if the bangDinhMuc is not valid,
     * or with status 500 (Internal Server Error) if the bangDinhMuc couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bang-dinh-mucs")
    @Timed
    public ResponseEntity<BangDinhMuc> updateBangDinhMuc(@RequestBody BangDinhMuc bangDinhMuc) throws URISyntaxException {
        log.debug("REST request to update BangDinhMuc : {}", bangDinhMuc);
        if (bangDinhMuc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BangDinhMuc result = bangDinhMucRepository.save(bangDinhMuc);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bangDinhMuc.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bang-dinh-mucs : get all the bangDinhMucs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of bangDinhMucs in body
     */
    @GetMapping("/bang-dinh-mucs")
    @Timed
    public List<BangDinhMuc> getAllBangDinhMucs() {
        log.debug("REST request to get all BangDinhMucs");
        return bangDinhMucRepository.findAll();
    }

    /**
     * GET  /bang-dinh-mucs/:id : get the "id" bangDinhMuc.
     *
     * @param id the id of the bangDinhMuc to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bangDinhMuc, or with status 404 (Not Found)
     */
    @GetMapping("/bang-dinh-mucs/{id}")
    @Timed
    public ResponseEntity<BangDinhMuc> getBangDinhMuc(@PathVariable Long id) {
        log.debug("REST request to get BangDinhMuc : {}", id);
        Optional<BangDinhMuc> bangDinhMuc = bangDinhMucRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(bangDinhMuc);
    }

    /**
     * DELETE  /bang-dinh-mucs/:id : delete the "id" bangDinhMuc.
     *
     * @param id the id of the bangDinhMuc to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bang-dinh-mucs/{id}")
    @Timed
    public ResponseEntity<Void> deleteBangDinhMuc(@PathVariable Long id) {
        log.debug("REST request to delete BangDinhMuc : {}", id);

        bangDinhMucRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
