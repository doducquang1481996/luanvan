package vn.com.thesis.storehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ChiTietBangDinhMuc.
 */
@Entity
@Table(name = "chi_tiet_bang_dinh_muc")
public class ChiTietBangDinhMuc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "chi_tiet_phieu_mua_hang_id")
    private Long chiTietPhieuMuaHangId;

    @Column(name = "dinh_muc")
    private Integer dinhMuc;

    @Column(name = "vi_tri_kho_id")
    private Long viTriKhoId;

    @Column(name = "kho_id")
    private Long khoId;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    @ManyToOne
    @JsonIgnoreProperties("")
    private BangDinhMuc bangDinhMuc;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChiTietPhieuMuaHangId() {
        return chiTietPhieuMuaHangId;
    }

    public ChiTietBangDinhMuc chiTietPhieuMuaHangId(Long chiTietPhieuMuaHangId) {
        this.chiTietPhieuMuaHangId = chiTietPhieuMuaHangId;
        return this;
    }

    public void setChiTietPhieuMuaHangId(Long chiTietPhieuMuaHangId) {
        this.chiTietPhieuMuaHangId = chiTietPhieuMuaHangId;
    }

    public Integer getDinhMuc() {
        return dinhMuc;
    }

    public ChiTietBangDinhMuc dinhMuc(Integer dinhMuc) {
        this.dinhMuc = dinhMuc;
        return this;
    }

    public void setDinhMuc(Integer dinhMuc) {
        this.dinhMuc = dinhMuc;
    }

    public Long getViTriKhoId() {
        return viTriKhoId;
    }

    public ChiTietBangDinhMuc viTriKhoId(Long viTriKhoId) {
        this.viTriKhoId = viTriKhoId;
        return this;
    }

    public void setViTriKhoId(Long viTriKhoId) {
        this.viTriKhoId = viTriKhoId;
    }

    public Long getKhoId() {
        return khoId;
    }

    public ChiTietBangDinhMuc khoId(Long khoId) {
        this.khoId = khoId;
        return this;
    }

    public void setKhoId(Long khoId) {
        this.khoId = khoId;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public ChiTietBangDinhMuc ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public ChiTietBangDinhMuc nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public ChiTietBangDinhMuc ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public ChiTietBangDinhMuc nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public BangDinhMuc getBangDinhMuc() {
        return bangDinhMuc;
    }

    public ChiTietBangDinhMuc bangDinhMuc(BangDinhMuc bangDinhMuc) {
        this.bangDinhMuc = bangDinhMuc;
        return this;
    }

    public void setBangDinhMuc(BangDinhMuc bangDinhMuc) {
        this.bangDinhMuc = bangDinhMuc;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChiTietBangDinhMuc chiTietBangDinhMuc = (ChiTietBangDinhMuc) o;
        if (chiTietBangDinhMuc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chiTietBangDinhMuc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChiTietBangDinhMuc{" +
            "id=" + getId() +
            ", chiTietPhieuMuaHangId=" + getChiTietPhieuMuaHangId() +
            ", dinhMuc=" + getDinhMuc() +
            ", viTriKhoId=" + getViTriKhoId() +
            ", khoId=" + getKhoId() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
