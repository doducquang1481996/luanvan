package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.ChiTietPhieuMuaHang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChiTietPhieuMuaHang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietPhieuMuaHangRepository extends JpaRepository<ChiTietPhieuMuaHang, Long> {

}
