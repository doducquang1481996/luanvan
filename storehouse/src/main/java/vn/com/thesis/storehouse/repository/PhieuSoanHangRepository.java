package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.PhieuSoanHang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PhieuSoanHang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhieuSoanHangRepository extends JpaRepository<PhieuSoanHang, Long> {

}
