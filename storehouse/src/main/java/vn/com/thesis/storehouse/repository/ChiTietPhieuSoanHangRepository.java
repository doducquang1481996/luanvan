package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.ChiTietPhieuSoanHang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChiTietPhieuSoanHang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietPhieuSoanHangRepository extends JpaRepository<ChiTietPhieuSoanHang, Long> {

}
