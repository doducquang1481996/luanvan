package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.DonDatHang;
import vn.com.thesis.storehouse.repository.DonDatHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DonDatHang.
 */
@RestController
@RequestMapping("/api")
public class DonDatHangResource {

    private final Logger log = LoggerFactory.getLogger(DonDatHangResource.class);

    private static final String ENTITY_NAME = "donDatHang";

    private final DonDatHangRepository donDatHangRepository;

    public DonDatHangResource(DonDatHangRepository donDatHangRepository) {
        this.donDatHangRepository = donDatHangRepository;
    }

    /**
     * POST  /don-dat-hangs : Create a new donDatHang.
     *
     * @param donDatHang the donDatHang to create
     * @return the ResponseEntity with status 201 (Created) and with body the new donDatHang, or with status 400 (Bad Request) if the donDatHang has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/don-dat-hangs")
    @Timed
    public ResponseEntity<DonDatHang> createDonDatHang(@RequestBody DonDatHang donDatHang) throws URISyntaxException {
        log.debug("REST request to save DonDatHang : {}", donDatHang);
        if (donDatHang.getId() != null) {
            throw new BadRequestAlertException("A new donDatHang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DonDatHang result = donDatHangRepository.save(donDatHang);
        return ResponseEntity.created(new URI("/api/don-dat-hangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /don-dat-hangs : Updates an existing donDatHang.
     *
     * @param donDatHang the donDatHang to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated donDatHang,
     * or with status 400 (Bad Request) if the donDatHang is not valid,
     * or with status 500 (Internal Server Error) if the donDatHang couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/don-dat-hangs")
    @Timed
    public ResponseEntity<DonDatHang> updateDonDatHang(@RequestBody DonDatHang donDatHang) throws URISyntaxException {
        log.debug("REST request to update DonDatHang : {}", donDatHang);
        if (donDatHang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DonDatHang result = donDatHangRepository.save(donDatHang);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, donDatHang.getId().toString()))
            .body(result);
    }

    /**
     * GET  /don-dat-hangs : get all the donDatHangs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of donDatHangs in body
     */
    @GetMapping("/don-dat-hangs")
    @Timed
    public List<DonDatHang> getAllDonDatHangs() {
        log.debug("REST request to get all DonDatHangs");
        return donDatHangRepository.findAll();
    }

    /**
     * GET  /don-dat-hangs/:id : get the "id" donDatHang.
     *
     * @param id the id of the donDatHang to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the donDatHang, or with status 404 (Not Found)
     */
    @GetMapping("/don-dat-hangs/{id}")
    @Timed
    public ResponseEntity<DonDatHang> getDonDatHang(@PathVariable Long id) {
        log.debug("REST request to get DonDatHang : {}", id);
        Optional<DonDatHang> donDatHang = donDatHangRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(donDatHang);
    }

    /**
     * DELETE  /don-dat-hangs/:id : delete the "id" donDatHang.
     *
     * @param id the id of the donDatHang to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/don-dat-hangs/{id}")
    @Timed
    public ResponseEntity<Void> deleteDonDatHang(@PathVariable Long id) {
        log.debug("REST request to delete DonDatHang : {}", id);

        donDatHangRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
