package vn.com.thesis.storehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ChiTietGiamDinh.
 */
@Entity
@Table(name = "chi_tiet_giam_dinh")
public class ChiTietGiamDinh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "so_luong")
    private Integer soLuong;

    @Column(name = "dat_yeu_cau")
    private Integer datYeuCau;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    @ManyToOne
    @JsonIgnoreProperties("")
    private ChiTietPhieuGiamDinh chiTietPhieuGiamDinh;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public ChiTietGiamDinh soLuong(Integer soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Integer getDatYeuCau() {
        return datYeuCau;
    }

    public ChiTietGiamDinh datYeuCau(Integer datYeuCau) {
        this.datYeuCau = datYeuCau;
        return this;
    }

    public void setDatYeuCau(Integer datYeuCau) {
        this.datYeuCau = datYeuCau;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public ChiTietGiamDinh ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public ChiTietGiamDinh nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public ChiTietGiamDinh ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public ChiTietGiamDinh nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public ChiTietPhieuGiamDinh getChiTietPhieuGiamDinh() {
        return chiTietPhieuGiamDinh;
    }

    public ChiTietGiamDinh chiTietPhieuGiamDinh(ChiTietPhieuGiamDinh chiTietPhieuGiamDinh) {
        this.chiTietPhieuGiamDinh = chiTietPhieuGiamDinh;
        return this;
    }

    public void setChiTietPhieuGiamDinh(ChiTietPhieuGiamDinh chiTietPhieuGiamDinh) {
        this.chiTietPhieuGiamDinh = chiTietPhieuGiamDinh;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChiTietGiamDinh chiTietGiamDinh = (ChiTietGiamDinh) o;
        if (chiTietGiamDinh.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chiTietGiamDinh.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChiTietGiamDinh{" +
            "id=" + getId() +
            ", soLuong=" + getSoLuong() +
            ", datYeuCau=" + getDatYeuCau() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
