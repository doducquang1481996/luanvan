package vn.com.thesis.storehouse.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PhieuMuaHang.
 */
@Entity
@Table(name = "phieu_mua_hang")
public class PhieuMuaHang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ten")
    private String ten;

    @Column(name = "ma")
    private String ma;

    @Column(name = "nha_cung_cap")
    private Long nhaCungCap;

    @Column(name = "ten_nha_cung_cap")
    private String tenNhaCungCap;

    @Column(name = "trang_thai")
    private Boolean trangThai;

    @Column(name = "duyet")
    private Boolean duyet;

    @Column(name = "ngay_dat_mua")
    private LocalDate ngayDatMua;

    @Column(name = "ngay_nhan")
    private LocalDate ngayNhan;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public PhieuMuaHang ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMa() {
        return ma;
    }

    public PhieuMuaHang ma(String ma) {
        this.ma = ma;
        return this;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public Long getNhaCungCap() {
        return nhaCungCap;
    }

    public PhieuMuaHang nhaCungCap(Long nhaCungCap) {
        this.nhaCungCap = nhaCungCap;
        return this;
    }

    public void setNhaCungCap(Long nhaCungCap) {
        this.nhaCungCap = nhaCungCap;
    }

    public String getTenNhaCungCap() {
        return tenNhaCungCap;
    }

    public PhieuMuaHang tenNhaCungCap(String tenNhaCungCap) {
        this.tenNhaCungCap = tenNhaCungCap;
        return this;
    }

    public void setTenNhaCungCap(String tenNhaCungCap) {
        this.tenNhaCungCap = tenNhaCungCap;
    }

    public Boolean isTrangThai() {
        return trangThai;
    }

    public PhieuMuaHang trangThai(Boolean trangThai) {
        this.trangThai = trangThai;
        return this;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    public Boolean isDuyet() {
        return duyet;
    }

    public PhieuMuaHang duyet(Boolean duyet) {
        this.duyet = duyet;
        return this;
    }

    public void setDuyet(Boolean duyet) {
        this.duyet = duyet;
    }

    public LocalDate getNgayDatMua() {
        return ngayDatMua;
    }

    public PhieuMuaHang ngayDatMua(LocalDate ngayDatMua) {
        this.ngayDatMua = ngayDatMua;
        return this;
    }

    public void setNgayDatMua(LocalDate ngayDatMua) {
        this.ngayDatMua = ngayDatMua;
    }

    public LocalDate getNgayNhan() {
        return ngayNhan;
    }

    public PhieuMuaHang ngayNhan(LocalDate ngayNhan) {
        this.ngayNhan = ngayNhan;
        return this;
    }

    public void setNgayNhan(LocalDate ngayNhan) {
        this.ngayNhan = ngayNhan;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public PhieuMuaHang ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public PhieuMuaHang nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public PhieuMuaHang ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public PhieuMuaHang nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PhieuMuaHang phieuMuaHang = (PhieuMuaHang) o;
        if (phieuMuaHang.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phieuMuaHang.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhieuMuaHang{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", ma='" + getMa() + "'" +
            ", nhaCungCap=" + getNhaCungCap() +
            ", tenNhaCungCap='" + getTenNhaCungCap() + "'" +
            ", trangThai='" + isTrangThai() + "'" +
            ", duyet='" + isDuyet() + "'" +
            ", ngayDatMua='" + getNgayDatMua() + "'" +
            ", ngayNhan='" + getNgayNhan() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
