package vn.com.thesis.storehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ChiTietPhieuGiamDinh.
 */
@Entity
@Table(name = "chi_tiet_phieu_giam_dinh")
public class ChiTietPhieuGiamDinh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ma")
    private String ma;

    @Column(name = "chi_tiet_phieu_mua_hang_id")
    private Long chiTietPhieuMuaHangId;

    @Column(name = "tinh_trang")
    private Boolean tinhTrang;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    @ManyToOne
    @JsonIgnoreProperties("")
    private PhieuGiamDinh phieuGiamDinh;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public ChiTietPhieuGiamDinh ma(String ma) {
        this.ma = ma;
        return this;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public Long getChiTietPhieuMuaHangId() {
        return chiTietPhieuMuaHangId;
    }

    public ChiTietPhieuGiamDinh chiTietPhieuMuaHangId(Long chiTietPhieuMuaHangId) {
        this.chiTietPhieuMuaHangId = chiTietPhieuMuaHangId;
        return this;
    }

    public void setChiTietPhieuMuaHangId(Long chiTietPhieuMuaHangId) {
        this.chiTietPhieuMuaHangId = chiTietPhieuMuaHangId;
    }

    public Boolean isTinhTrang() {
        return tinhTrang;
    }

    public ChiTietPhieuGiamDinh tinhTrang(Boolean tinhTrang) {
        this.tinhTrang = tinhTrang;
        return this;
    }

    public void setTinhTrang(Boolean tinhTrang) {
        this.tinhTrang = tinhTrang;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public ChiTietPhieuGiamDinh ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public ChiTietPhieuGiamDinh nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public ChiTietPhieuGiamDinh ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public ChiTietPhieuGiamDinh nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public PhieuGiamDinh getPhieuGiamDinh() {
        return phieuGiamDinh;
    }

    public ChiTietPhieuGiamDinh phieuGiamDinh(PhieuGiamDinh phieuGiamDinh) {
        this.phieuGiamDinh = phieuGiamDinh;
        return this;
    }

    public void setPhieuGiamDinh(PhieuGiamDinh phieuGiamDinh) {
        this.phieuGiamDinh = phieuGiamDinh;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChiTietPhieuGiamDinh chiTietPhieuGiamDinh = (ChiTietPhieuGiamDinh) o;
        if (chiTietPhieuGiamDinh.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chiTietPhieuGiamDinh.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChiTietPhieuGiamDinh{" +
            "id=" + getId() +
            ", ma='" + getMa() + "'" +
            ", chiTietPhieuMuaHangId=" + getChiTietPhieuMuaHangId() +
            ", tinhTrang='" + isTinhTrang() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
