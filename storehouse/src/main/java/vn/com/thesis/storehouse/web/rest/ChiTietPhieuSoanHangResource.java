package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.ChiTietPhieuSoanHang;
import vn.com.thesis.storehouse.repository.ChiTietPhieuSoanHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChiTietPhieuSoanHang.
 */
@RestController
@RequestMapping("/api")
public class ChiTietPhieuSoanHangResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietPhieuSoanHangResource.class);

    private static final String ENTITY_NAME = "chiTietPhieuSoanHang";

    private final ChiTietPhieuSoanHangRepository chiTietPhieuSoanHangRepository;

    public ChiTietPhieuSoanHangResource(ChiTietPhieuSoanHangRepository chiTietPhieuSoanHangRepository) {
        this.chiTietPhieuSoanHangRepository = chiTietPhieuSoanHangRepository;
    }

    /**
     * POST  /chi-tiet-phieu-soan-hangs : Create a new chiTietPhieuSoanHang.
     *
     * @param chiTietPhieuSoanHang the chiTietPhieuSoanHang to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chiTietPhieuSoanHang, or with status 400 (Bad Request) if the chiTietPhieuSoanHang has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chi-tiet-phieu-soan-hangs")
    @Timed
    public ResponseEntity<ChiTietPhieuSoanHang> createChiTietPhieuSoanHang(@RequestBody ChiTietPhieuSoanHang chiTietPhieuSoanHang) throws URISyntaxException {
        log.debug("REST request to save ChiTietPhieuSoanHang : {}", chiTietPhieuSoanHang);
        if (chiTietPhieuSoanHang.getId() != null) {
            throw new BadRequestAlertException("A new chiTietPhieuSoanHang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietPhieuSoanHang result = chiTietPhieuSoanHangRepository.save(chiTietPhieuSoanHang);
        return ResponseEntity.created(new URI("/api/chi-tiet-phieu-soan-hangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chi-tiet-phieu-soan-hangs : Updates an existing chiTietPhieuSoanHang.
     *
     * @param chiTietPhieuSoanHang the chiTietPhieuSoanHang to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chiTietPhieuSoanHang,
     * or with status 400 (Bad Request) if the chiTietPhieuSoanHang is not valid,
     * or with status 500 (Internal Server Error) if the chiTietPhieuSoanHang couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chi-tiet-phieu-soan-hangs")
    @Timed
    public ResponseEntity<ChiTietPhieuSoanHang> updateChiTietPhieuSoanHang(@RequestBody ChiTietPhieuSoanHang chiTietPhieuSoanHang) throws URISyntaxException {
        log.debug("REST request to update ChiTietPhieuSoanHang : {}", chiTietPhieuSoanHang);
        if (chiTietPhieuSoanHang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiTietPhieuSoanHang result = chiTietPhieuSoanHangRepository.save(chiTietPhieuSoanHang);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chiTietPhieuSoanHang.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chi-tiet-phieu-soan-hangs : get all the chiTietPhieuSoanHangs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chiTietPhieuSoanHangs in body
     */
    @GetMapping("/chi-tiet-phieu-soan-hangs")
    @Timed
    public List<ChiTietPhieuSoanHang> getAllChiTietPhieuSoanHangs() {
        log.debug("REST request to get all ChiTietPhieuSoanHangs");
        return chiTietPhieuSoanHangRepository.findAll();
    }

    /**
     * GET  /chi-tiet-phieu-soan-hangs/:id : get the "id" chiTietPhieuSoanHang.
     *
     * @param id the id of the chiTietPhieuSoanHang to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chiTietPhieuSoanHang, or with status 404 (Not Found)
     */
    @GetMapping("/chi-tiet-phieu-soan-hangs/{id}")
    @Timed
    public ResponseEntity<ChiTietPhieuSoanHang> getChiTietPhieuSoanHang(@PathVariable Long id) {
        log.debug("REST request to get ChiTietPhieuSoanHang : {}", id);
        Optional<ChiTietPhieuSoanHang> chiTietPhieuSoanHang = chiTietPhieuSoanHangRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chiTietPhieuSoanHang);
    }

    /**
     * DELETE  /chi-tiet-phieu-soan-hangs/:id : delete the "id" chiTietPhieuSoanHang.
     *
     * @param id the id of the chiTietPhieuSoanHang to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chi-tiet-phieu-soan-hangs/{id}")
    @Timed
    public ResponseEntity<Void> deleteChiTietPhieuSoanHang(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietPhieuSoanHang : {}", id);

        chiTietPhieuSoanHangRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
