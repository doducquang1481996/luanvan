package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.PhieuSoanHang;
import vn.com.thesis.storehouse.repository.PhieuSoanHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PhieuSoanHang.
 */
@RestController
@RequestMapping("/api")
public class PhieuSoanHangResource {

    private final Logger log = LoggerFactory.getLogger(PhieuSoanHangResource.class);

    private static final String ENTITY_NAME = "phieuSoanHang";

    private final PhieuSoanHangRepository phieuSoanHangRepository;

    public PhieuSoanHangResource(PhieuSoanHangRepository phieuSoanHangRepository) {
        this.phieuSoanHangRepository = phieuSoanHangRepository;
    }

    /**
     * POST  /phieu-soan-hangs : Create a new phieuSoanHang.
     *
     * @param phieuSoanHang the phieuSoanHang to create
     * @return the ResponseEntity with status 201 (Created) and with body the new phieuSoanHang, or with status 400 (Bad Request) if the phieuSoanHang has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/phieu-soan-hangs")
    @Timed
    public ResponseEntity<PhieuSoanHang> createPhieuSoanHang(@RequestBody PhieuSoanHang phieuSoanHang) throws URISyntaxException {
        log.debug("REST request to save PhieuSoanHang : {}", phieuSoanHang);
        if (phieuSoanHang.getId() != null) {
            throw new BadRequestAlertException("A new phieuSoanHang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhieuSoanHang result = phieuSoanHangRepository.save(phieuSoanHang);
        return ResponseEntity.created(new URI("/api/phieu-soan-hangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /phieu-soan-hangs : Updates an existing phieuSoanHang.
     *
     * @param phieuSoanHang the phieuSoanHang to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated phieuSoanHang,
     * or with status 400 (Bad Request) if the phieuSoanHang is not valid,
     * or with status 500 (Internal Server Error) if the phieuSoanHang couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/phieu-soan-hangs")
    @Timed
    public ResponseEntity<PhieuSoanHang> updatePhieuSoanHang(@RequestBody PhieuSoanHang phieuSoanHang) throws URISyntaxException {
        log.debug("REST request to update PhieuSoanHang : {}", phieuSoanHang);
        if (phieuSoanHang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhieuSoanHang result = phieuSoanHangRepository.save(phieuSoanHang);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, phieuSoanHang.getId().toString()))
            .body(result);
    }

    /**
     * GET  /phieu-soan-hangs : get all the phieuSoanHangs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of phieuSoanHangs in body
     */
    @GetMapping("/phieu-soan-hangs")
    @Timed
    public List<PhieuSoanHang> getAllPhieuSoanHangs() {
        log.debug("REST request to get all PhieuSoanHangs");
        return phieuSoanHangRepository.findAll();
    }

    /**
     * GET  /phieu-soan-hangs/:id : get the "id" phieuSoanHang.
     *
     * @param id the id of the phieuSoanHang to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the phieuSoanHang, or with status 404 (Not Found)
     */
    @GetMapping("/phieu-soan-hangs/{id}")
    @Timed
    public ResponseEntity<PhieuSoanHang> getPhieuSoanHang(@PathVariable Long id) {
        log.debug("REST request to get PhieuSoanHang : {}", id);
        Optional<PhieuSoanHang> phieuSoanHang = phieuSoanHangRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(phieuSoanHang);
    }

    /**
     * DELETE  /phieu-soan-hangs/:id : delete the "id" phieuSoanHang.
     *
     * @param id the id of the phieuSoanHang to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/phieu-soan-hangs/{id}")
    @Timed
    public ResponseEntity<Void> deletePhieuSoanHang(@PathVariable Long id) {
        log.debug("REST request to delete PhieuSoanHang : {}", id);

        phieuSoanHangRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
