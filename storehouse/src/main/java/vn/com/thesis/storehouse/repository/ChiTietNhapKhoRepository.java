package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.ChiTietNhapKho;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChiTietNhapKho entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietNhapKhoRepository extends JpaRepository<ChiTietNhapKho, Long> {

}
