package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.DanhSachNhapKho;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DanhSachNhapKho entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DanhSachNhapKhoRepository extends JpaRepository<DanhSachNhapKho, Long> {

}
