package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.PhieuMuaHang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PhieuMuaHang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhieuMuaHangRepository extends JpaRepository<PhieuMuaHang, Long> {

}
