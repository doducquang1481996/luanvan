package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.ChiTietDonDatHang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChiTietDonDatHang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietDonDatHangRepository extends JpaRepository<ChiTietDonDatHang, Long> {

}
