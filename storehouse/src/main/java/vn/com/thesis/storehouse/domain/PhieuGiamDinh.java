package vn.com.thesis.storehouse.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PhieuGiamDinh.
 */
@Entity
@Table(name = "phieu_giam_dinh")
public class PhieuGiamDinh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ma")
    private String ma;

    @Column(name = "ten")
    private String ten;

    @Column(name = "ngay_giam_dinh")
    private LocalDate ngayGiamDinh;

    @Column(name = "phieu_mua_hang_id")
    private Long phieuMuaHangId;

    @Column(name = "ma_phieu_mua_hang")
    private String maPhieuMuaHang;

    @Column(name = "trang_thai")
    private Boolean trangThai;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public PhieuGiamDinh ma(String ma) {
        this.ma = ma;
        return this;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public PhieuGiamDinh ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public LocalDate getNgayGiamDinh() {
        return ngayGiamDinh;
    }

    public PhieuGiamDinh ngayGiamDinh(LocalDate ngayGiamDinh) {
        this.ngayGiamDinh = ngayGiamDinh;
        return this;
    }

    public void setNgayGiamDinh(LocalDate ngayGiamDinh) {
        this.ngayGiamDinh = ngayGiamDinh;
    }

    public Long getPhieuMuaHangId() {
        return phieuMuaHangId;
    }

    public PhieuGiamDinh phieuMuaHangId(Long phieuMuaHangId) {
        this.phieuMuaHangId = phieuMuaHangId;
        return this;
    }

    public void setPhieuMuaHangId(Long phieuMuaHangId) {
        this.phieuMuaHangId = phieuMuaHangId;
    }

    public String getMaPhieuMuaHang() {
        return maPhieuMuaHang;
    }

    public PhieuGiamDinh maPhieuMuaHang(String maPhieuMuaHang) {
        this.maPhieuMuaHang = maPhieuMuaHang;
        return this;
    }

    public void setMaPhieuMuaHang(String maPhieuMuaHang) {
        this.maPhieuMuaHang = maPhieuMuaHang;
    }

    public Boolean isTrangThai() {
        return trangThai;
    }

    public PhieuGiamDinh trangThai(Boolean trangThai) {
        this.trangThai = trangThai;
        return this;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public PhieuGiamDinh ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public PhieuGiamDinh nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public PhieuGiamDinh ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public PhieuGiamDinh nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PhieuGiamDinh phieuGiamDinh = (PhieuGiamDinh) o;
        if (phieuGiamDinh.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phieuGiamDinh.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhieuGiamDinh{" +
            "id=" + getId() +
            ", ma='" + getMa() + "'" +
            ", ten='" + getTen() + "'" +
            ", ngayGiamDinh='" + getNgayGiamDinh() + "'" +
            ", phieuMuaHangId=" + getPhieuMuaHangId() +
            ", maPhieuMuaHang='" + getMaPhieuMuaHang() + "'" +
            ", trangThai='" + isTrangThai() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
