package vn.com.thesis.storehouse.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.storehouse.domain.ChiTietGiamDinh;
import vn.com.thesis.storehouse.repository.ChiTietGiamDinhRepository;
import vn.com.thesis.storehouse.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.storehouse.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChiTietGiamDinh.
 */
@RestController
@RequestMapping("/api")
public class ChiTietGiamDinhResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietGiamDinhResource.class);

    private static final String ENTITY_NAME = "chiTietGiamDinh";

    private final ChiTietGiamDinhRepository chiTietGiamDinhRepository;

    public ChiTietGiamDinhResource(ChiTietGiamDinhRepository chiTietGiamDinhRepository) {
        this.chiTietGiamDinhRepository = chiTietGiamDinhRepository;
    }

    /**
     * POST  /chi-tiet-giam-dinhs : Create a new chiTietGiamDinh.
     *
     * @param chiTietGiamDinh the chiTietGiamDinh to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chiTietGiamDinh, or with status 400 (Bad Request) if the chiTietGiamDinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chi-tiet-giam-dinhs")
    @Timed
    public ResponseEntity<ChiTietGiamDinh> createChiTietGiamDinh(@RequestBody ChiTietGiamDinh chiTietGiamDinh) throws URISyntaxException {
        log.debug("REST request to save ChiTietGiamDinh : {}", chiTietGiamDinh);
        if (chiTietGiamDinh.getId() != null) {
            throw new BadRequestAlertException("A new chiTietGiamDinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietGiamDinh result = chiTietGiamDinhRepository.save(chiTietGiamDinh);
        return ResponseEntity.created(new URI("/api/chi-tiet-giam-dinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chi-tiet-giam-dinhs : Updates an existing chiTietGiamDinh.
     *
     * @param chiTietGiamDinh the chiTietGiamDinh to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chiTietGiamDinh,
     * or with status 400 (Bad Request) if the chiTietGiamDinh is not valid,
     * or with status 500 (Internal Server Error) if the chiTietGiamDinh couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chi-tiet-giam-dinhs")
    @Timed
    public ResponseEntity<ChiTietGiamDinh> updateChiTietGiamDinh(@RequestBody ChiTietGiamDinh chiTietGiamDinh) throws URISyntaxException {
        log.debug("REST request to update ChiTietGiamDinh : {}", chiTietGiamDinh);
        if (chiTietGiamDinh.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiTietGiamDinh result = chiTietGiamDinhRepository.save(chiTietGiamDinh);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chiTietGiamDinh.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chi-tiet-giam-dinhs : get all the chiTietGiamDinhs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chiTietGiamDinhs in body
     */
    @GetMapping("/chi-tiet-giam-dinhs")
    @Timed
    public List<ChiTietGiamDinh> getAllChiTietGiamDinhs() {
        log.debug("REST request to get all ChiTietGiamDinhs");
        return chiTietGiamDinhRepository.findAll();
    }

    /**
     * GET  /chi-tiet-giam-dinhs/:id : get the "id" chiTietGiamDinh.
     *
     * @param id the id of the chiTietGiamDinh to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chiTietGiamDinh, or with status 404 (Not Found)
     */
    @GetMapping("/chi-tiet-giam-dinhs/{id}")
    @Timed
    public ResponseEntity<ChiTietGiamDinh> getChiTietGiamDinh(@PathVariable Long id) {
        log.debug("REST request to get ChiTietGiamDinh : {}", id);
        Optional<ChiTietGiamDinh> chiTietGiamDinh = chiTietGiamDinhRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chiTietGiamDinh);
    }

    /**
     * DELETE  /chi-tiet-giam-dinhs/:id : delete the "id" chiTietGiamDinh.
     *
     * @param id the id of the chiTietGiamDinh to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chi-tiet-giam-dinhs/{id}")
    @Timed
    public ResponseEntity<Void> deleteChiTietGiamDinh(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietGiamDinh : {}", id);

        chiTietGiamDinhRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
