package vn.com.thesis.storehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ChiTietPhieuMuaHang.
 */
@Entity
@Table(name = "chi_tiet_phieu_mua_hang")
public class ChiTietPhieuMuaHang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "san_pham_id")
    private Long sanPhamId;

    @Column(name = "chi_tiet_san_pham_id")
    private Long chiTietSanPhamId;

    @Column(name = "gia_mua")
    private Double giaMua;

    @Column(name = "so_luong")
    private Integer soLuong;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    @ManyToOne
    @JsonIgnoreProperties("")
    private PhieuMuaHang phieuMuaHang;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSanPhamId() {
        return sanPhamId;
    }

    public ChiTietPhieuMuaHang sanPhamId(Long sanPhamId) {
        this.sanPhamId = sanPhamId;
        return this;
    }

    public void setSanPhamId(Long sanPhamId) {
        this.sanPhamId = sanPhamId;
    }

    public Long getChiTietSanPhamId() {
        return chiTietSanPhamId;
    }

    public ChiTietPhieuMuaHang chiTietSanPhamId(Long chiTietSanPhamId) {
        this.chiTietSanPhamId = chiTietSanPhamId;
        return this;
    }

    public void setChiTietSanPhamId(Long chiTietSanPhamId) {
        this.chiTietSanPhamId = chiTietSanPhamId;
    }

    public Double getGiaMua() {
        return giaMua;
    }

    public ChiTietPhieuMuaHang giaMua(Double giaMua) {
        this.giaMua = giaMua;
        return this;
    }

    public void setGiaMua(Double giaMua) {
        this.giaMua = giaMua;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public ChiTietPhieuMuaHang soLuong(Integer soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public ChiTietPhieuMuaHang ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public ChiTietPhieuMuaHang nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public ChiTietPhieuMuaHang ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public ChiTietPhieuMuaHang nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public PhieuMuaHang getPhieuMuaHang() {
        return phieuMuaHang;
    }

    public ChiTietPhieuMuaHang phieuMuaHang(PhieuMuaHang phieuMuaHang) {
        this.phieuMuaHang = phieuMuaHang;
        return this;
    }

    public void setPhieuMuaHang(PhieuMuaHang phieuMuaHang) {
        this.phieuMuaHang = phieuMuaHang;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChiTietPhieuMuaHang chiTietPhieuMuaHang = (ChiTietPhieuMuaHang) o;
        if (chiTietPhieuMuaHang.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chiTietPhieuMuaHang.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChiTietPhieuMuaHang{" +
            "id=" + getId() +
            ", sanPhamId=" + getSanPhamId() +
            ", chiTietSanPhamId=" + getChiTietSanPhamId() +
            ", giaMua=" + getGiaMua() +
            ", soLuong=" + getSoLuong() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
