package vn.com.thesis.storehouse.repository;

import vn.com.thesis.storehouse.domain.DonDatHang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DonDatHang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DonDatHangRepository extends JpaRepository<DonDatHang, Long> {

}
