package vn.com.thesis.storehouse.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DonDatHang.
 */
@Entity
@Table(name = "don_dat_hang")
public class DonDatHang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ma")
    private String ma;

    @Column(name = "tenkhach_hang")
    private String tenkhachHang;

    @Column(name = "khach_hang_id")
    private Long khachHangId;

    @Column(name = "ngay_dat")
    private String ngayDat;

    @Column(name = "ngay_gia_hang")
    private String ngayGiaHang;

    @Column(name = "duyet")
    private Boolean duyet;

    @Column(name = "trang_thai")
    private Boolean trangThai;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public DonDatHang ma(String ma) {
        this.ma = ma;
        return this;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTenkhachHang() {
        return tenkhachHang;
    }

    public DonDatHang tenkhachHang(String tenkhachHang) {
        this.tenkhachHang = tenkhachHang;
        return this;
    }

    public void setTenkhachHang(String tenkhachHang) {
        this.tenkhachHang = tenkhachHang;
    }

    public Long getKhachHangId() {
        return khachHangId;
    }

    public DonDatHang khachHangId(Long khachHangId) {
        this.khachHangId = khachHangId;
        return this;
    }

    public void setKhachHangId(Long khachHangId) {
        this.khachHangId = khachHangId;
    }

    public String getNgayDat() {
        return ngayDat;
    }

    public DonDatHang ngayDat(String ngayDat) {
        this.ngayDat = ngayDat;
        return this;
    }

    public void setNgayDat(String ngayDat) {
        this.ngayDat = ngayDat;
    }

    public String getNgayGiaHang() {
        return ngayGiaHang;
    }

    public DonDatHang ngayGiaHang(String ngayGiaHang) {
        this.ngayGiaHang = ngayGiaHang;
        return this;
    }

    public void setNgayGiaHang(String ngayGiaHang) {
        this.ngayGiaHang = ngayGiaHang;
    }

    public Boolean isDuyet() {
        return duyet;
    }

    public DonDatHang duyet(Boolean duyet) {
        this.duyet = duyet;
        return this;
    }

    public void setDuyet(Boolean duyet) {
        this.duyet = duyet;
    }

    public Boolean isTrangThai() {
        return trangThai;
    }

    public DonDatHang trangThai(Boolean trangThai) {
        this.trangThai = trangThai;
        return this;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public DonDatHang ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public DonDatHang ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public DonDatHang nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public DonDatHang ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public DonDatHang nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DonDatHang donDatHang = (DonDatHang) o;
        if (donDatHang.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), donDatHang.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DonDatHang{" +
            "id=" + getId() +
            ", ma='" + getMa() + "'" +
            ", tenkhachHang='" + getTenkhachHang() + "'" +
            ", khachHangId=" + getKhachHangId() +
            ", ngayDat='" + getNgayDat() + "'" +
            ", ngayGiaHang='" + getNgayGiaHang() + "'" +
            ", duyet='" + isDuyet() + "'" +
            ", trangThai='" + isTrangThai() + "'" +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
