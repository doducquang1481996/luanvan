package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.ChiTietPhieuSoanHang;
import vn.com.thesis.storehouse.repository.ChiTietPhieuSoanHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChiTietPhieuSoanHangResource REST controller.
 *
 * @see ChiTietPhieuSoanHangResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class ChiTietPhieuSoanHangResourceIntTest {

    private static final Long DEFAULT_CHI_TIET_NHAP_KHO_ID = 1L;
    private static final Long UPDATED_CHI_TIET_NHAP_KHO_ID = 2L;

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final String DEFAULT_GIA_BAN = "AAAAAAAAAA";
    private static final String UPDATED_GIA_BAN = "BBBBBBBBBB";

    private static final String DEFAULT_DON_VI_BAN = "AAAAAAAAAA";
    private static final String UPDATED_DON_VI_BAN = "BBBBBBBBBB";

    private static final String DEFAULT_DON_VI_GOC = "AAAAAAAAAA";
    private static final String UPDATED_DON_VI_GOC = "BBBBBBBBBB";

    private static final Double DEFAULT_QUY_DOI = 1D;
    private static final Double UPDATED_QUY_DOI = 2D;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private ChiTietPhieuSoanHangRepository chiTietPhieuSoanHangRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChiTietPhieuSoanHangMockMvc;

    private ChiTietPhieuSoanHang chiTietPhieuSoanHang;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChiTietPhieuSoanHangResource chiTietPhieuSoanHangResource = new ChiTietPhieuSoanHangResource(chiTietPhieuSoanHangRepository);
        this.restChiTietPhieuSoanHangMockMvc = MockMvcBuilders.standaloneSetup(chiTietPhieuSoanHangResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietPhieuSoanHang createEntity(EntityManager em) {
        ChiTietPhieuSoanHang chiTietPhieuSoanHang = new ChiTietPhieuSoanHang()
            .chiTietNhapKhoId(DEFAULT_CHI_TIET_NHAP_KHO_ID)
            .soLuong(DEFAULT_SO_LUONG)
            .ghiChu(DEFAULT_GHI_CHU)
            .giaBan(DEFAULT_GIA_BAN)
            .donViBan(DEFAULT_DON_VI_BAN)
            .donViGoc(DEFAULT_DON_VI_GOC)
            .quyDoi(DEFAULT_QUY_DOI)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return chiTietPhieuSoanHang;
    }

    @Before
    public void initTest() {
        chiTietPhieuSoanHang = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiTietPhieuSoanHang() throws Exception {
        int databaseSizeBeforeCreate = chiTietPhieuSoanHangRepository.findAll().size();

        // Create the ChiTietPhieuSoanHang
        restChiTietPhieuSoanHangMockMvc.perform(post("/api/chi-tiet-phieu-soan-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuSoanHang)))
            .andExpect(status().isCreated());

        // Validate the ChiTietPhieuSoanHang in the database
        List<ChiTietPhieuSoanHang> chiTietPhieuSoanHangList = chiTietPhieuSoanHangRepository.findAll();
        assertThat(chiTietPhieuSoanHangList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietPhieuSoanHang testChiTietPhieuSoanHang = chiTietPhieuSoanHangList.get(chiTietPhieuSoanHangList.size() - 1);
        assertThat(testChiTietPhieuSoanHang.getChiTietNhapKhoId()).isEqualTo(DEFAULT_CHI_TIET_NHAP_KHO_ID);
        assertThat(testChiTietPhieuSoanHang.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiTietPhieuSoanHang.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testChiTietPhieuSoanHang.getGiaBan()).isEqualTo(DEFAULT_GIA_BAN);
        assertThat(testChiTietPhieuSoanHang.getDonViBan()).isEqualTo(DEFAULT_DON_VI_BAN);
        assertThat(testChiTietPhieuSoanHang.getDonViGoc()).isEqualTo(DEFAULT_DON_VI_GOC);
        assertThat(testChiTietPhieuSoanHang.getQuyDoi()).isEqualTo(DEFAULT_QUY_DOI);
        assertThat(testChiTietPhieuSoanHang.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testChiTietPhieuSoanHang.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testChiTietPhieuSoanHang.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testChiTietPhieuSoanHang.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createChiTietPhieuSoanHangWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiTietPhieuSoanHangRepository.findAll().size();

        // Create the ChiTietPhieuSoanHang with an existing ID
        chiTietPhieuSoanHang.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietPhieuSoanHangMockMvc.perform(post("/api/chi-tiet-phieu-soan-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuSoanHang)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietPhieuSoanHang in the database
        List<ChiTietPhieuSoanHang> chiTietPhieuSoanHangList = chiTietPhieuSoanHangRepository.findAll();
        assertThat(chiTietPhieuSoanHangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChiTietPhieuSoanHangs() throws Exception {
        // Initialize the database
        chiTietPhieuSoanHangRepository.saveAndFlush(chiTietPhieuSoanHang);

        // Get all the chiTietPhieuSoanHangList
        restChiTietPhieuSoanHangMockMvc.perform(get("/api/chi-tiet-phieu-soan-hangs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietPhieuSoanHang.getId().intValue())))
            .andExpect(jsonPath("$.[*].chiTietNhapKhoId").value(hasItem(DEFAULT_CHI_TIET_NHAP_KHO_ID.intValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU.toString())))
            .andExpect(jsonPath("$.[*].giaBan").value(hasItem(DEFAULT_GIA_BAN.toString())))
            .andExpect(jsonPath("$.[*].donViBan").value(hasItem(DEFAULT_DON_VI_BAN.toString())))
            .andExpect(jsonPath("$.[*].donViGoc").value(hasItem(DEFAULT_DON_VI_GOC.toString())))
            .andExpect(jsonPath("$.[*].quyDoi").value(hasItem(DEFAULT_QUY_DOI.doubleValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getChiTietPhieuSoanHang() throws Exception {
        // Initialize the database
        chiTietPhieuSoanHangRepository.saveAndFlush(chiTietPhieuSoanHang);

        // Get the chiTietPhieuSoanHang
        restChiTietPhieuSoanHangMockMvc.perform(get("/api/chi-tiet-phieu-soan-hangs/{id}", chiTietPhieuSoanHang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietPhieuSoanHang.getId().intValue()))
            .andExpect(jsonPath("$.chiTietNhapKhoId").value(DEFAULT_CHI_TIET_NHAP_KHO_ID.intValue()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU.toString()))
            .andExpect(jsonPath("$.giaBan").value(DEFAULT_GIA_BAN.toString()))
            .andExpect(jsonPath("$.donViBan").value(DEFAULT_DON_VI_BAN.toString()))
            .andExpect(jsonPath("$.donViGoc").value(DEFAULT_DON_VI_GOC.toString()))
            .andExpect(jsonPath("$.quyDoi").value(DEFAULT_QUY_DOI.doubleValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingChiTietPhieuSoanHang() throws Exception {
        // Get the chiTietPhieuSoanHang
        restChiTietPhieuSoanHangMockMvc.perform(get("/api/chi-tiet-phieu-soan-hangs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiTietPhieuSoanHang() throws Exception {
        // Initialize the database
        chiTietPhieuSoanHangRepository.saveAndFlush(chiTietPhieuSoanHang);

        int databaseSizeBeforeUpdate = chiTietPhieuSoanHangRepository.findAll().size();

        // Update the chiTietPhieuSoanHang
        ChiTietPhieuSoanHang updatedChiTietPhieuSoanHang = chiTietPhieuSoanHangRepository.findById(chiTietPhieuSoanHang.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietPhieuSoanHang are not directly saved in db
        em.detach(updatedChiTietPhieuSoanHang);
        updatedChiTietPhieuSoanHang
            .chiTietNhapKhoId(UPDATED_CHI_TIET_NHAP_KHO_ID)
            .soLuong(UPDATED_SO_LUONG)
            .ghiChu(UPDATED_GHI_CHU)
            .giaBan(UPDATED_GIA_BAN)
            .donViBan(UPDATED_DON_VI_BAN)
            .donViGoc(UPDATED_DON_VI_GOC)
            .quyDoi(UPDATED_QUY_DOI)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restChiTietPhieuSoanHangMockMvc.perform(put("/api/chi-tiet-phieu-soan-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChiTietPhieuSoanHang)))
            .andExpect(status().isOk());

        // Validate the ChiTietPhieuSoanHang in the database
        List<ChiTietPhieuSoanHang> chiTietPhieuSoanHangList = chiTietPhieuSoanHangRepository.findAll();
        assertThat(chiTietPhieuSoanHangList).hasSize(databaseSizeBeforeUpdate);
        ChiTietPhieuSoanHang testChiTietPhieuSoanHang = chiTietPhieuSoanHangList.get(chiTietPhieuSoanHangList.size() - 1);
        assertThat(testChiTietPhieuSoanHang.getChiTietNhapKhoId()).isEqualTo(UPDATED_CHI_TIET_NHAP_KHO_ID);
        assertThat(testChiTietPhieuSoanHang.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietPhieuSoanHang.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testChiTietPhieuSoanHang.getGiaBan()).isEqualTo(UPDATED_GIA_BAN);
        assertThat(testChiTietPhieuSoanHang.getDonViBan()).isEqualTo(UPDATED_DON_VI_BAN);
        assertThat(testChiTietPhieuSoanHang.getDonViGoc()).isEqualTo(UPDATED_DON_VI_GOC);
        assertThat(testChiTietPhieuSoanHang.getQuyDoi()).isEqualTo(UPDATED_QUY_DOI);
        assertThat(testChiTietPhieuSoanHang.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testChiTietPhieuSoanHang.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testChiTietPhieuSoanHang.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testChiTietPhieuSoanHang.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingChiTietPhieuSoanHang() throws Exception {
        int databaseSizeBeforeUpdate = chiTietPhieuSoanHangRepository.findAll().size();

        // Create the ChiTietPhieuSoanHang

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restChiTietPhieuSoanHangMockMvc.perform(put("/api/chi-tiet-phieu-soan-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuSoanHang)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietPhieuSoanHang in the database
        List<ChiTietPhieuSoanHang> chiTietPhieuSoanHangList = chiTietPhieuSoanHangRepository.findAll();
        assertThat(chiTietPhieuSoanHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiTietPhieuSoanHang() throws Exception {
        // Initialize the database
        chiTietPhieuSoanHangRepository.saveAndFlush(chiTietPhieuSoanHang);

        int databaseSizeBeforeDelete = chiTietPhieuSoanHangRepository.findAll().size();

        // Get the chiTietPhieuSoanHang
        restChiTietPhieuSoanHangMockMvc.perform(delete("/api/chi-tiet-phieu-soan-hangs/{id}", chiTietPhieuSoanHang.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChiTietPhieuSoanHang> chiTietPhieuSoanHangList = chiTietPhieuSoanHangRepository.findAll();
        assertThat(chiTietPhieuSoanHangList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietPhieuSoanHang.class);
        ChiTietPhieuSoanHang chiTietPhieuSoanHang1 = new ChiTietPhieuSoanHang();
        chiTietPhieuSoanHang1.setId(1L);
        ChiTietPhieuSoanHang chiTietPhieuSoanHang2 = new ChiTietPhieuSoanHang();
        chiTietPhieuSoanHang2.setId(chiTietPhieuSoanHang1.getId());
        assertThat(chiTietPhieuSoanHang1).isEqualTo(chiTietPhieuSoanHang2);
        chiTietPhieuSoanHang2.setId(2L);
        assertThat(chiTietPhieuSoanHang1).isNotEqualTo(chiTietPhieuSoanHang2);
        chiTietPhieuSoanHang1.setId(null);
        assertThat(chiTietPhieuSoanHang1).isNotEqualTo(chiTietPhieuSoanHang2);
    }
}
