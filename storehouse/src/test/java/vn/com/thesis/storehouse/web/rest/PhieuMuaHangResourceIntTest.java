package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.PhieuMuaHang;
import vn.com.thesis.storehouse.repository.PhieuMuaHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PhieuMuaHangResource REST controller.
 *
 * @see PhieuMuaHangResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class PhieuMuaHangResourceIntTest {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final Long DEFAULT_NHA_CUNG_CAP = 1L;
    private static final Long UPDATED_NHA_CUNG_CAP = 2L;

    private static final String DEFAULT_TEN_NHA_CUNG_CAP = "AAAAAAAAAA";
    private static final String UPDATED_TEN_NHA_CUNG_CAP = "BBBBBBBBBB";

    private static final Boolean DEFAULT_TRANG_THAI = false;
    private static final Boolean UPDATED_TRANG_THAI = true;

    private static final Boolean DEFAULT_DUYET = false;
    private static final Boolean UPDATED_DUYET = true;

    private static final LocalDate DEFAULT_NGAY_DAT_MUA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_DAT_MUA = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_NGAY_NHAN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_NHAN = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private PhieuMuaHangRepository phieuMuaHangRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPhieuMuaHangMockMvc;

    private PhieuMuaHang phieuMuaHang;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PhieuMuaHangResource phieuMuaHangResource = new PhieuMuaHangResource(phieuMuaHangRepository);
        this.restPhieuMuaHangMockMvc = MockMvcBuilders.standaloneSetup(phieuMuaHangResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuMuaHang createEntity(EntityManager em) {
        PhieuMuaHang phieuMuaHang = new PhieuMuaHang()
            .ten(DEFAULT_TEN)
            .ma(DEFAULT_MA)
            .nhaCungCap(DEFAULT_NHA_CUNG_CAP)
            .tenNhaCungCap(DEFAULT_TEN_NHA_CUNG_CAP)
            .trangThai(DEFAULT_TRANG_THAI)
            .duyet(DEFAULT_DUYET)
            .ngayDatMua(DEFAULT_NGAY_DAT_MUA)
            .ngayNhan(DEFAULT_NGAY_NHAN)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return phieuMuaHang;
    }

    @Before
    public void initTest() {
        phieuMuaHang = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhieuMuaHang() throws Exception {
        int databaseSizeBeforeCreate = phieuMuaHangRepository.findAll().size();

        // Create the PhieuMuaHang
        restPhieuMuaHangMockMvc.perform(post("/api/phieu-mua-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuMuaHang)))
            .andExpect(status().isCreated());

        // Validate the PhieuMuaHang in the database
        List<PhieuMuaHang> phieuMuaHangList = phieuMuaHangRepository.findAll();
        assertThat(phieuMuaHangList).hasSize(databaseSizeBeforeCreate + 1);
        PhieuMuaHang testPhieuMuaHang = phieuMuaHangList.get(phieuMuaHangList.size() - 1);
        assertThat(testPhieuMuaHang.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testPhieuMuaHang.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testPhieuMuaHang.getNhaCungCap()).isEqualTo(DEFAULT_NHA_CUNG_CAP);
        assertThat(testPhieuMuaHang.getTenNhaCungCap()).isEqualTo(DEFAULT_TEN_NHA_CUNG_CAP);
        assertThat(testPhieuMuaHang.isTrangThai()).isEqualTo(DEFAULT_TRANG_THAI);
        assertThat(testPhieuMuaHang.isDuyet()).isEqualTo(DEFAULT_DUYET);
        assertThat(testPhieuMuaHang.getNgayDatMua()).isEqualTo(DEFAULT_NGAY_DAT_MUA);
        assertThat(testPhieuMuaHang.getNgayNhan()).isEqualTo(DEFAULT_NGAY_NHAN);
        assertThat(testPhieuMuaHang.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testPhieuMuaHang.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testPhieuMuaHang.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testPhieuMuaHang.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createPhieuMuaHangWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phieuMuaHangRepository.findAll().size();

        // Create the PhieuMuaHang with an existing ID
        phieuMuaHang.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhieuMuaHangMockMvc.perform(post("/api/phieu-mua-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuMuaHang)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuMuaHang in the database
        List<PhieuMuaHang> phieuMuaHangList = phieuMuaHangRepository.findAll();
        assertThat(phieuMuaHangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPhieuMuaHangs() throws Exception {
        // Initialize the database
        phieuMuaHangRepository.saveAndFlush(phieuMuaHang);

        // Get all the phieuMuaHangList
        restPhieuMuaHangMockMvc.perform(get("/api/phieu-mua-hangs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuMuaHang.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN.toString())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA.toString())))
            .andExpect(jsonPath("$.[*].nhaCungCap").value(hasItem(DEFAULT_NHA_CUNG_CAP.intValue())))
            .andExpect(jsonPath("$.[*].tenNhaCungCap").value(hasItem(DEFAULT_TEN_NHA_CUNG_CAP.toString())))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI.booleanValue())))
            .andExpect(jsonPath("$.[*].duyet").value(hasItem(DEFAULT_DUYET.booleanValue())))
            .andExpect(jsonPath("$.[*].ngayDatMua").value(hasItem(DEFAULT_NGAY_DAT_MUA.toString())))
            .andExpect(jsonPath("$.[*].ngayNhan").value(hasItem(DEFAULT_NGAY_NHAN.toString())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getPhieuMuaHang() throws Exception {
        // Initialize the database
        phieuMuaHangRepository.saveAndFlush(phieuMuaHang);

        // Get the phieuMuaHang
        restPhieuMuaHangMockMvc.perform(get("/api/phieu-mua-hangs/{id}", phieuMuaHang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(phieuMuaHang.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN.toString()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA.toString()))
            .andExpect(jsonPath("$.nhaCungCap").value(DEFAULT_NHA_CUNG_CAP.intValue()))
            .andExpect(jsonPath("$.tenNhaCungCap").value(DEFAULT_TEN_NHA_CUNG_CAP.toString()))
            .andExpect(jsonPath("$.trangThai").value(DEFAULT_TRANG_THAI.booleanValue()))
            .andExpect(jsonPath("$.duyet").value(DEFAULT_DUYET.booleanValue()))
            .andExpect(jsonPath("$.ngayDatMua").value(DEFAULT_NGAY_DAT_MUA.toString()))
            .andExpect(jsonPath("$.ngayNhan").value(DEFAULT_NGAY_NHAN.toString()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingPhieuMuaHang() throws Exception {
        // Get the phieuMuaHang
        restPhieuMuaHangMockMvc.perform(get("/api/phieu-mua-hangs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhieuMuaHang() throws Exception {
        // Initialize the database
        phieuMuaHangRepository.saveAndFlush(phieuMuaHang);

        int databaseSizeBeforeUpdate = phieuMuaHangRepository.findAll().size();

        // Update the phieuMuaHang
        PhieuMuaHang updatedPhieuMuaHang = phieuMuaHangRepository.findById(phieuMuaHang.getId()).get();
        // Disconnect from session so that the updates on updatedPhieuMuaHang are not directly saved in db
        em.detach(updatedPhieuMuaHang);
        updatedPhieuMuaHang
            .ten(UPDATED_TEN)
            .ma(UPDATED_MA)
            .nhaCungCap(UPDATED_NHA_CUNG_CAP)
            .tenNhaCungCap(UPDATED_TEN_NHA_CUNG_CAP)
            .trangThai(UPDATED_TRANG_THAI)
            .duyet(UPDATED_DUYET)
            .ngayDatMua(UPDATED_NGAY_DAT_MUA)
            .ngayNhan(UPDATED_NGAY_NHAN)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restPhieuMuaHangMockMvc.perform(put("/api/phieu-mua-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPhieuMuaHang)))
            .andExpect(status().isOk());

        // Validate the PhieuMuaHang in the database
        List<PhieuMuaHang> phieuMuaHangList = phieuMuaHangRepository.findAll();
        assertThat(phieuMuaHangList).hasSize(databaseSizeBeforeUpdate);
        PhieuMuaHang testPhieuMuaHang = phieuMuaHangList.get(phieuMuaHangList.size() - 1);
        assertThat(testPhieuMuaHang.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testPhieuMuaHang.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testPhieuMuaHang.getNhaCungCap()).isEqualTo(UPDATED_NHA_CUNG_CAP);
        assertThat(testPhieuMuaHang.getTenNhaCungCap()).isEqualTo(UPDATED_TEN_NHA_CUNG_CAP);
        assertThat(testPhieuMuaHang.isTrangThai()).isEqualTo(UPDATED_TRANG_THAI);
        assertThat(testPhieuMuaHang.isDuyet()).isEqualTo(UPDATED_DUYET);
        assertThat(testPhieuMuaHang.getNgayDatMua()).isEqualTo(UPDATED_NGAY_DAT_MUA);
        assertThat(testPhieuMuaHang.getNgayNhan()).isEqualTo(UPDATED_NGAY_NHAN);
        assertThat(testPhieuMuaHang.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testPhieuMuaHang.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testPhieuMuaHang.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testPhieuMuaHang.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingPhieuMuaHang() throws Exception {
        int databaseSizeBeforeUpdate = phieuMuaHangRepository.findAll().size();

        // Create the PhieuMuaHang

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restPhieuMuaHangMockMvc.perform(put("/api/phieu-mua-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuMuaHang)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuMuaHang in the database
        List<PhieuMuaHang> phieuMuaHangList = phieuMuaHangRepository.findAll();
        assertThat(phieuMuaHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhieuMuaHang() throws Exception {
        // Initialize the database
        phieuMuaHangRepository.saveAndFlush(phieuMuaHang);

        int databaseSizeBeforeDelete = phieuMuaHangRepository.findAll().size();

        // Get the phieuMuaHang
        restPhieuMuaHangMockMvc.perform(delete("/api/phieu-mua-hangs/{id}", phieuMuaHang.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PhieuMuaHang> phieuMuaHangList = phieuMuaHangRepository.findAll();
        assertThat(phieuMuaHangList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuMuaHang.class);
        PhieuMuaHang phieuMuaHang1 = new PhieuMuaHang();
        phieuMuaHang1.setId(1L);
        PhieuMuaHang phieuMuaHang2 = new PhieuMuaHang();
        phieuMuaHang2.setId(phieuMuaHang1.getId());
        assertThat(phieuMuaHang1).isEqualTo(phieuMuaHang2);
        phieuMuaHang2.setId(2L);
        assertThat(phieuMuaHang1).isNotEqualTo(phieuMuaHang2);
        phieuMuaHang1.setId(null);
        assertThat(phieuMuaHang1).isNotEqualTo(phieuMuaHang2);
    }
}
