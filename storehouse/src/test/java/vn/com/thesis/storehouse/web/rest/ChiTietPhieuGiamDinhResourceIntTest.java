package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.ChiTietPhieuGiamDinh;
import vn.com.thesis.storehouse.repository.ChiTietPhieuGiamDinhRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChiTietPhieuGiamDinhResource REST controller.
 *
 * @see ChiTietPhieuGiamDinhResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class ChiTietPhieuGiamDinhResourceIntTest {

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final Long DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID = 1L;
    private static final Long UPDATED_CHI_TIET_PHIEU_MUA_HANG_ID = 2L;

    private static final Boolean DEFAULT_TINH_TRANG = false;
    private static final Boolean UPDATED_TINH_TRANG = true;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private ChiTietPhieuGiamDinhRepository chiTietPhieuGiamDinhRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChiTietPhieuGiamDinhMockMvc;

    private ChiTietPhieuGiamDinh chiTietPhieuGiamDinh;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChiTietPhieuGiamDinhResource chiTietPhieuGiamDinhResource = new ChiTietPhieuGiamDinhResource(chiTietPhieuGiamDinhRepository);
        this.restChiTietPhieuGiamDinhMockMvc = MockMvcBuilders.standaloneSetup(chiTietPhieuGiamDinhResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietPhieuGiamDinh createEntity(EntityManager em) {
        ChiTietPhieuGiamDinh chiTietPhieuGiamDinh = new ChiTietPhieuGiamDinh()
            .ma(DEFAULT_MA)
            .chiTietPhieuMuaHangId(DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID)
            .tinhTrang(DEFAULT_TINH_TRANG)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return chiTietPhieuGiamDinh;
    }

    @Before
    public void initTest() {
        chiTietPhieuGiamDinh = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiTietPhieuGiamDinh() throws Exception {
        int databaseSizeBeforeCreate = chiTietPhieuGiamDinhRepository.findAll().size();

        // Create the ChiTietPhieuGiamDinh
        restChiTietPhieuGiamDinhMockMvc.perform(post("/api/chi-tiet-phieu-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuGiamDinh)))
            .andExpect(status().isCreated());

        // Validate the ChiTietPhieuGiamDinh in the database
        List<ChiTietPhieuGiamDinh> chiTietPhieuGiamDinhList = chiTietPhieuGiamDinhRepository.findAll();
        assertThat(chiTietPhieuGiamDinhList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietPhieuGiamDinh testChiTietPhieuGiamDinh = chiTietPhieuGiamDinhList.get(chiTietPhieuGiamDinhList.size() - 1);
        assertThat(testChiTietPhieuGiamDinh.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testChiTietPhieuGiamDinh.getChiTietPhieuMuaHangId()).isEqualTo(DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID);
        assertThat(testChiTietPhieuGiamDinh.isTinhTrang()).isEqualTo(DEFAULT_TINH_TRANG);
        assertThat(testChiTietPhieuGiamDinh.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testChiTietPhieuGiamDinh.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testChiTietPhieuGiamDinh.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testChiTietPhieuGiamDinh.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createChiTietPhieuGiamDinhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiTietPhieuGiamDinhRepository.findAll().size();

        // Create the ChiTietPhieuGiamDinh with an existing ID
        chiTietPhieuGiamDinh.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietPhieuGiamDinhMockMvc.perform(post("/api/chi-tiet-phieu-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuGiamDinh)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietPhieuGiamDinh in the database
        List<ChiTietPhieuGiamDinh> chiTietPhieuGiamDinhList = chiTietPhieuGiamDinhRepository.findAll();
        assertThat(chiTietPhieuGiamDinhList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChiTietPhieuGiamDinhs() throws Exception {
        // Initialize the database
        chiTietPhieuGiamDinhRepository.saveAndFlush(chiTietPhieuGiamDinh);

        // Get all the chiTietPhieuGiamDinhList
        restChiTietPhieuGiamDinhMockMvc.perform(get("/api/chi-tiet-phieu-giam-dinhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietPhieuGiamDinh.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA.toString())))
            .andExpect(jsonPath("$.[*].chiTietPhieuMuaHangId").value(hasItem(DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID.intValue())))
            .andExpect(jsonPath("$.[*].tinhTrang").value(hasItem(DEFAULT_TINH_TRANG.booleanValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getChiTietPhieuGiamDinh() throws Exception {
        // Initialize the database
        chiTietPhieuGiamDinhRepository.saveAndFlush(chiTietPhieuGiamDinh);

        // Get the chiTietPhieuGiamDinh
        restChiTietPhieuGiamDinhMockMvc.perform(get("/api/chi-tiet-phieu-giam-dinhs/{id}", chiTietPhieuGiamDinh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietPhieuGiamDinh.getId().intValue()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA.toString()))
            .andExpect(jsonPath("$.chiTietPhieuMuaHangId").value(DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID.intValue()))
            .andExpect(jsonPath("$.tinhTrang").value(DEFAULT_TINH_TRANG.booleanValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingChiTietPhieuGiamDinh() throws Exception {
        // Get the chiTietPhieuGiamDinh
        restChiTietPhieuGiamDinhMockMvc.perform(get("/api/chi-tiet-phieu-giam-dinhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiTietPhieuGiamDinh() throws Exception {
        // Initialize the database
        chiTietPhieuGiamDinhRepository.saveAndFlush(chiTietPhieuGiamDinh);

        int databaseSizeBeforeUpdate = chiTietPhieuGiamDinhRepository.findAll().size();

        // Update the chiTietPhieuGiamDinh
        ChiTietPhieuGiamDinh updatedChiTietPhieuGiamDinh = chiTietPhieuGiamDinhRepository.findById(chiTietPhieuGiamDinh.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietPhieuGiamDinh are not directly saved in db
        em.detach(updatedChiTietPhieuGiamDinh);
        updatedChiTietPhieuGiamDinh
            .ma(UPDATED_MA)
            .chiTietPhieuMuaHangId(UPDATED_CHI_TIET_PHIEU_MUA_HANG_ID)
            .tinhTrang(UPDATED_TINH_TRANG)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restChiTietPhieuGiamDinhMockMvc.perform(put("/api/chi-tiet-phieu-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChiTietPhieuGiamDinh)))
            .andExpect(status().isOk());

        // Validate the ChiTietPhieuGiamDinh in the database
        List<ChiTietPhieuGiamDinh> chiTietPhieuGiamDinhList = chiTietPhieuGiamDinhRepository.findAll();
        assertThat(chiTietPhieuGiamDinhList).hasSize(databaseSizeBeforeUpdate);
        ChiTietPhieuGiamDinh testChiTietPhieuGiamDinh = chiTietPhieuGiamDinhList.get(chiTietPhieuGiamDinhList.size() - 1);
        assertThat(testChiTietPhieuGiamDinh.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testChiTietPhieuGiamDinh.getChiTietPhieuMuaHangId()).isEqualTo(UPDATED_CHI_TIET_PHIEU_MUA_HANG_ID);
        assertThat(testChiTietPhieuGiamDinh.isTinhTrang()).isEqualTo(UPDATED_TINH_TRANG);
        assertThat(testChiTietPhieuGiamDinh.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testChiTietPhieuGiamDinh.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testChiTietPhieuGiamDinh.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testChiTietPhieuGiamDinh.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingChiTietPhieuGiamDinh() throws Exception {
        int databaseSizeBeforeUpdate = chiTietPhieuGiamDinhRepository.findAll().size();

        // Create the ChiTietPhieuGiamDinh

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restChiTietPhieuGiamDinhMockMvc.perform(put("/api/chi-tiet-phieu-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuGiamDinh)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietPhieuGiamDinh in the database
        List<ChiTietPhieuGiamDinh> chiTietPhieuGiamDinhList = chiTietPhieuGiamDinhRepository.findAll();
        assertThat(chiTietPhieuGiamDinhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiTietPhieuGiamDinh() throws Exception {
        // Initialize the database
        chiTietPhieuGiamDinhRepository.saveAndFlush(chiTietPhieuGiamDinh);

        int databaseSizeBeforeDelete = chiTietPhieuGiamDinhRepository.findAll().size();

        // Get the chiTietPhieuGiamDinh
        restChiTietPhieuGiamDinhMockMvc.perform(delete("/api/chi-tiet-phieu-giam-dinhs/{id}", chiTietPhieuGiamDinh.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChiTietPhieuGiamDinh> chiTietPhieuGiamDinhList = chiTietPhieuGiamDinhRepository.findAll();
        assertThat(chiTietPhieuGiamDinhList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietPhieuGiamDinh.class);
        ChiTietPhieuGiamDinh chiTietPhieuGiamDinh1 = new ChiTietPhieuGiamDinh();
        chiTietPhieuGiamDinh1.setId(1L);
        ChiTietPhieuGiamDinh chiTietPhieuGiamDinh2 = new ChiTietPhieuGiamDinh();
        chiTietPhieuGiamDinh2.setId(chiTietPhieuGiamDinh1.getId());
        assertThat(chiTietPhieuGiamDinh1).isEqualTo(chiTietPhieuGiamDinh2);
        chiTietPhieuGiamDinh2.setId(2L);
        assertThat(chiTietPhieuGiamDinh1).isNotEqualTo(chiTietPhieuGiamDinh2);
        chiTietPhieuGiamDinh1.setId(null);
        assertThat(chiTietPhieuGiamDinh1).isNotEqualTo(chiTietPhieuGiamDinh2);
    }
}
