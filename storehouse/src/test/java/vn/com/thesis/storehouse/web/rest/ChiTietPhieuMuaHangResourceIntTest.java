package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.ChiTietPhieuMuaHang;
import vn.com.thesis.storehouse.repository.ChiTietPhieuMuaHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChiTietPhieuMuaHangResource REST controller.
 *
 * @see ChiTietPhieuMuaHangResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class ChiTietPhieuMuaHangResourceIntTest {

    private static final Long DEFAULT_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_SAN_PHAM_ID = 2L;

    private static final Long DEFAULT_CHI_TIET_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_CHI_TIET_SAN_PHAM_ID = 2L;

    private static final Double DEFAULT_GIA_MUA = 1D;
    private static final Double UPDATED_GIA_MUA = 2D;

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private ChiTietPhieuMuaHangRepository chiTietPhieuMuaHangRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChiTietPhieuMuaHangMockMvc;

    private ChiTietPhieuMuaHang chiTietPhieuMuaHang;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChiTietPhieuMuaHangResource chiTietPhieuMuaHangResource = new ChiTietPhieuMuaHangResource(chiTietPhieuMuaHangRepository);
        this.restChiTietPhieuMuaHangMockMvc = MockMvcBuilders.standaloneSetup(chiTietPhieuMuaHangResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietPhieuMuaHang createEntity(EntityManager em) {
        ChiTietPhieuMuaHang chiTietPhieuMuaHang = new ChiTietPhieuMuaHang()
            .sanPhamId(DEFAULT_SAN_PHAM_ID)
            .chiTietSanPhamId(DEFAULT_CHI_TIET_SAN_PHAM_ID)
            .giaMua(DEFAULT_GIA_MUA)
            .soLuong(DEFAULT_SO_LUONG)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return chiTietPhieuMuaHang;
    }

    @Before
    public void initTest() {
        chiTietPhieuMuaHang = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiTietPhieuMuaHang() throws Exception {
        int databaseSizeBeforeCreate = chiTietPhieuMuaHangRepository.findAll().size();

        // Create the ChiTietPhieuMuaHang
        restChiTietPhieuMuaHangMockMvc.perform(post("/api/chi-tiet-phieu-mua-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuMuaHang)))
            .andExpect(status().isCreated());

        // Validate the ChiTietPhieuMuaHang in the database
        List<ChiTietPhieuMuaHang> chiTietPhieuMuaHangList = chiTietPhieuMuaHangRepository.findAll();
        assertThat(chiTietPhieuMuaHangList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietPhieuMuaHang testChiTietPhieuMuaHang = chiTietPhieuMuaHangList.get(chiTietPhieuMuaHangList.size() - 1);
        assertThat(testChiTietPhieuMuaHang.getSanPhamId()).isEqualTo(DEFAULT_SAN_PHAM_ID);
        assertThat(testChiTietPhieuMuaHang.getChiTietSanPhamId()).isEqualTo(DEFAULT_CHI_TIET_SAN_PHAM_ID);
        assertThat(testChiTietPhieuMuaHang.getGiaMua()).isEqualTo(DEFAULT_GIA_MUA);
        assertThat(testChiTietPhieuMuaHang.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiTietPhieuMuaHang.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testChiTietPhieuMuaHang.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testChiTietPhieuMuaHang.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testChiTietPhieuMuaHang.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createChiTietPhieuMuaHangWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiTietPhieuMuaHangRepository.findAll().size();

        // Create the ChiTietPhieuMuaHang with an existing ID
        chiTietPhieuMuaHang.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietPhieuMuaHangMockMvc.perform(post("/api/chi-tiet-phieu-mua-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuMuaHang)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietPhieuMuaHang in the database
        List<ChiTietPhieuMuaHang> chiTietPhieuMuaHangList = chiTietPhieuMuaHangRepository.findAll();
        assertThat(chiTietPhieuMuaHangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChiTietPhieuMuaHangs() throws Exception {
        // Initialize the database
        chiTietPhieuMuaHangRepository.saveAndFlush(chiTietPhieuMuaHang);

        // Get all the chiTietPhieuMuaHangList
        restChiTietPhieuMuaHangMockMvc.perform(get("/api/chi-tiet-phieu-mua-hangs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietPhieuMuaHang.getId().intValue())))
            .andExpect(jsonPath("$.[*].sanPhamId").value(hasItem(DEFAULT_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].chiTietSanPhamId").value(hasItem(DEFAULT_CHI_TIET_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].giaMua").value(hasItem(DEFAULT_GIA_MUA.doubleValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getChiTietPhieuMuaHang() throws Exception {
        // Initialize the database
        chiTietPhieuMuaHangRepository.saveAndFlush(chiTietPhieuMuaHang);

        // Get the chiTietPhieuMuaHang
        restChiTietPhieuMuaHangMockMvc.perform(get("/api/chi-tiet-phieu-mua-hangs/{id}", chiTietPhieuMuaHang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietPhieuMuaHang.getId().intValue()))
            .andExpect(jsonPath("$.sanPhamId").value(DEFAULT_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.chiTietSanPhamId").value(DEFAULT_CHI_TIET_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.giaMua").value(DEFAULT_GIA_MUA.doubleValue()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingChiTietPhieuMuaHang() throws Exception {
        // Get the chiTietPhieuMuaHang
        restChiTietPhieuMuaHangMockMvc.perform(get("/api/chi-tiet-phieu-mua-hangs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiTietPhieuMuaHang() throws Exception {
        // Initialize the database
        chiTietPhieuMuaHangRepository.saveAndFlush(chiTietPhieuMuaHang);

        int databaseSizeBeforeUpdate = chiTietPhieuMuaHangRepository.findAll().size();

        // Update the chiTietPhieuMuaHang
        ChiTietPhieuMuaHang updatedChiTietPhieuMuaHang = chiTietPhieuMuaHangRepository.findById(chiTietPhieuMuaHang.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietPhieuMuaHang are not directly saved in db
        em.detach(updatedChiTietPhieuMuaHang);
        updatedChiTietPhieuMuaHang
            .sanPhamId(UPDATED_SAN_PHAM_ID)
            .chiTietSanPhamId(UPDATED_CHI_TIET_SAN_PHAM_ID)
            .giaMua(UPDATED_GIA_MUA)
            .soLuong(UPDATED_SO_LUONG)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restChiTietPhieuMuaHangMockMvc.perform(put("/api/chi-tiet-phieu-mua-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChiTietPhieuMuaHang)))
            .andExpect(status().isOk());

        // Validate the ChiTietPhieuMuaHang in the database
        List<ChiTietPhieuMuaHang> chiTietPhieuMuaHangList = chiTietPhieuMuaHangRepository.findAll();
        assertThat(chiTietPhieuMuaHangList).hasSize(databaseSizeBeforeUpdate);
        ChiTietPhieuMuaHang testChiTietPhieuMuaHang = chiTietPhieuMuaHangList.get(chiTietPhieuMuaHangList.size() - 1);
        assertThat(testChiTietPhieuMuaHang.getSanPhamId()).isEqualTo(UPDATED_SAN_PHAM_ID);
        assertThat(testChiTietPhieuMuaHang.getChiTietSanPhamId()).isEqualTo(UPDATED_CHI_TIET_SAN_PHAM_ID);
        assertThat(testChiTietPhieuMuaHang.getGiaMua()).isEqualTo(UPDATED_GIA_MUA);
        assertThat(testChiTietPhieuMuaHang.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietPhieuMuaHang.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testChiTietPhieuMuaHang.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testChiTietPhieuMuaHang.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testChiTietPhieuMuaHang.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingChiTietPhieuMuaHang() throws Exception {
        int databaseSizeBeforeUpdate = chiTietPhieuMuaHangRepository.findAll().size();

        // Create the ChiTietPhieuMuaHang

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restChiTietPhieuMuaHangMockMvc.perform(put("/api/chi-tiet-phieu-mua-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietPhieuMuaHang)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietPhieuMuaHang in the database
        List<ChiTietPhieuMuaHang> chiTietPhieuMuaHangList = chiTietPhieuMuaHangRepository.findAll();
        assertThat(chiTietPhieuMuaHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiTietPhieuMuaHang() throws Exception {
        // Initialize the database
        chiTietPhieuMuaHangRepository.saveAndFlush(chiTietPhieuMuaHang);

        int databaseSizeBeforeDelete = chiTietPhieuMuaHangRepository.findAll().size();

        // Get the chiTietPhieuMuaHang
        restChiTietPhieuMuaHangMockMvc.perform(delete("/api/chi-tiet-phieu-mua-hangs/{id}", chiTietPhieuMuaHang.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChiTietPhieuMuaHang> chiTietPhieuMuaHangList = chiTietPhieuMuaHangRepository.findAll();
        assertThat(chiTietPhieuMuaHangList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietPhieuMuaHang.class);
        ChiTietPhieuMuaHang chiTietPhieuMuaHang1 = new ChiTietPhieuMuaHang();
        chiTietPhieuMuaHang1.setId(1L);
        ChiTietPhieuMuaHang chiTietPhieuMuaHang2 = new ChiTietPhieuMuaHang();
        chiTietPhieuMuaHang2.setId(chiTietPhieuMuaHang1.getId());
        assertThat(chiTietPhieuMuaHang1).isEqualTo(chiTietPhieuMuaHang2);
        chiTietPhieuMuaHang2.setId(2L);
        assertThat(chiTietPhieuMuaHang1).isNotEqualTo(chiTietPhieuMuaHang2);
        chiTietPhieuMuaHang1.setId(null);
        assertThat(chiTietPhieuMuaHang1).isNotEqualTo(chiTietPhieuMuaHang2);
    }
}
