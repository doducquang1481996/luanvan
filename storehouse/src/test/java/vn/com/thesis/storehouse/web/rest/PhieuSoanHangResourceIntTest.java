package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.PhieuSoanHang;
import vn.com.thesis.storehouse.repository.PhieuSoanHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PhieuSoanHangResource REST controller.
 *
 * @see PhieuSoanHangResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class PhieuSoanHangResourceIntTest {

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final Long DEFAULT_DON_DAT_HANG_ID = 1L;
    private static final Long UPDATED_DON_DAT_HANG_ID = 2L;

    private static final String DEFAULT_MA_DON_DAT_HANG = "AAAAAAAAAA";
    private static final String UPDATED_MA_DON_DAT_HANG = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_SOAN_HANG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_SOAN_HANG = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_NGAY_GIAO_HANG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_GIAO_HANG = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_XUAT_HANG = false;
    private static final Boolean UPDATED_XUAT_HANG = true;

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private PhieuSoanHangRepository phieuSoanHangRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPhieuSoanHangMockMvc;

    private PhieuSoanHang phieuSoanHang;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PhieuSoanHangResource phieuSoanHangResource = new PhieuSoanHangResource(phieuSoanHangRepository);
        this.restPhieuSoanHangMockMvc = MockMvcBuilders.standaloneSetup(phieuSoanHangResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuSoanHang createEntity(EntityManager em) {
        PhieuSoanHang phieuSoanHang = new PhieuSoanHang()
            .ma(DEFAULT_MA)
            .donDatHangId(DEFAULT_DON_DAT_HANG_ID)
            .maDonDatHang(DEFAULT_MA_DON_DAT_HANG)
            .ngaySoanHang(DEFAULT_NGAY_SOAN_HANG)
            .ngayGiaoHang(DEFAULT_NGAY_GIAO_HANG)
            .xuatHang(DEFAULT_XUAT_HANG)
            .ghiChu(DEFAULT_GHI_CHU)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return phieuSoanHang;
    }

    @Before
    public void initTest() {
        phieuSoanHang = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhieuSoanHang() throws Exception {
        int databaseSizeBeforeCreate = phieuSoanHangRepository.findAll().size();

        // Create the PhieuSoanHang
        restPhieuSoanHangMockMvc.perform(post("/api/phieu-soan-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuSoanHang)))
            .andExpect(status().isCreated());

        // Validate the PhieuSoanHang in the database
        List<PhieuSoanHang> phieuSoanHangList = phieuSoanHangRepository.findAll();
        assertThat(phieuSoanHangList).hasSize(databaseSizeBeforeCreate + 1);
        PhieuSoanHang testPhieuSoanHang = phieuSoanHangList.get(phieuSoanHangList.size() - 1);
        assertThat(testPhieuSoanHang.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testPhieuSoanHang.getDonDatHangId()).isEqualTo(DEFAULT_DON_DAT_HANG_ID);
        assertThat(testPhieuSoanHang.getMaDonDatHang()).isEqualTo(DEFAULT_MA_DON_DAT_HANG);
        assertThat(testPhieuSoanHang.getNgaySoanHang()).isEqualTo(DEFAULT_NGAY_SOAN_HANG);
        assertThat(testPhieuSoanHang.getNgayGiaoHang()).isEqualTo(DEFAULT_NGAY_GIAO_HANG);
        assertThat(testPhieuSoanHang.isXuatHang()).isEqualTo(DEFAULT_XUAT_HANG);
        assertThat(testPhieuSoanHang.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testPhieuSoanHang.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testPhieuSoanHang.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testPhieuSoanHang.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testPhieuSoanHang.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createPhieuSoanHangWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phieuSoanHangRepository.findAll().size();

        // Create the PhieuSoanHang with an existing ID
        phieuSoanHang.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhieuSoanHangMockMvc.perform(post("/api/phieu-soan-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuSoanHang)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuSoanHang in the database
        List<PhieuSoanHang> phieuSoanHangList = phieuSoanHangRepository.findAll();
        assertThat(phieuSoanHangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPhieuSoanHangs() throws Exception {
        // Initialize the database
        phieuSoanHangRepository.saveAndFlush(phieuSoanHang);

        // Get all the phieuSoanHangList
        restPhieuSoanHangMockMvc.perform(get("/api/phieu-soan-hangs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuSoanHang.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA.toString())))
            .andExpect(jsonPath("$.[*].donDatHangId").value(hasItem(DEFAULT_DON_DAT_HANG_ID.intValue())))
            .andExpect(jsonPath("$.[*].maDonDatHang").value(hasItem(DEFAULT_MA_DON_DAT_HANG.toString())))
            .andExpect(jsonPath("$.[*].ngaySoanHang").value(hasItem(DEFAULT_NGAY_SOAN_HANG.toString())))
            .andExpect(jsonPath("$.[*].ngayGiaoHang").value(hasItem(DEFAULT_NGAY_GIAO_HANG.toString())))
            .andExpect(jsonPath("$.[*].xuatHang").value(hasItem(DEFAULT_XUAT_HANG.booleanValue())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU.toString())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getPhieuSoanHang() throws Exception {
        // Initialize the database
        phieuSoanHangRepository.saveAndFlush(phieuSoanHang);

        // Get the phieuSoanHang
        restPhieuSoanHangMockMvc.perform(get("/api/phieu-soan-hangs/{id}", phieuSoanHang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(phieuSoanHang.getId().intValue()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA.toString()))
            .andExpect(jsonPath("$.donDatHangId").value(DEFAULT_DON_DAT_HANG_ID.intValue()))
            .andExpect(jsonPath("$.maDonDatHang").value(DEFAULT_MA_DON_DAT_HANG.toString()))
            .andExpect(jsonPath("$.ngaySoanHang").value(DEFAULT_NGAY_SOAN_HANG.toString()))
            .andExpect(jsonPath("$.ngayGiaoHang").value(DEFAULT_NGAY_GIAO_HANG.toString()))
            .andExpect(jsonPath("$.xuatHang").value(DEFAULT_XUAT_HANG.booleanValue()))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU.toString()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingPhieuSoanHang() throws Exception {
        // Get the phieuSoanHang
        restPhieuSoanHangMockMvc.perform(get("/api/phieu-soan-hangs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhieuSoanHang() throws Exception {
        // Initialize the database
        phieuSoanHangRepository.saveAndFlush(phieuSoanHang);

        int databaseSizeBeforeUpdate = phieuSoanHangRepository.findAll().size();

        // Update the phieuSoanHang
        PhieuSoanHang updatedPhieuSoanHang = phieuSoanHangRepository.findById(phieuSoanHang.getId()).get();
        // Disconnect from session so that the updates on updatedPhieuSoanHang are not directly saved in db
        em.detach(updatedPhieuSoanHang);
        updatedPhieuSoanHang
            .ma(UPDATED_MA)
            .donDatHangId(UPDATED_DON_DAT_HANG_ID)
            .maDonDatHang(UPDATED_MA_DON_DAT_HANG)
            .ngaySoanHang(UPDATED_NGAY_SOAN_HANG)
            .ngayGiaoHang(UPDATED_NGAY_GIAO_HANG)
            .xuatHang(UPDATED_XUAT_HANG)
            .ghiChu(UPDATED_GHI_CHU)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restPhieuSoanHangMockMvc.perform(put("/api/phieu-soan-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPhieuSoanHang)))
            .andExpect(status().isOk());

        // Validate the PhieuSoanHang in the database
        List<PhieuSoanHang> phieuSoanHangList = phieuSoanHangRepository.findAll();
        assertThat(phieuSoanHangList).hasSize(databaseSizeBeforeUpdate);
        PhieuSoanHang testPhieuSoanHang = phieuSoanHangList.get(phieuSoanHangList.size() - 1);
        assertThat(testPhieuSoanHang.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testPhieuSoanHang.getDonDatHangId()).isEqualTo(UPDATED_DON_DAT_HANG_ID);
        assertThat(testPhieuSoanHang.getMaDonDatHang()).isEqualTo(UPDATED_MA_DON_DAT_HANG);
        assertThat(testPhieuSoanHang.getNgaySoanHang()).isEqualTo(UPDATED_NGAY_SOAN_HANG);
        assertThat(testPhieuSoanHang.getNgayGiaoHang()).isEqualTo(UPDATED_NGAY_GIAO_HANG);
        assertThat(testPhieuSoanHang.isXuatHang()).isEqualTo(UPDATED_XUAT_HANG);
        assertThat(testPhieuSoanHang.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testPhieuSoanHang.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testPhieuSoanHang.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testPhieuSoanHang.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testPhieuSoanHang.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingPhieuSoanHang() throws Exception {
        int databaseSizeBeforeUpdate = phieuSoanHangRepository.findAll().size();

        // Create the PhieuSoanHang

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restPhieuSoanHangMockMvc.perform(put("/api/phieu-soan-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuSoanHang)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuSoanHang in the database
        List<PhieuSoanHang> phieuSoanHangList = phieuSoanHangRepository.findAll();
        assertThat(phieuSoanHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhieuSoanHang() throws Exception {
        // Initialize the database
        phieuSoanHangRepository.saveAndFlush(phieuSoanHang);

        int databaseSizeBeforeDelete = phieuSoanHangRepository.findAll().size();

        // Get the phieuSoanHang
        restPhieuSoanHangMockMvc.perform(delete("/api/phieu-soan-hangs/{id}", phieuSoanHang.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PhieuSoanHang> phieuSoanHangList = phieuSoanHangRepository.findAll();
        assertThat(phieuSoanHangList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuSoanHang.class);
        PhieuSoanHang phieuSoanHang1 = new PhieuSoanHang();
        phieuSoanHang1.setId(1L);
        PhieuSoanHang phieuSoanHang2 = new PhieuSoanHang();
        phieuSoanHang2.setId(phieuSoanHang1.getId());
        assertThat(phieuSoanHang1).isEqualTo(phieuSoanHang2);
        phieuSoanHang2.setId(2L);
        assertThat(phieuSoanHang1).isNotEqualTo(phieuSoanHang2);
        phieuSoanHang1.setId(null);
        assertThat(phieuSoanHang1).isNotEqualTo(phieuSoanHang2);
    }
}
