package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.ChiTietNhapKho;
import vn.com.thesis.storehouse.repository.ChiTietNhapKhoRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChiTietNhapKhoResource REST controller.
 *
 * @see ChiTietNhapKhoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class ChiTietNhapKhoResourceIntTest {

    private static final Long DEFAULT_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_SAN_PHAM_ID = 2L;

    private static final Long DEFAULT_CHI_TIET_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_CHI_TIET_SAN_PHAM_ID = 2L;

    private static final Double DEFAULT_GIA_MUA = 1D;
    private static final Double UPDATED_GIA_MUA = 2D;

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private ChiTietNhapKhoRepository chiTietNhapKhoRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChiTietNhapKhoMockMvc;

    private ChiTietNhapKho chiTietNhapKho;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChiTietNhapKhoResource chiTietNhapKhoResource = new ChiTietNhapKhoResource(chiTietNhapKhoRepository);
        this.restChiTietNhapKhoMockMvc = MockMvcBuilders.standaloneSetup(chiTietNhapKhoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietNhapKho createEntity(EntityManager em) {
        ChiTietNhapKho chiTietNhapKho = new ChiTietNhapKho()
            .sanPhamId(DEFAULT_SAN_PHAM_ID)
            .chiTietSanPhamId(DEFAULT_CHI_TIET_SAN_PHAM_ID)
            .giaMua(DEFAULT_GIA_MUA)
            .soLuong(DEFAULT_SO_LUONG)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return chiTietNhapKho;
    }

    @Before
    public void initTest() {
        chiTietNhapKho = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiTietNhapKho() throws Exception {
        int databaseSizeBeforeCreate = chiTietNhapKhoRepository.findAll().size();

        // Create the ChiTietNhapKho
        restChiTietNhapKhoMockMvc.perform(post("/api/chi-tiet-nhap-khos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietNhapKho)))
            .andExpect(status().isCreated());

        // Validate the ChiTietNhapKho in the database
        List<ChiTietNhapKho> chiTietNhapKhoList = chiTietNhapKhoRepository.findAll();
        assertThat(chiTietNhapKhoList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietNhapKho testChiTietNhapKho = chiTietNhapKhoList.get(chiTietNhapKhoList.size() - 1);
        assertThat(testChiTietNhapKho.getSanPhamId()).isEqualTo(DEFAULT_SAN_PHAM_ID);
        assertThat(testChiTietNhapKho.getChiTietSanPhamId()).isEqualTo(DEFAULT_CHI_TIET_SAN_PHAM_ID);
        assertThat(testChiTietNhapKho.getGiaMua()).isEqualTo(DEFAULT_GIA_MUA);
        assertThat(testChiTietNhapKho.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiTietNhapKho.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testChiTietNhapKho.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testChiTietNhapKho.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testChiTietNhapKho.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createChiTietNhapKhoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiTietNhapKhoRepository.findAll().size();

        // Create the ChiTietNhapKho with an existing ID
        chiTietNhapKho.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietNhapKhoMockMvc.perform(post("/api/chi-tiet-nhap-khos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietNhapKho)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietNhapKho in the database
        List<ChiTietNhapKho> chiTietNhapKhoList = chiTietNhapKhoRepository.findAll();
        assertThat(chiTietNhapKhoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChiTietNhapKhos() throws Exception {
        // Initialize the database
        chiTietNhapKhoRepository.saveAndFlush(chiTietNhapKho);

        // Get all the chiTietNhapKhoList
        restChiTietNhapKhoMockMvc.perform(get("/api/chi-tiet-nhap-khos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietNhapKho.getId().intValue())))
            .andExpect(jsonPath("$.[*].sanPhamId").value(hasItem(DEFAULT_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].chiTietSanPhamId").value(hasItem(DEFAULT_CHI_TIET_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].giaMua").value(hasItem(DEFAULT_GIA_MUA.doubleValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getChiTietNhapKho() throws Exception {
        // Initialize the database
        chiTietNhapKhoRepository.saveAndFlush(chiTietNhapKho);

        // Get the chiTietNhapKho
        restChiTietNhapKhoMockMvc.perform(get("/api/chi-tiet-nhap-khos/{id}", chiTietNhapKho.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietNhapKho.getId().intValue()))
            .andExpect(jsonPath("$.sanPhamId").value(DEFAULT_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.chiTietSanPhamId").value(DEFAULT_CHI_TIET_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.giaMua").value(DEFAULT_GIA_MUA.doubleValue()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingChiTietNhapKho() throws Exception {
        // Get the chiTietNhapKho
        restChiTietNhapKhoMockMvc.perform(get("/api/chi-tiet-nhap-khos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiTietNhapKho() throws Exception {
        // Initialize the database
        chiTietNhapKhoRepository.saveAndFlush(chiTietNhapKho);

        int databaseSizeBeforeUpdate = chiTietNhapKhoRepository.findAll().size();

        // Update the chiTietNhapKho
        ChiTietNhapKho updatedChiTietNhapKho = chiTietNhapKhoRepository.findById(chiTietNhapKho.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietNhapKho are not directly saved in db
        em.detach(updatedChiTietNhapKho);
        updatedChiTietNhapKho
            .sanPhamId(UPDATED_SAN_PHAM_ID)
            .chiTietSanPhamId(UPDATED_CHI_TIET_SAN_PHAM_ID)
            .giaMua(UPDATED_GIA_MUA)
            .soLuong(UPDATED_SO_LUONG)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restChiTietNhapKhoMockMvc.perform(put("/api/chi-tiet-nhap-khos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChiTietNhapKho)))
            .andExpect(status().isOk());

        // Validate the ChiTietNhapKho in the database
        List<ChiTietNhapKho> chiTietNhapKhoList = chiTietNhapKhoRepository.findAll();
        assertThat(chiTietNhapKhoList).hasSize(databaseSizeBeforeUpdate);
        ChiTietNhapKho testChiTietNhapKho = chiTietNhapKhoList.get(chiTietNhapKhoList.size() - 1);
        assertThat(testChiTietNhapKho.getSanPhamId()).isEqualTo(UPDATED_SAN_PHAM_ID);
        assertThat(testChiTietNhapKho.getChiTietSanPhamId()).isEqualTo(UPDATED_CHI_TIET_SAN_PHAM_ID);
        assertThat(testChiTietNhapKho.getGiaMua()).isEqualTo(UPDATED_GIA_MUA);
        assertThat(testChiTietNhapKho.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietNhapKho.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testChiTietNhapKho.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testChiTietNhapKho.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testChiTietNhapKho.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingChiTietNhapKho() throws Exception {
        int databaseSizeBeforeUpdate = chiTietNhapKhoRepository.findAll().size();

        // Create the ChiTietNhapKho

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restChiTietNhapKhoMockMvc.perform(put("/api/chi-tiet-nhap-khos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietNhapKho)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietNhapKho in the database
        List<ChiTietNhapKho> chiTietNhapKhoList = chiTietNhapKhoRepository.findAll();
        assertThat(chiTietNhapKhoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiTietNhapKho() throws Exception {
        // Initialize the database
        chiTietNhapKhoRepository.saveAndFlush(chiTietNhapKho);

        int databaseSizeBeforeDelete = chiTietNhapKhoRepository.findAll().size();

        // Get the chiTietNhapKho
        restChiTietNhapKhoMockMvc.perform(delete("/api/chi-tiet-nhap-khos/{id}", chiTietNhapKho.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChiTietNhapKho> chiTietNhapKhoList = chiTietNhapKhoRepository.findAll();
        assertThat(chiTietNhapKhoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietNhapKho.class);
        ChiTietNhapKho chiTietNhapKho1 = new ChiTietNhapKho();
        chiTietNhapKho1.setId(1L);
        ChiTietNhapKho chiTietNhapKho2 = new ChiTietNhapKho();
        chiTietNhapKho2.setId(chiTietNhapKho1.getId());
        assertThat(chiTietNhapKho1).isEqualTo(chiTietNhapKho2);
        chiTietNhapKho2.setId(2L);
        assertThat(chiTietNhapKho1).isNotEqualTo(chiTietNhapKho2);
        chiTietNhapKho1.setId(null);
        assertThat(chiTietNhapKho1).isNotEqualTo(chiTietNhapKho2);
    }
}
