package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.DanhSachNhapKho;
import vn.com.thesis.storehouse.repository.DanhSachNhapKhoRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DanhSachNhapKhoResource REST controller.
 *
 * @see DanhSachNhapKhoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class DanhSachNhapKhoResourceIntTest {

    private static final Long DEFAULT_PHIEU_MUA_HANG_ID = 1L;
    private static final Long UPDATED_PHIEU_MUA_HANG_ID = 2L;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private DanhSachNhapKhoRepository danhSachNhapKhoRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDanhSachNhapKhoMockMvc;

    private DanhSachNhapKho danhSachNhapKho;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DanhSachNhapKhoResource danhSachNhapKhoResource = new DanhSachNhapKhoResource(danhSachNhapKhoRepository);
        this.restDanhSachNhapKhoMockMvc = MockMvcBuilders.standaloneSetup(danhSachNhapKhoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DanhSachNhapKho createEntity(EntityManager em) {
        DanhSachNhapKho danhSachNhapKho = new DanhSachNhapKho()
            .phieuMuaHangId(DEFAULT_PHIEU_MUA_HANG_ID)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return danhSachNhapKho;
    }

    @Before
    public void initTest() {
        danhSachNhapKho = createEntity(em);
    }

    @Test
    @Transactional
    public void createDanhSachNhapKho() throws Exception {
        int databaseSizeBeforeCreate = danhSachNhapKhoRepository.findAll().size();

        // Create the DanhSachNhapKho
        restDanhSachNhapKhoMockMvc.perform(post("/api/danh-sach-nhap-khos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(danhSachNhapKho)))
            .andExpect(status().isCreated());

        // Validate the DanhSachNhapKho in the database
        List<DanhSachNhapKho> danhSachNhapKhoList = danhSachNhapKhoRepository.findAll();
        assertThat(danhSachNhapKhoList).hasSize(databaseSizeBeforeCreate + 1);
        DanhSachNhapKho testDanhSachNhapKho = danhSachNhapKhoList.get(danhSachNhapKhoList.size() - 1);
        assertThat(testDanhSachNhapKho.getPhieuMuaHangId()).isEqualTo(DEFAULT_PHIEU_MUA_HANG_ID);
        assertThat(testDanhSachNhapKho.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testDanhSachNhapKho.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testDanhSachNhapKho.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testDanhSachNhapKho.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createDanhSachNhapKhoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = danhSachNhapKhoRepository.findAll().size();

        // Create the DanhSachNhapKho with an existing ID
        danhSachNhapKho.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDanhSachNhapKhoMockMvc.perform(post("/api/danh-sach-nhap-khos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(danhSachNhapKho)))
            .andExpect(status().isBadRequest());

        // Validate the DanhSachNhapKho in the database
        List<DanhSachNhapKho> danhSachNhapKhoList = danhSachNhapKhoRepository.findAll();
        assertThat(danhSachNhapKhoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDanhSachNhapKhos() throws Exception {
        // Initialize the database
        danhSachNhapKhoRepository.saveAndFlush(danhSachNhapKho);

        // Get all the danhSachNhapKhoList
        restDanhSachNhapKhoMockMvc.perform(get("/api/danh-sach-nhap-khos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(danhSachNhapKho.getId().intValue())))
            .andExpect(jsonPath("$.[*].phieuMuaHangId").value(hasItem(DEFAULT_PHIEU_MUA_HANG_ID.intValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getDanhSachNhapKho() throws Exception {
        // Initialize the database
        danhSachNhapKhoRepository.saveAndFlush(danhSachNhapKho);

        // Get the danhSachNhapKho
        restDanhSachNhapKhoMockMvc.perform(get("/api/danh-sach-nhap-khos/{id}", danhSachNhapKho.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(danhSachNhapKho.getId().intValue()))
            .andExpect(jsonPath("$.phieuMuaHangId").value(DEFAULT_PHIEU_MUA_HANG_ID.intValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingDanhSachNhapKho() throws Exception {
        // Get the danhSachNhapKho
        restDanhSachNhapKhoMockMvc.perform(get("/api/danh-sach-nhap-khos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDanhSachNhapKho() throws Exception {
        // Initialize the database
        danhSachNhapKhoRepository.saveAndFlush(danhSachNhapKho);

        int databaseSizeBeforeUpdate = danhSachNhapKhoRepository.findAll().size();

        // Update the danhSachNhapKho
        DanhSachNhapKho updatedDanhSachNhapKho = danhSachNhapKhoRepository.findById(danhSachNhapKho.getId()).get();
        // Disconnect from session so that the updates on updatedDanhSachNhapKho are not directly saved in db
        em.detach(updatedDanhSachNhapKho);
        updatedDanhSachNhapKho
            .phieuMuaHangId(UPDATED_PHIEU_MUA_HANG_ID)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restDanhSachNhapKhoMockMvc.perform(put("/api/danh-sach-nhap-khos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDanhSachNhapKho)))
            .andExpect(status().isOk());

        // Validate the DanhSachNhapKho in the database
        List<DanhSachNhapKho> danhSachNhapKhoList = danhSachNhapKhoRepository.findAll();
        assertThat(danhSachNhapKhoList).hasSize(databaseSizeBeforeUpdate);
        DanhSachNhapKho testDanhSachNhapKho = danhSachNhapKhoList.get(danhSachNhapKhoList.size() - 1);
        assertThat(testDanhSachNhapKho.getPhieuMuaHangId()).isEqualTo(UPDATED_PHIEU_MUA_HANG_ID);
        assertThat(testDanhSachNhapKho.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testDanhSachNhapKho.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testDanhSachNhapKho.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testDanhSachNhapKho.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingDanhSachNhapKho() throws Exception {
        int databaseSizeBeforeUpdate = danhSachNhapKhoRepository.findAll().size();

        // Create the DanhSachNhapKho

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restDanhSachNhapKhoMockMvc.perform(put("/api/danh-sach-nhap-khos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(danhSachNhapKho)))
            .andExpect(status().isBadRequest());

        // Validate the DanhSachNhapKho in the database
        List<DanhSachNhapKho> danhSachNhapKhoList = danhSachNhapKhoRepository.findAll();
        assertThat(danhSachNhapKhoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDanhSachNhapKho() throws Exception {
        // Initialize the database
        danhSachNhapKhoRepository.saveAndFlush(danhSachNhapKho);

        int databaseSizeBeforeDelete = danhSachNhapKhoRepository.findAll().size();

        // Get the danhSachNhapKho
        restDanhSachNhapKhoMockMvc.perform(delete("/api/danh-sach-nhap-khos/{id}", danhSachNhapKho.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DanhSachNhapKho> danhSachNhapKhoList = danhSachNhapKhoRepository.findAll();
        assertThat(danhSachNhapKhoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DanhSachNhapKho.class);
        DanhSachNhapKho danhSachNhapKho1 = new DanhSachNhapKho();
        danhSachNhapKho1.setId(1L);
        DanhSachNhapKho danhSachNhapKho2 = new DanhSachNhapKho();
        danhSachNhapKho2.setId(danhSachNhapKho1.getId());
        assertThat(danhSachNhapKho1).isEqualTo(danhSachNhapKho2);
        danhSachNhapKho2.setId(2L);
        assertThat(danhSachNhapKho1).isNotEqualTo(danhSachNhapKho2);
        danhSachNhapKho1.setId(null);
        assertThat(danhSachNhapKho1).isNotEqualTo(danhSachNhapKho2);
    }
}
