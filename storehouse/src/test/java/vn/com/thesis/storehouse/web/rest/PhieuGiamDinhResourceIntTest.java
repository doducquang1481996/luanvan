package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.PhieuGiamDinh;
import vn.com.thesis.storehouse.repository.PhieuGiamDinhRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PhieuGiamDinhResource REST controller.
 *
 * @see PhieuGiamDinhResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class PhieuGiamDinhResourceIntTest {

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_GIAM_DINH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_GIAM_DINH = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_PHIEU_MUA_HANG_ID = 1L;
    private static final Long UPDATED_PHIEU_MUA_HANG_ID = 2L;

    private static final String DEFAULT_MA_PHIEU_MUA_HANG = "AAAAAAAAAA";
    private static final String UPDATED_MA_PHIEU_MUA_HANG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_TRANG_THAI = false;
    private static final Boolean UPDATED_TRANG_THAI = true;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private PhieuGiamDinhRepository phieuGiamDinhRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPhieuGiamDinhMockMvc;

    private PhieuGiamDinh phieuGiamDinh;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PhieuGiamDinhResource phieuGiamDinhResource = new PhieuGiamDinhResource(phieuGiamDinhRepository);
        this.restPhieuGiamDinhMockMvc = MockMvcBuilders.standaloneSetup(phieuGiamDinhResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuGiamDinh createEntity(EntityManager em) {
        PhieuGiamDinh phieuGiamDinh = new PhieuGiamDinh()
            .ma(DEFAULT_MA)
            .ten(DEFAULT_TEN)
            .ngayGiamDinh(DEFAULT_NGAY_GIAM_DINH)
            .phieuMuaHangId(DEFAULT_PHIEU_MUA_HANG_ID)
            .maPhieuMuaHang(DEFAULT_MA_PHIEU_MUA_HANG)
            .trangThai(DEFAULT_TRANG_THAI)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return phieuGiamDinh;
    }

    @Before
    public void initTest() {
        phieuGiamDinh = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhieuGiamDinh() throws Exception {
        int databaseSizeBeforeCreate = phieuGiamDinhRepository.findAll().size();

        // Create the PhieuGiamDinh
        restPhieuGiamDinhMockMvc.perform(post("/api/phieu-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuGiamDinh)))
            .andExpect(status().isCreated());

        // Validate the PhieuGiamDinh in the database
        List<PhieuGiamDinh> phieuGiamDinhList = phieuGiamDinhRepository.findAll();
        assertThat(phieuGiamDinhList).hasSize(databaseSizeBeforeCreate + 1);
        PhieuGiamDinh testPhieuGiamDinh = phieuGiamDinhList.get(phieuGiamDinhList.size() - 1);
        assertThat(testPhieuGiamDinh.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testPhieuGiamDinh.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testPhieuGiamDinh.getNgayGiamDinh()).isEqualTo(DEFAULT_NGAY_GIAM_DINH);
        assertThat(testPhieuGiamDinh.getPhieuMuaHangId()).isEqualTo(DEFAULT_PHIEU_MUA_HANG_ID);
        assertThat(testPhieuGiamDinh.getMaPhieuMuaHang()).isEqualTo(DEFAULT_MA_PHIEU_MUA_HANG);
        assertThat(testPhieuGiamDinh.isTrangThai()).isEqualTo(DEFAULT_TRANG_THAI);
        assertThat(testPhieuGiamDinh.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testPhieuGiamDinh.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testPhieuGiamDinh.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testPhieuGiamDinh.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createPhieuGiamDinhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phieuGiamDinhRepository.findAll().size();

        // Create the PhieuGiamDinh with an existing ID
        phieuGiamDinh.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhieuGiamDinhMockMvc.perform(post("/api/phieu-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuGiamDinh)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuGiamDinh in the database
        List<PhieuGiamDinh> phieuGiamDinhList = phieuGiamDinhRepository.findAll();
        assertThat(phieuGiamDinhList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPhieuGiamDinhs() throws Exception {
        // Initialize the database
        phieuGiamDinhRepository.saveAndFlush(phieuGiamDinh);

        // Get all the phieuGiamDinhList
        restPhieuGiamDinhMockMvc.perform(get("/api/phieu-giam-dinhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuGiamDinh.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA.toString())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN.toString())))
            .andExpect(jsonPath("$.[*].ngayGiamDinh").value(hasItem(DEFAULT_NGAY_GIAM_DINH.toString())))
            .andExpect(jsonPath("$.[*].phieuMuaHangId").value(hasItem(DEFAULT_PHIEU_MUA_HANG_ID.intValue())))
            .andExpect(jsonPath("$.[*].maPhieuMuaHang").value(hasItem(DEFAULT_MA_PHIEU_MUA_HANG.toString())))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI.booleanValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getPhieuGiamDinh() throws Exception {
        // Initialize the database
        phieuGiamDinhRepository.saveAndFlush(phieuGiamDinh);

        // Get the phieuGiamDinh
        restPhieuGiamDinhMockMvc.perform(get("/api/phieu-giam-dinhs/{id}", phieuGiamDinh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(phieuGiamDinh.getId().intValue()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA.toString()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN.toString()))
            .andExpect(jsonPath("$.ngayGiamDinh").value(DEFAULT_NGAY_GIAM_DINH.toString()))
            .andExpect(jsonPath("$.phieuMuaHangId").value(DEFAULT_PHIEU_MUA_HANG_ID.intValue()))
            .andExpect(jsonPath("$.maPhieuMuaHang").value(DEFAULT_MA_PHIEU_MUA_HANG.toString()))
            .andExpect(jsonPath("$.trangThai").value(DEFAULT_TRANG_THAI.booleanValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingPhieuGiamDinh() throws Exception {
        // Get the phieuGiamDinh
        restPhieuGiamDinhMockMvc.perform(get("/api/phieu-giam-dinhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhieuGiamDinh() throws Exception {
        // Initialize the database
        phieuGiamDinhRepository.saveAndFlush(phieuGiamDinh);

        int databaseSizeBeforeUpdate = phieuGiamDinhRepository.findAll().size();

        // Update the phieuGiamDinh
        PhieuGiamDinh updatedPhieuGiamDinh = phieuGiamDinhRepository.findById(phieuGiamDinh.getId()).get();
        // Disconnect from session so that the updates on updatedPhieuGiamDinh are not directly saved in db
        em.detach(updatedPhieuGiamDinh);
        updatedPhieuGiamDinh
            .ma(UPDATED_MA)
            .ten(UPDATED_TEN)
            .ngayGiamDinh(UPDATED_NGAY_GIAM_DINH)
            .phieuMuaHangId(UPDATED_PHIEU_MUA_HANG_ID)
            .maPhieuMuaHang(UPDATED_MA_PHIEU_MUA_HANG)
            .trangThai(UPDATED_TRANG_THAI)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restPhieuGiamDinhMockMvc.perform(put("/api/phieu-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPhieuGiamDinh)))
            .andExpect(status().isOk());

        // Validate the PhieuGiamDinh in the database
        List<PhieuGiamDinh> phieuGiamDinhList = phieuGiamDinhRepository.findAll();
        assertThat(phieuGiamDinhList).hasSize(databaseSizeBeforeUpdate);
        PhieuGiamDinh testPhieuGiamDinh = phieuGiamDinhList.get(phieuGiamDinhList.size() - 1);
        assertThat(testPhieuGiamDinh.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testPhieuGiamDinh.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testPhieuGiamDinh.getNgayGiamDinh()).isEqualTo(UPDATED_NGAY_GIAM_DINH);
        assertThat(testPhieuGiamDinh.getPhieuMuaHangId()).isEqualTo(UPDATED_PHIEU_MUA_HANG_ID);
        assertThat(testPhieuGiamDinh.getMaPhieuMuaHang()).isEqualTo(UPDATED_MA_PHIEU_MUA_HANG);
        assertThat(testPhieuGiamDinh.isTrangThai()).isEqualTo(UPDATED_TRANG_THAI);
        assertThat(testPhieuGiamDinh.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testPhieuGiamDinh.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testPhieuGiamDinh.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testPhieuGiamDinh.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingPhieuGiamDinh() throws Exception {
        int databaseSizeBeforeUpdate = phieuGiamDinhRepository.findAll().size();

        // Create the PhieuGiamDinh

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restPhieuGiamDinhMockMvc.perform(put("/api/phieu-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phieuGiamDinh)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuGiamDinh in the database
        List<PhieuGiamDinh> phieuGiamDinhList = phieuGiamDinhRepository.findAll();
        assertThat(phieuGiamDinhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhieuGiamDinh() throws Exception {
        // Initialize the database
        phieuGiamDinhRepository.saveAndFlush(phieuGiamDinh);

        int databaseSizeBeforeDelete = phieuGiamDinhRepository.findAll().size();

        // Get the phieuGiamDinh
        restPhieuGiamDinhMockMvc.perform(delete("/api/phieu-giam-dinhs/{id}", phieuGiamDinh.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PhieuGiamDinh> phieuGiamDinhList = phieuGiamDinhRepository.findAll();
        assertThat(phieuGiamDinhList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuGiamDinh.class);
        PhieuGiamDinh phieuGiamDinh1 = new PhieuGiamDinh();
        phieuGiamDinh1.setId(1L);
        PhieuGiamDinh phieuGiamDinh2 = new PhieuGiamDinh();
        phieuGiamDinh2.setId(phieuGiamDinh1.getId());
        assertThat(phieuGiamDinh1).isEqualTo(phieuGiamDinh2);
        phieuGiamDinh2.setId(2L);
        assertThat(phieuGiamDinh1).isNotEqualTo(phieuGiamDinh2);
        phieuGiamDinh1.setId(null);
        assertThat(phieuGiamDinh1).isNotEqualTo(phieuGiamDinh2);
    }
}
