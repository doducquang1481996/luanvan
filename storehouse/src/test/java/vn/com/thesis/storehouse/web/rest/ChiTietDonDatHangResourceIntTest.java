package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.ChiTietDonDatHang;
import vn.com.thesis.storehouse.repository.ChiTietDonDatHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChiTietDonDatHangResource REST controller.
 *
 * @see ChiTietDonDatHangResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class ChiTietDonDatHangResourceIntTest {

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final String DEFAULT_CHI_TIET_SAN_PHAM = "AAAAAAAAAA";
    private static final String UPDATED_CHI_TIET_SAN_PHAM = "BBBBBBBBBB";

    private static final String DEFAULT_GIA_BAN = "AAAAAAAAAA";
    private static final String UPDATED_GIA_BAN = "BBBBBBBBBB";

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private ChiTietDonDatHangRepository chiTietDonDatHangRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChiTietDonDatHangMockMvc;

    private ChiTietDonDatHang chiTietDonDatHang;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChiTietDonDatHangResource chiTietDonDatHangResource = new ChiTietDonDatHangResource(chiTietDonDatHangRepository);
        this.restChiTietDonDatHangMockMvc = MockMvcBuilders.standaloneSetup(chiTietDonDatHangResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietDonDatHang createEntity(EntityManager em) {
        ChiTietDonDatHang chiTietDonDatHang = new ChiTietDonDatHang()
            .ma(DEFAULT_MA)
            .chiTietSanPham(DEFAULT_CHI_TIET_SAN_PHAM)
            .giaBan(DEFAULT_GIA_BAN)
            .soLuong(DEFAULT_SO_LUONG)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return chiTietDonDatHang;
    }

    @Before
    public void initTest() {
        chiTietDonDatHang = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiTietDonDatHang() throws Exception {
        int databaseSizeBeforeCreate = chiTietDonDatHangRepository.findAll().size();

        // Create the ChiTietDonDatHang
        restChiTietDonDatHangMockMvc.perform(post("/api/chi-tiet-don-dat-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietDonDatHang)))
            .andExpect(status().isCreated());

        // Validate the ChiTietDonDatHang in the database
        List<ChiTietDonDatHang> chiTietDonDatHangList = chiTietDonDatHangRepository.findAll();
        assertThat(chiTietDonDatHangList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietDonDatHang testChiTietDonDatHang = chiTietDonDatHangList.get(chiTietDonDatHangList.size() - 1);
        assertThat(testChiTietDonDatHang.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testChiTietDonDatHang.getChiTietSanPham()).isEqualTo(DEFAULT_CHI_TIET_SAN_PHAM);
        assertThat(testChiTietDonDatHang.getGiaBan()).isEqualTo(DEFAULT_GIA_BAN);
        assertThat(testChiTietDonDatHang.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiTietDonDatHang.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testChiTietDonDatHang.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testChiTietDonDatHang.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testChiTietDonDatHang.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createChiTietDonDatHangWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiTietDonDatHangRepository.findAll().size();

        // Create the ChiTietDonDatHang with an existing ID
        chiTietDonDatHang.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietDonDatHangMockMvc.perform(post("/api/chi-tiet-don-dat-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietDonDatHang)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietDonDatHang in the database
        List<ChiTietDonDatHang> chiTietDonDatHangList = chiTietDonDatHangRepository.findAll();
        assertThat(chiTietDonDatHangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChiTietDonDatHangs() throws Exception {
        // Initialize the database
        chiTietDonDatHangRepository.saveAndFlush(chiTietDonDatHang);

        // Get all the chiTietDonDatHangList
        restChiTietDonDatHangMockMvc.perform(get("/api/chi-tiet-don-dat-hangs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietDonDatHang.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA.toString())))
            .andExpect(jsonPath("$.[*].chiTietSanPham").value(hasItem(DEFAULT_CHI_TIET_SAN_PHAM.toString())))
            .andExpect(jsonPath("$.[*].giaBan").value(hasItem(DEFAULT_GIA_BAN.toString())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getChiTietDonDatHang() throws Exception {
        // Initialize the database
        chiTietDonDatHangRepository.saveAndFlush(chiTietDonDatHang);

        // Get the chiTietDonDatHang
        restChiTietDonDatHangMockMvc.perform(get("/api/chi-tiet-don-dat-hangs/{id}", chiTietDonDatHang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietDonDatHang.getId().intValue()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA.toString()))
            .andExpect(jsonPath("$.chiTietSanPham").value(DEFAULT_CHI_TIET_SAN_PHAM.toString()))
            .andExpect(jsonPath("$.giaBan").value(DEFAULT_GIA_BAN.toString()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingChiTietDonDatHang() throws Exception {
        // Get the chiTietDonDatHang
        restChiTietDonDatHangMockMvc.perform(get("/api/chi-tiet-don-dat-hangs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiTietDonDatHang() throws Exception {
        // Initialize the database
        chiTietDonDatHangRepository.saveAndFlush(chiTietDonDatHang);

        int databaseSizeBeforeUpdate = chiTietDonDatHangRepository.findAll().size();

        // Update the chiTietDonDatHang
        ChiTietDonDatHang updatedChiTietDonDatHang = chiTietDonDatHangRepository.findById(chiTietDonDatHang.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietDonDatHang are not directly saved in db
        em.detach(updatedChiTietDonDatHang);
        updatedChiTietDonDatHang
            .ma(UPDATED_MA)
            .chiTietSanPham(UPDATED_CHI_TIET_SAN_PHAM)
            .giaBan(UPDATED_GIA_BAN)
            .soLuong(UPDATED_SO_LUONG)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restChiTietDonDatHangMockMvc.perform(put("/api/chi-tiet-don-dat-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChiTietDonDatHang)))
            .andExpect(status().isOk());

        // Validate the ChiTietDonDatHang in the database
        List<ChiTietDonDatHang> chiTietDonDatHangList = chiTietDonDatHangRepository.findAll();
        assertThat(chiTietDonDatHangList).hasSize(databaseSizeBeforeUpdate);
        ChiTietDonDatHang testChiTietDonDatHang = chiTietDonDatHangList.get(chiTietDonDatHangList.size() - 1);
        assertThat(testChiTietDonDatHang.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testChiTietDonDatHang.getChiTietSanPham()).isEqualTo(UPDATED_CHI_TIET_SAN_PHAM);
        assertThat(testChiTietDonDatHang.getGiaBan()).isEqualTo(UPDATED_GIA_BAN);
        assertThat(testChiTietDonDatHang.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietDonDatHang.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testChiTietDonDatHang.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testChiTietDonDatHang.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testChiTietDonDatHang.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingChiTietDonDatHang() throws Exception {
        int databaseSizeBeforeUpdate = chiTietDonDatHangRepository.findAll().size();

        // Create the ChiTietDonDatHang

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restChiTietDonDatHangMockMvc.perform(put("/api/chi-tiet-don-dat-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietDonDatHang)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietDonDatHang in the database
        List<ChiTietDonDatHang> chiTietDonDatHangList = chiTietDonDatHangRepository.findAll();
        assertThat(chiTietDonDatHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiTietDonDatHang() throws Exception {
        // Initialize the database
        chiTietDonDatHangRepository.saveAndFlush(chiTietDonDatHang);

        int databaseSizeBeforeDelete = chiTietDonDatHangRepository.findAll().size();

        // Get the chiTietDonDatHang
        restChiTietDonDatHangMockMvc.perform(delete("/api/chi-tiet-don-dat-hangs/{id}", chiTietDonDatHang.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChiTietDonDatHang> chiTietDonDatHangList = chiTietDonDatHangRepository.findAll();
        assertThat(chiTietDonDatHangList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietDonDatHang.class);
        ChiTietDonDatHang chiTietDonDatHang1 = new ChiTietDonDatHang();
        chiTietDonDatHang1.setId(1L);
        ChiTietDonDatHang chiTietDonDatHang2 = new ChiTietDonDatHang();
        chiTietDonDatHang2.setId(chiTietDonDatHang1.getId());
        assertThat(chiTietDonDatHang1).isEqualTo(chiTietDonDatHang2);
        chiTietDonDatHang2.setId(2L);
        assertThat(chiTietDonDatHang1).isNotEqualTo(chiTietDonDatHang2);
        chiTietDonDatHang1.setId(null);
        assertThat(chiTietDonDatHang1).isNotEqualTo(chiTietDonDatHang2);
    }
}
