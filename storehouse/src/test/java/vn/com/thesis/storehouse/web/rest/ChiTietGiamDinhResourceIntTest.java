package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.ChiTietGiamDinh;
import vn.com.thesis.storehouse.repository.ChiTietGiamDinhRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChiTietGiamDinhResource REST controller.
 *
 * @see ChiTietGiamDinhResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class ChiTietGiamDinhResourceIntTest {

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;

    private static final Integer DEFAULT_DAT_YEU_CAU = 1;
    private static final Integer UPDATED_DAT_YEU_CAU = 2;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private ChiTietGiamDinhRepository chiTietGiamDinhRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChiTietGiamDinhMockMvc;

    private ChiTietGiamDinh chiTietGiamDinh;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChiTietGiamDinhResource chiTietGiamDinhResource = new ChiTietGiamDinhResource(chiTietGiamDinhRepository);
        this.restChiTietGiamDinhMockMvc = MockMvcBuilders.standaloneSetup(chiTietGiamDinhResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietGiamDinh createEntity(EntityManager em) {
        ChiTietGiamDinh chiTietGiamDinh = new ChiTietGiamDinh()
            .soLuong(DEFAULT_SO_LUONG)
            .datYeuCau(DEFAULT_DAT_YEU_CAU)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return chiTietGiamDinh;
    }

    @Before
    public void initTest() {
        chiTietGiamDinh = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiTietGiamDinh() throws Exception {
        int databaseSizeBeforeCreate = chiTietGiamDinhRepository.findAll().size();

        // Create the ChiTietGiamDinh
        restChiTietGiamDinhMockMvc.perform(post("/api/chi-tiet-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietGiamDinh)))
            .andExpect(status().isCreated());

        // Validate the ChiTietGiamDinh in the database
        List<ChiTietGiamDinh> chiTietGiamDinhList = chiTietGiamDinhRepository.findAll();
        assertThat(chiTietGiamDinhList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietGiamDinh testChiTietGiamDinh = chiTietGiamDinhList.get(chiTietGiamDinhList.size() - 1);
        assertThat(testChiTietGiamDinh.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiTietGiamDinh.getDatYeuCau()).isEqualTo(DEFAULT_DAT_YEU_CAU);
        assertThat(testChiTietGiamDinh.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testChiTietGiamDinh.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testChiTietGiamDinh.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testChiTietGiamDinh.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createChiTietGiamDinhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiTietGiamDinhRepository.findAll().size();

        // Create the ChiTietGiamDinh with an existing ID
        chiTietGiamDinh.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietGiamDinhMockMvc.perform(post("/api/chi-tiet-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietGiamDinh)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietGiamDinh in the database
        List<ChiTietGiamDinh> chiTietGiamDinhList = chiTietGiamDinhRepository.findAll();
        assertThat(chiTietGiamDinhList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChiTietGiamDinhs() throws Exception {
        // Initialize the database
        chiTietGiamDinhRepository.saveAndFlush(chiTietGiamDinh);

        // Get all the chiTietGiamDinhList
        restChiTietGiamDinhMockMvc.perform(get("/api/chi-tiet-giam-dinhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietGiamDinh.getId().intValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].datYeuCau").value(hasItem(DEFAULT_DAT_YEU_CAU)))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getChiTietGiamDinh() throws Exception {
        // Initialize the database
        chiTietGiamDinhRepository.saveAndFlush(chiTietGiamDinh);

        // Get the chiTietGiamDinh
        restChiTietGiamDinhMockMvc.perform(get("/api/chi-tiet-giam-dinhs/{id}", chiTietGiamDinh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietGiamDinh.getId().intValue()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.datYeuCau").value(DEFAULT_DAT_YEU_CAU))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingChiTietGiamDinh() throws Exception {
        // Get the chiTietGiamDinh
        restChiTietGiamDinhMockMvc.perform(get("/api/chi-tiet-giam-dinhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiTietGiamDinh() throws Exception {
        // Initialize the database
        chiTietGiamDinhRepository.saveAndFlush(chiTietGiamDinh);

        int databaseSizeBeforeUpdate = chiTietGiamDinhRepository.findAll().size();

        // Update the chiTietGiamDinh
        ChiTietGiamDinh updatedChiTietGiamDinh = chiTietGiamDinhRepository.findById(chiTietGiamDinh.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietGiamDinh are not directly saved in db
        em.detach(updatedChiTietGiamDinh);
        updatedChiTietGiamDinh
            .soLuong(UPDATED_SO_LUONG)
            .datYeuCau(UPDATED_DAT_YEU_CAU)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restChiTietGiamDinhMockMvc.perform(put("/api/chi-tiet-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChiTietGiamDinh)))
            .andExpect(status().isOk());

        // Validate the ChiTietGiamDinh in the database
        List<ChiTietGiamDinh> chiTietGiamDinhList = chiTietGiamDinhRepository.findAll();
        assertThat(chiTietGiamDinhList).hasSize(databaseSizeBeforeUpdate);
        ChiTietGiamDinh testChiTietGiamDinh = chiTietGiamDinhList.get(chiTietGiamDinhList.size() - 1);
        assertThat(testChiTietGiamDinh.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietGiamDinh.getDatYeuCau()).isEqualTo(UPDATED_DAT_YEU_CAU);
        assertThat(testChiTietGiamDinh.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testChiTietGiamDinh.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testChiTietGiamDinh.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testChiTietGiamDinh.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingChiTietGiamDinh() throws Exception {
        int databaseSizeBeforeUpdate = chiTietGiamDinhRepository.findAll().size();

        // Create the ChiTietGiamDinh

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restChiTietGiamDinhMockMvc.perform(put("/api/chi-tiet-giam-dinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietGiamDinh)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietGiamDinh in the database
        List<ChiTietGiamDinh> chiTietGiamDinhList = chiTietGiamDinhRepository.findAll();
        assertThat(chiTietGiamDinhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiTietGiamDinh() throws Exception {
        // Initialize the database
        chiTietGiamDinhRepository.saveAndFlush(chiTietGiamDinh);

        int databaseSizeBeforeDelete = chiTietGiamDinhRepository.findAll().size();

        // Get the chiTietGiamDinh
        restChiTietGiamDinhMockMvc.perform(delete("/api/chi-tiet-giam-dinhs/{id}", chiTietGiamDinh.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChiTietGiamDinh> chiTietGiamDinhList = chiTietGiamDinhRepository.findAll();
        assertThat(chiTietGiamDinhList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietGiamDinh.class);
        ChiTietGiamDinh chiTietGiamDinh1 = new ChiTietGiamDinh();
        chiTietGiamDinh1.setId(1L);
        ChiTietGiamDinh chiTietGiamDinh2 = new ChiTietGiamDinh();
        chiTietGiamDinh2.setId(chiTietGiamDinh1.getId());
        assertThat(chiTietGiamDinh1).isEqualTo(chiTietGiamDinh2);
        chiTietGiamDinh2.setId(2L);
        assertThat(chiTietGiamDinh1).isNotEqualTo(chiTietGiamDinh2);
        chiTietGiamDinh1.setId(null);
        assertThat(chiTietGiamDinh1).isNotEqualTo(chiTietGiamDinh2);
    }
}
