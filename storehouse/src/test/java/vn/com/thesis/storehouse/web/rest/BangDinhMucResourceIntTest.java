package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.BangDinhMuc;
import vn.com.thesis.storehouse.repository.BangDinhMucRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BangDinhMucResource REST controller.
 *
 * @see BangDinhMucResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class BangDinhMucResourceIntTest {

    private static final Long DEFAULT_PHIEU_MUA_HANG_ID = 1L;
    private static final Long UPDATED_PHIEU_MUA_HANG_ID = 2L;

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final Boolean DEFAULT_TRANG_THAI = false;
    private static final Boolean UPDATED_TRANG_THAI = true;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private BangDinhMucRepository bangDinhMucRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBangDinhMucMockMvc;

    private BangDinhMuc bangDinhMuc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BangDinhMucResource bangDinhMucResource = new BangDinhMucResource(bangDinhMucRepository);
        this.restBangDinhMucMockMvc = MockMvcBuilders.standaloneSetup(bangDinhMucResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BangDinhMuc createEntity(EntityManager em) {
        BangDinhMuc bangDinhMuc = new BangDinhMuc()
            .phieuMuaHangId(DEFAULT_PHIEU_MUA_HANG_ID)
            .ma(DEFAULT_MA)
            .trangThai(DEFAULT_TRANG_THAI)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return bangDinhMuc;
    }

    @Before
    public void initTest() {
        bangDinhMuc = createEntity(em);
    }

    @Test
    @Transactional
    public void createBangDinhMuc() throws Exception {
        int databaseSizeBeforeCreate = bangDinhMucRepository.findAll().size();

        // Create the BangDinhMuc
        restBangDinhMucMockMvc.perform(post("/api/bang-dinh-mucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bangDinhMuc)))
            .andExpect(status().isCreated());

        // Validate the BangDinhMuc in the database
        List<BangDinhMuc> bangDinhMucList = bangDinhMucRepository.findAll();
        assertThat(bangDinhMucList).hasSize(databaseSizeBeforeCreate + 1);
        BangDinhMuc testBangDinhMuc = bangDinhMucList.get(bangDinhMucList.size() - 1);
        assertThat(testBangDinhMuc.getPhieuMuaHangId()).isEqualTo(DEFAULT_PHIEU_MUA_HANG_ID);
        assertThat(testBangDinhMuc.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testBangDinhMuc.isTrangThai()).isEqualTo(DEFAULT_TRANG_THAI);
        assertThat(testBangDinhMuc.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testBangDinhMuc.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testBangDinhMuc.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testBangDinhMuc.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createBangDinhMucWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bangDinhMucRepository.findAll().size();

        // Create the BangDinhMuc with an existing ID
        bangDinhMuc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBangDinhMucMockMvc.perform(post("/api/bang-dinh-mucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bangDinhMuc)))
            .andExpect(status().isBadRequest());

        // Validate the BangDinhMuc in the database
        List<BangDinhMuc> bangDinhMucList = bangDinhMucRepository.findAll();
        assertThat(bangDinhMucList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBangDinhMucs() throws Exception {
        // Initialize the database
        bangDinhMucRepository.saveAndFlush(bangDinhMuc);

        // Get all the bangDinhMucList
        restBangDinhMucMockMvc.perform(get("/api/bang-dinh-mucs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bangDinhMuc.getId().intValue())))
            .andExpect(jsonPath("$.[*].phieuMuaHangId").value(hasItem(DEFAULT_PHIEU_MUA_HANG_ID.intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA.toString())))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI.booleanValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getBangDinhMuc() throws Exception {
        // Initialize the database
        bangDinhMucRepository.saveAndFlush(bangDinhMuc);

        // Get the bangDinhMuc
        restBangDinhMucMockMvc.perform(get("/api/bang-dinh-mucs/{id}", bangDinhMuc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bangDinhMuc.getId().intValue()))
            .andExpect(jsonPath("$.phieuMuaHangId").value(DEFAULT_PHIEU_MUA_HANG_ID.intValue()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA.toString()))
            .andExpect(jsonPath("$.trangThai").value(DEFAULT_TRANG_THAI.booleanValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingBangDinhMuc() throws Exception {
        // Get the bangDinhMuc
        restBangDinhMucMockMvc.perform(get("/api/bang-dinh-mucs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBangDinhMuc() throws Exception {
        // Initialize the database
        bangDinhMucRepository.saveAndFlush(bangDinhMuc);

        int databaseSizeBeforeUpdate = bangDinhMucRepository.findAll().size();

        // Update the bangDinhMuc
        BangDinhMuc updatedBangDinhMuc = bangDinhMucRepository.findById(bangDinhMuc.getId()).get();
        // Disconnect from session so that the updates on updatedBangDinhMuc are not directly saved in db
        em.detach(updatedBangDinhMuc);
        updatedBangDinhMuc
            .phieuMuaHangId(UPDATED_PHIEU_MUA_HANG_ID)
            .ma(UPDATED_MA)
            .trangThai(UPDATED_TRANG_THAI)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restBangDinhMucMockMvc.perform(put("/api/bang-dinh-mucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBangDinhMuc)))
            .andExpect(status().isOk());

        // Validate the BangDinhMuc in the database
        List<BangDinhMuc> bangDinhMucList = bangDinhMucRepository.findAll();
        assertThat(bangDinhMucList).hasSize(databaseSizeBeforeUpdate);
        BangDinhMuc testBangDinhMuc = bangDinhMucList.get(bangDinhMucList.size() - 1);
        assertThat(testBangDinhMuc.getPhieuMuaHangId()).isEqualTo(UPDATED_PHIEU_MUA_HANG_ID);
        assertThat(testBangDinhMuc.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testBangDinhMuc.isTrangThai()).isEqualTo(UPDATED_TRANG_THAI);
        assertThat(testBangDinhMuc.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testBangDinhMuc.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testBangDinhMuc.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testBangDinhMuc.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingBangDinhMuc() throws Exception {
        int databaseSizeBeforeUpdate = bangDinhMucRepository.findAll().size();

        // Create the BangDinhMuc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restBangDinhMucMockMvc.perform(put("/api/bang-dinh-mucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bangDinhMuc)))
            .andExpect(status().isBadRequest());

        // Validate the BangDinhMuc in the database
        List<BangDinhMuc> bangDinhMucList = bangDinhMucRepository.findAll();
        assertThat(bangDinhMucList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBangDinhMuc() throws Exception {
        // Initialize the database
        bangDinhMucRepository.saveAndFlush(bangDinhMuc);

        int databaseSizeBeforeDelete = bangDinhMucRepository.findAll().size();

        // Get the bangDinhMuc
        restBangDinhMucMockMvc.perform(delete("/api/bang-dinh-mucs/{id}", bangDinhMuc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BangDinhMuc> bangDinhMucList = bangDinhMucRepository.findAll();
        assertThat(bangDinhMucList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BangDinhMuc.class);
        BangDinhMuc bangDinhMuc1 = new BangDinhMuc();
        bangDinhMuc1.setId(1L);
        BangDinhMuc bangDinhMuc2 = new BangDinhMuc();
        bangDinhMuc2.setId(bangDinhMuc1.getId());
        assertThat(bangDinhMuc1).isEqualTo(bangDinhMuc2);
        bangDinhMuc2.setId(2L);
        assertThat(bangDinhMuc1).isNotEqualTo(bangDinhMuc2);
        bangDinhMuc1.setId(null);
        assertThat(bangDinhMuc1).isNotEqualTo(bangDinhMuc2);
    }
}
