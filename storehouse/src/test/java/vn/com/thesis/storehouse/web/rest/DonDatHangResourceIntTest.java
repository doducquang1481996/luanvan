package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.DonDatHang;
import vn.com.thesis.storehouse.repository.DonDatHangRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DonDatHangResource REST controller.
 *
 * @see DonDatHangResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class DonDatHangResourceIntTest {

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final String DEFAULT_TENKHACH_HANG = "AAAAAAAAAA";
    private static final String UPDATED_TENKHACH_HANG = "BBBBBBBBBB";

    private static final Long DEFAULT_KHACH_HANG_ID = 1L;
    private static final Long UPDATED_KHACH_HANG_ID = 2L;

    private static final String DEFAULT_NGAY_DAT = "AAAAAAAAAA";
    private static final String UPDATED_NGAY_DAT = "BBBBBBBBBB";

    private static final String DEFAULT_NGAY_GIA_HANG = "AAAAAAAAAA";
    private static final String UPDATED_NGAY_GIA_HANG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DUYET = false;
    private static final Boolean UPDATED_DUYET = true;

    private static final Boolean DEFAULT_TRANG_THAI = false;
    private static final Boolean UPDATED_TRANG_THAI = true;

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private DonDatHangRepository donDatHangRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDonDatHangMockMvc;

    private DonDatHang donDatHang;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DonDatHangResource donDatHangResource = new DonDatHangResource(donDatHangRepository);
        this.restDonDatHangMockMvc = MockMvcBuilders.standaloneSetup(donDatHangResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DonDatHang createEntity(EntityManager em) {
        DonDatHang donDatHang = new DonDatHang()
            .ma(DEFAULT_MA)
            .tenkhachHang(DEFAULT_TENKHACH_HANG)
            .khachHangId(DEFAULT_KHACH_HANG_ID)
            .ngayDat(DEFAULT_NGAY_DAT)
            .ngayGiaHang(DEFAULT_NGAY_GIA_HANG)
            .duyet(DEFAULT_DUYET)
            .trangThai(DEFAULT_TRANG_THAI)
            .ghiChu(DEFAULT_GHI_CHU)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return donDatHang;
    }

    @Before
    public void initTest() {
        donDatHang = createEntity(em);
    }

    @Test
    @Transactional
    public void createDonDatHang() throws Exception {
        int databaseSizeBeforeCreate = donDatHangRepository.findAll().size();

        // Create the DonDatHang
        restDonDatHangMockMvc.perform(post("/api/don-dat-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donDatHang)))
            .andExpect(status().isCreated());

        // Validate the DonDatHang in the database
        List<DonDatHang> donDatHangList = donDatHangRepository.findAll();
        assertThat(donDatHangList).hasSize(databaseSizeBeforeCreate + 1);
        DonDatHang testDonDatHang = donDatHangList.get(donDatHangList.size() - 1);
        assertThat(testDonDatHang.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testDonDatHang.getTenkhachHang()).isEqualTo(DEFAULT_TENKHACH_HANG);
        assertThat(testDonDatHang.getKhachHangId()).isEqualTo(DEFAULT_KHACH_HANG_ID);
        assertThat(testDonDatHang.getNgayDat()).isEqualTo(DEFAULT_NGAY_DAT);
        assertThat(testDonDatHang.getNgayGiaHang()).isEqualTo(DEFAULT_NGAY_GIA_HANG);
        assertThat(testDonDatHang.isDuyet()).isEqualTo(DEFAULT_DUYET);
        assertThat(testDonDatHang.isTrangThai()).isEqualTo(DEFAULT_TRANG_THAI);
        assertThat(testDonDatHang.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testDonDatHang.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testDonDatHang.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testDonDatHang.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testDonDatHang.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createDonDatHangWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = donDatHangRepository.findAll().size();

        // Create the DonDatHang with an existing ID
        donDatHang.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDonDatHangMockMvc.perform(post("/api/don-dat-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donDatHang)))
            .andExpect(status().isBadRequest());

        // Validate the DonDatHang in the database
        List<DonDatHang> donDatHangList = donDatHangRepository.findAll();
        assertThat(donDatHangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDonDatHangs() throws Exception {
        // Initialize the database
        donDatHangRepository.saveAndFlush(donDatHang);

        // Get all the donDatHangList
        restDonDatHangMockMvc.perform(get("/api/don-dat-hangs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(donDatHang.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA.toString())))
            .andExpect(jsonPath("$.[*].tenkhachHang").value(hasItem(DEFAULT_TENKHACH_HANG.toString())))
            .andExpect(jsonPath("$.[*].khachHangId").value(hasItem(DEFAULT_KHACH_HANG_ID.intValue())))
            .andExpect(jsonPath("$.[*].ngayDat").value(hasItem(DEFAULT_NGAY_DAT.toString())))
            .andExpect(jsonPath("$.[*].ngayGiaHang").value(hasItem(DEFAULT_NGAY_GIA_HANG.toString())))
            .andExpect(jsonPath("$.[*].duyet").value(hasItem(DEFAULT_DUYET.booleanValue())))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI.booleanValue())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU.toString())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getDonDatHang() throws Exception {
        // Initialize the database
        donDatHangRepository.saveAndFlush(donDatHang);

        // Get the donDatHang
        restDonDatHangMockMvc.perform(get("/api/don-dat-hangs/{id}", donDatHang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(donDatHang.getId().intValue()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA.toString()))
            .andExpect(jsonPath("$.tenkhachHang").value(DEFAULT_TENKHACH_HANG.toString()))
            .andExpect(jsonPath("$.khachHangId").value(DEFAULT_KHACH_HANG_ID.intValue()))
            .andExpect(jsonPath("$.ngayDat").value(DEFAULT_NGAY_DAT.toString()))
            .andExpect(jsonPath("$.ngayGiaHang").value(DEFAULT_NGAY_GIA_HANG.toString()))
            .andExpect(jsonPath("$.duyet").value(DEFAULT_DUYET.booleanValue()))
            .andExpect(jsonPath("$.trangThai").value(DEFAULT_TRANG_THAI.booleanValue()))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU.toString()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingDonDatHang() throws Exception {
        // Get the donDatHang
        restDonDatHangMockMvc.perform(get("/api/don-dat-hangs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDonDatHang() throws Exception {
        // Initialize the database
        donDatHangRepository.saveAndFlush(donDatHang);

        int databaseSizeBeforeUpdate = donDatHangRepository.findAll().size();

        // Update the donDatHang
        DonDatHang updatedDonDatHang = donDatHangRepository.findById(donDatHang.getId()).get();
        // Disconnect from session so that the updates on updatedDonDatHang are not directly saved in db
        em.detach(updatedDonDatHang);
        updatedDonDatHang
            .ma(UPDATED_MA)
            .tenkhachHang(UPDATED_TENKHACH_HANG)
            .khachHangId(UPDATED_KHACH_HANG_ID)
            .ngayDat(UPDATED_NGAY_DAT)
            .ngayGiaHang(UPDATED_NGAY_GIA_HANG)
            .duyet(UPDATED_DUYET)
            .trangThai(UPDATED_TRANG_THAI)
            .ghiChu(UPDATED_GHI_CHU)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restDonDatHangMockMvc.perform(put("/api/don-dat-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDonDatHang)))
            .andExpect(status().isOk());

        // Validate the DonDatHang in the database
        List<DonDatHang> donDatHangList = donDatHangRepository.findAll();
        assertThat(donDatHangList).hasSize(databaseSizeBeforeUpdate);
        DonDatHang testDonDatHang = donDatHangList.get(donDatHangList.size() - 1);
        assertThat(testDonDatHang.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testDonDatHang.getTenkhachHang()).isEqualTo(UPDATED_TENKHACH_HANG);
        assertThat(testDonDatHang.getKhachHangId()).isEqualTo(UPDATED_KHACH_HANG_ID);
        assertThat(testDonDatHang.getNgayDat()).isEqualTo(UPDATED_NGAY_DAT);
        assertThat(testDonDatHang.getNgayGiaHang()).isEqualTo(UPDATED_NGAY_GIA_HANG);
        assertThat(testDonDatHang.isDuyet()).isEqualTo(UPDATED_DUYET);
        assertThat(testDonDatHang.isTrangThai()).isEqualTo(UPDATED_TRANG_THAI);
        assertThat(testDonDatHang.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testDonDatHang.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testDonDatHang.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testDonDatHang.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testDonDatHang.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingDonDatHang() throws Exception {
        int databaseSizeBeforeUpdate = donDatHangRepository.findAll().size();

        // Create the DonDatHang

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restDonDatHangMockMvc.perform(put("/api/don-dat-hangs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donDatHang)))
            .andExpect(status().isBadRequest());

        // Validate the DonDatHang in the database
        List<DonDatHang> donDatHangList = donDatHangRepository.findAll();
        assertThat(donDatHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDonDatHang() throws Exception {
        // Initialize the database
        donDatHangRepository.saveAndFlush(donDatHang);

        int databaseSizeBeforeDelete = donDatHangRepository.findAll().size();

        // Get the donDatHang
        restDonDatHangMockMvc.perform(delete("/api/don-dat-hangs/{id}", donDatHang.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DonDatHang> donDatHangList = donDatHangRepository.findAll();
        assertThat(donDatHangList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DonDatHang.class);
        DonDatHang donDatHang1 = new DonDatHang();
        donDatHang1.setId(1L);
        DonDatHang donDatHang2 = new DonDatHang();
        donDatHang2.setId(donDatHang1.getId());
        assertThat(donDatHang1).isEqualTo(donDatHang2);
        donDatHang2.setId(2L);
        assertThat(donDatHang1).isNotEqualTo(donDatHang2);
        donDatHang1.setId(null);
        assertThat(donDatHang1).isNotEqualTo(donDatHang2);
    }
}
