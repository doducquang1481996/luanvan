package vn.com.thesis.storehouse.web.rest;

import vn.com.thesis.storehouse.StorehouseApp;

import vn.com.thesis.storehouse.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.storehouse.domain.ChiTietBangDinhMuc;
import vn.com.thesis.storehouse.repository.ChiTietBangDinhMucRepository;
import vn.com.thesis.storehouse.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.storehouse.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChiTietBangDinhMucResource REST controller.
 *
 * @see ChiTietBangDinhMucResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, StorehouseApp.class})
public class ChiTietBangDinhMucResourceIntTest {

    private static final Long DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID = 1L;
    private static final Long UPDATED_CHI_TIET_PHIEU_MUA_HANG_ID = 2L;

    private static final Integer DEFAULT_DINH_MUC = 1;
    private static final Integer UPDATED_DINH_MUC = 2;

    private static final Long DEFAULT_VI_TRI_KHO_ID = 1L;
    private static final Long UPDATED_VI_TRI_KHO_ID = 2L;

    private static final Long DEFAULT_KHO_ID = 1L;
    private static final Long UPDATED_KHO_ID = 2L;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private ChiTietBangDinhMucRepository chiTietBangDinhMucRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChiTietBangDinhMucMockMvc;

    private ChiTietBangDinhMuc chiTietBangDinhMuc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChiTietBangDinhMucResource chiTietBangDinhMucResource = new ChiTietBangDinhMucResource(chiTietBangDinhMucRepository);
        this.restChiTietBangDinhMucMockMvc = MockMvcBuilders.standaloneSetup(chiTietBangDinhMucResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietBangDinhMuc createEntity(EntityManager em) {
        ChiTietBangDinhMuc chiTietBangDinhMuc = new ChiTietBangDinhMuc()
            .chiTietPhieuMuaHangId(DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID)
            .dinhMuc(DEFAULT_DINH_MUC)
            .viTriKhoId(DEFAULT_VI_TRI_KHO_ID)
            .khoId(DEFAULT_KHO_ID)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return chiTietBangDinhMuc;
    }

    @Before
    public void initTest() {
        chiTietBangDinhMuc = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiTietBangDinhMuc() throws Exception {
        int databaseSizeBeforeCreate = chiTietBangDinhMucRepository.findAll().size();

        // Create the ChiTietBangDinhMuc
        restChiTietBangDinhMucMockMvc.perform(post("/api/chi-tiet-bang-dinh-mucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietBangDinhMuc)))
            .andExpect(status().isCreated());

        // Validate the ChiTietBangDinhMuc in the database
        List<ChiTietBangDinhMuc> chiTietBangDinhMucList = chiTietBangDinhMucRepository.findAll();
        assertThat(chiTietBangDinhMucList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietBangDinhMuc testChiTietBangDinhMuc = chiTietBangDinhMucList.get(chiTietBangDinhMucList.size() - 1);
        assertThat(testChiTietBangDinhMuc.getChiTietPhieuMuaHangId()).isEqualTo(DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID);
        assertThat(testChiTietBangDinhMuc.getDinhMuc()).isEqualTo(DEFAULT_DINH_MUC);
        assertThat(testChiTietBangDinhMuc.getViTriKhoId()).isEqualTo(DEFAULT_VI_TRI_KHO_ID);
        assertThat(testChiTietBangDinhMuc.getKhoId()).isEqualTo(DEFAULT_KHO_ID);
        assertThat(testChiTietBangDinhMuc.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testChiTietBangDinhMuc.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testChiTietBangDinhMuc.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testChiTietBangDinhMuc.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createChiTietBangDinhMucWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiTietBangDinhMucRepository.findAll().size();

        // Create the ChiTietBangDinhMuc with an existing ID
        chiTietBangDinhMuc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietBangDinhMucMockMvc.perform(post("/api/chi-tiet-bang-dinh-mucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietBangDinhMuc)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietBangDinhMuc in the database
        List<ChiTietBangDinhMuc> chiTietBangDinhMucList = chiTietBangDinhMucRepository.findAll();
        assertThat(chiTietBangDinhMucList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChiTietBangDinhMucs() throws Exception {
        // Initialize the database
        chiTietBangDinhMucRepository.saveAndFlush(chiTietBangDinhMuc);

        // Get all the chiTietBangDinhMucList
        restChiTietBangDinhMucMockMvc.perform(get("/api/chi-tiet-bang-dinh-mucs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietBangDinhMuc.getId().intValue())))
            .andExpect(jsonPath("$.[*].chiTietPhieuMuaHangId").value(hasItem(DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID.intValue())))
            .andExpect(jsonPath("$.[*].dinhMuc").value(hasItem(DEFAULT_DINH_MUC)))
            .andExpect(jsonPath("$.[*].viTriKhoId").value(hasItem(DEFAULT_VI_TRI_KHO_ID.intValue())))
            .andExpect(jsonPath("$.[*].khoId").value(hasItem(DEFAULT_KHO_ID.intValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getChiTietBangDinhMuc() throws Exception {
        // Initialize the database
        chiTietBangDinhMucRepository.saveAndFlush(chiTietBangDinhMuc);

        // Get the chiTietBangDinhMuc
        restChiTietBangDinhMucMockMvc.perform(get("/api/chi-tiet-bang-dinh-mucs/{id}", chiTietBangDinhMuc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietBangDinhMuc.getId().intValue()))
            .andExpect(jsonPath("$.chiTietPhieuMuaHangId").value(DEFAULT_CHI_TIET_PHIEU_MUA_HANG_ID.intValue()))
            .andExpect(jsonPath("$.dinhMuc").value(DEFAULT_DINH_MUC))
            .andExpect(jsonPath("$.viTriKhoId").value(DEFAULT_VI_TRI_KHO_ID.intValue()))
            .andExpect(jsonPath("$.khoId").value(DEFAULT_KHO_ID.intValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingChiTietBangDinhMuc() throws Exception {
        // Get the chiTietBangDinhMuc
        restChiTietBangDinhMucMockMvc.perform(get("/api/chi-tiet-bang-dinh-mucs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiTietBangDinhMuc() throws Exception {
        // Initialize the database
        chiTietBangDinhMucRepository.saveAndFlush(chiTietBangDinhMuc);

        int databaseSizeBeforeUpdate = chiTietBangDinhMucRepository.findAll().size();

        // Update the chiTietBangDinhMuc
        ChiTietBangDinhMuc updatedChiTietBangDinhMuc = chiTietBangDinhMucRepository.findById(chiTietBangDinhMuc.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietBangDinhMuc are not directly saved in db
        em.detach(updatedChiTietBangDinhMuc);
        updatedChiTietBangDinhMuc
            .chiTietPhieuMuaHangId(UPDATED_CHI_TIET_PHIEU_MUA_HANG_ID)
            .dinhMuc(UPDATED_DINH_MUC)
            .viTriKhoId(UPDATED_VI_TRI_KHO_ID)
            .khoId(UPDATED_KHO_ID)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restChiTietBangDinhMucMockMvc.perform(put("/api/chi-tiet-bang-dinh-mucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChiTietBangDinhMuc)))
            .andExpect(status().isOk());

        // Validate the ChiTietBangDinhMuc in the database
        List<ChiTietBangDinhMuc> chiTietBangDinhMucList = chiTietBangDinhMucRepository.findAll();
        assertThat(chiTietBangDinhMucList).hasSize(databaseSizeBeforeUpdate);
        ChiTietBangDinhMuc testChiTietBangDinhMuc = chiTietBangDinhMucList.get(chiTietBangDinhMucList.size() - 1);
        assertThat(testChiTietBangDinhMuc.getChiTietPhieuMuaHangId()).isEqualTo(UPDATED_CHI_TIET_PHIEU_MUA_HANG_ID);
        assertThat(testChiTietBangDinhMuc.getDinhMuc()).isEqualTo(UPDATED_DINH_MUC);
        assertThat(testChiTietBangDinhMuc.getViTriKhoId()).isEqualTo(UPDATED_VI_TRI_KHO_ID);
        assertThat(testChiTietBangDinhMuc.getKhoId()).isEqualTo(UPDATED_KHO_ID);
        assertThat(testChiTietBangDinhMuc.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testChiTietBangDinhMuc.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testChiTietBangDinhMuc.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testChiTietBangDinhMuc.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingChiTietBangDinhMuc() throws Exception {
        int databaseSizeBeforeUpdate = chiTietBangDinhMucRepository.findAll().size();

        // Create the ChiTietBangDinhMuc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restChiTietBangDinhMucMockMvc.perform(put("/api/chi-tiet-bang-dinh-mucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chiTietBangDinhMuc)))
            .andExpect(status().isBadRequest());

        // Validate the ChiTietBangDinhMuc in the database
        List<ChiTietBangDinhMuc> chiTietBangDinhMucList = chiTietBangDinhMucRepository.findAll();
        assertThat(chiTietBangDinhMucList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiTietBangDinhMuc() throws Exception {
        // Initialize the database
        chiTietBangDinhMucRepository.saveAndFlush(chiTietBangDinhMuc);

        int databaseSizeBeforeDelete = chiTietBangDinhMucRepository.findAll().size();

        // Get the chiTietBangDinhMuc
        restChiTietBangDinhMucMockMvc.perform(delete("/api/chi-tiet-bang-dinh-mucs/{id}", chiTietBangDinhMuc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChiTietBangDinhMuc> chiTietBangDinhMucList = chiTietBangDinhMucRepository.findAll();
        assertThat(chiTietBangDinhMucList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietBangDinhMuc.class);
        ChiTietBangDinhMuc chiTietBangDinhMuc1 = new ChiTietBangDinhMuc();
        chiTietBangDinhMuc1.setId(1L);
        ChiTietBangDinhMuc chiTietBangDinhMuc2 = new ChiTietBangDinhMuc();
        chiTietBangDinhMuc2.setId(chiTietBangDinhMuc1.getId());
        assertThat(chiTietBangDinhMuc1).isEqualTo(chiTietBangDinhMuc2);
        chiTietBangDinhMuc2.setId(2L);
        assertThat(chiTietBangDinhMuc1).isNotEqualTo(chiTietBangDinhMuc2);
        chiTietBangDinhMuc1.setId(null);
        assertThat(chiTietBangDinhMuc1).isNotEqualTo(chiTietBangDinhMuc2);
    }
}
