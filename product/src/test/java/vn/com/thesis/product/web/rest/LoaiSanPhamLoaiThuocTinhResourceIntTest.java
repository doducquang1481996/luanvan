package vn.com.thesis.product.web.rest;

import vn.com.thesis.product.ProductApp;

import vn.com.thesis.product.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.product.domain.LoaiSanPhamLoaiThuocTinh;
import vn.com.thesis.product.repository.LoaiSanPhamLoaiThuocTinhRepository;
import vn.com.thesis.product.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.product.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LoaiSanPhamLoaiThuocTinhResource REST controller.
 *
 * @see LoaiSanPhamLoaiThuocTinhResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, ProductApp.class})
public class LoaiSanPhamLoaiThuocTinhResourceIntTest {

    private static final Long DEFAULT_LOAI_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_LOAI_SAN_PHAM_ID = 2L;

    private static final Long DEFAULT_LOAI_THUOC_TINH_ID = 1L;
    private static final Long UPDATED_LOAI_THUOC_TINH_ID = 2L;

    private static final String DEFAULT_TEN_LOAI_THUOC_TINH = "AAAAAAAAAA";
    private static final String UPDATED_TEN_LOAI_THUOC_TINH = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    @Autowired
    private LoaiSanPhamLoaiThuocTinhRepository loaiSanPhamLoaiThuocTinhRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLoaiSanPhamLoaiThuocTinhMockMvc;

    private LoaiSanPhamLoaiThuocTinh loaiSanPhamLoaiThuocTinh;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LoaiSanPhamLoaiThuocTinhResource loaiSanPhamLoaiThuocTinhResource = new LoaiSanPhamLoaiThuocTinhResource(loaiSanPhamLoaiThuocTinhRepository);
        this.restLoaiSanPhamLoaiThuocTinhMockMvc = MockMvcBuilders.standaloneSetup(loaiSanPhamLoaiThuocTinhResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiSanPhamLoaiThuocTinh createEntity(EntityManager em) {
        LoaiSanPhamLoaiThuocTinh loaiSanPhamLoaiThuocTinh = new LoaiSanPhamLoaiThuocTinh()
            .loaiSanPhamId(DEFAULT_LOAI_SAN_PHAM_ID)
            .loaiThuocTinhId(DEFAULT_LOAI_THUOC_TINH_ID)
            .tenLoaiThuocTinh(DEFAULT_TEN_LOAI_THUOC_TINH)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO);
        return loaiSanPhamLoaiThuocTinh;
    }

    @Before
    public void initTest() {
        loaiSanPhamLoaiThuocTinh = createEntity(em);
    }

    @Test
    @Transactional
    public void createLoaiSanPhamLoaiThuocTinh() throws Exception {
        int databaseSizeBeforeCreate = loaiSanPhamLoaiThuocTinhRepository.findAll().size();

        // Create the LoaiSanPhamLoaiThuocTinh
        restLoaiSanPhamLoaiThuocTinhMockMvc.perform(post("/api/loai-san-pham-loai-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(loaiSanPhamLoaiThuocTinh)))
            .andExpect(status().isCreated());

        // Validate the LoaiSanPhamLoaiThuocTinh in the database
        List<LoaiSanPhamLoaiThuocTinh> loaiSanPhamLoaiThuocTinhList = loaiSanPhamLoaiThuocTinhRepository.findAll();
        assertThat(loaiSanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeCreate + 1);
        LoaiSanPhamLoaiThuocTinh testLoaiSanPhamLoaiThuocTinh = loaiSanPhamLoaiThuocTinhList.get(loaiSanPhamLoaiThuocTinhList.size() - 1);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getLoaiSanPhamId()).isEqualTo(DEFAULT_LOAI_SAN_PHAM_ID);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getLoaiThuocTinhId()).isEqualTo(DEFAULT_LOAI_THUOC_TINH_ID);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getTenLoaiThuocTinh()).isEqualTo(DEFAULT_TEN_LOAI_THUOC_TINH);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
    }

    @Test
    @Transactional
    public void createLoaiSanPhamLoaiThuocTinhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = loaiSanPhamLoaiThuocTinhRepository.findAll().size();

        // Create the LoaiSanPhamLoaiThuocTinh with an existing ID
        loaiSanPhamLoaiThuocTinh.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoaiSanPhamLoaiThuocTinhMockMvc.perform(post("/api/loai-san-pham-loai-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(loaiSanPhamLoaiThuocTinh)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiSanPhamLoaiThuocTinh in the database
        List<LoaiSanPhamLoaiThuocTinh> loaiSanPhamLoaiThuocTinhList = loaiSanPhamLoaiThuocTinhRepository.findAll();
        assertThat(loaiSanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLoaiSanPhamLoaiThuocTinhs() throws Exception {
        // Initialize the database
        loaiSanPhamLoaiThuocTinhRepository.saveAndFlush(loaiSanPhamLoaiThuocTinh);

        // Get all the loaiSanPhamLoaiThuocTinhList
        restLoaiSanPhamLoaiThuocTinhMockMvc.perform(get("/api/loai-san-pham-loai-thuoc-tinhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiSanPhamLoaiThuocTinh.getId().intValue())))
            .andExpect(jsonPath("$.[*].loaiSanPhamId").value(hasItem(DEFAULT_LOAI_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].loaiThuocTinhId").value(hasItem(DEFAULT_LOAI_THUOC_TINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].tenLoaiThuocTinh").value(hasItem(DEFAULT_TEN_LOAI_THUOC_TINH.toString())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())));
    }
    

    @Test
    @Transactional
    public void getLoaiSanPhamLoaiThuocTinh() throws Exception {
        // Initialize the database
        loaiSanPhamLoaiThuocTinhRepository.saveAndFlush(loaiSanPhamLoaiThuocTinh);

        // Get the loaiSanPhamLoaiThuocTinh
        restLoaiSanPhamLoaiThuocTinhMockMvc.perform(get("/api/loai-san-pham-loai-thuoc-tinhs/{id}", loaiSanPhamLoaiThuocTinh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(loaiSanPhamLoaiThuocTinh.getId().intValue()))
            .andExpect(jsonPath("$.loaiSanPhamId").value(DEFAULT_LOAI_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.loaiThuocTinhId").value(DEFAULT_LOAI_THUOC_TINH_ID.intValue()))
            .andExpect(jsonPath("$.tenLoaiThuocTinh").value(DEFAULT_TEN_LOAI_THUOC_TINH.toString()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingLoaiSanPhamLoaiThuocTinh() throws Exception {
        // Get the loaiSanPhamLoaiThuocTinh
        restLoaiSanPhamLoaiThuocTinhMockMvc.perform(get("/api/loai-san-pham-loai-thuoc-tinhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLoaiSanPhamLoaiThuocTinh() throws Exception {
        // Initialize the database
        loaiSanPhamLoaiThuocTinhRepository.saveAndFlush(loaiSanPhamLoaiThuocTinh);

        int databaseSizeBeforeUpdate = loaiSanPhamLoaiThuocTinhRepository.findAll().size();

        // Update the loaiSanPhamLoaiThuocTinh
        LoaiSanPhamLoaiThuocTinh updatedLoaiSanPhamLoaiThuocTinh = loaiSanPhamLoaiThuocTinhRepository.findById(loaiSanPhamLoaiThuocTinh.getId()).get();
        // Disconnect from session so that the updates on updatedLoaiSanPhamLoaiThuocTinh are not directly saved in db
        em.detach(updatedLoaiSanPhamLoaiThuocTinh);
        updatedLoaiSanPhamLoaiThuocTinh
            .loaiSanPhamId(UPDATED_LOAI_SAN_PHAM_ID)
            .loaiThuocTinhId(UPDATED_LOAI_THUOC_TINH_ID)
            .tenLoaiThuocTinh(UPDATED_TEN_LOAI_THUOC_TINH)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO);

        restLoaiSanPhamLoaiThuocTinhMockMvc.perform(put("/api/loai-san-pham-loai-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLoaiSanPhamLoaiThuocTinh)))
            .andExpect(status().isOk());

        // Validate the LoaiSanPhamLoaiThuocTinh in the database
        List<LoaiSanPhamLoaiThuocTinh> loaiSanPhamLoaiThuocTinhList = loaiSanPhamLoaiThuocTinhRepository.findAll();
        assertThat(loaiSanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeUpdate);
        LoaiSanPhamLoaiThuocTinh testLoaiSanPhamLoaiThuocTinh = loaiSanPhamLoaiThuocTinhList.get(loaiSanPhamLoaiThuocTinhList.size() - 1);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getLoaiSanPhamId()).isEqualTo(UPDATED_LOAI_SAN_PHAM_ID);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getLoaiThuocTinhId()).isEqualTo(UPDATED_LOAI_THUOC_TINH_ID);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getTenLoaiThuocTinh()).isEqualTo(UPDATED_TEN_LOAI_THUOC_TINH);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testLoaiSanPhamLoaiThuocTinh.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
    }

    @Test
    @Transactional
    public void updateNonExistingLoaiSanPhamLoaiThuocTinh() throws Exception {
        int databaseSizeBeforeUpdate = loaiSanPhamLoaiThuocTinhRepository.findAll().size();

        // Create the LoaiSanPhamLoaiThuocTinh

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restLoaiSanPhamLoaiThuocTinhMockMvc.perform(put("/api/loai-san-pham-loai-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(loaiSanPhamLoaiThuocTinh)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiSanPhamLoaiThuocTinh in the database
        List<LoaiSanPhamLoaiThuocTinh> loaiSanPhamLoaiThuocTinhList = loaiSanPhamLoaiThuocTinhRepository.findAll();
        assertThat(loaiSanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLoaiSanPhamLoaiThuocTinh() throws Exception {
        // Initialize the database
        loaiSanPhamLoaiThuocTinhRepository.saveAndFlush(loaiSanPhamLoaiThuocTinh);

        int databaseSizeBeforeDelete = loaiSanPhamLoaiThuocTinhRepository.findAll().size();

        // Get the loaiSanPhamLoaiThuocTinh
        restLoaiSanPhamLoaiThuocTinhMockMvc.perform(delete("/api/loai-san-pham-loai-thuoc-tinhs/{id}", loaiSanPhamLoaiThuocTinh.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LoaiSanPhamLoaiThuocTinh> loaiSanPhamLoaiThuocTinhList = loaiSanPhamLoaiThuocTinhRepository.findAll();
        assertThat(loaiSanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiSanPhamLoaiThuocTinh.class);
        LoaiSanPhamLoaiThuocTinh loaiSanPhamLoaiThuocTinh1 = new LoaiSanPhamLoaiThuocTinh();
        loaiSanPhamLoaiThuocTinh1.setId(1L);
        LoaiSanPhamLoaiThuocTinh loaiSanPhamLoaiThuocTinh2 = new LoaiSanPhamLoaiThuocTinh();
        loaiSanPhamLoaiThuocTinh2.setId(loaiSanPhamLoaiThuocTinh1.getId());
        assertThat(loaiSanPhamLoaiThuocTinh1).isEqualTo(loaiSanPhamLoaiThuocTinh2);
        loaiSanPhamLoaiThuocTinh2.setId(2L);
        assertThat(loaiSanPhamLoaiThuocTinh1).isNotEqualTo(loaiSanPhamLoaiThuocTinh2);
        loaiSanPhamLoaiThuocTinh1.setId(null);
        assertThat(loaiSanPhamLoaiThuocTinh1).isNotEqualTo(loaiSanPhamLoaiThuocTinh2);
    }
}
