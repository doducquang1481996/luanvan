package vn.com.thesis.product.web.rest;

import vn.com.thesis.product.ProductApp;

import vn.com.thesis.product.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.product.domain.SanPhamLoaiThuocTinh;
import vn.com.thesis.product.repository.SanPhamLoaiThuocTinhRepository;
import vn.com.thesis.product.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.product.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SanPhamLoaiThuocTinhResource REST controller.
 *
 * @see SanPhamLoaiThuocTinhResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, ProductApp.class})
public class SanPhamLoaiThuocTinhResourceIntTest {

    private static final Long DEFAULT_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_SAN_PHAM_ID = 2L;

    private static final Long DEFAULT_LOAI_THUOC_TINH_ID = 1L;
    private static final Long UPDATED_LOAI_THUOC_TINH_ID = 2L;

    private static final String DEFAULT_TEN_LOAI_THUOC_TINH = "AAAAAAAAAA";
    private static final String UPDATED_TEN_LOAI_THUOC_TINH = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    @Autowired
    private SanPhamLoaiThuocTinhRepository sanPhamLoaiThuocTinhRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSanPhamLoaiThuocTinhMockMvc;

    private SanPhamLoaiThuocTinh sanPhamLoaiThuocTinh;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SanPhamLoaiThuocTinhResource sanPhamLoaiThuocTinhResource = new SanPhamLoaiThuocTinhResource(sanPhamLoaiThuocTinhRepository);
        this.restSanPhamLoaiThuocTinhMockMvc = MockMvcBuilders.standaloneSetup(sanPhamLoaiThuocTinhResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SanPhamLoaiThuocTinh createEntity(EntityManager em) {
        SanPhamLoaiThuocTinh sanPhamLoaiThuocTinh = new SanPhamLoaiThuocTinh()
            .sanPhamId(DEFAULT_SAN_PHAM_ID)
            .loaiThuocTinhId(DEFAULT_LOAI_THUOC_TINH_ID)
            .tenLoaiThuocTinh(DEFAULT_TEN_LOAI_THUOC_TINH)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO);
        return sanPhamLoaiThuocTinh;
    }

    @Before
    public void initTest() {
        sanPhamLoaiThuocTinh = createEntity(em);
    }

    @Test
    @Transactional
    public void createSanPhamLoaiThuocTinh() throws Exception {
        int databaseSizeBeforeCreate = sanPhamLoaiThuocTinhRepository.findAll().size();

        // Create the SanPhamLoaiThuocTinh
        restSanPhamLoaiThuocTinhMockMvc.perform(post("/api/san-pham-loai-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamLoaiThuocTinh)))
            .andExpect(status().isCreated());

        // Validate the SanPhamLoaiThuocTinh in the database
        List<SanPhamLoaiThuocTinh> sanPhamLoaiThuocTinhList = sanPhamLoaiThuocTinhRepository.findAll();
        assertThat(sanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeCreate + 1);
        SanPhamLoaiThuocTinh testSanPhamLoaiThuocTinh = sanPhamLoaiThuocTinhList.get(sanPhamLoaiThuocTinhList.size() - 1);
        assertThat(testSanPhamLoaiThuocTinh.getSanPhamId()).isEqualTo(DEFAULT_SAN_PHAM_ID);
        assertThat(testSanPhamLoaiThuocTinh.getLoaiThuocTinhId()).isEqualTo(DEFAULT_LOAI_THUOC_TINH_ID);
        assertThat(testSanPhamLoaiThuocTinh.getTenLoaiThuocTinh()).isEqualTo(DEFAULT_TEN_LOAI_THUOC_TINH);
        assertThat(testSanPhamLoaiThuocTinh.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testSanPhamLoaiThuocTinh.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
    }

    @Test
    @Transactional
    public void createSanPhamLoaiThuocTinhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sanPhamLoaiThuocTinhRepository.findAll().size();

        // Create the SanPhamLoaiThuocTinh with an existing ID
        sanPhamLoaiThuocTinh.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSanPhamLoaiThuocTinhMockMvc.perform(post("/api/san-pham-loai-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamLoaiThuocTinh)))
            .andExpect(status().isBadRequest());

        // Validate the SanPhamLoaiThuocTinh in the database
        List<SanPhamLoaiThuocTinh> sanPhamLoaiThuocTinhList = sanPhamLoaiThuocTinhRepository.findAll();
        assertThat(sanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSanPhamLoaiThuocTinhs() throws Exception {
        // Initialize the database
        sanPhamLoaiThuocTinhRepository.saveAndFlush(sanPhamLoaiThuocTinh);

        // Get all the sanPhamLoaiThuocTinhList
        restSanPhamLoaiThuocTinhMockMvc.perform(get("/api/san-pham-loai-thuoc-tinhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sanPhamLoaiThuocTinh.getId().intValue())))
            .andExpect(jsonPath("$.[*].sanPhamId").value(hasItem(DEFAULT_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].loaiThuocTinhId").value(hasItem(DEFAULT_LOAI_THUOC_TINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].tenLoaiThuocTinh").value(hasItem(DEFAULT_TEN_LOAI_THUOC_TINH.toString())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())));
    }
    

    @Test
    @Transactional
    public void getSanPhamLoaiThuocTinh() throws Exception {
        // Initialize the database
        sanPhamLoaiThuocTinhRepository.saveAndFlush(sanPhamLoaiThuocTinh);

        // Get the sanPhamLoaiThuocTinh
        restSanPhamLoaiThuocTinhMockMvc.perform(get("/api/san-pham-loai-thuoc-tinhs/{id}", sanPhamLoaiThuocTinh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sanPhamLoaiThuocTinh.getId().intValue()))
            .andExpect(jsonPath("$.sanPhamId").value(DEFAULT_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.loaiThuocTinhId").value(DEFAULT_LOAI_THUOC_TINH_ID.intValue()))
            .andExpect(jsonPath("$.tenLoaiThuocTinh").value(DEFAULT_TEN_LOAI_THUOC_TINH.toString()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSanPhamLoaiThuocTinh() throws Exception {
        // Get the sanPhamLoaiThuocTinh
        restSanPhamLoaiThuocTinhMockMvc.perform(get("/api/san-pham-loai-thuoc-tinhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSanPhamLoaiThuocTinh() throws Exception {
        // Initialize the database
        sanPhamLoaiThuocTinhRepository.saveAndFlush(sanPhamLoaiThuocTinh);

        int databaseSizeBeforeUpdate = sanPhamLoaiThuocTinhRepository.findAll().size();

        // Update the sanPhamLoaiThuocTinh
        SanPhamLoaiThuocTinh updatedSanPhamLoaiThuocTinh = sanPhamLoaiThuocTinhRepository.findById(sanPhamLoaiThuocTinh.getId()).get();
        // Disconnect from session so that the updates on updatedSanPhamLoaiThuocTinh are not directly saved in db
        em.detach(updatedSanPhamLoaiThuocTinh);
        updatedSanPhamLoaiThuocTinh
            .sanPhamId(UPDATED_SAN_PHAM_ID)
            .loaiThuocTinhId(UPDATED_LOAI_THUOC_TINH_ID)
            .tenLoaiThuocTinh(UPDATED_TEN_LOAI_THUOC_TINH)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO);

        restSanPhamLoaiThuocTinhMockMvc.perform(put("/api/san-pham-loai-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSanPhamLoaiThuocTinh)))
            .andExpect(status().isOk());

        // Validate the SanPhamLoaiThuocTinh in the database
        List<SanPhamLoaiThuocTinh> sanPhamLoaiThuocTinhList = sanPhamLoaiThuocTinhRepository.findAll();
        assertThat(sanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeUpdate);
        SanPhamLoaiThuocTinh testSanPhamLoaiThuocTinh = sanPhamLoaiThuocTinhList.get(sanPhamLoaiThuocTinhList.size() - 1);
        assertThat(testSanPhamLoaiThuocTinh.getSanPhamId()).isEqualTo(UPDATED_SAN_PHAM_ID);
        assertThat(testSanPhamLoaiThuocTinh.getLoaiThuocTinhId()).isEqualTo(UPDATED_LOAI_THUOC_TINH_ID);
        assertThat(testSanPhamLoaiThuocTinh.getTenLoaiThuocTinh()).isEqualTo(UPDATED_TEN_LOAI_THUOC_TINH);
        assertThat(testSanPhamLoaiThuocTinh.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testSanPhamLoaiThuocTinh.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
    }

    @Test
    @Transactional
    public void updateNonExistingSanPhamLoaiThuocTinh() throws Exception {
        int databaseSizeBeforeUpdate = sanPhamLoaiThuocTinhRepository.findAll().size();

        // Create the SanPhamLoaiThuocTinh

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restSanPhamLoaiThuocTinhMockMvc.perform(put("/api/san-pham-loai-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamLoaiThuocTinh)))
            .andExpect(status().isBadRequest());

        // Validate the SanPhamLoaiThuocTinh in the database
        List<SanPhamLoaiThuocTinh> sanPhamLoaiThuocTinhList = sanPhamLoaiThuocTinhRepository.findAll();
        assertThat(sanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSanPhamLoaiThuocTinh() throws Exception {
        // Initialize the database
        sanPhamLoaiThuocTinhRepository.saveAndFlush(sanPhamLoaiThuocTinh);

        int databaseSizeBeforeDelete = sanPhamLoaiThuocTinhRepository.findAll().size();

        // Get the sanPhamLoaiThuocTinh
        restSanPhamLoaiThuocTinhMockMvc.perform(delete("/api/san-pham-loai-thuoc-tinhs/{id}", sanPhamLoaiThuocTinh.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SanPhamLoaiThuocTinh> sanPhamLoaiThuocTinhList = sanPhamLoaiThuocTinhRepository.findAll();
        assertThat(sanPhamLoaiThuocTinhList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SanPhamLoaiThuocTinh.class);
        SanPhamLoaiThuocTinh sanPhamLoaiThuocTinh1 = new SanPhamLoaiThuocTinh();
        sanPhamLoaiThuocTinh1.setId(1L);
        SanPhamLoaiThuocTinh sanPhamLoaiThuocTinh2 = new SanPhamLoaiThuocTinh();
        sanPhamLoaiThuocTinh2.setId(sanPhamLoaiThuocTinh1.getId());
        assertThat(sanPhamLoaiThuocTinh1).isEqualTo(sanPhamLoaiThuocTinh2);
        sanPhamLoaiThuocTinh2.setId(2L);
        assertThat(sanPhamLoaiThuocTinh1).isNotEqualTo(sanPhamLoaiThuocTinh2);
        sanPhamLoaiThuocTinh1.setId(null);
        assertThat(sanPhamLoaiThuocTinh1).isNotEqualTo(sanPhamLoaiThuocTinh2);
    }
}
