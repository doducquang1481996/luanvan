package vn.com.thesis.product.web.rest;

import vn.com.thesis.product.ProductApp;

import vn.com.thesis.product.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.product.domain.SanPhamChiTiet;
import vn.com.thesis.product.repository.SanPhamChiTietRepository;
import vn.com.thesis.product.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.product.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SanPhamChiTietResource REST controller.
 *
 * @see SanPhamChiTietResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, ProductApp.class})
public class SanPhamChiTietResourceIntTest {

    private static final Long DEFAULT_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_SAN_PHAM_ID = 2L;

    private static final String DEFAULT_MA_SAN_PHAM_ID = "AAAAAAAAAA";
    private static final String UPDATED_MA_SAN_PHAM_ID = "BBBBBBBBBB";

    private static final String DEFAULT_TENCHI_TIET = "AAAAAAAAAA";
    private static final String UPDATED_TENCHI_TIET = "BBBBBBBBBB";

    private static final String DEFAULT_MA_SAN_PHAM_CHI_TIET = "AAAAAAAAAA";
    private static final String UPDATED_MA_SAN_PHAM_CHI_TIET = "BBBBBBBBBB";

    private static final Long DEFAULT_DON_VI_ID = 1L;
    private static final Long UPDATED_DON_VI_ID = 2L;

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    private static final String DEFAULT_NGAY_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGAY_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private SanPhamChiTietRepository sanPhamChiTietRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSanPhamChiTietMockMvc;

    private SanPhamChiTiet sanPhamChiTiet;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SanPhamChiTietResource sanPhamChiTietResource = new SanPhamChiTietResource(sanPhamChiTietRepository);
        this.restSanPhamChiTietMockMvc = MockMvcBuilders.standaloneSetup(sanPhamChiTietResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SanPhamChiTiet createEntity(EntityManager em) {
        SanPhamChiTiet sanPhamChiTiet = new SanPhamChiTiet()
            .sanPhamId(DEFAULT_SAN_PHAM_ID)
            .maSanPhamId(DEFAULT_MA_SAN_PHAM_ID)
            .tenchiTiet(DEFAULT_TENCHI_TIET)
            .maSanPhamChiTiet(DEFAULT_MA_SAN_PHAM_CHI_TIET)
            .donViId(DEFAULT_DON_VI_ID)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT);
        return sanPhamChiTiet;
    }

    @Before
    public void initTest() {
        sanPhamChiTiet = createEntity(em);
    }

    @Test
    @Transactional
    public void createSanPhamChiTiet() throws Exception {
        int databaseSizeBeforeCreate = sanPhamChiTietRepository.findAll().size();

        // Create the SanPhamChiTiet
        restSanPhamChiTietMockMvc.perform(post("/api/san-pham-chi-tiets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamChiTiet)))
            .andExpect(status().isCreated());

        // Validate the SanPhamChiTiet in the database
        List<SanPhamChiTiet> sanPhamChiTietList = sanPhamChiTietRepository.findAll();
        assertThat(sanPhamChiTietList).hasSize(databaseSizeBeforeCreate + 1);
        SanPhamChiTiet testSanPhamChiTiet = sanPhamChiTietList.get(sanPhamChiTietList.size() - 1);
        assertThat(testSanPhamChiTiet.getSanPhamId()).isEqualTo(DEFAULT_SAN_PHAM_ID);
        assertThat(testSanPhamChiTiet.getMaSanPhamId()).isEqualTo(DEFAULT_MA_SAN_PHAM_ID);
        assertThat(testSanPhamChiTiet.getTenchiTiet()).isEqualTo(DEFAULT_TENCHI_TIET);
        assertThat(testSanPhamChiTiet.getMaSanPhamChiTiet()).isEqualTo(DEFAULT_MA_SAN_PHAM_CHI_TIET);
        assertThat(testSanPhamChiTiet.getDonViId()).isEqualTo(DEFAULT_DON_VI_ID);
        assertThat(testSanPhamChiTiet.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testSanPhamChiTiet.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testSanPhamChiTiet.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
        assertThat(testSanPhamChiTiet.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createSanPhamChiTietWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sanPhamChiTietRepository.findAll().size();

        // Create the SanPhamChiTiet with an existing ID
        sanPhamChiTiet.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSanPhamChiTietMockMvc.perform(post("/api/san-pham-chi-tiets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamChiTiet)))
            .andExpect(status().isBadRequest());

        // Validate the SanPhamChiTiet in the database
        List<SanPhamChiTiet> sanPhamChiTietList = sanPhamChiTietRepository.findAll();
        assertThat(sanPhamChiTietList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSanPhamChiTiets() throws Exception {
        // Initialize the database
        sanPhamChiTietRepository.saveAndFlush(sanPhamChiTiet);

        // Get all the sanPhamChiTietList
        restSanPhamChiTietMockMvc.perform(get("/api/san-pham-chi-tiets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sanPhamChiTiet.getId().intValue())))
            .andExpect(jsonPath("$.[*].sanPhamId").value(hasItem(DEFAULT_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].maSanPhamId").value(hasItem(DEFAULT_MA_SAN_PHAM_ID.toString())))
            .andExpect(jsonPath("$.[*].tenchiTiet").value(hasItem(DEFAULT_TENCHI_TIET.toString())))
            .andExpect(jsonPath("$.[*].maSanPhamChiTiet").value(hasItem(DEFAULT_MA_SAN_PHAM_CHI_TIET.toString())))
            .andExpect(jsonPath("$.[*].donViId").value(hasItem(DEFAULT_DON_VI_ID.intValue())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getSanPhamChiTiet() throws Exception {
        // Initialize the database
        sanPhamChiTietRepository.saveAndFlush(sanPhamChiTiet);

        // Get the sanPhamChiTiet
        restSanPhamChiTietMockMvc.perform(get("/api/san-pham-chi-tiets/{id}", sanPhamChiTiet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sanPhamChiTiet.getId().intValue()))
            .andExpect(jsonPath("$.sanPhamId").value(DEFAULT_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.maSanPhamId").value(DEFAULT_MA_SAN_PHAM_ID.toString()))
            .andExpect(jsonPath("$.tenchiTiet").value(DEFAULT_TENCHI_TIET.toString()))
            .andExpect(jsonPath("$.maSanPhamChiTiet").value(DEFAULT_MA_SAN_PHAM_CHI_TIET.toString()))
            .andExpect(jsonPath("$.donViId").value(DEFAULT_DON_VI_ID.intValue()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSanPhamChiTiet() throws Exception {
        // Get the sanPhamChiTiet
        restSanPhamChiTietMockMvc.perform(get("/api/san-pham-chi-tiets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSanPhamChiTiet() throws Exception {
        // Initialize the database
        sanPhamChiTietRepository.saveAndFlush(sanPhamChiTiet);

        int databaseSizeBeforeUpdate = sanPhamChiTietRepository.findAll().size();

        // Update the sanPhamChiTiet
        SanPhamChiTiet updatedSanPhamChiTiet = sanPhamChiTietRepository.findById(sanPhamChiTiet.getId()).get();
        // Disconnect from session so that the updates on updatedSanPhamChiTiet are not directly saved in db
        em.detach(updatedSanPhamChiTiet);
        updatedSanPhamChiTiet
            .sanPhamId(UPDATED_SAN_PHAM_ID)
            .maSanPhamId(UPDATED_MA_SAN_PHAM_ID)
            .tenchiTiet(UPDATED_TENCHI_TIET)
            .maSanPhamChiTiet(UPDATED_MA_SAN_PHAM_CHI_TIET)
            .donViId(UPDATED_DON_VI_ID)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT);

        restSanPhamChiTietMockMvc.perform(put("/api/san-pham-chi-tiets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSanPhamChiTiet)))
            .andExpect(status().isOk());

        // Validate the SanPhamChiTiet in the database
        List<SanPhamChiTiet> sanPhamChiTietList = sanPhamChiTietRepository.findAll();
        assertThat(sanPhamChiTietList).hasSize(databaseSizeBeforeUpdate);
        SanPhamChiTiet testSanPhamChiTiet = sanPhamChiTietList.get(sanPhamChiTietList.size() - 1);
        assertThat(testSanPhamChiTiet.getSanPhamId()).isEqualTo(UPDATED_SAN_PHAM_ID);
        assertThat(testSanPhamChiTiet.getMaSanPhamId()).isEqualTo(UPDATED_MA_SAN_PHAM_ID);
        assertThat(testSanPhamChiTiet.getTenchiTiet()).isEqualTo(UPDATED_TENCHI_TIET);
        assertThat(testSanPhamChiTiet.getMaSanPhamChiTiet()).isEqualTo(UPDATED_MA_SAN_PHAM_CHI_TIET);
        assertThat(testSanPhamChiTiet.getDonViId()).isEqualTo(UPDATED_DON_VI_ID);
        assertThat(testSanPhamChiTiet.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testSanPhamChiTiet.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testSanPhamChiTiet.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
        assertThat(testSanPhamChiTiet.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingSanPhamChiTiet() throws Exception {
        int databaseSizeBeforeUpdate = sanPhamChiTietRepository.findAll().size();

        // Create the SanPhamChiTiet

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restSanPhamChiTietMockMvc.perform(put("/api/san-pham-chi-tiets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamChiTiet)))
            .andExpect(status().isBadRequest());

        // Validate the SanPhamChiTiet in the database
        List<SanPhamChiTiet> sanPhamChiTietList = sanPhamChiTietRepository.findAll();
        assertThat(sanPhamChiTietList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSanPhamChiTiet() throws Exception {
        // Initialize the database
        sanPhamChiTietRepository.saveAndFlush(sanPhamChiTiet);

        int databaseSizeBeforeDelete = sanPhamChiTietRepository.findAll().size();

        // Get the sanPhamChiTiet
        restSanPhamChiTietMockMvc.perform(delete("/api/san-pham-chi-tiets/{id}", sanPhamChiTiet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SanPhamChiTiet> sanPhamChiTietList = sanPhamChiTietRepository.findAll();
        assertThat(sanPhamChiTietList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SanPhamChiTiet.class);
        SanPhamChiTiet sanPhamChiTiet1 = new SanPhamChiTiet();
        sanPhamChiTiet1.setId(1L);
        SanPhamChiTiet sanPhamChiTiet2 = new SanPhamChiTiet();
        sanPhamChiTiet2.setId(sanPhamChiTiet1.getId());
        assertThat(sanPhamChiTiet1).isEqualTo(sanPhamChiTiet2);
        sanPhamChiTiet2.setId(2L);
        assertThat(sanPhamChiTiet1).isNotEqualTo(sanPhamChiTiet2);
        sanPhamChiTiet1.setId(null);
        assertThat(sanPhamChiTiet1).isNotEqualTo(sanPhamChiTiet2);
    }
}
