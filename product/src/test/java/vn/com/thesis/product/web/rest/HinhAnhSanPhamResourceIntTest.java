package vn.com.thesis.product.web.rest;

import vn.com.thesis.product.ProductApp;

import vn.com.thesis.product.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.product.domain.HinhAnhSanPham;
import vn.com.thesis.product.repository.HinhAnhSanPhamRepository;
import vn.com.thesis.product.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.product.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HinhAnhSanPhamResource REST controller.
 *
 * @see HinhAnhSanPhamResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, ProductApp.class})
public class HinhAnhSanPhamResourceIntTest {

    private static final Long DEFAULT_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_SAN_PHAM_ID = 2L;

    private static final Long DEFAULT_SAN_PHAM_CHI_TIET_ID = 1L;
    private static final Long UPDATED_SAN_PHAM_CHI_TIET_ID = 2L;

    private static final byte[] DEFAULT_HINH_ANH = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_HINH_ANH = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_HINH_ANH_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_HINH_ANH_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_TIEU_DE = "AAAAAAAAAA";
    private static final String UPDATED_TIEU_DE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    @Autowired
    private HinhAnhSanPhamRepository hinhAnhSanPhamRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restHinhAnhSanPhamMockMvc;

    private HinhAnhSanPham hinhAnhSanPham;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HinhAnhSanPhamResource hinhAnhSanPhamResource = new HinhAnhSanPhamResource(hinhAnhSanPhamRepository);
        this.restHinhAnhSanPhamMockMvc = MockMvcBuilders.standaloneSetup(hinhAnhSanPhamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HinhAnhSanPham createEntity(EntityManager em) {
        HinhAnhSanPham hinhAnhSanPham = new HinhAnhSanPham()
            .sanPhamId(DEFAULT_SAN_PHAM_ID)
            .sanPhamChiTietId(DEFAULT_SAN_PHAM_CHI_TIET_ID)
            .hinhAnh(DEFAULT_HINH_ANH)
            .hinhAnhContentType(DEFAULT_HINH_ANH_CONTENT_TYPE)
            .tieuDe(DEFAULT_TIEU_DE)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO);
        return hinhAnhSanPham;
    }

    @Before
    public void initTest() {
        hinhAnhSanPham = createEntity(em);
    }

    @Test
    @Transactional
    public void createHinhAnhSanPham() throws Exception {
        int databaseSizeBeforeCreate = hinhAnhSanPhamRepository.findAll().size();

        // Create the HinhAnhSanPham
        restHinhAnhSanPhamMockMvc.perform(post("/api/hinh-anh-san-phams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hinhAnhSanPham)))
            .andExpect(status().isCreated());

        // Validate the HinhAnhSanPham in the database
        List<HinhAnhSanPham> hinhAnhSanPhamList = hinhAnhSanPhamRepository.findAll();
        assertThat(hinhAnhSanPhamList).hasSize(databaseSizeBeforeCreate + 1);
        HinhAnhSanPham testHinhAnhSanPham = hinhAnhSanPhamList.get(hinhAnhSanPhamList.size() - 1);
        assertThat(testHinhAnhSanPham.getSanPhamId()).isEqualTo(DEFAULT_SAN_PHAM_ID);
        assertThat(testHinhAnhSanPham.getSanPhamChiTietId()).isEqualTo(DEFAULT_SAN_PHAM_CHI_TIET_ID);
        assertThat(testHinhAnhSanPham.getHinhAnh()).isEqualTo(DEFAULT_HINH_ANH);
        assertThat(testHinhAnhSanPham.getHinhAnhContentType()).isEqualTo(DEFAULT_HINH_ANH_CONTENT_TYPE);
        assertThat(testHinhAnhSanPham.getTieuDe()).isEqualTo(DEFAULT_TIEU_DE);
        assertThat(testHinhAnhSanPham.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testHinhAnhSanPham.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
    }

    @Test
    @Transactional
    public void createHinhAnhSanPhamWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = hinhAnhSanPhamRepository.findAll().size();

        // Create the HinhAnhSanPham with an existing ID
        hinhAnhSanPham.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHinhAnhSanPhamMockMvc.perform(post("/api/hinh-anh-san-phams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hinhAnhSanPham)))
            .andExpect(status().isBadRequest());

        // Validate the HinhAnhSanPham in the database
        List<HinhAnhSanPham> hinhAnhSanPhamList = hinhAnhSanPhamRepository.findAll();
        assertThat(hinhAnhSanPhamList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllHinhAnhSanPhams() throws Exception {
        // Initialize the database
        hinhAnhSanPhamRepository.saveAndFlush(hinhAnhSanPham);

        // Get all the hinhAnhSanPhamList
        restHinhAnhSanPhamMockMvc.perform(get("/api/hinh-anh-san-phams?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hinhAnhSanPham.getId().intValue())))
            .andExpect(jsonPath("$.[*].sanPhamId").value(hasItem(DEFAULT_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].sanPhamChiTietId").value(hasItem(DEFAULT_SAN_PHAM_CHI_TIET_ID.intValue())))
            .andExpect(jsonPath("$.[*].hinhAnhContentType").value(hasItem(DEFAULT_HINH_ANH_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].hinhAnh").value(hasItem(Base64Utils.encodeToString(DEFAULT_HINH_ANH))))
            .andExpect(jsonPath("$.[*].tieuDe").value(hasItem(DEFAULT_TIEU_DE.toString())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())));
    }
    

    @Test
    @Transactional
    public void getHinhAnhSanPham() throws Exception {
        // Initialize the database
        hinhAnhSanPhamRepository.saveAndFlush(hinhAnhSanPham);

        // Get the hinhAnhSanPham
        restHinhAnhSanPhamMockMvc.perform(get("/api/hinh-anh-san-phams/{id}", hinhAnhSanPham.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(hinhAnhSanPham.getId().intValue()))
            .andExpect(jsonPath("$.sanPhamId").value(DEFAULT_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.sanPhamChiTietId").value(DEFAULT_SAN_PHAM_CHI_TIET_ID.intValue()))
            .andExpect(jsonPath("$.hinhAnhContentType").value(DEFAULT_HINH_ANH_CONTENT_TYPE))
            .andExpect(jsonPath("$.hinhAnh").value(Base64Utils.encodeToString(DEFAULT_HINH_ANH)))
            .andExpect(jsonPath("$.tieuDe").value(DEFAULT_TIEU_DE.toString()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingHinhAnhSanPham() throws Exception {
        // Get the hinhAnhSanPham
        restHinhAnhSanPhamMockMvc.perform(get("/api/hinh-anh-san-phams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHinhAnhSanPham() throws Exception {
        // Initialize the database
        hinhAnhSanPhamRepository.saveAndFlush(hinhAnhSanPham);

        int databaseSizeBeforeUpdate = hinhAnhSanPhamRepository.findAll().size();

        // Update the hinhAnhSanPham
        HinhAnhSanPham updatedHinhAnhSanPham = hinhAnhSanPhamRepository.findById(hinhAnhSanPham.getId()).get();
        // Disconnect from session so that the updates on updatedHinhAnhSanPham are not directly saved in db
        em.detach(updatedHinhAnhSanPham);
        updatedHinhAnhSanPham
            .sanPhamId(UPDATED_SAN_PHAM_ID)
            .sanPhamChiTietId(UPDATED_SAN_PHAM_CHI_TIET_ID)
            .hinhAnh(UPDATED_HINH_ANH)
            .hinhAnhContentType(UPDATED_HINH_ANH_CONTENT_TYPE)
            .tieuDe(UPDATED_TIEU_DE)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO);

        restHinhAnhSanPhamMockMvc.perform(put("/api/hinh-anh-san-phams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHinhAnhSanPham)))
            .andExpect(status().isOk());

        // Validate the HinhAnhSanPham in the database
        List<HinhAnhSanPham> hinhAnhSanPhamList = hinhAnhSanPhamRepository.findAll();
        assertThat(hinhAnhSanPhamList).hasSize(databaseSizeBeforeUpdate);
        HinhAnhSanPham testHinhAnhSanPham = hinhAnhSanPhamList.get(hinhAnhSanPhamList.size() - 1);
        assertThat(testHinhAnhSanPham.getSanPhamId()).isEqualTo(UPDATED_SAN_PHAM_ID);
        assertThat(testHinhAnhSanPham.getSanPhamChiTietId()).isEqualTo(UPDATED_SAN_PHAM_CHI_TIET_ID);
        assertThat(testHinhAnhSanPham.getHinhAnh()).isEqualTo(UPDATED_HINH_ANH);
        assertThat(testHinhAnhSanPham.getHinhAnhContentType()).isEqualTo(UPDATED_HINH_ANH_CONTENT_TYPE);
        assertThat(testHinhAnhSanPham.getTieuDe()).isEqualTo(UPDATED_TIEU_DE);
        assertThat(testHinhAnhSanPham.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testHinhAnhSanPham.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
    }

    @Test
    @Transactional
    public void updateNonExistingHinhAnhSanPham() throws Exception {
        int databaseSizeBeforeUpdate = hinhAnhSanPhamRepository.findAll().size();

        // Create the HinhAnhSanPham

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restHinhAnhSanPhamMockMvc.perform(put("/api/hinh-anh-san-phams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hinhAnhSanPham)))
            .andExpect(status().isBadRequest());

        // Validate the HinhAnhSanPham in the database
        List<HinhAnhSanPham> hinhAnhSanPhamList = hinhAnhSanPhamRepository.findAll();
        assertThat(hinhAnhSanPhamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHinhAnhSanPham() throws Exception {
        // Initialize the database
        hinhAnhSanPhamRepository.saveAndFlush(hinhAnhSanPham);

        int databaseSizeBeforeDelete = hinhAnhSanPhamRepository.findAll().size();

        // Get the hinhAnhSanPham
        restHinhAnhSanPhamMockMvc.perform(delete("/api/hinh-anh-san-phams/{id}", hinhAnhSanPham.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<HinhAnhSanPham> hinhAnhSanPhamList = hinhAnhSanPhamRepository.findAll();
        assertThat(hinhAnhSanPhamList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HinhAnhSanPham.class);
        HinhAnhSanPham hinhAnhSanPham1 = new HinhAnhSanPham();
        hinhAnhSanPham1.setId(1L);
        HinhAnhSanPham hinhAnhSanPham2 = new HinhAnhSanPham();
        hinhAnhSanPham2.setId(hinhAnhSanPham1.getId());
        assertThat(hinhAnhSanPham1).isEqualTo(hinhAnhSanPham2);
        hinhAnhSanPham2.setId(2L);
        assertThat(hinhAnhSanPham1).isNotEqualTo(hinhAnhSanPham2);
        hinhAnhSanPham1.setId(null);
        assertThat(hinhAnhSanPham1).isNotEqualTo(hinhAnhSanPham2);
    }
}
