package vn.com.thesis.product.web.rest;

import vn.com.thesis.product.ProductApp;

import vn.com.thesis.product.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.product.domain.SanPhamThuocTinh;
import vn.com.thesis.product.repository.SanPhamThuocTinhRepository;
import vn.com.thesis.product.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.product.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SanPhamThuocTinhResource REST controller.
 *
 * @see SanPhamThuocTinhResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, ProductApp.class})
public class SanPhamThuocTinhResourceIntTest {

    private static final Long DEFAULT_SAN_PHAM_ID = 1L;
    private static final Long UPDATED_SAN_PHAM_ID = 2L;

    private static final Long DEFAULT_LOAI_THUOC_TINH_ID = 1L;
    private static final Long UPDATED_LOAI_THUOC_TINH_ID = 2L;

    private static final Long DEFAULT_THUOC_TINH_ID = 1L;
    private static final Long UPDATED_THUOC_TINH_ID = 2L;

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    @Autowired
    private SanPhamThuocTinhRepository sanPhamThuocTinhRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSanPhamThuocTinhMockMvc;

    private SanPhamThuocTinh sanPhamThuocTinh;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SanPhamThuocTinhResource sanPhamThuocTinhResource = new SanPhamThuocTinhResource(sanPhamThuocTinhRepository);
        this.restSanPhamThuocTinhMockMvc = MockMvcBuilders.standaloneSetup(sanPhamThuocTinhResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SanPhamThuocTinh createEntity(EntityManager em) {
        SanPhamThuocTinh sanPhamThuocTinh = new SanPhamThuocTinh()
            .sanPhamId(DEFAULT_SAN_PHAM_ID)
            .loaiThuocTinhId(DEFAULT_LOAI_THUOC_TINH_ID)
            .thuocTinhId(DEFAULT_THUOC_TINH_ID)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO);
        return sanPhamThuocTinh;
    }

    @Before
    public void initTest() {
        sanPhamThuocTinh = createEntity(em);
    }

    @Test
    @Transactional
    public void createSanPhamThuocTinh() throws Exception {
        int databaseSizeBeforeCreate = sanPhamThuocTinhRepository.findAll().size();

        // Create the SanPhamThuocTinh
        restSanPhamThuocTinhMockMvc.perform(post("/api/san-pham-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamThuocTinh)))
            .andExpect(status().isCreated());

        // Validate the SanPhamThuocTinh in the database
        List<SanPhamThuocTinh> sanPhamThuocTinhList = sanPhamThuocTinhRepository.findAll();
        assertThat(sanPhamThuocTinhList).hasSize(databaseSizeBeforeCreate + 1);
        SanPhamThuocTinh testSanPhamThuocTinh = sanPhamThuocTinhList.get(sanPhamThuocTinhList.size() - 1);
        assertThat(testSanPhamThuocTinh.getSanPhamId()).isEqualTo(DEFAULT_SAN_PHAM_ID);
        assertThat(testSanPhamThuocTinh.getLoaiThuocTinhId()).isEqualTo(DEFAULT_LOAI_THUOC_TINH_ID);
        assertThat(testSanPhamThuocTinh.getThuocTinhId()).isEqualTo(DEFAULT_THUOC_TINH_ID);
        assertThat(testSanPhamThuocTinh.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testSanPhamThuocTinh.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
    }

    @Test
    @Transactional
    public void createSanPhamThuocTinhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sanPhamThuocTinhRepository.findAll().size();

        // Create the SanPhamThuocTinh with an existing ID
        sanPhamThuocTinh.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSanPhamThuocTinhMockMvc.perform(post("/api/san-pham-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamThuocTinh)))
            .andExpect(status().isBadRequest());

        // Validate the SanPhamThuocTinh in the database
        List<SanPhamThuocTinh> sanPhamThuocTinhList = sanPhamThuocTinhRepository.findAll();
        assertThat(sanPhamThuocTinhList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSanPhamThuocTinhs() throws Exception {
        // Initialize the database
        sanPhamThuocTinhRepository.saveAndFlush(sanPhamThuocTinh);

        // Get all the sanPhamThuocTinhList
        restSanPhamThuocTinhMockMvc.perform(get("/api/san-pham-thuoc-tinhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sanPhamThuocTinh.getId().intValue())))
            .andExpect(jsonPath("$.[*].sanPhamId").value(hasItem(DEFAULT_SAN_PHAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].loaiThuocTinhId").value(hasItem(DEFAULT_LOAI_THUOC_TINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].thuocTinhId").value(hasItem(DEFAULT_THUOC_TINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())));
    }
    

    @Test
    @Transactional
    public void getSanPhamThuocTinh() throws Exception {
        // Initialize the database
        sanPhamThuocTinhRepository.saveAndFlush(sanPhamThuocTinh);

        // Get the sanPhamThuocTinh
        restSanPhamThuocTinhMockMvc.perform(get("/api/san-pham-thuoc-tinhs/{id}", sanPhamThuocTinh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sanPhamThuocTinh.getId().intValue()))
            .andExpect(jsonPath("$.sanPhamId").value(DEFAULT_SAN_PHAM_ID.intValue()))
            .andExpect(jsonPath("$.loaiThuocTinhId").value(DEFAULT_LOAI_THUOC_TINH_ID.intValue()))
            .andExpect(jsonPath("$.thuocTinhId").value(DEFAULT_THUOC_TINH_ID.intValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSanPhamThuocTinh() throws Exception {
        // Get the sanPhamThuocTinh
        restSanPhamThuocTinhMockMvc.perform(get("/api/san-pham-thuoc-tinhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSanPhamThuocTinh() throws Exception {
        // Initialize the database
        sanPhamThuocTinhRepository.saveAndFlush(sanPhamThuocTinh);

        int databaseSizeBeforeUpdate = sanPhamThuocTinhRepository.findAll().size();

        // Update the sanPhamThuocTinh
        SanPhamThuocTinh updatedSanPhamThuocTinh = sanPhamThuocTinhRepository.findById(sanPhamThuocTinh.getId()).get();
        // Disconnect from session so that the updates on updatedSanPhamThuocTinh are not directly saved in db
        em.detach(updatedSanPhamThuocTinh);
        updatedSanPhamThuocTinh
            .sanPhamId(UPDATED_SAN_PHAM_ID)
            .loaiThuocTinhId(UPDATED_LOAI_THUOC_TINH_ID)
            .thuocTinhId(UPDATED_THUOC_TINH_ID)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO);

        restSanPhamThuocTinhMockMvc.perform(put("/api/san-pham-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSanPhamThuocTinh)))
            .andExpect(status().isOk());

        // Validate the SanPhamThuocTinh in the database
        List<SanPhamThuocTinh> sanPhamThuocTinhList = sanPhamThuocTinhRepository.findAll();
        assertThat(sanPhamThuocTinhList).hasSize(databaseSizeBeforeUpdate);
        SanPhamThuocTinh testSanPhamThuocTinh = sanPhamThuocTinhList.get(sanPhamThuocTinhList.size() - 1);
        assertThat(testSanPhamThuocTinh.getSanPhamId()).isEqualTo(UPDATED_SAN_PHAM_ID);
        assertThat(testSanPhamThuocTinh.getLoaiThuocTinhId()).isEqualTo(UPDATED_LOAI_THUOC_TINH_ID);
        assertThat(testSanPhamThuocTinh.getThuocTinhId()).isEqualTo(UPDATED_THUOC_TINH_ID);
        assertThat(testSanPhamThuocTinh.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testSanPhamThuocTinh.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
    }

    @Test
    @Transactional
    public void updateNonExistingSanPhamThuocTinh() throws Exception {
        int databaseSizeBeforeUpdate = sanPhamThuocTinhRepository.findAll().size();

        // Create the SanPhamThuocTinh

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restSanPhamThuocTinhMockMvc.perform(put("/api/san-pham-thuoc-tinhs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sanPhamThuocTinh)))
            .andExpect(status().isBadRequest());

        // Validate the SanPhamThuocTinh in the database
        List<SanPhamThuocTinh> sanPhamThuocTinhList = sanPhamThuocTinhRepository.findAll();
        assertThat(sanPhamThuocTinhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSanPhamThuocTinh() throws Exception {
        // Initialize the database
        sanPhamThuocTinhRepository.saveAndFlush(sanPhamThuocTinh);

        int databaseSizeBeforeDelete = sanPhamThuocTinhRepository.findAll().size();

        // Get the sanPhamThuocTinh
        restSanPhamThuocTinhMockMvc.perform(delete("/api/san-pham-thuoc-tinhs/{id}", sanPhamThuocTinh.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SanPhamThuocTinh> sanPhamThuocTinhList = sanPhamThuocTinhRepository.findAll();
        assertThat(sanPhamThuocTinhList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SanPhamThuocTinh.class);
        SanPhamThuocTinh sanPhamThuocTinh1 = new SanPhamThuocTinh();
        sanPhamThuocTinh1.setId(1L);
        SanPhamThuocTinh sanPhamThuocTinh2 = new SanPhamThuocTinh();
        sanPhamThuocTinh2.setId(sanPhamThuocTinh1.getId());
        assertThat(sanPhamThuocTinh1).isEqualTo(sanPhamThuocTinh2);
        sanPhamThuocTinh2.setId(2L);
        assertThat(sanPhamThuocTinh1).isNotEqualTo(sanPhamThuocTinh2);
        sanPhamThuocTinh1.setId(null);
        assertThat(sanPhamThuocTinh1).isNotEqualTo(sanPhamThuocTinh2);
    }
}
