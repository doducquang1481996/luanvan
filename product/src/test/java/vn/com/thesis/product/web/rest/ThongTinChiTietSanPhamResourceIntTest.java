package vn.com.thesis.product.web.rest;

import vn.com.thesis.product.ProductApp;

import vn.com.thesis.product.config.SecurityBeanOverrideConfiguration;

import vn.com.thesis.product.domain.ThongTinChiTietSanPham;
import vn.com.thesis.product.repository.ThongTinChiTietSanPhamRepository;
import vn.com.thesis.product.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static vn.com.thesis.product.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ThongTinChiTietSanPhamResource REST controller.
 *
 * @see ThongTinChiTietSanPhamResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, ProductApp.class})
public class ThongTinChiTietSanPhamResourceIntTest {

    private static final Long DEFAULT_LOAI_THUOC_TINH_ID = 1L;
    private static final Long UPDATED_LOAI_THUOC_TINH_ID = 2L;

    private static final Long DEFAULT_SAN_PHAM_THUOC_TINH_ID = 1L;
    private static final Long UPDATED_SAN_PHAM_THUOC_TINH_ID = 2L;

    private static final String DEFAULT_GIA_TRI = "AAAAAAAAAA";
    private static final String UPDATED_GIA_TRI = "BBBBBBBBBB";

    private static final String DEFAULT_DON_VI = "AAAAAAAAAA";
    private static final String UPDATED_DON_VI = "BBBBBBBBBB";

    private static final String DEFAULT_MA_DON_VI = "AAAAAAAAAA";
    private static final String UPDATED_MA_DON_VI = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_TAO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_TAO = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_TAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_NHAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_NHAT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NGUOI_CAP_NHAT = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CAP_NHAT = "BBBBBBBBBB";

    @Autowired
    private ThongTinChiTietSanPhamRepository thongTinChiTietSanPhamRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restThongTinChiTietSanPhamMockMvc;

    private ThongTinChiTietSanPham thongTinChiTietSanPham;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ThongTinChiTietSanPhamResource thongTinChiTietSanPhamResource = new ThongTinChiTietSanPhamResource(thongTinChiTietSanPhamRepository);
        this.restThongTinChiTietSanPhamMockMvc = MockMvcBuilders.standaloneSetup(thongTinChiTietSanPhamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThongTinChiTietSanPham createEntity(EntityManager em) {
        ThongTinChiTietSanPham thongTinChiTietSanPham = new ThongTinChiTietSanPham()
            .loaiThuocTinhId(DEFAULT_LOAI_THUOC_TINH_ID)
            .sanPhamThuocTinhId(DEFAULT_SAN_PHAM_THUOC_TINH_ID)
            .giaTri(DEFAULT_GIA_TRI)
            .donVi(DEFAULT_DON_VI)
            .maDonVi(DEFAULT_MA_DON_VI)
            .moTa(DEFAULT_MO_TA)
            .ngayTao(DEFAULT_NGAY_TAO)
            .nguoiTao(DEFAULT_NGUOI_TAO)
            .ngayCapNhat(DEFAULT_NGAY_CAP_NHAT)
            .nguoiCapNhat(DEFAULT_NGUOI_CAP_NHAT);
        return thongTinChiTietSanPham;
    }

    @Before
    public void initTest() {
        thongTinChiTietSanPham = createEntity(em);
    }

    @Test
    @Transactional
    public void createThongTinChiTietSanPham() throws Exception {
        int databaseSizeBeforeCreate = thongTinChiTietSanPhamRepository.findAll().size();

        // Create the ThongTinChiTietSanPham
        restThongTinChiTietSanPhamMockMvc.perform(post("/api/thong-tin-chi-tiet-san-phams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(thongTinChiTietSanPham)))
            .andExpect(status().isCreated());

        // Validate the ThongTinChiTietSanPham in the database
        List<ThongTinChiTietSanPham> thongTinChiTietSanPhamList = thongTinChiTietSanPhamRepository.findAll();
        assertThat(thongTinChiTietSanPhamList).hasSize(databaseSizeBeforeCreate + 1);
        ThongTinChiTietSanPham testThongTinChiTietSanPham = thongTinChiTietSanPhamList.get(thongTinChiTietSanPhamList.size() - 1);
        assertThat(testThongTinChiTietSanPham.getLoaiThuocTinhId()).isEqualTo(DEFAULT_LOAI_THUOC_TINH_ID);
        assertThat(testThongTinChiTietSanPham.getSanPhamThuocTinhId()).isEqualTo(DEFAULT_SAN_PHAM_THUOC_TINH_ID);
        assertThat(testThongTinChiTietSanPham.getGiaTri()).isEqualTo(DEFAULT_GIA_TRI);
        assertThat(testThongTinChiTietSanPham.getDonVi()).isEqualTo(DEFAULT_DON_VI);
        assertThat(testThongTinChiTietSanPham.getMaDonVi()).isEqualTo(DEFAULT_MA_DON_VI);
        assertThat(testThongTinChiTietSanPham.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testThongTinChiTietSanPham.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testThongTinChiTietSanPham.getNguoiTao()).isEqualTo(DEFAULT_NGUOI_TAO);
        assertThat(testThongTinChiTietSanPham.getNgayCapNhat()).isEqualTo(DEFAULT_NGAY_CAP_NHAT);
        assertThat(testThongTinChiTietSanPham.getNguoiCapNhat()).isEqualTo(DEFAULT_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void createThongTinChiTietSanPhamWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = thongTinChiTietSanPhamRepository.findAll().size();

        // Create the ThongTinChiTietSanPham with an existing ID
        thongTinChiTietSanPham.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restThongTinChiTietSanPhamMockMvc.perform(post("/api/thong-tin-chi-tiet-san-phams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(thongTinChiTietSanPham)))
            .andExpect(status().isBadRequest());

        // Validate the ThongTinChiTietSanPham in the database
        List<ThongTinChiTietSanPham> thongTinChiTietSanPhamList = thongTinChiTietSanPhamRepository.findAll();
        assertThat(thongTinChiTietSanPhamList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllThongTinChiTietSanPhams() throws Exception {
        // Initialize the database
        thongTinChiTietSanPhamRepository.saveAndFlush(thongTinChiTietSanPham);

        // Get all the thongTinChiTietSanPhamList
        restThongTinChiTietSanPhamMockMvc.perform(get("/api/thong-tin-chi-tiet-san-phams?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thongTinChiTietSanPham.getId().intValue())))
            .andExpect(jsonPath("$.[*].loaiThuocTinhId").value(hasItem(DEFAULT_LOAI_THUOC_TINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].sanPhamThuocTinhId").value(hasItem(DEFAULT_SAN_PHAM_THUOC_TINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].giaTri").value(hasItem(DEFAULT_GIA_TRI.toString())))
            .andExpect(jsonPath("$.[*].donVi").value(hasItem(DEFAULT_DON_VI.toString())))
            .andExpect(jsonPath("$.[*].maDonVi").value(hasItem(DEFAULT_MA_DON_VI.toString())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA.toString())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].nguoiTao").value(hasItem(DEFAULT_NGUOI_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayCapNhat").value(hasItem(DEFAULT_NGAY_CAP_NHAT.toString())))
            .andExpect(jsonPath("$.[*].nguoiCapNhat").value(hasItem(DEFAULT_NGUOI_CAP_NHAT.toString())));
    }
    

    @Test
    @Transactional
    public void getThongTinChiTietSanPham() throws Exception {
        // Initialize the database
        thongTinChiTietSanPhamRepository.saveAndFlush(thongTinChiTietSanPham);

        // Get the thongTinChiTietSanPham
        restThongTinChiTietSanPhamMockMvc.perform(get("/api/thong-tin-chi-tiet-san-phams/{id}", thongTinChiTietSanPham.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(thongTinChiTietSanPham.getId().intValue()))
            .andExpect(jsonPath("$.loaiThuocTinhId").value(DEFAULT_LOAI_THUOC_TINH_ID.intValue()))
            .andExpect(jsonPath("$.sanPhamThuocTinhId").value(DEFAULT_SAN_PHAM_THUOC_TINH_ID.intValue()))
            .andExpect(jsonPath("$.giaTri").value(DEFAULT_GIA_TRI.toString()))
            .andExpect(jsonPath("$.donVi").value(DEFAULT_DON_VI.toString()))
            .andExpect(jsonPath("$.maDonVi").value(DEFAULT_MA_DON_VI.toString()))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA.toString()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.nguoiTao").value(DEFAULT_NGUOI_TAO.toString()))
            .andExpect(jsonPath("$.ngayCapNhat").value(DEFAULT_NGAY_CAP_NHAT.toString()))
            .andExpect(jsonPath("$.nguoiCapNhat").value(DEFAULT_NGUOI_CAP_NHAT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingThongTinChiTietSanPham() throws Exception {
        // Get the thongTinChiTietSanPham
        restThongTinChiTietSanPhamMockMvc.perform(get("/api/thong-tin-chi-tiet-san-phams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateThongTinChiTietSanPham() throws Exception {
        // Initialize the database
        thongTinChiTietSanPhamRepository.saveAndFlush(thongTinChiTietSanPham);

        int databaseSizeBeforeUpdate = thongTinChiTietSanPhamRepository.findAll().size();

        // Update the thongTinChiTietSanPham
        ThongTinChiTietSanPham updatedThongTinChiTietSanPham = thongTinChiTietSanPhamRepository.findById(thongTinChiTietSanPham.getId()).get();
        // Disconnect from session so that the updates on updatedThongTinChiTietSanPham are not directly saved in db
        em.detach(updatedThongTinChiTietSanPham);
        updatedThongTinChiTietSanPham
            .loaiThuocTinhId(UPDATED_LOAI_THUOC_TINH_ID)
            .sanPhamThuocTinhId(UPDATED_SAN_PHAM_THUOC_TINH_ID)
            .giaTri(UPDATED_GIA_TRI)
            .donVi(UPDATED_DON_VI)
            .maDonVi(UPDATED_MA_DON_VI)
            .moTa(UPDATED_MO_TA)
            .ngayTao(UPDATED_NGAY_TAO)
            .nguoiTao(UPDATED_NGUOI_TAO)
            .ngayCapNhat(UPDATED_NGAY_CAP_NHAT)
            .nguoiCapNhat(UPDATED_NGUOI_CAP_NHAT);

        restThongTinChiTietSanPhamMockMvc.perform(put("/api/thong-tin-chi-tiet-san-phams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedThongTinChiTietSanPham)))
            .andExpect(status().isOk());

        // Validate the ThongTinChiTietSanPham in the database
        List<ThongTinChiTietSanPham> thongTinChiTietSanPhamList = thongTinChiTietSanPhamRepository.findAll();
        assertThat(thongTinChiTietSanPhamList).hasSize(databaseSizeBeforeUpdate);
        ThongTinChiTietSanPham testThongTinChiTietSanPham = thongTinChiTietSanPhamList.get(thongTinChiTietSanPhamList.size() - 1);
        assertThat(testThongTinChiTietSanPham.getLoaiThuocTinhId()).isEqualTo(UPDATED_LOAI_THUOC_TINH_ID);
        assertThat(testThongTinChiTietSanPham.getSanPhamThuocTinhId()).isEqualTo(UPDATED_SAN_PHAM_THUOC_TINH_ID);
        assertThat(testThongTinChiTietSanPham.getGiaTri()).isEqualTo(UPDATED_GIA_TRI);
        assertThat(testThongTinChiTietSanPham.getDonVi()).isEqualTo(UPDATED_DON_VI);
        assertThat(testThongTinChiTietSanPham.getMaDonVi()).isEqualTo(UPDATED_MA_DON_VI);
        assertThat(testThongTinChiTietSanPham.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testThongTinChiTietSanPham.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testThongTinChiTietSanPham.getNguoiTao()).isEqualTo(UPDATED_NGUOI_TAO);
        assertThat(testThongTinChiTietSanPham.getNgayCapNhat()).isEqualTo(UPDATED_NGAY_CAP_NHAT);
        assertThat(testThongTinChiTietSanPham.getNguoiCapNhat()).isEqualTo(UPDATED_NGUOI_CAP_NHAT);
    }

    @Test
    @Transactional
    public void updateNonExistingThongTinChiTietSanPham() throws Exception {
        int databaseSizeBeforeUpdate = thongTinChiTietSanPhamRepository.findAll().size();

        // Create the ThongTinChiTietSanPham

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restThongTinChiTietSanPhamMockMvc.perform(put("/api/thong-tin-chi-tiet-san-phams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(thongTinChiTietSanPham)))
            .andExpect(status().isBadRequest());

        // Validate the ThongTinChiTietSanPham in the database
        List<ThongTinChiTietSanPham> thongTinChiTietSanPhamList = thongTinChiTietSanPhamRepository.findAll();
        assertThat(thongTinChiTietSanPhamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteThongTinChiTietSanPham() throws Exception {
        // Initialize the database
        thongTinChiTietSanPhamRepository.saveAndFlush(thongTinChiTietSanPham);

        int databaseSizeBeforeDelete = thongTinChiTietSanPhamRepository.findAll().size();

        // Get the thongTinChiTietSanPham
        restThongTinChiTietSanPhamMockMvc.perform(delete("/api/thong-tin-chi-tiet-san-phams/{id}", thongTinChiTietSanPham.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ThongTinChiTietSanPham> thongTinChiTietSanPhamList = thongTinChiTietSanPhamRepository.findAll();
        assertThat(thongTinChiTietSanPhamList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThongTinChiTietSanPham.class);
        ThongTinChiTietSanPham thongTinChiTietSanPham1 = new ThongTinChiTietSanPham();
        thongTinChiTietSanPham1.setId(1L);
        ThongTinChiTietSanPham thongTinChiTietSanPham2 = new ThongTinChiTietSanPham();
        thongTinChiTietSanPham2.setId(thongTinChiTietSanPham1.getId());
        assertThat(thongTinChiTietSanPham1).isEqualTo(thongTinChiTietSanPham2);
        thongTinChiTietSanPham2.setId(2L);
        assertThat(thongTinChiTietSanPham1).isNotEqualTo(thongTinChiTietSanPham2);
        thongTinChiTietSanPham1.setId(null);
        assertThat(thongTinChiTietSanPham1).isNotEqualTo(thongTinChiTietSanPham2);
    }
}
