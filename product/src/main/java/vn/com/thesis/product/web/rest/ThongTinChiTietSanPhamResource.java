package vn.com.thesis.product.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.product.domain.ThongTinChiTietSanPham;
import vn.com.thesis.product.repository.ThongTinChiTietSanPhamRepository;
import vn.com.thesis.product.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.product.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ThongTinChiTietSanPham.
 */
@RestController
@RequestMapping("/api")
public class ThongTinChiTietSanPhamResource {

    private final Logger log = LoggerFactory.getLogger(ThongTinChiTietSanPhamResource.class);

    private static final String ENTITY_NAME = "thongTinChiTietSanPham";

    private final ThongTinChiTietSanPhamRepository thongTinChiTietSanPhamRepository;

    public ThongTinChiTietSanPhamResource(ThongTinChiTietSanPhamRepository thongTinChiTietSanPhamRepository) {
        this.thongTinChiTietSanPhamRepository = thongTinChiTietSanPhamRepository;
    }

    /**
     * POST  /thong-tin-chi-tiet-san-phams : Create a new thongTinChiTietSanPham.
     *
     * @param thongTinChiTietSanPham the thongTinChiTietSanPham to create
     * @return the ResponseEntity with status 201 (Created) and with body the new thongTinChiTietSanPham, or with status 400 (Bad Request) if the thongTinChiTietSanPham has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/thong-tin-chi-tiet-san-phams")
    @Timed
    public ResponseEntity<ThongTinChiTietSanPham> createThongTinChiTietSanPham(@RequestBody ThongTinChiTietSanPham thongTinChiTietSanPham) throws URISyntaxException {
        log.debug("REST request to save ThongTinChiTietSanPham : {}", thongTinChiTietSanPham);
        if (thongTinChiTietSanPham.getId() != null) {
            throw new BadRequestAlertException("A new thongTinChiTietSanPham cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ThongTinChiTietSanPham result = thongTinChiTietSanPhamRepository.save(thongTinChiTietSanPham);
        return ResponseEntity.created(new URI("/api/thong-tin-chi-tiet-san-phams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /thong-tin-chi-tiet-san-phams : Updates an existing thongTinChiTietSanPham.
     *
     * @param thongTinChiTietSanPham the thongTinChiTietSanPham to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated thongTinChiTietSanPham,
     * or with status 400 (Bad Request) if the thongTinChiTietSanPham is not valid,
     * or with status 500 (Internal Server Error) if the thongTinChiTietSanPham couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/thong-tin-chi-tiet-san-phams")
    @Timed
    public ResponseEntity<ThongTinChiTietSanPham> updateThongTinChiTietSanPham(@RequestBody ThongTinChiTietSanPham thongTinChiTietSanPham) throws URISyntaxException {
        log.debug("REST request to update ThongTinChiTietSanPham : {}", thongTinChiTietSanPham);
        if (thongTinChiTietSanPham.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ThongTinChiTietSanPham result = thongTinChiTietSanPhamRepository.save(thongTinChiTietSanPham);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, thongTinChiTietSanPham.getId().toString()))
            .body(result);
    }

    /**
     * GET  /thong-tin-chi-tiet-san-phams : get all the thongTinChiTietSanPhams.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of thongTinChiTietSanPhams in body
     */
    @GetMapping("/thong-tin-chi-tiet-san-phams")
    @Timed
    public List<ThongTinChiTietSanPham> getAllThongTinChiTietSanPhams() {
        log.debug("REST request to get all ThongTinChiTietSanPhams");
        return thongTinChiTietSanPhamRepository.findAll();
    }

    /**
     * GET  /thong-tin-chi-tiet-san-phams/:id : get the "id" thongTinChiTietSanPham.
     *
     * @param id the id of the thongTinChiTietSanPham to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the thongTinChiTietSanPham, or with status 404 (Not Found)
     */
    @GetMapping("/thong-tin-chi-tiet-san-phams/{id}")
    @Timed
    public ResponseEntity<ThongTinChiTietSanPham> getThongTinChiTietSanPham(@PathVariable Long id) {
        log.debug("REST request to get ThongTinChiTietSanPham : {}", id);
        Optional<ThongTinChiTietSanPham> thongTinChiTietSanPham = thongTinChiTietSanPhamRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(thongTinChiTietSanPham);
    }

    /**
     * DELETE  /thong-tin-chi-tiet-san-phams/:id : delete the "id" thongTinChiTietSanPham.
     *
     * @param id the id of the thongTinChiTietSanPham to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/thong-tin-chi-tiet-san-phams/{id}")
    @Timed
    public ResponseEntity<Void> deleteThongTinChiTietSanPham(@PathVariable Long id) {
        log.debug("REST request to delete ThongTinChiTietSanPham : {}", id);

        thongTinChiTietSanPhamRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
