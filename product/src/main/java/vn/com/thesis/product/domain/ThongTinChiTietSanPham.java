package vn.com.thesis.product.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ThongTinChiTietSanPham.
 */
@Entity
@Table(name = "thong_tin_chi_tiet_san_pham")
public class ThongTinChiTietSanPham implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "loai_thuoc_tinh_id")
    private Long loaiThuocTinhId;

    @Column(name = "san_pham_thuoc_tinh_id")
    private Long sanPhamThuocTinhId;

    @Column(name = "gia_tri")
    private String giaTri;

    @Column(name = "don_vi")
    private String donVi;

    @Column(name = "ma_don_vi")
    private String maDonVi;

    @Column(name = "mo_ta")
    private String moTa;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    @ManyToOne
    @JsonIgnoreProperties("")
    private SanPhamChiTiet sanPhamChiTiet;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLoaiThuocTinhId() {
        return loaiThuocTinhId;
    }

    public ThongTinChiTietSanPham loaiThuocTinhId(Long loaiThuocTinhId) {
        this.loaiThuocTinhId = loaiThuocTinhId;
        return this;
    }

    public void setLoaiThuocTinhId(Long loaiThuocTinhId) {
        this.loaiThuocTinhId = loaiThuocTinhId;
    }

    public Long getSanPhamThuocTinhId() {
        return sanPhamThuocTinhId;
    }

    public ThongTinChiTietSanPham sanPhamThuocTinhId(Long sanPhamThuocTinhId) {
        this.sanPhamThuocTinhId = sanPhamThuocTinhId;
        return this;
    }

    public void setSanPhamThuocTinhId(Long sanPhamThuocTinhId) {
        this.sanPhamThuocTinhId = sanPhamThuocTinhId;
    }

    public String getGiaTri() {
        return giaTri;
    }

    public ThongTinChiTietSanPham giaTri(String giaTri) {
        this.giaTri = giaTri;
        return this;
    }

    public void setGiaTri(String giaTri) {
        this.giaTri = giaTri;
    }

    public String getDonVi() {
        return donVi;
    }

    public ThongTinChiTietSanPham donVi(String donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(String donVi) {
        this.donVi = donVi;
    }

    public String getMaDonVi() {
        return maDonVi;
    }

    public ThongTinChiTietSanPham maDonVi(String maDonVi) {
        this.maDonVi = maDonVi;
        return this;
    }

    public void setMaDonVi(String maDonVi) {
        this.maDonVi = maDonVi;
    }

    public String getMoTa() {
        return moTa;
    }

    public ThongTinChiTietSanPham moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public ThongTinChiTietSanPham ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public ThongTinChiTietSanPham nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public ThongTinChiTietSanPham ngayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public ThongTinChiTietSanPham nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public SanPhamChiTiet getSanPhamChiTiet() {
        return sanPhamChiTiet;
    }

    public ThongTinChiTietSanPham sanPhamChiTiet(SanPhamChiTiet sanPhamChiTiet) {
        this.sanPhamChiTiet = sanPhamChiTiet;
        return this;
    }

    public void setSanPhamChiTiet(SanPhamChiTiet sanPhamChiTiet) {
        this.sanPhamChiTiet = sanPhamChiTiet;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ThongTinChiTietSanPham thongTinChiTietSanPham = (ThongTinChiTietSanPham) o;
        if (thongTinChiTietSanPham.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), thongTinChiTietSanPham.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ThongTinChiTietSanPham{" +
            "id=" + getId() +
            ", loaiThuocTinhId=" + getLoaiThuocTinhId() +
            ", sanPhamThuocTinhId=" + getSanPhamThuocTinhId() +
            ", giaTri='" + getGiaTri() + "'" +
            ", donVi='" + getDonVi() + "'" +
            ", maDonVi='" + getMaDonVi() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            "}";
    }
}
