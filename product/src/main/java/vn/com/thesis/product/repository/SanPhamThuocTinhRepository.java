package vn.com.thesis.product.repository;

import vn.com.thesis.product.domain.SanPhamThuocTinh;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SanPhamThuocTinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SanPhamThuocTinhRepository extends JpaRepository<SanPhamThuocTinh, Long> {

}
