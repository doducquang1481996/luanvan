package vn.com.thesis.product.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A HinhAnhSanPham.
 */
@Entity
@Table(name = "hinh_anh_san_pham")
public class HinhAnhSanPham implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "san_pham_id")
    private Long sanPhamId;

    @Column(name = "san_pham_chi_tiet_id")
    private Long sanPhamChiTietId;

    @Lob
    @Column(name = "hinh_anh")
    private byte[] hinhAnh;

    @Column(name = "hinh_anh_content_type")
    private String hinhAnhContentType;

    @Column(name = "tieu_de")
    private String tieuDe;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSanPhamId() {
        return sanPhamId;
    }

    public HinhAnhSanPham sanPhamId(Long sanPhamId) {
        this.sanPhamId = sanPhamId;
        return this;
    }

    public void setSanPhamId(Long sanPhamId) {
        this.sanPhamId = sanPhamId;
    }

    public Long getSanPhamChiTietId() {
        return sanPhamChiTietId;
    }

    public HinhAnhSanPham sanPhamChiTietId(Long sanPhamChiTietId) {
        this.sanPhamChiTietId = sanPhamChiTietId;
        return this;
    }

    public void setSanPhamChiTietId(Long sanPhamChiTietId) {
        this.sanPhamChiTietId = sanPhamChiTietId;
    }

    public byte[] getHinhAnh() {
        return hinhAnh;
    }

    public HinhAnhSanPham hinhAnh(byte[] hinhAnh) {
        this.hinhAnh = hinhAnh;
        return this;
    }

    public void setHinhAnh(byte[] hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    public String getHinhAnhContentType() {
        return hinhAnhContentType;
    }

    public HinhAnhSanPham hinhAnhContentType(String hinhAnhContentType) {
        this.hinhAnhContentType = hinhAnhContentType;
        return this;
    }

    public void setHinhAnhContentType(String hinhAnhContentType) {
        this.hinhAnhContentType = hinhAnhContentType;
    }

    public String getTieuDe() {
        return tieuDe;
    }

    public HinhAnhSanPham tieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
        return this;
    }

    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public HinhAnhSanPham ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public HinhAnhSanPham nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HinhAnhSanPham hinhAnhSanPham = (HinhAnhSanPham) o;
        if (hinhAnhSanPham.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hinhAnhSanPham.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HinhAnhSanPham{" +
            "id=" + getId() +
            ", sanPhamId=" + getSanPhamId() +
            ", sanPhamChiTietId=" + getSanPhamChiTietId() +
            ", hinhAnh='" + getHinhAnh() + "'" +
            ", hinhAnhContentType='" + getHinhAnhContentType() + "'" +
            ", tieuDe='" + getTieuDe() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            "}";
    }
}
