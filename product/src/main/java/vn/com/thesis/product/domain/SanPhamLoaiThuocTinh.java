package vn.com.thesis.product.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A SanPhamLoaiThuocTinh.
 */
@Entity
@Table(name = "san_pham_loai_thuoc_tinh")
public class SanPhamLoaiThuocTinh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "san_pham_id")
    private Long sanPhamId;

    @Column(name = "loai_thuoc_tinh_id")
    private Long loaiThuocTinhId;

    @Column(name = "ten_loai_thuoc_tinh")
    private String tenLoaiThuocTinh;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSanPhamId() {
        return sanPhamId;
    }

    public SanPhamLoaiThuocTinh sanPhamId(Long sanPhamId) {
        this.sanPhamId = sanPhamId;
        return this;
    }

    public void setSanPhamId(Long sanPhamId) {
        this.sanPhamId = sanPhamId;
    }

    public Long getLoaiThuocTinhId() {
        return loaiThuocTinhId;
    }

    public SanPhamLoaiThuocTinh loaiThuocTinhId(Long loaiThuocTinhId) {
        this.loaiThuocTinhId = loaiThuocTinhId;
        return this;
    }

    public void setLoaiThuocTinhId(Long loaiThuocTinhId) {
        this.loaiThuocTinhId = loaiThuocTinhId;
    }

    public String getTenLoaiThuocTinh() {
        return tenLoaiThuocTinh;
    }

    public SanPhamLoaiThuocTinh tenLoaiThuocTinh(String tenLoaiThuocTinh) {
        this.tenLoaiThuocTinh = tenLoaiThuocTinh;
        return this;
    }

    public void setTenLoaiThuocTinh(String tenLoaiThuocTinh) {
        this.tenLoaiThuocTinh = tenLoaiThuocTinh;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public SanPhamLoaiThuocTinh ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public SanPhamLoaiThuocTinh nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SanPhamLoaiThuocTinh sanPhamLoaiThuocTinh = (SanPhamLoaiThuocTinh) o;
        if (sanPhamLoaiThuocTinh.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sanPhamLoaiThuocTinh.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SanPhamLoaiThuocTinh{" +
            "id=" + getId() +
            ", sanPhamId=" + getSanPhamId() +
            ", loaiThuocTinhId=" + getLoaiThuocTinhId() +
            ", tenLoaiThuocTinh='" + getTenLoaiThuocTinh() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiTao='" + getNguoiTao() + "'" +
            "}";
    }
}
