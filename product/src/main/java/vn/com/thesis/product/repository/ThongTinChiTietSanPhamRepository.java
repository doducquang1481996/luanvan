package vn.com.thesis.product.repository;

import vn.com.thesis.product.domain.ThongTinChiTietSanPham;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ThongTinChiTietSanPham entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThongTinChiTietSanPhamRepository extends JpaRepository<ThongTinChiTietSanPham, Long> {

}
