package vn.com.thesis.product.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A SanPhamChiTiet.
 */
@Entity
@Table(name = "san_pham_chi_tiet")
public class SanPhamChiTiet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "san_pham_id")
    private Long sanPhamId;

    @Column(name = "ma_san_pham_id")
    private String maSanPhamId;

    @Column(name = "tenchi_tiet")
    private String tenchiTiet;

    @Column(name = "ma_san_pham_chi_tiet")
    private String maSanPhamChiTiet;

    @Column(name = "don_vi_id")
    private Long donViId;

    @Column(name = "nguoi_tao")
    private String nguoiTao;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "nguoi_cap_nhat")
    private String nguoiCapNhat;

    @Column(name = "ngay_cap_nhat")
    private String ngayCapNhat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSanPhamId() {
        return sanPhamId;
    }

    public SanPhamChiTiet sanPhamId(Long sanPhamId) {
        this.sanPhamId = sanPhamId;
        return this;
    }

    public void setSanPhamId(Long sanPhamId) {
        this.sanPhamId = sanPhamId;
    }

    public String getMaSanPhamId() {
        return maSanPhamId;
    }

    public SanPhamChiTiet maSanPhamId(String maSanPhamId) {
        this.maSanPhamId = maSanPhamId;
        return this;
    }

    public void setMaSanPhamId(String maSanPhamId) {
        this.maSanPhamId = maSanPhamId;
    }

    public String getTenchiTiet() {
        return tenchiTiet;
    }

    public SanPhamChiTiet tenchiTiet(String tenchiTiet) {
        this.tenchiTiet = tenchiTiet;
        return this;
    }

    public void setTenchiTiet(String tenchiTiet) {
        this.tenchiTiet = tenchiTiet;
    }

    public String getMaSanPhamChiTiet() {
        return maSanPhamChiTiet;
    }

    public SanPhamChiTiet maSanPhamChiTiet(String maSanPhamChiTiet) {
        this.maSanPhamChiTiet = maSanPhamChiTiet;
        return this;
    }

    public void setMaSanPhamChiTiet(String maSanPhamChiTiet) {
        this.maSanPhamChiTiet = maSanPhamChiTiet;
    }

    public Long getDonViId() {
        return donViId;
    }

    public SanPhamChiTiet donViId(Long donViId) {
        this.donViId = donViId;
        return this;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public SanPhamChiTiet nguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
        return this;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public SanPhamChiTiet ngayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public SanPhamChiTiet nguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
        return this;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }

    public String getNgayCapNhat() {
        return ngayCapNhat;
    }

    public SanPhamChiTiet ngayCapNhat(String ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
        return this;
    }

    public void setNgayCapNhat(String ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SanPhamChiTiet sanPhamChiTiet = (SanPhamChiTiet) o;
        if (sanPhamChiTiet.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sanPhamChiTiet.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SanPhamChiTiet{" +
            "id=" + getId() +
            ", sanPhamId=" + getSanPhamId() +
            ", maSanPhamId='" + getMaSanPhamId() + "'" +
            ", tenchiTiet='" + getTenchiTiet() + "'" +
            ", maSanPhamChiTiet='" + getMaSanPhamChiTiet() + "'" +
            ", donViId=" + getDonViId() +
            ", nguoiTao='" + getNguoiTao() + "'" +
            ", ngayTao='" + getNgayTao() + "'" +
            ", nguoiCapNhat='" + getNguoiCapNhat() + "'" +
            ", ngayCapNhat='" + getNgayCapNhat() + "'" +
            "}";
    }
}
