package vn.com.thesis.product.repository;

import vn.com.thesis.product.domain.SanPhamChiTiet;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SanPhamChiTiet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SanPhamChiTietRepository extends JpaRepository<SanPhamChiTiet, Long> {

}
