package vn.com.thesis.product.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.product.domain.SanPhamChiTiet;
import vn.com.thesis.product.repository.SanPhamChiTietRepository;
import vn.com.thesis.product.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.product.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SanPhamChiTiet.
 */
@RestController
@RequestMapping("/api")
public class SanPhamChiTietResource {

    private final Logger log = LoggerFactory.getLogger(SanPhamChiTietResource.class);

    private static final String ENTITY_NAME = "sanPhamChiTiet";

    private final SanPhamChiTietRepository sanPhamChiTietRepository;

    public SanPhamChiTietResource(SanPhamChiTietRepository sanPhamChiTietRepository) {
        this.sanPhamChiTietRepository = sanPhamChiTietRepository;
    }

    /**
     * POST  /san-pham-chi-tiets : Create a new sanPhamChiTiet.
     *
     * @param sanPhamChiTiet the sanPhamChiTiet to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sanPhamChiTiet, or with status 400 (Bad Request) if the sanPhamChiTiet has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/san-pham-chi-tiets")
    @Timed
    public ResponseEntity<SanPhamChiTiet> createSanPhamChiTiet(@RequestBody SanPhamChiTiet sanPhamChiTiet) throws URISyntaxException {
        log.debug("REST request to save SanPhamChiTiet : {}", sanPhamChiTiet);
        if (sanPhamChiTiet.getId() != null) {
            throw new BadRequestAlertException("A new sanPhamChiTiet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SanPhamChiTiet result = sanPhamChiTietRepository.save(sanPhamChiTiet);
        return ResponseEntity.created(new URI("/api/san-pham-chi-tiets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /san-pham-chi-tiets : Updates an existing sanPhamChiTiet.
     *
     * @param sanPhamChiTiet the sanPhamChiTiet to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sanPhamChiTiet,
     * or with status 400 (Bad Request) if the sanPhamChiTiet is not valid,
     * or with status 500 (Internal Server Error) if the sanPhamChiTiet couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/san-pham-chi-tiets")
    @Timed
    public ResponseEntity<SanPhamChiTiet> updateSanPhamChiTiet(@RequestBody SanPhamChiTiet sanPhamChiTiet) throws URISyntaxException {
        log.debug("REST request to update SanPhamChiTiet : {}", sanPhamChiTiet);
        if (sanPhamChiTiet.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SanPhamChiTiet result = sanPhamChiTietRepository.save(sanPhamChiTiet);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sanPhamChiTiet.getId().toString()))
            .body(result);
    }

    /**
     * GET  /san-pham-chi-tiets : get all the sanPhamChiTiets.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sanPhamChiTiets in body
     */
    @GetMapping("/san-pham-chi-tiets")
    @Timed
    public List<SanPhamChiTiet> getAllSanPhamChiTiets() {
        log.debug("REST request to get all SanPhamChiTiets");
        return sanPhamChiTietRepository.findAll();
    }

    /**
     * GET  /san-pham-chi-tiets/:id : get the "id" sanPhamChiTiet.
     *
     * @param id the id of the sanPhamChiTiet to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sanPhamChiTiet, or with status 404 (Not Found)
     */
    @GetMapping("/san-pham-chi-tiets/{id}")
    @Timed
    public ResponseEntity<SanPhamChiTiet> getSanPhamChiTiet(@PathVariable Long id) {
        log.debug("REST request to get SanPhamChiTiet : {}", id);
        Optional<SanPhamChiTiet> sanPhamChiTiet = sanPhamChiTietRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(sanPhamChiTiet);
    }

    /**
     * DELETE  /san-pham-chi-tiets/:id : delete the "id" sanPhamChiTiet.
     *
     * @param id the id of the sanPhamChiTiet to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/san-pham-chi-tiets/{id}")
    @Timed
    public ResponseEntity<Void> deleteSanPhamChiTiet(@PathVariable Long id) {
        log.debug("REST request to delete SanPhamChiTiet : {}", id);

        sanPhamChiTietRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
