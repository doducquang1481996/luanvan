package vn.com.thesis.product.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.product.domain.LoaiSanPhamLoaiThuocTinh;
import vn.com.thesis.product.repository.LoaiSanPhamLoaiThuocTinhRepository;
import vn.com.thesis.product.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.product.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LoaiSanPhamLoaiThuocTinh.
 */
@RestController
@RequestMapping("/api")
public class LoaiSanPhamLoaiThuocTinhResource {

    private final Logger log = LoggerFactory.getLogger(LoaiSanPhamLoaiThuocTinhResource.class);

    private static final String ENTITY_NAME = "loaiSanPhamLoaiThuocTinh";

    private final LoaiSanPhamLoaiThuocTinhRepository loaiSanPhamLoaiThuocTinhRepository;

    public LoaiSanPhamLoaiThuocTinhResource(LoaiSanPhamLoaiThuocTinhRepository loaiSanPhamLoaiThuocTinhRepository) {
        this.loaiSanPhamLoaiThuocTinhRepository = loaiSanPhamLoaiThuocTinhRepository;
    }

    /**
     * POST  /loai-san-pham-loai-thuoc-tinhs : Create a new loaiSanPhamLoaiThuocTinh.
     *
     * @param loaiSanPhamLoaiThuocTinh the loaiSanPhamLoaiThuocTinh to create
     * @return the ResponseEntity with status 201 (Created) and with body the new loaiSanPhamLoaiThuocTinh, or with status 400 (Bad Request) if the loaiSanPhamLoaiThuocTinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/loai-san-pham-loai-thuoc-tinhs")
    @Timed
    public ResponseEntity<LoaiSanPhamLoaiThuocTinh> createLoaiSanPhamLoaiThuocTinh(@RequestBody LoaiSanPhamLoaiThuocTinh loaiSanPhamLoaiThuocTinh) throws URISyntaxException {
        log.debug("REST request to save LoaiSanPhamLoaiThuocTinh : {}", loaiSanPhamLoaiThuocTinh);
        if (loaiSanPhamLoaiThuocTinh.getId() != null) {
            throw new BadRequestAlertException("A new loaiSanPhamLoaiThuocTinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoaiSanPhamLoaiThuocTinh result = loaiSanPhamLoaiThuocTinhRepository.save(loaiSanPhamLoaiThuocTinh);
        return ResponseEntity.created(new URI("/api/loai-san-pham-loai-thuoc-tinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /loai-san-pham-loai-thuoc-tinhs : Updates an existing loaiSanPhamLoaiThuocTinh.
     *
     * @param loaiSanPhamLoaiThuocTinh the loaiSanPhamLoaiThuocTinh to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated loaiSanPhamLoaiThuocTinh,
     * or with status 400 (Bad Request) if the loaiSanPhamLoaiThuocTinh is not valid,
     * or with status 500 (Internal Server Error) if the loaiSanPhamLoaiThuocTinh couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/loai-san-pham-loai-thuoc-tinhs")
    @Timed
    public ResponseEntity<LoaiSanPhamLoaiThuocTinh> updateLoaiSanPhamLoaiThuocTinh(@RequestBody LoaiSanPhamLoaiThuocTinh loaiSanPhamLoaiThuocTinh) throws URISyntaxException {
        log.debug("REST request to update LoaiSanPhamLoaiThuocTinh : {}", loaiSanPhamLoaiThuocTinh);
        if (loaiSanPhamLoaiThuocTinh.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoaiSanPhamLoaiThuocTinh result = loaiSanPhamLoaiThuocTinhRepository.save(loaiSanPhamLoaiThuocTinh);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, loaiSanPhamLoaiThuocTinh.getId().toString()))
            .body(result);
    }

    /**
     * GET  /loai-san-pham-loai-thuoc-tinhs : get all the loaiSanPhamLoaiThuocTinhs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of loaiSanPhamLoaiThuocTinhs in body
     */
    @GetMapping("/loai-san-pham-loai-thuoc-tinhs")
    @Timed
    public List<LoaiSanPhamLoaiThuocTinh> getAllLoaiSanPhamLoaiThuocTinhs() {
        log.debug("REST request to get all LoaiSanPhamLoaiThuocTinhs");
        return loaiSanPhamLoaiThuocTinhRepository.findAll();
    }

    /**
     * GET  /loai-san-pham-loai-thuoc-tinhs/:id : get the "id" loaiSanPhamLoaiThuocTinh.
     *
     * @param id the id of the loaiSanPhamLoaiThuocTinh to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the loaiSanPhamLoaiThuocTinh, or with status 404 (Not Found)
     */
    @GetMapping("/loai-san-pham-loai-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<LoaiSanPhamLoaiThuocTinh> getLoaiSanPhamLoaiThuocTinh(@PathVariable Long id) {
        log.debug("REST request to get LoaiSanPhamLoaiThuocTinh : {}", id);
        Optional<LoaiSanPhamLoaiThuocTinh> loaiSanPhamLoaiThuocTinh = loaiSanPhamLoaiThuocTinhRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(loaiSanPhamLoaiThuocTinh);
    }

    /**
     * DELETE  /loai-san-pham-loai-thuoc-tinhs/:id : delete the "id" loaiSanPhamLoaiThuocTinh.
     *
     * @param id the id of the loaiSanPhamLoaiThuocTinh to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/loai-san-pham-loai-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<Void> deleteLoaiSanPhamLoaiThuocTinh(@PathVariable Long id) {
        log.debug("REST request to delete LoaiSanPhamLoaiThuocTinh : {}", id);

        loaiSanPhamLoaiThuocTinhRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
