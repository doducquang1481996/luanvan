package vn.com.thesis.product.repository;

import vn.com.thesis.product.domain.LoaiSanPhamLoaiThuocTinh;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the LoaiSanPhamLoaiThuocTinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiSanPhamLoaiThuocTinhRepository extends JpaRepository<LoaiSanPhamLoaiThuocTinh, Long> {

}
