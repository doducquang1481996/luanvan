package vn.com.thesis.product.repository;

import vn.com.thesis.product.domain.SanPhamLoaiThuocTinh;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SanPhamLoaiThuocTinh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SanPhamLoaiThuocTinhRepository extends JpaRepository<SanPhamLoaiThuocTinh, Long> {

}
