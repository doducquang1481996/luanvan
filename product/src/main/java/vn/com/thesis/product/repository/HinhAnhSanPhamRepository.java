package vn.com.thesis.product.repository;

import vn.com.thesis.product.domain.HinhAnhSanPham;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HinhAnhSanPham entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HinhAnhSanPhamRepository extends JpaRepository<HinhAnhSanPham, Long> {

}
