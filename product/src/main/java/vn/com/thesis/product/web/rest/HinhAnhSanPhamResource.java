package vn.com.thesis.product.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.product.domain.HinhAnhSanPham;
import vn.com.thesis.product.repository.HinhAnhSanPhamRepository;
import vn.com.thesis.product.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.product.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing HinhAnhSanPham.
 */
@RestController
@RequestMapping("/api")
public class HinhAnhSanPhamResource {

    private final Logger log = LoggerFactory.getLogger(HinhAnhSanPhamResource.class);

    private static final String ENTITY_NAME = "hinhAnhSanPham";

    private final HinhAnhSanPhamRepository hinhAnhSanPhamRepository;

    public HinhAnhSanPhamResource(HinhAnhSanPhamRepository hinhAnhSanPhamRepository) {
        this.hinhAnhSanPhamRepository = hinhAnhSanPhamRepository;
    }

    /**
     * POST  /hinh-anh-san-phams : Create a new hinhAnhSanPham.
     *
     * @param hinhAnhSanPham the hinhAnhSanPham to create
     * @return the ResponseEntity with status 201 (Created) and with body the new hinhAnhSanPham, or with status 400 (Bad Request) if the hinhAnhSanPham has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/hinh-anh-san-phams")
    @Timed
    public ResponseEntity<HinhAnhSanPham> createHinhAnhSanPham(@RequestBody HinhAnhSanPham hinhAnhSanPham) throws URISyntaxException {
        log.debug("REST request to save HinhAnhSanPham : {}", hinhAnhSanPham);
        if (hinhAnhSanPham.getId() != null) {
            throw new BadRequestAlertException("A new hinhAnhSanPham cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HinhAnhSanPham result = hinhAnhSanPhamRepository.save(hinhAnhSanPham);
        return ResponseEntity.created(new URI("/api/hinh-anh-san-phams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /hinh-anh-san-phams : Updates an existing hinhAnhSanPham.
     *
     * @param hinhAnhSanPham the hinhAnhSanPham to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated hinhAnhSanPham,
     * or with status 400 (Bad Request) if the hinhAnhSanPham is not valid,
     * or with status 500 (Internal Server Error) if the hinhAnhSanPham couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/hinh-anh-san-phams")
    @Timed
    public ResponseEntity<HinhAnhSanPham> updateHinhAnhSanPham(@RequestBody HinhAnhSanPham hinhAnhSanPham) throws URISyntaxException {
        log.debug("REST request to update HinhAnhSanPham : {}", hinhAnhSanPham);
        if (hinhAnhSanPham.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HinhAnhSanPham result = hinhAnhSanPhamRepository.save(hinhAnhSanPham);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, hinhAnhSanPham.getId().toString()))
            .body(result);
    }

    /**
     * GET  /hinh-anh-san-phams : get all the hinhAnhSanPhams.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of hinhAnhSanPhams in body
     */
    @GetMapping("/hinh-anh-san-phams")
    @Timed
    public List<HinhAnhSanPham> getAllHinhAnhSanPhams() {
        log.debug("REST request to get all HinhAnhSanPhams");
        return hinhAnhSanPhamRepository.findAll();
    }

    /**
     * GET  /hinh-anh-san-phams/:id : get the "id" hinhAnhSanPham.
     *
     * @param id the id of the hinhAnhSanPham to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the hinhAnhSanPham, or with status 404 (Not Found)
     */
    @GetMapping("/hinh-anh-san-phams/{id}")
    @Timed
    public ResponseEntity<HinhAnhSanPham> getHinhAnhSanPham(@PathVariable Long id) {
        log.debug("REST request to get HinhAnhSanPham : {}", id);
        Optional<HinhAnhSanPham> hinhAnhSanPham = hinhAnhSanPhamRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(hinhAnhSanPham);
    }

    /**
     * DELETE  /hinh-anh-san-phams/:id : delete the "id" hinhAnhSanPham.
     *
     * @param id the id of the hinhAnhSanPham to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/hinh-anh-san-phams/{id}")
    @Timed
    public ResponseEntity<Void> deleteHinhAnhSanPham(@PathVariable Long id) {
        log.debug("REST request to delete HinhAnhSanPham : {}", id);

        hinhAnhSanPhamRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
