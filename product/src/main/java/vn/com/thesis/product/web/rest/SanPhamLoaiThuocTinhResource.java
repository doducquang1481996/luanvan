package vn.com.thesis.product.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.product.domain.SanPhamLoaiThuocTinh;
import vn.com.thesis.product.repository.SanPhamLoaiThuocTinhRepository;
import vn.com.thesis.product.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.product.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SanPhamLoaiThuocTinh.
 */
@RestController
@RequestMapping("/api")
public class SanPhamLoaiThuocTinhResource {

    private final Logger log = LoggerFactory.getLogger(SanPhamLoaiThuocTinhResource.class);

    private static final String ENTITY_NAME = "sanPhamLoaiThuocTinh";

    private final SanPhamLoaiThuocTinhRepository sanPhamLoaiThuocTinhRepository;

    public SanPhamLoaiThuocTinhResource(SanPhamLoaiThuocTinhRepository sanPhamLoaiThuocTinhRepository) {
        this.sanPhamLoaiThuocTinhRepository = sanPhamLoaiThuocTinhRepository;
    }

    /**
     * POST  /san-pham-loai-thuoc-tinhs : Create a new sanPhamLoaiThuocTinh.
     *
     * @param sanPhamLoaiThuocTinh the sanPhamLoaiThuocTinh to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sanPhamLoaiThuocTinh, or with status 400 (Bad Request) if the sanPhamLoaiThuocTinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/san-pham-loai-thuoc-tinhs")
    @Timed
    public ResponseEntity<SanPhamLoaiThuocTinh> createSanPhamLoaiThuocTinh(@RequestBody SanPhamLoaiThuocTinh sanPhamLoaiThuocTinh) throws URISyntaxException {
        log.debug("REST request to save SanPhamLoaiThuocTinh : {}", sanPhamLoaiThuocTinh);
        if (sanPhamLoaiThuocTinh.getId() != null) {
            throw new BadRequestAlertException("A new sanPhamLoaiThuocTinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SanPhamLoaiThuocTinh result = sanPhamLoaiThuocTinhRepository.save(sanPhamLoaiThuocTinh);
        return ResponseEntity.created(new URI("/api/san-pham-loai-thuoc-tinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /san-pham-loai-thuoc-tinhs : Updates an existing sanPhamLoaiThuocTinh.
     *
     * @param sanPhamLoaiThuocTinh the sanPhamLoaiThuocTinh to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sanPhamLoaiThuocTinh,
     * or with status 400 (Bad Request) if the sanPhamLoaiThuocTinh is not valid,
     * or with status 500 (Internal Server Error) if the sanPhamLoaiThuocTinh couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/san-pham-loai-thuoc-tinhs")
    @Timed
    public ResponseEntity<SanPhamLoaiThuocTinh> updateSanPhamLoaiThuocTinh(@RequestBody SanPhamLoaiThuocTinh sanPhamLoaiThuocTinh) throws URISyntaxException {
        log.debug("REST request to update SanPhamLoaiThuocTinh : {}", sanPhamLoaiThuocTinh);
        if (sanPhamLoaiThuocTinh.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SanPhamLoaiThuocTinh result = sanPhamLoaiThuocTinhRepository.save(sanPhamLoaiThuocTinh);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sanPhamLoaiThuocTinh.getId().toString()))
            .body(result);
    }

    /**
     * GET  /san-pham-loai-thuoc-tinhs : get all the sanPhamLoaiThuocTinhs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sanPhamLoaiThuocTinhs in body
     */
    @GetMapping("/san-pham-loai-thuoc-tinhs")
    @Timed
    public List<SanPhamLoaiThuocTinh> getAllSanPhamLoaiThuocTinhs() {
        log.debug("REST request to get all SanPhamLoaiThuocTinhs");
        return sanPhamLoaiThuocTinhRepository.findAll();
    }

    /**
     * GET  /san-pham-loai-thuoc-tinhs/:id : get the "id" sanPhamLoaiThuocTinh.
     *
     * @param id the id of the sanPhamLoaiThuocTinh to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sanPhamLoaiThuocTinh, or with status 404 (Not Found)
     */
    @GetMapping("/san-pham-loai-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<SanPhamLoaiThuocTinh> getSanPhamLoaiThuocTinh(@PathVariable Long id) {
        log.debug("REST request to get SanPhamLoaiThuocTinh : {}", id);
        Optional<SanPhamLoaiThuocTinh> sanPhamLoaiThuocTinh = sanPhamLoaiThuocTinhRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(sanPhamLoaiThuocTinh);
    }

    /**
     * DELETE  /san-pham-loai-thuoc-tinhs/:id : delete the "id" sanPhamLoaiThuocTinh.
     *
     * @param id the id of the sanPhamLoaiThuocTinh to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/san-pham-loai-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<Void> deleteSanPhamLoaiThuocTinh(@PathVariable Long id) {
        log.debug("REST request to delete SanPhamLoaiThuocTinh : {}", id);

        sanPhamLoaiThuocTinhRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
