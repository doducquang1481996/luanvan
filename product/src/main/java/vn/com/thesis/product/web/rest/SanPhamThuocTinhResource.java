package vn.com.thesis.product.web.rest;

import com.codahale.metrics.annotation.Timed;
import vn.com.thesis.product.domain.SanPhamThuocTinh;
import vn.com.thesis.product.repository.SanPhamThuocTinhRepository;
import vn.com.thesis.product.web.rest.errors.BadRequestAlertException;
import vn.com.thesis.product.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SanPhamThuocTinh.
 */
@RestController
@RequestMapping("/api")
public class SanPhamThuocTinhResource {

    private final Logger log = LoggerFactory.getLogger(SanPhamThuocTinhResource.class);

    private static final String ENTITY_NAME = "sanPhamThuocTinh";

    private final SanPhamThuocTinhRepository sanPhamThuocTinhRepository;

    public SanPhamThuocTinhResource(SanPhamThuocTinhRepository sanPhamThuocTinhRepository) {
        this.sanPhamThuocTinhRepository = sanPhamThuocTinhRepository;
    }

    /**
     * POST  /san-pham-thuoc-tinhs : Create a new sanPhamThuocTinh.
     *
     * @param sanPhamThuocTinh the sanPhamThuocTinh to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sanPhamThuocTinh, or with status 400 (Bad Request) if the sanPhamThuocTinh has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/san-pham-thuoc-tinhs")
    @Timed
    public ResponseEntity<SanPhamThuocTinh> createSanPhamThuocTinh(@RequestBody SanPhamThuocTinh sanPhamThuocTinh) throws URISyntaxException {
        log.debug("REST request to save SanPhamThuocTinh : {}", sanPhamThuocTinh);
        if (sanPhamThuocTinh.getId() != null) {
            throw new BadRequestAlertException("A new sanPhamThuocTinh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SanPhamThuocTinh result = sanPhamThuocTinhRepository.save(sanPhamThuocTinh);
        return ResponseEntity.created(new URI("/api/san-pham-thuoc-tinhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /san-pham-thuoc-tinhs : Updates an existing sanPhamThuocTinh.
     *
     * @param sanPhamThuocTinh the sanPhamThuocTinh to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sanPhamThuocTinh,
     * or with status 400 (Bad Request) if the sanPhamThuocTinh is not valid,
     * or with status 500 (Internal Server Error) if the sanPhamThuocTinh couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/san-pham-thuoc-tinhs")
    @Timed
    public ResponseEntity<SanPhamThuocTinh> updateSanPhamThuocTinh(@RequestBody SanPhamThuocTinh sanPhamThuocTinh) throws URISyntaxException {
        log.debug("REST request to update SanPhamThuocTinh : {}", sanPhamThuocTinh);
        if (sanPhamThuocTinh.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SanPhamThuocTinh result = sanPhamThuocTinhRepository.save(sanPhamThuocTinh);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sanPhamThuocTinh.getId().toString()))
            .body(result);
    }

    /**
     * GET  /san-pham-thuoc-tinhs : get all the sanPhamThuocTinhs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sanPhamThuocTinhs in body
     */
    @GetMapping("/san-pham-thuoc-tinhs")
    @Timed
    public List<SanPhamThuocTinh> getAllSanPhamThuocTinhs() {
        log.debug("REST request to get all SanPhamThuocTinhs");
        return sanPhamThuocTinhRepository.findAll();
    }

    /**
     * GET  /san-pham-thuoc-tinhs/:id : get the "id" sanPhamThuocTinh.
     *
     * @param id the id of the sanPhamThuocTinh to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sanPhamThuocTinh, or with status 404 (Not Found)
     */
    @GetMapping("/san-pham-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<SanPhamThuocTinh> getSanPhamThuocTinh(@PathVariable Long id) {
        log.debug("REST request to get SanPhamThuocTinh : {}", id);
        Optional<SanPhamThuocTinh> sanPhamThuocTinh = sanPhamThuocTinhRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(sanPhamThuocTinh);
    }

    /**
     * DELETE  /san-pham-thuoc-tinhs/:id : delete the "id" sanPhamThuocTinh.
     *
     * @param id the id of the sanPhamThuocTinh to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/san-pham-thuoc-tinhs/{id}")
    @Timed
    public ResponseEntity<Void> deleteSanPhamThuocTinh(@PathVariable Long id) {
        log.debug("REST request to delete SanPhamThuocTinh : {}", id);

        sanPhamThuocTinhRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
